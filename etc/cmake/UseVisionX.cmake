# This file contains macros for projects depending on VisionX

find_package(OpenMP QUIET)
if(OPENMP_FOUND)
    set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${OpenMP_C_FLAGS}")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OpenMP_CXX_FLAGS}")
    set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} ${OpenMP_EXE_LINKER_FLAGS}")
endif()

find_package(IVT QUIET)
find_package(IVTRecognition QUIET)
find_package(VTK QUIET)
find_package(PCL 1.8 QUIET)
find_package(OpenCV QUIET)

find_package(MemoryX QUIET)

if(PCL_FOUND)
    remove_empty_elements(PCL_DEFINITIONS)
endif()
