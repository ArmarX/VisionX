import logging
import datetime
import sys
import select

from armarx.robots import A6
from armarx import FramedPositionBase

from .db_model import Person

robot = A6()
logger = logging.getLogger(__name__)


# ToDo: Update to new data types / move data types to armarx-dev.


def report_to_memory(p: Person):
    import numpy as np
    from armarx_memory.client import MemoryNameSystem
    from armarx_memory.segments.PersonInstance import PersonInstanceWriter
    from armarx_memory.segments.Person import PersonWriter

    mns = MemoryNameSystem.get_mns(logger=logger)

    person_writer = PersonWriter.from_mns(mns)
    person_id = person_writer.core_segment_id.with_provider_segment_name('FaceRecognition')
    person_id = person_id.with_entity_name(str(p))
    family_name = p.family_name or ''
    person_writer.commit(entity_id=person_id, given_name=p.given_name, family_name=family_name, roles=p.roles.split(','))

    writer = PersonInstanceWriter.from_mns(mns)
    entity_id = writer.core_segment_id.with_provider_segment_name('FaceRecognition')
    entity_id = entity_id.with_entity_name(str(p))
    pose = np.identity(4)
    pose[0, 3] = p.x
    pose[1, 3] = p.y
    pose[2, 3] = p.z
    pose = pose.astype(np.float32)
    writer.commit(entity_id=entity_id, personID=person_id, pose=pose)


def on_detected_faces(detected_faces, time):
    logger.info('on_detect (%r)', detected_faces)
    now = datetime.datetime.now()
    for f in detected_faces:
        if f.person.given_name == 'Unknown':
            identify_face(f)

            report_to_memory(f.person)
        else:

            report_to_memory(f.person)
            check_birthday(f.person)
            greet_persons(f.person)
            # look_at_person(f.person)
            f.person.update(last_seen=now)


def look_at_person(p: Person):
    f = FramedPositionBase(p.x, p.y, p.z, frame='Global')
    robot.gaze.fixate(f, 1000)


def check_birthday(p: Person):
    if not p.birthday:
        return
    now = datetime.datetime.now()
    birthday_in_current_year = p.birthday(year=now.date().year)
    if birthday_in_current_year != now.date():
        return
    if p.birthday_year_congratulated >= now.date().year:
        return
    robot.say(f'Happy birthday dear {p.given_name}.')
    p.birthday_year_congratulated = now.date().year
    p.save()


def greet_persons(p: Person):
    if not p.given_name:
        logger.error('no name set for person %s', p)
        return
    if not p.last_greeted:
        robot.say(f'Hi {p.given_name}!')
        p.last_greeted = datetime.datetime.now()
        p.save()
    elif (datetime.datetime.now() - p.last_greeted).total_seconds() > 120:
        robot.say(f'Hi {p.given_name}!')
        p.last_greeted = datetime.datetime.now()
        p.save()


def identify_face(f):
    robot.say("What is your name?")

    name = label_face()
    if not name:
        return
    person = Person.select().where(Person.given_name==name).limit(1)
    if not person:
        person = Person.create(given_name=name)
    f.person = person
    f.save()


def label_face() -> str:
    robot.say("What is your name?")
    i, o, e = select.select([sys.stdin], [], [], 5)
    if i:
        name = sys.stdin.readline().strip()
        return name
    return None
