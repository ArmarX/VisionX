#!/usr/bin/env python3

import logging
import typing as ty

import numpy as np

from armarx.parser import ArmarXArgumentParser as ArgumentParser
from armarx_memory.client import MemoryNameSystem

from armarx_face_recognition.face_detection import FaceDetection, Person


logger = logging.getLogger(__name__)


def main():
    parser = ArgumentParser("Example Image Provider")
    parser.add_argument("-i", "--input-provider", default="OpenNIPointCloudProvider")
    parser.add_argument("--depth", action="store_true", help="Use a depth image.")
    parser.add_argument("--enable-result-image", action="store_true", help="Enable drawing and providing a result image.")
    parser.add_argument("--create-tables", action="store_true", help="Initializes the database.")
    parser.add_argument("--drop-tables", action="store_true", help="Drop tables if they already exist.")
    parser.add_argument("--images", default="/tmp/*.jpg", help="Known faces.")
    args = parser.parse_args()

    np.set_printoptions(suppress=True, precision=2)

    mns = MemoryNameSystem.wait_for_mns()

    persons: ty.List[Person] = FaceDetection.query_human_profiles(mns=mns, logger=logger)
    if persons:
        logger.debug(f"Found {len(persons)} persons with images: {persons}")
    else:
        logger.error("No persons with images found. Aborting.")
        return

    logger.debug("Starting image processor")
    image_processor = FaceDetection(
        args.input_provider,
        persons=persons,
        has_depth=args.depth,
        enable_result_image=args.enable_result_image,
    )
    image_processor.on_connect()
    image_processor.update_calibration()


if __name__ == "__main__":
    main()
