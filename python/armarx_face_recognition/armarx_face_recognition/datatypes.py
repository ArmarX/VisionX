import dataclasses as dc
import numpy as np
import typing as ty

from armarx_memory import client as mem
from armarx_memory.aron.aron_dataclass import AronDataclass
from armarx_memory.aron.conversion import to_aron


@dc.dataclass
class Person(AronDataclass):

    face_encodings: ty.List[np.ndarray] = dc.field(default_factory=list)

    profile_id: ty.Optional[mem.MemoryID] = None
    profile: ty.Dict = dc.field(default_factory=dict)

    def name(self) -> str:
        try:
            return self.profile["spokenNames"][0]
        except KeyError:
            if self.profile_id is not None:
                return self.profile_id.entity_name
            else:
                return "<unknown>"


@dc.dataclass
class FaceRecognition(AronDataclass):

    position_3d: np.ndarray
    position_2d: np.ndarray
    extents_2d: np.ndarray

    profile_id: mem.MemoryID

    last_seen_usec: int = -1
    last_greeted_usec: int = -1

    def to_aron_ice(self):
        return to_aron({
            "position3D": self.position_3d.astype(np.float32).reshape(3, 1),
            "position2D": self.position_2d.astype(np.int32),
            "extents2D": self.extents_2d.astype(np.int32),
            "profileID": self.profile_id.to_aron() if self.profile_id else mem.MemoryID().to_aron(),
        })
