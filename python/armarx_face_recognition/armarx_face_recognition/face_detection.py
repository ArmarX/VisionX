#!/usr/bin/env python3

import os
import logging
import math
import typing as ty

import Ice
import cv2
import numpy as np
import face_recognition

from armarx import cmake_helper
from armarx import arviz as viz
from armarx_memory import client as mem

from armarx.pose_helper import convert_position_to_global
from armarx import FramedPositionBase

from visionx.image_processor import ImageProcessor
from visionx import StereoCalibrationInterfacePrx

from armarx_face_recognition.datatypes import Person, FaceRecognition


class FaceDetection(ImageProcessor):

    def __init__(
            self,
            provider_name: str,
            persons: ty.List[Person],
            num_result_images=None,
            enable_result_image=True,
            has_depth=False,
            face_recognition_segment_id=mem.MemoryID("Human", "FaceRecognition"),
    ):
        super().__init__(provider_name=provider_name, num_result_images=num_result_images)

        self.persons = persons
        self.unknown_person = Person()

        self.has_depth = has_depth
        self.calibration = {
            "fx": 500, "fy": 500,
            "width": 640, "height": 480,
            "horizontal_fov": 0.8, "vertical_fov": 0.9
        }

        self.enable_result_image = enable_result_image

        self.agent_name = "Armar6"
        self.camera_frame_name = "AzureKinectCamera"

        self.logger = logging.getLogger(__name__)
        self.arviz = viz.Client(__name__)

        self.mns = mem.MemoryNameSystem.wait_for_mns()
        self.face_recognition_segment_id = face_recognition_segment_id
        self.face_reco_writer = self.mns.wait_for_writer(face_recognition_segment_id)


    @classmethod
    def query_human_profiles(
            cls,
            mns: mem.MemoryNameSystem,
            logger=None
    ):
        human_reader = mns.wait_for_reader(mem.MemoryID("Human"))
        result = human_reader.query_core_segment("Profile")

        def gather_persons(id: mem.MemoryID, profile) -> ty.Optional[Person]:
            if logger:
                logger.info(f"Gathering face images of {id} ...")

            face_encodings = []
            for face_image_path in profile["faceImagePaths"]:
                package: str = face_image_path["package"]
                rel_path: str = face_image_path["path"]

                [data_path] = cmake_helper.get_data_path(package)
                abs_path = os.path.join(data_path, package, rel_path)
                if os.path.isfile(abs_path):
                    name = profile["names"]["spoken"][0] or id.entity_name
                    if logger is not None:
                        logger.info("Loading person '%s'.", name)
                        logger.debug("Creating face encoding for person '%s'.", name)
                    image = face_recognition.load_image_file(abs_path)
                    encoding = face_recognition.face_encodings(image)[0]

                    face_encodings.append(encoding)

            if face_encodings:
                return Person(face_encodings=face_encodings, profile_id=id, profile=profile)
            else:
                return None

        persons: ty.List[Person] = human_reader.for_each_instance_data(gather_persons, result, discard_none=True)
        return persons

    def update_calibration(self):
        image_format = self.image_source.getImageFormat()

        width = image_format.dimension.width
        height = image_format.dimension.height

        proxy = StereoCalibrationInterfacePrx.get_proxy(self.provider_name)
        if proxy is not None:
            stereo_calibration = proxy.getStereoCalibration()
            fx = stereo_calibration.calibrationRight.cameraParam.focalLength[0]
            fy = stereo_calibration.calibrationRight.cameraParam.focalLength[1]
            calibration = {"fx": fx, "fy": fy, "width": width, "height": height,
                           "vertical_fov": 2.0 * math.atan(height / (2.0 * fy)),
                           "horizontal_fov": 2.0 * math.atan(width / (2.0 * fx))}

            self.calibration.update(calibration)

    def process_images(self, images: np.ndarray, info):
        image_rgb = images[0]

        face_locations = face_recognition.face_locations(image_rgb)
        face_encodings = face_recognition.face_encodings(image_rgb, face_locations)
        # self.logger.info("Found %s faces.", len(face_locations))

        detected_persons = self.detect_persons(face_encodings)
        self.logger.info(f"Found {len(detected_persons)} persons "
                         f"({', '.join(p.name() for p in detected_persons)}).")

        face_recognitions = self.make_face_recognitions(images, face_locations, detected_persons)

        self.commit_recognitions(face_recognitions, info.timeProvided)

        if self.enable_result_image:
            self.draw_result_image(image_rgb, face_locations, detected_persons)
            self.result_image_provider.update_image(images, info.timeProvided)

        # It seems we must return images to avoid errors ...
        return images, info

    def detect_persons(self, face_encodings) -> ty.List[Person]:
        face_encoding_index: ty.Dict[int, Person] = {}
        known_face_encodings: ty.List[np.ndarray] = []
        for person in self.persons:
            for encoding in person.face_encodings:
                face_encoding_index[len(known_face_encodings)] = person
                known_face_encodings.append(encoding)

        detected_persons = []
        for face_encoding in face_encodings:
            matches = face_recognition.compare_faces(face_encoding, known_face_encodings)
            face_distances = face_recognition.face_distance(face_encoding, known_face_encodings)
            best_match_index = np.argmin(face_distances)

            if matches[best_match_index] and face_distances[best_match_index] < 0.5:
                person = face_encoding_index[best_match_index]
            else:
                person = self.unknown_person
            detected_persons.append(person)

        return detected_persons

    def make_face_recognitions(
            self,
            images,
            face_locations,
            detected_persons: ty.List[Person],
    ) -> ty.List[FaceRecognition]:
        calibration = self.calibration
        scale_x = math.tan(calibration["horizontal_fov"] / 2.0) * 2.0
        scale_y = math.tan(calibration["vertical_fov"] / 2.0) * 2.0

        face_recognitions = []
        for (top, right, bottom, left), person in zip(face_locations, detected_persons):
            i = int(left + 0.5 * (right - left))
            j = int(top + 0.5 * (bottom - top))
            if self.has_depth:
                z = float(images[1][j][i][0] + (images[1][j][i][1] << 8) + (images[1][j][i][2] << 16))
            else:
                z = 1000.0
            x = -1.0 * (i - calibration["width"] / 2.0) / calibration["width"] * z * scale_x
            y = (calibration["height"] / 2.0 - j) / calibration["height"] * z * scale_y

            f = FramedPositionBase(-x, -y, z, self.camera_frame_name, self.agent_name)
            try:
                global_transform_mat = convert_position_to_global(f)
            except AttributeError:
                # AttributeError: 'NoneType' object has no attribute 'getSynchronizedRobot'
                position_3d = np.array([-x, -y, z])
            except Ice.UnknownUserException:
                position_3d = np.array([-x, -y, z])
            else:
                position_3d = global_transform_mat[:3, 3]

            position_2d = 0.5 * np.array([left + right, bottom + top])
            extents_2d = np.array([right - left, top - bottom])

            recognition = FaceRecognition(
                position_3d=position_3d,
                position_2d=position_2d,
                extents_2d=extents_2d,
                profile_id=person.profile_id
            )
            face_recognitions.append(recognition)

            self.visu_pose(person.name(), position_3d)

        return face_recognitions

    def commit_recognitions(
            self,
            recognitions: ty.List[FaceRecognition],
            time_usec: int
    ):
        commit = mem.Commit()
        for reco in recognitions:
            update = commit.add()
            entity_name = reco.profile_id.entity_name if reco.profile_id else "<unknown>"
            update.entity_id = (self.face_recognition_segment_id
                                .with_provider_segment_name(__name__)
                                .with_entity_name(entity_name))
            update.time_created_usec = time_usec
            update.instances_data = [reco.to_aron_ice()]

        self.face_reco_writer.commit(commit)

    def visu_pose(
            self,
            person_name,
            position: np.ndarray,
    ):
        with self.arviz.begin_stage(commit_on_exit=True) as stage:
            layer = stage.layer("Location")
            layer.add(viz.Sphere(person_name, position=position, radius=100, color=(255, 0, 255)))

    @classmethod
    def draw_result_image(
            cls,
            image_rgb,
            face_locations,
            detected_persons,
    ):
        for (top, right, bottom, left), person in zip(face_locations, detected_persons):
            cv2.rectangle(image_rgb, (left, top), (right, bottom), (0, 0, 255), 2)
            cv2.rectangle(image_rgb, (left, bottom - 35), (right, bottom), (0, 0, 255), cv2.FILLED)

            cv2.putText(image_rgb, person.name(), (left + 6, bottom - 6),
                        cv2.FONT_HERSHEY_DUPLEX, 1.0, (255, 255, 255), 1)
