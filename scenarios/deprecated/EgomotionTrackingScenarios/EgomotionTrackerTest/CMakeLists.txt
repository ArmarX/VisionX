set(SCENARIO_COMPONENTS #KinematicUnitSimulation
                        KinematicUnitObserver
                        PlatformUnitObserver
                        ConditionHandler
                        SystemObserver
                        RobotStateComponent
                        RobotControl
                        CommonStorage
                        WorkingMemory
                        LongtermMemory
                        PriorKnowledge
                        #ImageSequenceProvider
                        EgomotionTrackerApp)
armarx_scenario("EgomotionTrackerTest" "${SCENARIO_COMPONENTS}")

