/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX
 * @author     Peter Kaiser (peter dot kaiser at kit dot edu)
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/services/tasks/RunningTask.h>

#include <RobotAPI/interface/visualization/DebugDrawerInterface.h>
#include <RobotAPI/libraries/core/FramedPose.h>

#include <VisionX/interface/components/PointCloudSegmenter.h>
#include <VisionX/interface/components/AffordancePipelineVisualization.h>
#include <VisionX/libraries/AffordanceKitArmarX/SceneArmarX.h>

#include <AffordanceKit/primitives/Plane.h>
#include <AffordanceKit/primitives/Cylinder.h>
#include <AffordanceKit/primitives/Sphere.h>
#include <AffordanceKit/primitives/Box.h>

#include <MemoryX/interface/components/WorkingMemoryInterface.h>
#include <MemoryX/core/memory/SegmentedMemory.h>
#include <MemoryX/libraries/memorytypes/segment/AffordanceSegment.h>
#include <MemoryX/libraries/memorytypes/segment/EnvironmentalPrimitiveSegment.h>

#include <Eigen/Eigen>
#include <IceUtil/IceUtil.h>

#include <mutex>

namespace armarx
{
    /**
     * @class AffordanceExtractionPropertyDefinitions
     * @brief
     */
    class AffordancePipelineVisualizationPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        AffordancePipelineVisualizationPropertyDefinitions(std::string prefix):
            ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("WorkingMemoryName", "WorkingMemory", "Name of WorkingMemory component");
            defineOptionalProperty<std::string>("DebugDrawerTopicName", "DebugDrawerUpdates", "The name of the debug drawer topic");
            defineOptionalProperty<std::string>("DebugDrawerSelectionsTopicName", "DebugDrawerSelections", "The name of the debug drawer topic");
            defineOptionalProperty<std::string>("VisualizationUpdateTopicName", "AffordanceVisualizationTopic", "Topic under which visualization updates are reported");
            defineOptionalProperty<bool>("EnableDebugDrawerSelections", true, "Allow users to select primitives via debug drawer");

            defineOptionalProperty<bool>("EnablePrimitiveVisualization", true, "Initially enable primitive visualization");
            defineOptionalProperty<bool>("EnableAffordanceVisualization", true, "Initially enable affordance visualization");
            defineOptionalProperty<bool>("EnableDebugVisualization", false, "Initially enable debug visualization");

            defineOptionalProperty<std::string>("VisualizeAffordanceBeliefFunction", "", "The affordance belief function that should initially be visualized");
            defineOptionalProperty<float>("MinExpectedProbability", 0, "Visualize only samplings with expected probability greater than X");

            defineOptionalProperty<bool>("NoAffordanceExtractionConfigured", false, "In this case visualization will start as soon as primitives are computed");

            defineOptionalProperty<std::string>("FileName", "", "Load affordances from a file name and exit.");
        }
    };

    /**
     * @class AffordanceExtraction
     * @ingroup RobotSkillTemplates-Components
     * @brief A brief description
     *
     * Detailed Description
     */
    class AffordancePipelineVisualization :
        virtual public Component,
        virtual public AffordancePipelineVisualizationInterface
    {
    public:
        AffordancePipelineVisualization();

        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override
        {
            return "AffordancePipelineVisualization";
        }

        void reportNewPointCloudSegmentation(const Ice::Current& c = Ice::emptyCurrent) override;
        void reportNewAffordances(const Ice::Current& c = Ice::emptyCurrent) override;

        void selectPrimitive(const std::string& id, const Ice::Current& c = Ice::emptyCurrent) override;
        void deselectPrimitive(const std::string& id, const Ice::Current& c = Ice::emptyCurrent) override;
        void deselectAllPrimitives(const Ice::Current& c = Ice::emptyCurrent) override;

        void enableVisualization(bool enablePrimitives, bool enableAffordances, bool enableDebugInformation, const Ice::Current& c = Ice::emptyCurrent) override;
        void configureAffordanceVisualization(AffordanceVisualizationStyle style, const std::vector<std::string>& primitives, const std::vector<std::string>& affordances, float minExpectedProbability, const Ice::Current& c = Ice::emptyCurrent) override;
        void triggerVisualization(const Ice::Current& c = Ice::emptyCurrent) override;

        void exportVisualizationAsIV(const std::string& filename, const Ice::Current& c = Ice::emptyCurrent) override;
        void exportVisualizationAsJSON(const std::string& filename, const Ice::Current& c = Ice::emptyCurrent) override;

        void clearVisualization(const Ice::Current& c = Ice::emptyCurrent) override;

        // DebugDrawerListener interface
        void reportSelectionChanged(const DebugDrawerSelectionList& selectedObjects, const ::Ice::Current& = Ice::emptyCurrent) override;

    protected:
        /**
         * @see armarx::ManagedIceObject::onInitComponent()
         */
        void onInitComponent() override;

        /**
         * @see armarx::ManagedIceObject::onConnectComponent()
         */
        void onConnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onDisconnectComponent()
         */
        void onDisconnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onExitComponent()
         */
        void onExitComponent() override;

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        PropertyDefinitionsPtr createPropertyDefinitions() override;

        void visualizeScene(const AffordanceKit::ScenePtr& scene, bool updatePrimitives = true, bool updateAffordances = true, bool updateDebugInfo = true);
        void visualizePrimitives(const AffordanceKit::PrimitiveSetPtr& primitives);
        void visualizePrimitive(const AffordanceKit::PrimitivePtr& primitive);
        void visualizeAffordances(const AffordanceKit::ScenePtr& scene);
        void visualizeAffordance(const AffordanceKit::AffordancePtr& affordance, const AffordanceKit::PrimitivePtr& primitve, unsigned int index);
        void visualizeAffordanceLabels(const AffordanceKit::PrimitivePtr& primitive, const std::vector<std::string>& labels);
        void visualizeDebugInformation(const AffordanceKit::ScenePtr& scene);

        void visualizePlane(const AffordanceKit::PlanePtr& plane);
        void visualizeSphere(const AffordanceKit::SpherePtr& sphere);
        void visualizeCylinder(const AffordanceKit::CylinderPtr& cylinder);
        void visualizeBox(const AffordanceKit::BoxPtr& box);

    protected:
        void visualize(bool updatePrimitives = true, bool updateAffordances = true, bool updateDebugInfo = true);

    protected:
        DebugDrawerInterfacePrx debugDrawerTopicPrx;
        AffordancePipelineVisualizationListenerPrx affordanceVisualizationTopicPrx;
        memoryx::WorkingMemoryInterfacePrx workingMemoryPrx;
        memoryx::AffordanceSegmentBasePrx affordanceSegment;
        memoryx::EnvironmentalPrimitiveSegmentBasePrx environmentalPrimitiveSegment;

        std::set<std::string> selections;

        std::map<int, std::list<std::string>> labelMap;

        DrawColor primitiveColor;
        DrawColor primitiveColorFrame;
        DrawColor affordanceLabelColor;

        int affordanceLabelSize;

        bool primitiveVisualizationEnabled;
        bool affordanceVisualizationEnabled;
        bool debugVisualizationEnabled;

        AffordanceVisualizationStyle visualizationStyle;
        std::vector<std::string> visualizationPrimitiveSet;
        std::vector<std::string> visualizationAffordanceSet;
        float visualizationMinExpectedProbability;

        std::mutex sceneMutex;
        AffordanceKitArmarX::SceneArmarXPtr scene;
    };
}
