/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Imagine::ArmarXObjects::ArMarkerExternalCameraCalibration
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <mutex>

#include <VirtualRobot/Robot.h>

#include <ArmarXCore/core/Component.h>

#include <ArmarXCore/core/services/tasks/TaskUtil.h>
#include <ArmarXCore/interface/observers/ObserverInterface.h>
#include <ArmarXGui/libraries/ArmarXGuiComponentPlugins/LightweightRemoteGuiComponentPlugin.h>
#include <RobotAPI/libraries/RobotAPIComponentPlugins/ArVizComponentPlugin.h>
#include <RobotAPI/libraries/RobotAPIComponentPlugins/RobotStateComponentPlugin.h>

#include <VisionX/interface/components/ArMarkerLocalizerInterface.h>
#include <VisionX/interface/components/PointCloudToArViz.h>

#include "ExternalCameraCalibration.h"


namespace armarx
{

    /**
     * @defgroup Component-ArMarkerExternalCameraCalibration ArMarkerExternalCameraCalibration
     * @ingroup VisionX-Components
     * A description of the component ArMarkerExternalCameraCalibration.
     *
     * @class ArMarkerExternalCameraCalibration
     * @ingroup Component-ArMarkerExternalCameraCalibration
     * @brief Brief description of class ArMarkerExternalCameraCalibration.
     *
     * Detailed description of class ArMarkerExternalCameraCalibration.
     */
    class ArMarkerExternalCameraCalibration :
        virtual public armarx::Component
        , virtual public armarx::RobotStateComponentPluginUser
        , virtual public armarx::ArVizComponentPluginUser
        , virtual public armarx::LightweightRemoteGuiComponentPluginUser
    {
        using RobotState = RobotStateComponentPluginUser;

    public:

        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;


    protected:

        /// @see armarx::ManagedIceObject::onInitComponent()
        void onInitComponent() override;

        /// @see armarx::ManagedIceObject::onConnectComponent()
        void onConnectComponent() override;

        /// @see armarx::ManagedIceObject::onDisconnectComponent()
        void onDisconnectComponent() override;

        /// @see armarx::ManagedIceObject::onExitComponent()
        void onExitComponent() override;

        /// @see PropertyUser::createPropertyDefinitions()
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;


        void createRemoteGuiTab();
        void RemoteGui_update() override;


    private:

        void run();
        void onLocalizationResult(const Eigen::Matrix4f& markerPoseInCamera);
        void storeCalibration();


    private:

        visionx::ArMarkerLocalizerInterfacePrx localizer;
        int localizerMarkerID = -1;

        armarx::SimpleRunningTask<>::pointer_type task;


        // Robot

        std::string robotArMarkerFileName = "VisionX/external-camera-calibrtation/Camera_ArMarker.xml";

        VirtualRobot::RobotPtr robotArMarker;

        std::string robotBaseNodeName = "Camera_ArMarker_Base";
        std::string robotArMarkerMarkerNodeName = "ArMarker_Marker";


        // Calibration data
        std::mutex markerPoseMutex;
        /// The marker pose in camera frame.
        Eigen::Matrix4f markerPoseInCamera = Eigen::Matrix4f::Identity();
        Eigen::Matrix4f cameraPoseInRobotBase = Eigen::Matrix4f::Identity();
        std::string calibrationFilePath = "VisionX/external-camera-calibration/calibration.json";


        // Remote Gui
    private:

        struct Tab : armarx::RemoteGui::Client::Tab
        {
            armarx::RemoteGui::Client::Button storeButton;
        };
        Tab tab;


        // Visu
    private:

        struct Visu
        {
            viz::Layer layerCamera;
            viz::Pose markerPoseInCamera = viz::Pose("Marker_Camera");

            viz::Layer layerRobotBase;
            viz::Pose markerPoseInRobotBase = viz::Pose("Marker_RobotBase");
            viz::Pose cameraPoseInRobotBase = viz::Pose("Camera_RobotBase");
            viz::Pose markerModelInRobotBase = viz::Pose("MarkerModel_RobotBase");

            visionx::PointCloudToArVizInterfacePrx pointCloudToArViz;
        };
        Visu vis;

    };
}
