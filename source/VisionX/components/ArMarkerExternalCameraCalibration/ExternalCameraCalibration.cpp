#include "ExternalCameraCalibration.h"

#include <SimoxUtility/json/eigen_conversion.h>

#include <ArmarXCore/core/time/TimeUtil.h>


void armarx::to_json(nlohmann::json& j, const armarx::ExternalCameraCalibration& cal)
{
    j["camera_pose"] = cal.cameraPose;
    j["camera_pose_frame"] = cal.cameraPoseFrame;
    j["timestamp_datetime"] = TimeUtil::toStringDateTime(cal.timestamp);
    j["timestamp_seconds"] = cal.timestamp.toSeconds();
}


void armarx::from_json(const nlohmann::json& j, armarx::ExternalCameraCalibration& cal)
{
    j.at("camera_pose").get_to(cal.cameraPose);
    j.at("camera_pose_frame").get_to(cal.cameraPoseFrame);
    cal.timestamp = IceUtil::Time::seconds(j.at("timestamp_seconds"));
}
