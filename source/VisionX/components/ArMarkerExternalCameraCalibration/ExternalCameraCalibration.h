#pragma once

#include <string>

#include <Eigen/Core>

#include <IceUtil/Time.h>

#include <SimoxUtility/json/json.hpp>


namespace armarx
{

    class ExternalCameraCalibration
    {
    public:

        Eigen::Matrix4f cameraPose;
        std::string cameraPoseFrame;
        IceUtil::Time timestamp;

    };


    void to_json(nlohmann::json& j, const ExternalCameraCalibration& cal);
    void from_json(const nlohmann::json& j, ExternalCameraCalibration& cal);

}
