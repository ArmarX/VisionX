/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::DummyArMarkerLocalizer
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <ArmarXCore/core/Component.h>
#include <VisionX/interface/components/ArMarkerLocalizerInterface.h>
#include <VirtualRobot/math/AbstractFunctionR1R6.h>
#include <ArmarXCore/core/time/TimeUtil.h>
#include <mutex>

namespace visionx
{
    /**
     * @class DummyArMarkerLocalizerPropertyDefinitions
     * @brief
     */
    class DummyArMarkerLocalizerPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        DummyArMarkerLocalizerPropertyDefinitions(std::string prefix):
            armarx::ComponentPropertyDefinitions(prefix)
        {
            //defineRequiredProperty<std::string>("PropertyName", "Description");
            //defineOptionalProperty<std::string>("PropertyName", "DefaultValue", "Description");
            defineOptionalProperty<std::string>("AgentName", "Armar6", "Agent name");
            defineOptionalProperty<std::string>("FrameName", "root", "Frame name");
        }
    };

    /**
     * @defgroup Component-DummyArMarkerLocalizer DummyArMarkerLocalizer
     * @ingroup VisionX-Components
     * A description of the component DummyArMarkerLocalizer.
     *
     * @class DummyArMarkerLocalizer
     * @ingroup Component-DummyArMarkerLocalizer
     * @brief Brief description of class DummyArMarkerLocalizer.
     *
     * Detailed description of class DummyArMarkerLocalizer.
     */
    class DummyArMarkerLocalizer :
        virtual public ArMarkerLocalizerInterface,
        virtual public armarx::Component
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override
        {
            return "ArMarkerLocalizer";
        }

    protected:
        /**
         * @see armarx::ManagedIceObject::onInitComponent()
         */
        void onInitComponent() override;

        /**
         * @see armarx::ManagedIceObject::onConnectComponent()
         */
        void onConnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onDisconnectComponent()
         */
        void onDisconnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onExitComponent()
         */
        void onExitComponent() override;

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        std::string frameName;
        std::string agentName;
        math::SimpleAbstractFunctionR1R6Ptr trajectory;
        IceUtil::Time startTime;

    public:
        void reportImageAvailable(const std::string&, const Ice::Current&) override;
        ArMarkerLocalizationResultList LocalizeAllMarkersNow(const Ice::Current&) override;
        ArMarkerLocalizationResultList GetLatestLocalizationResult(const Ice::Current&) override;
    };
}
