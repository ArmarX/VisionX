/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::RecordingManager
 * @author     Christian R. G. Dreher <c.dreher@kit.edu>
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include <VisionX/components/ImageRecordingManager/ImageRecordingManager.h>


// Simox
#include <SimoxUtility/json.h>

// ArmarX
#include <VisionX/components/ImageRecordingManager/json_conversions.h>


visionx::imrecman::Component::~Component()
{

}


void
visionx::imrecman::Component::onInitComponent()
{
    if (not m_configs_json.empty())
    {
        std::map<std::string, imrec::Config> configs = nlohmann::json::parse(m_configs_json);
        m_configs = configs;
    }
}


void
visionx::imrecman::Component::onConnectComponent()
{

}


void
visionx::imrecman::Component::onDisconnectComponent()
{

}


void
visionx::imrecman::Component::onExitComponent()
{

}


void
visionx::imrecman::Component::configureRecordings(
    const std::map<std::string, imrec::Config>& configs,
    const Ice::Current&)
{
    std::unique_lock l{m_configs_mx};

    if (not configs.empty())
    {
        m_configs = configs;
    }
    else
    {
        m_configs.reset();
    }
}


void
visionx::imrecman::Component::configureRecordingsFromJson(
    const std::string& configs_json,
    const Ice::Current& c)
{
    const std::map<std::string, imrec::Config>& configs = nlohmann::json::parse(configs_json);
    configureRecordings(configs, c);
}


std::map<std::string, visionx::imrec::Config>
visionx::imrecman::Component::getConfiguration(const Ice::Current&)
{
    std::shared_lock l{m_configs_mx};

    if (not m_configs.has_value())
    {
        throw imrecman::NotConfiguredException{};
    }

    return m_configs.value();
}


bool
visionx::imrecman::Component::isConfigured(const Ice::Current&)
{
    std::shared_lock l{m_configs_mx};
    return m_configs.has_value();
}


std::map<std::string, visionx::imrecman::StartStopStatus>
visionx::imrecman::Component::startRecordings(const Ice::Current&)
{
    ARMARX_TRACE;
    ARMARX_DEBUG << "Starting recordings...";

    std::shared_lock l{m_configs_mx};

    std::map<std::string, visionx::imrecman::StartStopStatus> statuses;

    if (not m_configs.has_value())
    {
        throw imrecman::NotConfiguredException{};
    }

    for (auto [proxy_name, config] : m_configs.value())
    {
        StartStopStatus status;
        try
        {
            ARMARX_DEBUG << "Calling startRecording on image provider '" << proxy_name << "'.";
            const bool call_status =
                getProxy<RecordingImageProviderInterfacePrx>(proxy_name)->startImageRecording(config);
            ARMARX_DEBUG << "Got response from image provider.";
            status = call_status ? StartStopStatus::ok : StartStopStatus::already_recording;
        }
        catch (...)
        {
            ARMARX_DEBUG << "Image provider '" << proxy_name << "' offline.";
            status = StartStopStatus::offline;
        }
        statuses[proxy_name] = status;
    }

    ARMARX_DEBUG << "Done delegating call.";

    return statuses;
}


std::map<std::string, visionx::imrec::Status>
visionx::imrecman::Component::getRecordingStatuses(const Ice::Current&)
{
    ARMARX_TRACE;
    ARMARX_DEBUG << "Fetching statuses...";

    std::shared_lock l{m_configs_mx};

    std::map<std::string, visionx::imrec::Status> statuses;

    if (not m_configs.has_value())
    {
        throw imrecman::NotConfiguredException{};
    }

    for (auto& [proxy_name, config] : m_configs.value())
    {
        imrec::Status status;
        try
        {
            ARMARX_DEBUG << "Calling getRecordingStatus on image provider '" << proxy_name << "'.";
            status = getProxy<RecordingImageProviderInterfacePrx>(proxy_name)->getImageRecordingStatus();
            ARMARX_DEBUG << "Got response from image provider.";
        }
        catch (...)
        {
            ARMARX_DEBUG << "Image provider '" << proxy_name << "' offline.";
            status.type = imrec::State::offline;
            status.framesBuffered = -1;
            status.framesWritten = -1;
        }
        statuses[proxy_name] = status;
    }

    ARMARX_DEBUG << "Done delegating call.";

    return statuses;
}


std::map<std::string, visionx::imrecman::StartStopStatus>
visionx::imrecman::Component::stopRecordings(const Ice::Current&)
{
    ARMARX_TRACE;
    ARMARX_DEBUG << "Stopping recordings...";

    std::shared_lock l{m_configs_mx};

    std::map<std::string, visionx::imrecman::StartStopStatus> statuses;

    if (not m_configs.has_value())
    {
        throw imrecman::NotConfiguredException{};
    }

    std::map<std::string, Ice::AsyncResultPtr> promises;

    for (auto& [proxy_name, config] : m_configs.value())
    {
        try
        {
            ARMARX_DEBUG << "Calling stopRecording on image provider '" << proxy_name << "'.";
            promises[proxy_name] = getProxy<RecordingImageProviderInterfacePrx>(proxy_name)->begin_stopImageRecording();
        }
        catch (...)
        {
            promises[proxy_name] = nullptr;
        }
    }

    for (auto& [proxy_name, promise] : promises)
    {
        StartStopStatus status;
        try
        {
            if (promise)
            {
                const bool call_status = getProxy<RecordingImageProviderInterfacePrx>(proxy_name)->end_stopImageRecording(promise);
                ARMARX_DEBUG << "Got response from image provider.";
                status = call_status ? StartStopStatus::ok : StartStopStatus::already_recording;
            }
            else
            {
                ARMARX_DEBUG << "Image provider '" << proxy_name << "' offline.";
                status = StartStopStatus::offline;
            }
        }
        catch (...)
        {
            ARMARX_DEBUG << "Image provider '" << proxy_name << "' offline.";
            status = StartStopStatus::offline;
        }
        statuses[proxy_name] = status;
    }

    ARMARX_DEBUG << "Done delegating stopRecording call.";

    return statuses;
}


std::string
visionx::imrecman::Component::GetDefaultName()
{
    return "ImageRecordingManager";
}


std::string
visionx::imrecman::Component::getDefaultName() const
{
    return GetDefaultName();
}


armarx::PropertyDefinitionsPtr
visionx::imrecman::Component::createPropertyDefinitions()
{
    armarx::PropertyDefinitionsPtr defs =
        new armarx::ComponentPropertyDefinitions{getConfigIdentifier()};

    defs->optional(m_configs_json, "config_json", "JSON to initialize default config from.");

    return defs;
}
