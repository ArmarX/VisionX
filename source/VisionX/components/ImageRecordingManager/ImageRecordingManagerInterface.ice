/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::RecordingManager
 * @author     Christian R. G. Dreher <c.dreher@kit.edu>
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


// ArmarX
#include <ArmarXCore/interface/core/UserException.ice>
#include <VisionX/interface/core/ImageProviderInterface.ice>


module visionx
{

    module imrecman
    {

        exception NotConfiguredException extends armarx::UserException
        {

        };


        enum StartStopStatus
        {
            ok,
            not_recording,
            already_recording,
            offline
        };


        dictionary<string, StartStopStatus> StartStopStatusesMap;

    };


    interface ImageRecordingManagerInterface
    {
        void configureRecordings(imrec::ConfigMap configs);
        void configureRecordingsFromJson(string configs_json);
        imrec::ConfigMap getConfiguration()
                throws imrecman::NotConfiguredException;
        bool isConfigured();
        imrecman::StartStopStatusesMap startRecordings()
                throws imrecman::NotConfiguredException;
        imrec::StatusesMap getRecordingStatuses()
                throws imrecman::NotConfiguredException;
        imrecman::StartStopStatusesMap stopRecordings()
                throws imrecman::NotConfiguredException;
    };

};
