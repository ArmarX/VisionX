/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::MemoryGrapher
 * @author     [Author Name] ( [Author Email] )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "MemoryGrapher.h"

// Include headers you only need in function definitions in the .cpp.

// #include <Eigen/Core>

// #include <SimoxUtility/color/Color.h>

#include <VisionX/libraries/SemanticObjectRelations/ice_serialization.h>

#include <VisionX/libraries/armem_idgraph/MemoryIDResolver.h>
#include <VisionX/libraries/armem_idgraph/json_conversions.h>


namespace armarx::visionx::components::MemoryGrapher
{

    const std::string MemoryGrapher::defaultName = "MemoryGrapher";


    armarx::PropertyDefinitionsPtr
    MemoryGrapher::createPropertyDefinitions()
    {
        armarx::PropertyDefinitionsPtr def =
            new armarx::ComponentPropertyDefinitions(getConfigIdentifier());

        // Publish to a topic (passing the TopicListenerPrx).
        def->topic(graphTopic, "MemoryGraphTopic");

        // Subscribe to a topic (passing the topic name).
        // def->topic<PlatformUnitListener>("MyTopic");

        // Use (and depend on) another component (passing the ComponentInterfacePrx).
        // def->component(myComponentProxy)


        // Add a required property. (The component won't start without a value being set.)
        // def->required(properties.boxLayerName, "p.box.LayerName", "Name of the box layer in ArViz.");

        // Add an optionalproperty.

        return def;
    }


    void
    MemoryGrapher::onInitComponent()
    {
        // Topics and properties defined above are automagically registered.

        // Keep debug observer data until calling `sendDebugObserverBatch()`.
        // (Requies the armarx::DebugObserverComponentPluginUser.)
        // setDebugObserverBatchModeEnabled(true);
    }


    void
    MemoryGrapher::onConnectComponent()
    {
        // Do things after connecting to topics and components.

        /* (Requies the armarx::DebugObserverComponentPluginUser.)
        // Use the debug observer to log data over time.
        // The data can be viewed in the ObserverView and the LivePlotter.
        // (Before starting any threads, we don't need to lock mutexes.)
        {
            setDebugObserverDatafield("numBoxes", properties.numBoxes);
            setDebugObserverDatafield("boxLayerName", properties.boxLayerName);
            sendDebugObserverBatch();
        }
        */

        /* (Requires the armarx::ArVizComponentPluginUser.)
        // Draw boxes in ArViz.
        // (Before starting any threads, we don't need to lock mutexes.)
        drawBoxes(properties, arviz);
        */

        // Setup the remote GUI.
        {
            createRemoteGuiTab();
            RemoteGui_startRunningTask();
        }
    }

    void
    MemoryGrapher::run(const std::string& memoryID)
    {
        armarx::armem::MemoryID converted = armarx::armem::MemoryID::fromString(memoryID);
        armarx::armem::id_graph::MemoryIDResolver resolver(memoryNameSystem());
        armarx::armem::id_graph::MemoryGraph graph = resolver.resolveToGraph(converted);

        graphTopic->reportGraph("MemoryGraph", armarx::semantic::toIce(graph));
        finished.store(true, std::memory_order_release);
    }


    void
    MemoryGrapher::onDisconnectComponent()
    {
        if (worker.joinable())
        {
            worker.join();
        }
    }


    void
    MemoryGrapher::onExitComponent()
    {
        if (worker.joinable())
        {
            worker.join();
        }
    }


    std::string
    MemoryGrapher::getDefaultName() const
    {
        return MemoryGrapher::defaultName;
    }


    void
    MemoryGrapher::createRemoteGuiTab()
    {
        using namespace armarx::RemoteGui::Client;

        // Setup the widgets.

        tab.memoryID.setValue("");
        tab.memoryID.setName("Memory ID");

        tab.createGraph.setLabel("Create Graph");

        // Setup the layout.

        HBoxLayout root = {tab.memoryID, tab.createGraph};
        RemoteGui_createTab(getName(), root, &tab);
    }


    void
    MemoryGrapher::RemoteGui_update()
    {
        if (tab.createGraph.wasClicked())
        {
            std::string memoryID = tab.memoryID.getValue();

            if (worker.joinable())
            {
                if (finished.load(std::memory_order_acquire))
                {
                    worker.join();
                }
                else
                {
                    return;
                }
            }
            finished.store(false, std::memory_order_release);
            worker = std::thread([this, memoryID]() { this->run(memoryID); });
        }
    }


    /* (Requires the armarx::ArVizComponentPluginUser.)
    void
    MemoryGrapher::drawBoxes(const Component::Properties& p, viz::Client& arviz)
    {
        // Draw something in ArViz (requires the armarx::ArVizComponentPluginUser.
        // See the ArVizExample in RobotAPI for more examples.

        viz::Layer layer = arviz.layer(p.boxLayerName);
        for (int i = 0; i < p.numBoxes; ++i)
        {
            layer.add(viz::Box("box_" + std::to_string(i))
                      .position(Eigen::Vector3f(i * 100, 0, 0))
                      .size(20).color(simox::Color::blue()));
        }
        arviz.commit(layer);
    }
    */


} // namespace VisionX::components::MemoryGrapher
