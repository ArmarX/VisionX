
# Libs required for the tests
SET(LIBS ${LIBS} ArmarXCore SemanticGraphExample)

armarx_add_test(SemanticGraphExampleTest SemanticGraphExampleTest.cpp "${LIBS}")
