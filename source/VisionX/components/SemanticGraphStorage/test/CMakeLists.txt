
# Libs required for the tests
SET(LIBS ${LIBS} ArmarXCore SemanticGraphStorage)
 
armarx_add_test(SemanticGraphStorageTest SemanticGraphStorageTest.cpp "${LIBS}")
