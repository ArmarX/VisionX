/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::SemanticRelationAnalyzer
 * @author     Fabian Paus ( fabian dot paus at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// Weird Eigen hacks from Simox require to be included first
#include <VirtualRobot/VirtualRobot.h>

#include <VisionX/libraries/VisionXComponentPlugins/SemanticGraphStorageComponentPlugin.h>
#include <VisionX/interface/components/SemanticRelationAnalyzer.h>
#include <VisionX/interface/libraries/SemanticObjectRelations/GraphStorage.h>
#include <VisionX/libraries/SemanticObjectRelations/JsonSimoxShapeSerializer.h>
#include <RobotAPI/interface/visualization/DebugDrawerInterface.h>
#include <MemoryX/interface/components/WorkingMemoryInterface.h>
#include <MemoryX/libraries/helpers/VirtualRobotHelpers/ObjectInstanceSegmentWrapper.h>
#include <ArmarXCore/core/services/tasks/PeriodicTask.h>
#include <ArmarXCore/core/Component.h>

#include <SemanticObjectRelations/SupportAnalysis/SupportAnalysis.h>

#include <mutex>

namespace armarx
{
    /**
     * @class SemanticRelationAnalyzerPropertyDefinitions
     * @brief
     */
    class SemanticRelationAnalyzerPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        SemanticRelationAnalyzerPropertyDefinitions(std::string prefix);
    };

    /**
     * @defgroup Component-SemanticRelationAnalyzer SemanticRelationAnalyzer
     * @ingroup VisionX-Components
     * A description of the component SemanticRelationAnalyzer.
     *
     * @class SemanticRelationAnalyzer
     * @ingroup Component-SemanticRelationAnalyzer
     * @brief Brief description of class SemanticRelationAnalyzer.
     *
     * Detailed description of class SemanticRelationAnalyzer.
     */
    class SemanticRelationAnalyzer
        : virtual public armarx::Component
        , virtual public armarx::SemanticGraphStorageComponentPluginUser
        , virtual public armarx::SemanticRelationAnalyzerInterface
    {
    public:
        // SemanticRelationAnalyzerInterface interface
        semantic::data::Graph extractSupportGraphFromWorkingMemory(const Ice::Current&) override;

        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override
        {
            return "SemanticRelationAnalyzer";
        }

    protected:
        /**
         * @see armarx::ManagedIceObject::onInitComponent()
         */
        void onInitComponent() override;

        /**
         * @see armarx::ManagedIceObject::onConnectComponent()
         */
        void onConnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onDisconnectComponent()
         */
        void onDisconnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onExitComponent()
         */
        void onExitComponent() override;

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

    private:
        void update();

    private:
        std::string prop_WorkingMemoryName;
        std::string prop_PriorKnowledgeName;
        std::string prop_RobotStateComponentName;
        std::string prop_DebugDrawerTopicName;
        int prop_UpdatePeriodInMS = 10;
        float prop_ContactMarginInMM = 5.0f;

        memoryx::WorkingMemoryInterfacePrx workingMemory;
        memoryx::PriorKnowledgeInterfacePrx priorKnowledge;
        memoryx::RelationMemorySegmentBasePrx relationSegment;
        RobotStateComponentInterfacePrx robotStateComponent;
        DebugDrawerInterfacePrx debugDrawer;

        std::shared_ptr<armarx::semantic::JsonSimoxShapeSerializer> jsonSerializer;

        PeriodicTask<SemanticRelationAnalyzer>::pointer_type updateTask;

        std::mutex componentMutex;
        memoryx::ObjectInstanceSegmentWrapper objectInstancesSegment_;
        semrel::SupportAnalysis supportAnalysis;

    };
}
