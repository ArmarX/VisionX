
# Libs required for the tests
SET(LIBS ${LIBS} ArmarXCore ShapesSupportRelations)
 
armarx_add_test(ShapesSupportRelationsTest ShapesSupportRelationsTest.cpp "${LIBS}")
