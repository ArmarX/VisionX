/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::SimpleEpisodicMemorySemanticGraphConnector
 * @author     fabian.peller-konrad@kit.edu ( fabian dot peller-konrad at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// STD/STL
#include <memory>
#include <mutex>
#include <string>
#include <vector>

// Base Class
#include <ArmarXCore/core/Component.h>

// IVT

// Ice
#include <Ice/Current.h>
#include <IceUtil/Time.h>

// ArmarX
#include <ArmarXCore/core/logging/Logging.h>
#include <MemoryX/components/SimpleEpisodicMemory/SimpleEpisodicMemoryConnector.h>
#include <VisionX/interface/libraries/SemanticObjectRelations/GraphStorage.h>
//#include <VisionX/libraries/SemanticObjectRelations/ice_serialization/graph.h>


namespace visionx
{

    class SimpleEpisodicMemorySemanticGraphConnector :
        virtual public armarx::Component,
        virtual public armarx::semantic::GraphStorageTopic,
        public memoryx::SimpleEpisodicMemoryConnector
    {
    public:

        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;

        // GraphStorageTopic interface
        void reportGraph(std::string const& id, armarx::semantic::data::Graph const& graph, const Ice::Current&) override;


    protected:

        /// @see armarx::ManagedIceObject::onInitComponent()
        void onInitComponent() override;

        /// @see armarx::ManagedIceObject::onConnectComponent()
        void onConnectComponent() override;

        /// @see armarx::ManagedIceObject::onDisconnectComponent()
        void onDisconnectComponent() override;

        /// @see armarx::ManagedIceObject::onExitComponent()
        void onExitComponent() override;

        /// @see PropertyUser::createPropertyDefinitions()
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;


    private:
        std::string graphId = "SpatialGraph";
    };
}
