/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::ExampleMemory
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "HumanMemory.h"

#include <RobotAPI/libraries/armem/server/wm/memory_definitions.h>


namespace armarx::armem::server
{

    armarx::PropertyDefinitionsPtr HumanMemory::createPropertyDefinitions()
    {
        armarx::PropertyDefinitionsPtr defs = new ComponentPropertyDefinitions(getConfigIdentifier());

        setMemoryName("Human");

        profileSegment.defineProperties(defs, "profile.");
        personInstanceSegment.defineProperties(defs, "instance");
        poseSegment.defineProperties(defs, "pose.");
        faceRecognitionSegment.defineProperties(defs, "face.");
        identificationSegment.defineProperties(defs, "ident.");

        return defs;
    }


    HumanMemory::HumanMemory() :
        profileSegment(iceAdapter()),
        personInstanceSegment(iceAdapter()),
        poseSegment(iceAdapter()),
        faceRecognitionSegment(iceAdapter()),
        identificationSegment(iceAdapter())
    {
    }


    std::string HumanMemory::getDefaultName() const
    {
        return "HumanMemory";
    }


    void HumanMemory::onInitComponent()
    {
        profileSegment.init();
        personInstanceSegment.init();
        poseSegment.init();
        faceRecognitionSegment.init();
        identificationSegment.init();

        workingMemory().addCoreSegment("RecognizedAction");
    }


    void HumanMemory::onConnectComponent()
    {
    }


    void HumanMemory::onDisconnectComponent()
    {
    }


    void HumanMemory::onExitComponent()
    {
    }

}
