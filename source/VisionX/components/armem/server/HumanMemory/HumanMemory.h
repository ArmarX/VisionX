/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::ExampleMemory
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/core/Component.h>

#include <RobotAPI/libraries/armem/server/plugins/ReadWritePluginUser.h>
#include <VisionX/libraries/armem_human/server/PoseSegment.h>
#include <VisionX/libraries/armem_human/server/IdentificationSegment.h>
#include <VisionX/libraries/armem_human/server/PersonInstanceSegment.h>
#include <VisionX/libraries/armem_human/server/profile/Segment.h>
#include <VisionX/libraries/armem_human/server/face_recognition/Segment.h>


namespace armarx::armem::server
{
    class HumanMemory :
        virtual public armarx::Component
        , virtual public armem::server::ReadWritePluginUser
    {
    public:

        HumanMemory();

        std::string getDefaultName() const override;

    protected:

        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        void onInitComponent() override;
        void onConnectComponent() override;
        void onDisconnectComponent() override;
        void onExitComponent() override;


    private:

        human::ProfileSegment profileSegment;
        human::PersonInstanceSegment personInstanceSegment;
        human::PoseSegment poseSegment;
        human::FaceRecognitionSegment faceRecognitionSegment;
        human::IdentificationSegment identificationSegment;

    };
}
