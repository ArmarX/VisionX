/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::ArMemVisionMemory
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "VisionMemory.h"

#include <VisionX/libraries/armem_images_core/aron/ImageRGB.aron.generated.h>
#include <VisionX/libraries/armem_images_core/aron/ImageDepth.aron.generated.h>

#include <RobotAPI/libraries/armem/server/wm/memory_definitions.h>
#include <RobotAPI/libraries/armem_vision/aron/OccupancyGrid.aron.generated.h>
#include <RobotAPI/libraries/armem_vision/aron/LaserScannerFeatures.aron.generated.h>

namespace armarx::armem::server
{

    armarx::PropertyDefinitionsPtr VisionMemory::createPropertyDefinitions()
    {
        armarx::PropertyDefinitionsPtr defs = new ComponentPropertyDefinitions(getConfigIdentifier());
        defs->optional(properties.imageRGBMaxHistorySize, "imageRGBMaxHistorySize");
        defs->optional(properties.imageDepthMaxHistorySize, "imageDepthMaxHistorySize");
        defs->optional(properties.occupancyGridMaxHistorySize, "occupancyGridMaxHistorySize");
        defs->optional(properties.pointCloudMaxHistorySize, "pointCloudMaxHistorySize");

        setMemoryName("Vision");

        return defs;
    }


    std::string VisionMemory::getDefaultName() const
    {
        return "VisionMemory";
    }


    void VisionMemory::onInitComponent()
    {
        // Initialize default segments
        {
            wm::CoreSegment& cs = workingMemory().addCoreSegment("ImageRGB", visionx::armem_images::arondto::ImageRGB::ToAronType());
            cs.setMaxHistorySize(properties.imageRGBMaxHistorySize);
        }

        {
            wm::CoreSegment& cs = workingMemory().addCoreSegment("ImageDepth", visionx::armem_images::arondto::ImageDepth::ToAronType());
            cs.setMaxHistorySize(properties.imageDepthMaxHistorySize);
        }

        {
            wm::CoreSegment& cs = workingMemory().addCoreSegment("OccupancyGrid", armem::vision::arondto::OccupancyGrid::ToAronType());
            cs.setMaxHistorySize(properties.occupancyGridMaxHistorySize);
        }

        {
            wm::CoreSegment& cs = workingMemory().addCoreSegment("LaserScannerFeatures", armem::vision::arondto::LaserScannerFeatures::ToAronType());
            cs.setMaxHistorySize(properties.laserScannerFeaturesMaxHistorySize);
        }

        {
            wm::CoreSegment& cs = workingMemory().addCoreSegment("PointCloud");
            cs.setMaxHistorySize(properties.pointCloudMaxHistorySize);
        }
    }


    void VisionMemory::onConnectComponent()
    {
    }


    void VisionMemory::onDisconnectComponent()
    {
    }


    void VisionMemory::onExitComponent()
    {
    }

}
