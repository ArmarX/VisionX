/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::ArMemVisionMemory
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotAPI/libraries/armem/server/plugins/ReadWritePluginUser.h>

#include <ArmarXCore/core/Component.h>


namespace armarx::armem::server
{
    /**
     * @defgroup Component-ArMemVisionMemory ArMemVisionMemory
     * @ingroup VisionX-Components
     * A description of the component ArMemVisionMemory.
     *
     * @class ArMemVisionMemory
     * @ingroup Component-ArMemVisionMemory
     * @brief Brief description of class ArMemVisionMemory.
     *
     * Detailed description of class ArMemVisionMemory.
     */
    class VisionMemory :
        virtual public armarx::Component
        , virtual public armem::server::ReadWritePluginUser
    {
    public:

        std::string getDefaultName() const override;


    protected:

        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        void onInitComponent() override;
        void onConnectComponent() override;
        void onDisconnectComponent() override;
        void onExitComponent() override;


    private:

        struct Properties
        {
            long imageRGBMaxHistorySize = 20;
            long imageDepthMaxHistorySize = 20;
            long occupancyGridMaxHistorySize = 20;
            long laserScannerFeaturesMaxHistorySize = 20;

            long pointCloudMaxHistorySize = 20;
        };
        Properties properties;

    };
}
