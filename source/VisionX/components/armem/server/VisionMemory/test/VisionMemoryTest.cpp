/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::ArMemVisionMemory
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE VisionX::ArmarXObjects::ArMemVisionMemory

#define ARMARX_BOOST_TEST

#include <VisionX/Test.h>
#include "../ArMemVisionMemory.h"

#include <iostream>

#include <VisionX/components/armem/ArMemVisionMemory/aron/ImageRGB.aron.generated.h>


namespace ArMemVisionMemoryTest
{
    struct Fixture
    {
        Fixture()
        {
        }
        ~Fixture()
        {
        }
    };
}


BOOST_FIXTURE_TEST_SUITE(VisionMemoryTest, ArMemVisionMemoryTest::Fixture)


BOOST_AUTO_TEST_CASE(test_stuff)
{
    BOOST_CHECK(true);
}


BOOST_AUTO_TEST_SUITE_END()
