/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::OpenPoseStressTest
 * @author     Christian R. G. Dreher <c.dreher@kit.edu>
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include <VisionX/components/development/OpenPoseStressTest/OpenPoseStressTest.h>


// STD/STL
#include <chrono>
#include <thread>

// ArmarX
#include <ArmarXCore/core/application/properties/PluginAll.h>


visionx::opstress::Component::~Component() = default;


void
visionx::opstress::Component::onInitComponent()
{

}


void
visionx::opstress::Component::onConnectComponent()
{
    task = new armarx::RunningTask<Component>(this, &Component::run);
    task->start();
}


void
visionx::opstress::Component::onDisconnectComponent()
{
    const bool join = true;
    task->stop(join);
}


void
visionx::opstress::Component::onExitComponent()
{

}


std::string
visionx::opstress::Component::GetDefaultName()
{
    return "OpenPoseStressTest";
}


std::string
visionx::opstress::Component::getDefaultName() const
{
    return GetDefaultName();
}


armarx::PropertyDefinitionsPtr
visionx::opstress::Component::createPropertyDefinitions()
{
    armarx::PropertyDefinitionsPtr defs =
        new armarx::ComponentPropertyDefinitions{getConfigIdentifier()};

    defs->component(op);
    defs->optional(wait_for, "wait_for", "Time to wait between starting and stopping OpenPose.");

    return defs;
}


void
visionx::opstress::Component::run()
{
    const std::chrono::milliseconds t1 = std::chrono::milliseconds{wait_for.toMilliSeconds()};
    const std::chrono::milliseconds t2{200};

    while (not task->isStopped())
    {
        op->start();
        std::this_thread::sleep_for(t1);
        op->stop();
        std::this_thread::sleep_for(t2);
    }
}
