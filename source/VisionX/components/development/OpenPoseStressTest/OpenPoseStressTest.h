/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::OpenPoseStressTest
 * @author     Christian R. G. Dreher <c.dreher@kit.edu>
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


// STD/STL
#include <string>

// ArmarX
#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/util/tasks.h>
#include <ArmarXCore/util/time.h>
#include <VisionX/interface/components/OpenPoseEstimationInterface.h>


namespace visionx::opstress
{

    class Component:
        virtual public armarx::Component
    {

    public:

        ~Component() override;

        void onInitComponent() override;

        void onConnectComponent() override;

        void onDisconnectComponent() override;

        void onExitComponent() override;

        static std::string GetDefaultName();

        std::string getDefaultName() const override;

        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

    private:

        void run();

    private:

        armarx::RunningTask<Component>::pointer_type task;

        armarx::OpenPoseEstimationInterfacePrx op;

        IceUtil::Time wait_for = IceUtil::Time::seconds(5);

    };

}


namespace visionx
{
    using OpenPoseStressTest = opstress::Component;
}
