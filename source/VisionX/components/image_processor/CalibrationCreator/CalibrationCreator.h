/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::Component
 * @author     David Schiebener (schiebener at kit dot edu)
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once

#include <opencv2/opencv.hpp>
#include <string>

// VisionX
#include <VisionX/core/ImageProcessor.h>

// IVT
#include <Math/Math3d.h>
#include <Math/Math2d.h>

#include "calibfilter.h"


class CvCalibFilter;
class CByteImage;

namespace visionx
{

    class CalibrationCreatorPropertyDefinitions:
        public visionx::ImageProcessorPropertyDefinitions
    {
    public:
        CalibrationCreatorPropertyDefinitions(std::string prefix):
            visionx::ImageProcessorPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("ImageProviderAdapterName", "ImageProvider", "Ice Adapter name of the image provider");
            defineOptionalProperty<int>("NumberOfImages", 20, "Number of images used for the calibration");
            defineOptionalProperty<int>("WaitingIntervalBetweenImages", 500, "Waiting time between captured images, in ms");
            defineOptionalProperty<int>("NumberOfRows", 5, "Number of rows on the calibration pattern");
            defineOptionalProperty<int>("NumberOfColumns", 5, "Number of columns on the calibration pattern");
            defineOptionalProperty<double>("PatternSquareSize", 30.0, "Size of the squares on the calibration pattern");
            defineOptionalProperty<std::string>("OutputFileName", "cameras.txt", "Path to the file for saving the calibration");
        }
    };

    /**
     * CalibrationCreator determines if the robot hand is colliding with another object, causing it to move.
     * To this end, it localizes the robot hand using the marker ball and the finger tips, calculates the optical
     * flow in the image, clusters it, and looks for a cluster that exist solely in front of the hand.
     *
     * \componentproperties
     * \prop VisionX.CalibrationCreator.ImageProviderAdapterName: Name of the
     *       image provider that delivers the camera images.
     * \prop VisionX.CalibrationCreator.RobotStateProxyName: Name of the robot state
     *       proxy used to obtain the current robot state.
     * \prop VisionX.CalibrationCreator.HandFrameName: Name of the frame in the robot
     *       model that corresponds to the localized hand.
     * \prop VisionX.CalibrationCreator.CameraFrameName: Name of the robot state frame of the primary camera
     */
    class CalibrationCreator :
        virtual public visionx::ImageProcessor
    {
    public:

        /**
        * @see Component::getDefaultName()
        */
        std::string getDefaultName() const override
        {
            return "CalibrationCreator";
        }


    protected:
        // inherited from VisionComponent
        void onInitImageProcessor() override;
        void onConnectImageProcessor() override;
        void onExitImageProcessor() override;

        void process() override;


        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override
        {
            return armarx::PropertyDefinitionsPtr(
                       new CalibrationCreatorPropertyDefinitions(
                           getConfigIdentifier()));
        }

    private:
        std::string providerName;
        ImageProviderInterfacePrx imageProviderPrx;
        CvCalibFilter* m_pCalibFilter;
        CvPoint2D32f* m_pCorners2DFloat;
        CvPoint2D64f* m_pCorners2D;
        CvPoint3D64f* m_pCorners3D;

        CByteImage** cameraImages;
        int waitingIntervalBetweenImages, desiredNumberOfImages, numberOfCapturedImages;
        std::string m_sCameraParameterFileName;

        IceUtil::Time startingTime, timeOfLastCapture;
        bool finished;
    };

}

