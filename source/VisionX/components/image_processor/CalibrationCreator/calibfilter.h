#ifndef CALIBFILTER_H
#define CALIBFILTER_H

// This has been copied from the OpenCV 2 source since the calibration filter
// is no longer part of OpenCV 3.

#include <opencv2/opencv.hpp>
//#include <OpenCVLegacy.h>

/************ Epiline functions *******************/



//typedef struct CvStereoLineCoeff
//{
//    double Xcoef;
//    double XcoefA;
//    double XcoefB;
//    double XcoefAB;
//
//    double Ycoef;
//    double YcoefA;
//    double YcoefB;
//    double YcoefAB;
//
//    double Zcoef;
//    double ZcoefA;
//    double ZcoefB;
//    double ZcoefAB;
//} CvStereoLineCoeff;
//
//
//typedef struct CvCamera
//{
//    float   imgSize[2]; /* size of the camera view, used during calibration */
//    float   matrix[9]; /* intinsic camera parameters:  [ fx 0 cx; 0 fy cy; 0 0 1 ] */
//    float   distortion[4]; /* distortion coefficients - two coefficients for radial distortion
//                              and another two for tangential: [ k1 k2 p1 p2 ] */
//    float   rotMatr[9];
//    float   transVect[3]; /* rotation matrix and transition vector relatively
//                             to some reference point in the space. */
//} CvCamera;
//
//typedef struct CvStereoCamera
//{
//    CvCamera* camera[2]; /* two individual camera parameters */
//    float fundMatr[9]; /* fundamental matrix */
//
//    /* New part for stereo */
//    CvPoint3D32f epipole[2];
//    CvPoint2D32f quad[2][4]; /* coordinates of destination quadrangle after
//                                epipolar geometry rectification */
//    double coeffs[2][3][3];/* coefficients for transformation */
//    CvPoint2D32f border[2][4];
//    CvSize warpSize;
//    CvStereoLineCoeff* lineCoeffs;
//    int needSwapCameras;/* flag set to 1 if need to swap cameras for good reconstruction */
//    float rotMatrix[9];
//    float transVector[3];
//} CvStereoCamera;

#define CV_CAMERA_TO_WARP 1
#define CV_WARP_TO_CAMERA 2


/****************************************************************************************\
*                                   Calibration engine                                   *
\****************************************************************************************/

typedef enum CvCalibEtalonType
{
    CV_CALIB_ETALON_USER = -1,
    CV_CALIB_ETALON_CHESSBOARD = 0,
    CV_CALIB_ETALON_CHECKERBOARD = CV_CALIB_ETALON_CHESSBOARD
}
CvCalibEtalonType;

class CV_EXPORTS CvCalibFilter
{
public:
    /* Constructor & destructor */
    CvCalibFilter();
    virtual ~CvCalibFilter();

    /* Sets etalon type - one for all cameras.
       etalonParams is used in case of pre-defined etalons (such as chessboard).
       Number of elements in etalonParams is determined by etalonType.
       E.g., if etalon type is CV_ETALON_TYPE_CHESSBOARD then:
         etalonParams[0] is number of squares per one side of etalon
         etalonParams[1] is number of squares per another side of etalon
         etalonParams[2] is linear size of squares in the board in arbitrary units.
       pointCount & points are used in case of
       CV_CALIB_ETALON_USER (user-defined) etalon. */
    virtual bool
    SetEtalon(CvCalibEtalonType etalonType, double* etalonParams,
              int pointCount = 0, CvPoint2D32f* points = 0);

    /* Retrieves etalon parameters/or and points */
    virtual CvCalibEtalonType
    GetEtalon(int* paramCount = 0, const double** etalonParams = 0,
              int* pointCount = 0, const CvPoint2D32f** etalonPoints = 0) const;

    /* Sets number of cameras calibrated simultaneously. It is equal to 1 initially */
    virtual void SetCameraCount(int cameraCount);

    /* Retrieves number of cameras */
    int GetCameraCount() const
    {
        return cameraCount;
    }

    /* Starts cameras calibration */
    virtual bool SetFrames(int totalFrames);

    /* Stops cameras calibration */
    virtual void Stop(bool calibrate = false);

    /* Retrieves number of cameras */
    bool IsCalibrated() const
    {
        return isCalibrated;
    }

    /* Feeds another serie of snapshots (one per each camera) to filter.
       Etalon points on these images are found automatically.
       If the function can't locate points, it returns false */
    virtual bool FindEtalon(IplImage** imgs);

    /* The same but takes matrices */
    virtual bool FindEtalon(CvMat** imgs);

    /* Lower-level function for feeding filter with already found etalon points.
       Array of point arrays for each camera is passed. */
    virtual bool Push(const CvPoint2D32f** points = 0);

    /* Returns total number of accepted frames and, optionally,
       total number of frames to collect */
    virtual int GetFrameCount(int* framesTotal = 0) const;

    /* Retrieves camera parameters for specified camera.
       If camera is not calibrated the function returns 0 */
    virtual const CvCamera* GetCameraParams(int idx = 0) const;

    virtual const CvStereoCamera* GetStereoParams() const;

    /* Sets camera parameters for all cameras */
    virtual bool SetCameraParams(CvCamera* params);

    /* Saves all camera parameters to file */
    virtual bool SaveCameraParams(const char* filename);

    /* Loads all camera parameters from file */
    virtual bool LoadCameraParams(const char* filename);

    /* Undistorts images using camera parameters. Some of src pointers can be NULL. */
    virtual bool Undistort(IplImage** src, IplImage** dst);

    /* Undistorts images using camera parameters. Some of src pointers can be NULL. */
    virtual bool Undistort(CvMat** src, CvMat** dst);

    /* Returns array of etalon points detected/partally detected
       on the latest frame for idx-th camera */
    virtual bool GetLatestPoints(int idx, CvPoint2D32f** pts,
                                 int* count, bool* found);

    /* Draw the latest detected/partially detected etalon */
    virtual void DrawPoints(IplImage** dst);

    /* Draw the latest detected/partially detected etalon */
    virtual void DrawPoints(CvMat** dst);

    virtual bool Rectify(IplImage** srcarr, IplImage** dstarr);
    virtual bool Rectify(CvMat** srcarr, CvMat** dstarr);

protected:

    enum { MAX_CAMERAS = 3 };

    /* etalon data */
    CvCalibEtalonType  etalonType;
    int     etalonParamCount;
    double* etalonParams;
    int     etalonPointCount;
    CvPoint2D32f* etalonPoints;
    CvSize  imgSize;
    CvMat*  grayImg;
    CvMat*  tempImg;
    CvMemStorage* storage;

    /* camera data */
    int     cameraCount;
    CvCamera cameraParams[MAX_CAMERAS];
    CvStereoCamera stereo;
    CvPoint2D32f* points[MAX_CAMERAS];
    CvMat*  undistMap[MAX_CAMERAS][2];
    CvMat*  undistImg;
    int     latestCounts[MAX_CAMERAS];
    CvPoint2D32f* latestPoints[MAX_CAMERAS];
    CvMat*  rectMap[MAX_CAMERAS][2];

    /* Added by Valery */
    //CvStereoCamera stereoParams;

    int     maxPoints;
    int     framesTotal;
    int     framesAccepted;
    bool    isCalibrated;
};

#endif // CALIBFILTER_H
