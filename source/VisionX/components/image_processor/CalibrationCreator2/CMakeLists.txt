armarx_component_set_name("CalibrationCreator2")
set(COMPONENT_LIBS
    VisionXCore
    visionx-imrec
    RobotAPICore
    ArmarXCoreObservers
    RobotAPIRobotStateComponent
)

set(SOURCES CalibrationCreator2.cpp)
set(HEADERS CalibrationCreator2.h)

armarx_add_component("${SOURCES}" "${HEADERS}")
armarx_add_component_executable(main.cpp)
