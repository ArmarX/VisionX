/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::CalibrationCreator2
 * @author     Christian R. G. Dreher <c.dreher@kit.edu>
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


#include <string>

// VisionX
#include <VisionX/core/ImageProcessor.h>

// IVT
#include <Math/Math3d.h>
#include <Math/Math2d.h>

#include <opencv2/core.hpp>
#include <opencv2/core/utility.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/calib3d.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/highgui.hpp>


class CByteImage;

namespace visionx
{

    class Settings
    {
    public:
        Settings() : goodInput(false) {}
        enum Pattern { NOT_EXISTING, CHESSBOARD, CIRCLES_GRID, ASYMMETRIC_CIRCLES_GRID };

        void validate()
        {
            goodInput = true;
            if (boardSize.width <= 0 || boardSize.height <= 0)
            {
                ARMARX_ERROR << "Invalid Board size: " << boardSize.width << " " << boardSize.height;
                goodInput = false;
            }
            if (squareSize <= 10e-6)
            {
                ARMARX_ERROR << "Invalid square size " << squareSize;
                goodInput = false;
            }
            if (nrFrames <= 0)
            {
                ARMARX_ERROR << "Invalid number of frames " << nrFrames;
                goodInput = false;
            }

            flag = 0;

            if (useK3ToK6)
            {
                flag |= cv::CALIB_RATIONAL_MODEL;
            }
            else
            {
                flag |= cv::CALIB_FIX_K3;
                flag |= cv::CALIB_FIX_K4;
                flag |= cv::CALIB_FIX_K5;
                flag |= cv::CALIB_FIX_K6;
            }

            //flag |= cv::CALIB_FIX_PRINCIPAL_POINT;
            //flag |= cv::CALIB_ZERO_TANGENT_DIST;
            if (aspectRatio)
            {
                flag |= cv::CALIB_FIX_ASPECT_RATIO;
            }
            //flag |= cv::CALIB_FIX_K1;
            //flag |= cv::CALIB_FIX_K2;

            if (calibrationPattern == NOT_EXISTING)
            {
                ARMARX_ERROR << " Camera calibration mode does not exist. ";
                goodInput = false;
            }
        }

    public:
        cv::Size boardSize;              // The size of the board -> Number of items by width and height
        Pattern calibrationPattern;  // One of the Chessboard, circles, or asymmetric circle pattern
        double squareSize;            // The size of a square in your defined unit (point, millimeter,etc).
        int nrFrames;                // The number of frames to use from the input for calibration
        float aspectRatio;           // The aspect ratio
        bool writePoints;            // Write detected feature points
        bool writeExtrinsics;        // Write extrinsic parameters
        bool flipVertical;           // Flip the captured images around the horizontal axis
        std::string outputFileName;       // The name of the file where to write
        bool useK3ToK6;

        bool goodInput;
        int flag;

    };


    class CalibrationCreator2 :
        virtual public visionx::ImageProcessor
    {
    public:

        static const std::string default_name;

        std::string getDefaultName() const override
        {
            return default_name;
        }

    protected:
        // inherited from VisionComponent
        void onInitImageProcessor() override;
        void onConnectImageProcessor() override;
        void onDisconnectImageProcessor() override;
        void onExitImageProcessor() override;

        void process() override;


        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override
        {
            armarx::PropertyDefinitionsPtr def = new armarx::ComponentPropertyDefinitions{getConfigIdentifier()};

            def->defineOptionalProperty<std::string>("ImageProviderAdapterName", "ImageProvider", "Ice Adapter name of the image provider");
            def->defineOptionalProperty<int>("NumberOfImages", 20, "Number of images used for the calibration");
            def->defineOptionalProperty<int>("WaitingIntervalBetweenImages", 500, "Waiting time between captured images, in ms");
            def->defineOptionalProperty<int>("NumberOfRows", 5, "Number of corner rows on the calibration pattern (height; chessboard squares - 1)");
            def->defineOptionalProperty<int>("NumberOfColumns", 5, "Number of corner columns on the calibration pattern (width; chessboard squares - 1)");
            def->defineOptionalProperty<double>("PatternSquareSize", 30.0, "Size of the squares on the calibration pattern");
            def->defineOptionalProperty<std::string>("OutputFileName", "cameras.txt", "Path to the file for saving the calibration");
            def->defineOptionalProperty<bool>("UseAdditionalParams", false, "Also use K3-K6 parameters");
            def->defineOptionalProperty<bool>("EnableFixAspectRatio", false, "Fix/Calibrate focal length aspect ratio");
            def->defineOptionalProperty<std::string>("ImageToUse", "left", "Use left or rigth image for calibration").map("left", "left").map("right", "right");
            return def;
        }

    private:

        enum {CAPTURING, CALIBRATED};

        int m_mode = CAPTURING;

        Settings m_settings;

        std::vector<std::vector<cv::Point2f>> imagePoints;
        cv::Mat cameraMatrix, distCoeffs;
        cv::Size imageSize;

        std::string providerName;
        ImageProviderInterfacePrx imageProviderPrx;

        //        CvCalibFilter* m_pCalibFilter;
        //        CvPoint2D32f* m_pCorners2DFloat;
        //        CvPoint2D64f* m_pCorners2D;
        //        CvPoint3D64f* m_pCorners3D;

        CByteImage** cameraImages;
        //int waitingIntervalBetweenImages, desiredNumberOfImages, numberOfCapturedImages;
        std::string m_outputFileName;

        IceUtil::Time waitingTimeBetweenCaptures;
        IceUtil::Time timeOfLastCapture;
        unsigned int imageID;
        bool finished;
    };

}

