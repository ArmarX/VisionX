/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::SimpleEpisodicMemoryImageConnector
 * @author     Fabian Peller ( fabian dot peller-konrad at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <ArmarXCore/core/Component.h>

#include <mutex>
#include <string>
#include <vector>

// IVT
#include <Image/ByteImage.h>

// Ice
#include <Ice/Current.h>
#include <IceUtil/Time.h>

// ArmarX
#include <ArmarXCore/interface/observers/ObserverInterface.h>

#include <RobotAPI/libraries/armem/client/plugins.h>
#include <VisionX/core/ImageProcessor.h>

#include <VisionX/libraries/armem_images_core/ImagesProperties.h>
#include <VisionX/libraries/armem_images_core/ImageToArMem.h>
#include <VisionX/libraries/armem_images_core/aron/ImageRGB.aron.generated.h>
#include <VisionX/libraries/armem_images_core/aron/ImageDepth.aron.generated.h>


namespace visionx
{
    class ImageToArMem :
        public visionx::ImageProcessor
        , public armarx::armem::client::PluginUser
    {
    public:

        std::string getDefaultName() const override;


    protected:

        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        // ImageProcessor interface
        void onInitImageProcessor() override;
        void onConnectImageProcessor()  override;
        void onDisconnectImageProcessor() override;
        void onExitImageProcessor() override;

        void process() override;


    private:

        armarx::DebugObserverInterfacePrx debugObserver;

        visionx::ImageProviderInterfacePrx imageProvider;
        visionx::ImageProviderInfo imageProviderInfo;
        armarx::MetaInfoSizeBase::PointerType imageMetaInfo;

        // Image buffer
        std::mutex inputImageMutex;
        std::vector<CByteImage> inputImages;
        std::vector<CByteImage*> inputImageBuffer;


        // ArMem

        visionx::armem_images::ImageToArMem imageToArMem;

        struct Properties
        {
            visionx::armem_images::ImagesProperties images;

            std::string imageProviderName = "ImageProvider";
            std::string providerSegmentName = "(auto)";

            bool clearProviderSegmentWhenExists = false;
        };
        Properties p;

    };

}
