armarx_set_target("KinectAndCameraCalibration Lib")
set(LIB_NAME       VisionXKinectAndCameraCalibration)

set(LIBS    VisionXInterfaces
            VisionXCore
            VisionXTools
            CalibrationCreator2
            ArmarXCoreInterfaces
            ArmarXCore
            RobotAPICore
            ArmarXCoreObservers
            RobotAPIRobotStateComponent
)

set(LIB_FILES
    KinectAndCameraCalibration.cpp
    KinectAndCameraCalibrationObserver.cpp
)

set(LIB_HEADERS
    KinectAndCameraCalibration.h
    KinectAndCameraCalibrationObserver.h
)

armarx_add_library("${LIB_NAME}"  "${LIB_FILES}" "${LIB_HEADERS}" "${LIBS}")

set(COMPONENT_APPLICATION_TYPE    visionx::VisionXApplication)
set(COMPONENT_APPLICATION_INCLUDE VisionX/core/VisionXApplication.h)
set(COMPONENT_NAMESPACE           visionx)
set(CONFIG_DOMAIN                 VisionX)

armarx_generate_and_add_component_executable(
    COMPONENT_NAME KinectAndCameraCalibration
    CONFIG_NAME    KinectAndCameraCalibration
    APPLICATION_APP_SUFFIX)
armarx_generate_and_add_component_executable(
    COMPONENT_NAME KinectAndCameraCalibrationObserver
    CONFIG_NAME    KinectAndCameraCalibrationObserver
    APPLICATION_APP_SUFFIX)
