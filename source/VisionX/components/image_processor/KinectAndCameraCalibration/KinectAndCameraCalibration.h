/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::Component
 * @author     David Schiebener (schiebener at kit dot edu)
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once

#include <opencv2/opencv.hpp>
#include <string>

// VisionX
#include <VisionX/core/ImageProcessor.h>
#include <VisionX/interface/components/KinectAndCameraCalibration.h>

// IVT
#include <Math/Math3d.h>
#include <Math/Math2d.h>

#include <VisionX/components/image_processor/CalibrationCreator/calibfilter.h>


class CvCalibFilter;
class CByteImage;



namespace visionx
{

    class KinectAndCameraCalibrationPropertyDefinitions:
        public visionx::ImageProcessorPropertyDefinitions
    {
    public:
        KinectAndCameraCalibrationPropertyDefinitions(std::string prefix):
            visionx::ImageProcessorPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("CameraImageProviderAdapterName", "Armar3ImageProvider", "Ice Adapter name of the camera image provider");
            defineOptionalProperty<std::string>("KinectImageProviderAdapterName", "OpenNIPointCloudProvider", "Ice Adapter name of the Kinect image provider");
            defineOptionalProperty<int>("NumberOfImages", 20, "Number of images used for the calibration");
            defineOptionalProperty<int>("WaitingIntervalBetweenImages", 500, "Waiting time between captured images, in ms");
            defineOptionalProperty<int>("NumberOfRows", 5, "Number of rows on the calibration pattern");
            defineOptionalProperty<int>("NumberOfColumns", 5, "Number of columns on the calibration pattern");
            defineOptionalProperty<double>("PatternSquareSize", 30.0, "Size of the squares on the calibration pattern");
            defineOptionalProperty<std::string>("OutputFileName", "cameras.txt", "Path to the file for saving the calibration");
        }
    };

    /**
     * KinectAndCameraCalibration executes the calibration for the left camera of the stereo and kinect rgb camera.
     * To this end, it gives you the pose of the left camera based on the kinect frame with saving the file including calibration data.
     *
     * \componentproperties
     * \prop VisionX.KinectAndCameraCalibration.CameraImageProviderAdapterName: Ice Adapter name of the camera image provider.
     * \prop VisionX.KinectAndCameraCalibration.KinectImageProviderAdapterName: Ice Adapter name of the Kinect image provider.
     * \prop VisionX.KinectAndCameraCalibration.NumberOfImages: Number of images used for the calibration.
     * \prop VisionX.KinectAndCameraCalibration.WaitingIntervalBetweenImages: Waiting time between captured images, in ms.
     * \prop VisionX.KinectAndCameraCalibration.NumberOfRows: Number of rows on the calibration pattern.
     * \prop VisionX.KinectAndCameraCalibration.NumberOfColumns: Number of columns on the calibration pattern.
     * \prop VisionX.KinectAndCameraCalibration.PatternSquareSize: Size of the squares on the calibration pattern.
     * \prop VisionX.KinectAndCameraCalibration.OutputFileName: Path to the file for saving the calibration.
     */

    class KinectAndCameraCalibration:
        virtual public visionx::ImageProcessor,
        virtual public visionx::KinectAndCameraCalibrationInterface
    {
    public:

        /**
        * @see Component::getDefaultName()
        */
        std::string getDefaultName() const override
        {
            return "KinectAndCameraCalibration";
        }


        /**
        * ice related functions (todo: put comments)
        */
        void startCalibration(const ::Ice::Current& c = Ice::emptyCurrent) override;
        void stopCalibration(const ::Ice::Current& c = Ice::emptyCurrent) override;
        visionx::KinectPoseCalibration getCalibrationParameters(const ::Ice::Current& c = Ice::emptyCurrent) override;


    protected:
        // inherited from VisionComponent
        void onInitImageProcessor() override;
        void onConnectImageProcessor() override;
        void onExitImageProcessor() override;

        void process() override;


        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override
        {
            return armarx::PropertyDefinitionsPtr(
                       new KinectAndCameraCalibrationPropertyDefinitions(
                           getConfigIdentifier()));
        }

    private:
        std::string cameraImageProviderName, kinectImageProviderName;
        ImageProviderInterfacePrx cameraImageProviderPrx, kinectImageProviderPrx;
        CvCalibFilter* m_pCalibFilter;

        CByteImage** cameraImages, ** kinectImages;
        int waitingIntervalBetweenImages, desiredNumberOfImages, numberOfCapturedImages;
        double etalonParams[3];

        std::string m_sCameraParameterFileName;

        IceUtil::Time startingTime, timeOfLastCapture;
        bool finished;

        bool requested;
        KinectPoseCalibration result;
        KinectAndCameraCalibrationListenerPrx listenerPrx;
    };

}

