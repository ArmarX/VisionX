/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::MultiImageProvider
 * @author     Markus Grotz ( markus dot grotz at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once



#include <VisionX/interface/components/Calibration.h>


#include <VisionX/core/ImageProcessor.h>
#include <VisionX/core/ImageProcessor.h>

#include <mutex>

namespace armarx
{

    /**
     * @class MultiImageProviderPropertyDefinitions
     * @brief
     */
    class MultiImageProviderPropertyDefinitions:
        public visionx::ImageProcessorPropertyDefinitions
    {
    public:
        MultiImageProviderPropertyDefinitions(std::string prefix):
            visionx::ImageProcessorPropertyDefinitions(prefix)
        {
            defineRequiredProperty<std::string>("ImageProviders", "Split names by ;");
        }
    };

    /**
     * @defgroup Component-MultiImageProvider MultiImageProvider
     * @ingroup VisionX-Components
     * A description of the component MultiImageProvider.
     *
     * @class MultiImageProvider
     * @ingroup Component-MultiImageProvider
     * @brief Brief description of class MultiImageProvider.
     *
     * Detailed description of class MultiImageProvider.
     */
    class MultiImageProvider :
        virtual public visionx::ImageProcessor
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override
        {
            return "MultiImageProvider";
        }

    protected:

        /**
         * @see visionx::ImageProcessor::onInitImageProcessor()
         */
        void onInitImageProcessor() override;

        /**
         * @see visionx::ImageProcessor::onConnectImageProcessor()
         */
        void onConnectImageProcessor() override;

        /**
         * @see visionx::ImageProcessor::onExitImageProcessor()
         */
        void onExitImageProcessor() override;

        /**
         * @see visionx::ImageProcessor::process()
         */
        void process() override;

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;


    private:
        std::mutex mutex;
        struct ImageProviderData
        {
            int numImages = 0;
            std::vector<visionx::CByteImageUPtr> srcCameraImages;
            std::vector<visionx::CByteImageUPtr> targetCameraImages;
            bool resize = false;
            std::set<size_t> imageSelection;
            visionx::ImageProviderInfo providerInfo;
        };
        size_t totalNumberOfImages = 0;
        std::map<std::string, ImageProviderData> imageProviderData;


    };
}

