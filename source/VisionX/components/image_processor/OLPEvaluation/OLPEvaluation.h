/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::OLPEvaluation
 * @author     David ( david dot schiebener at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <ArmarXCore/core/Component.h>

#include <RobotAPI/interface/units/KinematicUnitInterface.h>

#include <VisionX/interface/components/ObjectLearningByPushing.h>
#include <VisionX/interface/components/Calibration.h>


namespace visionx
{
    /**
     * @class OLPEvaluationPropertyDefinitions
     * @brief
     */
    class OLPEvaluationPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        OLPEvaluationPropertyDefinitions(std::string prefix):
            armarx::ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("OLPName", "ObjectLearningByPushing", "Description");
            defineOptionalProperty<std::string>("OLPObserverName", "ObjectLearningByPushingObserver", "Description");
            defineOptionalProperty<std::string>("ImageProviderName", "ImageSequenceProvider", "Description");
            defineOptionalProperty<std::string>("KinematicUnitName", "Armar3KinematicUnit", "Name of the kinematic unit that should be used");
            defineOptionalProperty<bool>("TestRecognition", false, "Set true to evaluate the object recognition");
        }
    };

    /**
     * @defgroup Component-OLPEvaluation OLPEvaluation
     * @ingroup VisionX-Components
     * A description of the component OLPEvaluation.
     *
     * @class OLPEvaluation
     * @ingroup Component-OLPEvaluation
     * @brief Brief description of class OLPEvaluation.
     *
     * Detailed description of class OLPEvaluation.
     */
    class OLPEvaluation :
        virtual public armarx::Component
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override
        {
            return "OLPEvaluation";
        }

    protected:
        /**
         * @see armarx::ManagedIceObject::onInitComponent()
         */
        void onInitComponent() override;

        /**
         * @see armarx::ManagedIceObject::onConnectComponent()
         */
        void onConnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onDisconnectComponent()
         */
        void onDisconnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onExitComponent()
         */
        void onExitComponent() override;

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

    public:
        ObjectLearningByPushingInterfacePrx olpProxy;
        ObjectLearningByPushingListenerPrx olpObserverProxy;
        ImageFileSequenceProviderInterfacePrx imageProviderProxy;
        armarx::KinematicUnitInterfacePrx kinematicUnitProxy;
    };
}

