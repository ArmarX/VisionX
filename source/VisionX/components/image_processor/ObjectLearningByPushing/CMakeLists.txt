armarx_set_target("ObjectLearningByPushingLib")

set(LIB_NAME       VisionXObjectLearningByPushing)

set(LIBS
    VisionXCore
    VisionXPointCloud
    ArmarXCore
    RobotAPICore
    ArmarXCoreObservers
    RobotAPIRobotStateComponent
)

set(LIB_FILES
    ColorICP.cpp
    FeatureCalculation.cpp
    GaussBackground.cpp
    HypothesisGeneration.cpp
    HypothesisValidationRGBD.cpp
    HypothesisVisualization.cpp
    LCCPSegmentation.cpp
    MSERCalculation.cpp
    ObjectLearningByPushing.cpp
    ObjectLearningByPushingObserver.cpp
    ObjectRecognition.cpp
    OLPTools.cpp
    PointCloudRegistration.cpp
    SaliencyCalculation.cpp
    SegmentedPointCloudFusion.cpp
)

set(LIB_HEADERS
    ColorICP.h
    FeatureCalculation.h
    GaussBackground.h
    HypothesisGeneration.h
    HypothesisValidationRGBD.h
    HypothesisVisualization.h
    LCCPSegmentation.h
    MSERCalculation.h
    ObjectHypothesis.h
    ObjectLearningByPushing.h
    ObjectLearningByPushingDefinitions.h
    ObjectLearningByPushingObserver.h
    ObjectRecognition.h
    OLPTools.h
    PointCloudRegistration.h
    SaliencyCalculation.h
    SegmentedPointCloudFusion.h
)

armarx_add_library("${LIB_NAME}"  "${LIB_FILES}" "${LIB_HEADERS}" "${LIBS}")

set(COMPONENT_APPLICATION_TYPE    visionx::VisionXApplication)
set(COMPONENT_APPLICATION_INCLUDE VisionX/core/VisionXApplication.h)
set(COMPONENT_NAMESPACE           visionx)
set(CONFIG_DOMAIN                 VisionX)

armarx_generate_and_add_component_executable(COMPONENT_NAME ObjectLearningByPushingObserver CONFIG_NAME ObjectLearningByPushingObserver)
armarx_generate_and_add_component_executable(COMPONENT_NAME ObjectLearningByPushing         CONFIG_NAME ObjectLearningByPushing)
