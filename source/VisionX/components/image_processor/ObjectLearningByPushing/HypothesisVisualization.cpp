/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "HypothesisVisualization.h"
#include "OLPTools.h"

// IVT
#include <Image/ByteImage.h>
#include <Image/ImageProcessor.h>
#include <Image/IplImageAdaptor.h>
#include <Calibration/Calibration.h>

// OpenCV
//#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc_c.h>

//#include <cstdlib>
//#include <cstdio>

//#include <ArmarXCore/core/logging/Logging.h>




CHypothesisVisualization::CHypothesisVisualization(CCalibration* calibration)
{
#ifdef OLP_SHOW_RESULT_IMAGES
    // create windows
    //cvNamedWindow("Confirmed hypotheses", CV_WINDOW_AUTOSIZE);
    //cvNamedWindow("Hypotheses (left)", CV_WINDOW_AUTOSIZE);
    //cvNamedWindow("Hypotheses (right)", CV_WINDOW_AUTOSIZE);
#endif

    // image for visualisation
    m_pIplImageLeft = cvCreateImage(cvSize(OLP_IMG_WIDTH, OLP_IMG_HEIGHT), IPL_DEPTH_8U, 3);
    m_pIplImageRight = cvCreateImage(cvSize(OLP_IMG_WIDTH, OLP_IMG_HEIGHT), IPL_DEPTH_8U, 3);
    m_pOldVisualizationImageIpl = cvCloneImage(m_pIplImageLeft);

    // set stereo calibration
    this->calibration = calibration;

    // disparity stuff
    //cvNamedWindow("Disparity", CV_WINDOW_AUTOSIZE);
    //cvNamedWindow("Disparity edges", CV_WINDOW_AUTOSIZE);
    //cvNamedWindow("Combined edges", CV_WINDOW_AUTOSIZE);
    //m_pIplImageLeftRectified = cvCreateImage(cvSize(OLP_IMG_WIDTH,OLP_IMG_HEIGHT), IPL_DEPTH_8U, 3);
    //m_pIplImageRightRectified = cvCreateImage(cvSize(OLP_IMG_WIDTH,OLP_IMG_HEIGHT), IPL_DEPTH_8U, 3);
    //m_pIplImageDisparity = cvCreateImage(cvSize(OLP_IMG_WIDTH,OLP_IMG_HEIGHT), IPL_DEPTH_8U, 1);

    // define colors
    int nMaxColors = 1000;
    colors = new CvScalar[nMaxColors];
    colors[0] = cvScalar(255, 20, 20);
    colors[1] = cvScalar(0, 0, 255);
    colors[2] = cvScalar(0, 240, 255);
    colors[3] = cvScalar(0, 255, 0);
    colors[4] = cvScalar(255, 255, 0);
    colors[5] = cvScalar(255, 0, 255);
    colors[6] = cvScalar(45, 200, 105);
    colors[7] = cvScalar(180, 240, 150);
    colors[8] = cvScalar(160, 30, 55);
    colors[9] = cvScalar(230, 130, 70);
    colors[10] = cvScalar(70, 190, 210);
    colors[11] = cvScalar(75, 160, 110);
    colors[12] = cvScalar(150, 210, 155);
    colors[13] = cvScalar(150, 30, 180);
    colors[14] = cvScalar(210, 80, 55);
    colors[15] = cvScalar(120, 120, 120);
    colors[16] = cvScalar(170, 70, 75);
    colors[17] = cvScalar(170, 200, 175);
    colors[18] = cvScalar(40, 70, 75);
    colors[19] = cvScalar(220, 70, 190);

    for (int n = 20; n < nMaxColors; n++)
    {
        colors[n] = cvScalar(20 + rand() / (RAND_MAX / 235), 20 + rand() / (RAND_MAX / 235), 20 + rand() / (RAND_MAX / 235));
    }


    screenshotImage = new CByteImage(OLP_IMG_WIDTH, OLP_IMG_HEIGHT, CByteImage::eRGB24);
    segmentationImage = new CByteImage(OLP_IMG_WIDTH, OLP_IMG_HEIGHT, CByteImage::eGrayScale);
    segmentedCameraImage = new CByteImage(OLP_IMG_WIDTH, OLP_IMG_HEIGHT, CByteImage::eRGB24);
    screenshotFileName = OLP_SCREENSHOT_PATH;
    screenshotFileName.append("hyp0000.bmp");
    segmentationImageFileName = OLP_SCREENSHOT_PATH;
    segmentationImageFileName.append("segm0000.bmp");
    segmentedCameraImageFileName = OLP_SCREENSHOT_PATH;
    segmentedCameraImageFileName.append("segmcam0000.bmp");
    screenshotCounter = 0;
}




CHypothesisVisualization::~CHypothesisVisualization(void)
{
#ifdef OLP_SHOW_RESULT_IMAGES
    cv::destroyWindow("Hypotheses (left)");
    //cvDestroyWindow("Hypotheses (right)");
    cv::destroyWindow("Confirmed hypotheses");
#endif
    delete[] colors;
    cvReleaseImage(&m_pIplImageLeft);
    cvReleaseImage(&m_pIplImageRight);
    cvReleaseImage(&m_pOldVisualizationImageIpl);
    delete screenshotImage;
    delete segmentationImage;
    delete segmentedCameraImage;
}




bool CHypothesisVisualization::VisualizeHypotheses(const CByteImage* pByteImageColorLeft, const CByteImage* pByteImageColorRight, const CObjectHypothesisArray& aHypotheses,
        const CSIFTFeatureArray& aAllPoints, std::vector<CMSERDescriptor3D*>& aAllMSERs, std::vector<CMSERDescriptor3D*>& aCorrespondingMSERs,
        bool bConfirmedHypotheses, CByteImage* pResultImageLeft, CByteImage* pResultImageRight, const bool bMakeScreenshot, const bool bIsLeftImage)
{

    //**************************************************************************************************************
    // visualize the found features and planes
    //**************************************************************************************************************


#ifdef OLP_SHOW_RESULT_IMAGES
    cvReleaseImage(&m_pOldVisualizationImageIpl);
    m_pOldVisualizationImageIpl = cvCloneImage(m_pIplImageLeft);
#endif


    // load image with OpenCV
    for (int j = 0; j < OLP_IMG_HEIGHT; j++)
    {
        for (int i = 0; i < OLP_IMG_WIDTH; i++)
        {
            ((uchar*)(m_pIplImageLeft->imageData + j * m_pIplImageLeft->widthStep))[i * m_pIplImageLeft->nChannels + 2] = pByteImageColorLeft->pixels[3 * (OLP_IMG_WIDTH * j + i)];
            ((uchar*)(m_pIplImageLeft->imageData + j * m_pIplImageLeft->widthStep))[i * m_pIplImageLeft->nChannels + 1] = pByteImageColorLeft->pixels[3 * (OLP_IMG_WIDTH * j + i) + 1];
            ((uchar*)(m_pIplImageLeft->imageData + j * m_pIplImageLeft->widthStep))[i * m_pIplImageLeft->nChannels + 0] = pByteImageColorLeft->pixels[3 * (OLP_IMG_WIDTH * j + i) + 2];

            ((uchar*)(m_pIplImageRight->imageData + j * m_pIplImageRight->widthStep))[i * m_pIplImageRight->nChannels + 2] = pByteImageColorRight->pixels[3 * (OLP_IMG_WIDTH * j + i)];
            ((uchar*)(m_pIplImageRight->imageData + j * m_pIplImageRight->widthStep))[i * m_pIplImageRight->nChannels + 1] = pByteImageColorRight->pixels[3 * (OLP_IMG_WIDTH * j + i) + 1];
            ((uchar*)(m_pIplImageRight->imageData + j * m_pIplImageRight->widthStep))[i * m_pIplImageRight->nChannels + 0] = pByteImageColorRight->pixels[3 * (OLP_IMG_WIDTH * j + i) + 2];

        }
    }

#ifndef OLP_USE_ARMAR3_ARMAR3_4

    if (bMakeScreenshot)
    {
        CByteImage* pScreenshotImageGrey = new CByteImage(OLP_IMG_WIDTH, OLP_IMG_HEIGHT, CByteImage::eGrayScale);
        ImageProcessor::ConvertImage(pByteImageColorLeft, pScreenshotImageGrey);
        ImageProcessor::ConvertImage(pScreenshotImageGrey, screenshotImage);
        delete pScreenshotImageGrey;
    }

#endif




    //****************************************************************************
    //  visualize the hypotheses
    //****************************************************************************


    const int nNumHypotheses = aHypotheses.GetSize();


    CvFont cFont1, cFont2, cFont3;
    cvInitFont(&cFont1, CV_FONT_HERSHEY_SIMPLEX, 0.9, 0.9, 0, 3);
    cvInitFont(&cFont2, CV_FONT_HERSHEY_SIMPLEX, 0.9, 0.9, 0, 2);
    cvInitFont(&cFont3, CV_FONT_HERSHEY_SIMPLEX, 0.3, 0.3, 0, 1);


    // visualize the found hypotheses
    {
        CvScalar cColor;
        char* pcN = new char[4];

        for (int n = nNumHypotheses - 1; n >= 0; n--)
        {
            cColor = colors[aHypotheses[n]->nHypothesisNumber];
            const int nHypothesisSize = aHypotheses[n]->aNewPoints.size() + aHypotheses[n]->aVisibleConfirmedPoints.size();

            // project points to 2D
            Vec2d* pPoints2D = new Vec2d[nHypothesisSize];

            for (int i = 0; i < (int)aHypotheses[n]->aNewPoints.size(); i++)
            {
                calibration->WorldToImageCoordinates(aHypotheses[n]->aNewPoints.at(i)->vPosition, pPoints2D[i], false);
            }

            for (int i = 0; i < (int)aHypotheses[n]->aVisibleConfirmedPoints.size(); i++)
            {
                calibration->WorldToImageCoordinates(aHypotheses[n]->aVisibleConfirmedPoints.at(i)->vPosition, pPoints2D[aHypotheses[n]->aNewPoints.size() + i], false);
            }

            // paint the points into the image
            for (int i = 0; i < nHypothesisSize; i++)
            {
                //cvCircle(m_pIplImageLeft, cvPoint((int)pPoints2D[i].x, (int)pPoints2D[i].y), 0, cvScalar(255-cColor.val[0], 255-cColor.val[1], 255-cColor.val[2]));
                //cvCircle(m_pIplImageLeft, cvPoint((int)pPoints2D[i].x, (int)pPoints2D[i].y), 1, cColor);
                //cvCircle(m_pIplImageLeft, cvPoint((int)pPoints2D[i].x, (int)pPoints2D[i].y), 2, cColor);

                cvCircle(m_pIplImageLeft, cvPoint((int)pPoints2D[i].x, (int)pPoints2D[i].y), 0, cColor);

                //cvCircle(m_pIplImageLeft, cvPoint((int)pPoints2D[i].x+1, (int)pPoints2D[i].y), 0, cColor);
                if (!bConfirmedHypotheses)
                {
                    cvCircle(m_pIplImageLeft, cvPoint((int)pPoints2D[i].x, (int)pPoints2D[i].y), 1, cColor);
                    //cvCircle(m_pIplImageLeft, cvPoint((int)pPoints2D[i].x, (int)pPoints2D[i].y), 2, cColor);
                }

                if ((!bConfirmedHypotheses) && bMakeScreenshot)
                {
                    MarkConfirmedPoint(screenshotImage, (int)pPoints2D[i].x, (int)pPoints2D[i].y, cColor);
                }
                else if (bMakeScreenshot)
                {
                    MarkUnconfirmedPoint(screenshotImage, (int)pPoints2D[i].x, (int)pPoints2D[i].y, cColor);
                }

                //if (i<(int)aHypotheses[n]->aNewPoints.size())
                //{
                //_itoa(((int)aHypotheses[n]->aNewPoints.at(i)->vPosition.z%100), pcZ, 10);
                //sARMARX_VERBOSE_S << pcZ, "%d", ((int)aHypotheses[n]->aNewPoints.at(i)->vPosition.z/10));
                //cvPutText(m_pIplImageLeft, pcZ, cvPoint((int)pPoints2D[i].x, (int)pPoints2D[i].y), &cFont3, cvScalar(255, 255, 255));
                //}

                //if (aHypotheses[n]->eType == CObjectHypothesis::ePlane)
                //{
                //for (int j=i+1; j<nHypothesisSize; j++)
                //{
                //  cvLine(m_pIplImageLeft, cvPoint((int)pPoints2D[i].x,(int)pPoints2D[i].y), cvPoint((int)pPoints2D[j].x,(int)pPoints2D[j].y), cColor, 1);
                //}
                //}
            }

            for (int i = aHypotheses[n]->aNewPoints.size(); i < nHypothesisSize; i++)
            {
                //cvCircle(m_pIplImageLeft, cvPoint((int)pPoints2D[i].x, (int)pPoints2D[i].y), 0, cColor);
                cvCircle(m_pIplImageLeft, cvPoint((int)pPoints2D[i].x, (int)pPoints2D[i].y), 1, cColor);
                cvCircle(m_pIplImageLeft, cvPoint((int)pPoints2D[i].x, (int)pPoints2D[i].y), 2, cColor);

                if (bMakeScreenshot)
                {
                    MarkConfirmedPoint(screenshotImage, (int)pPoints2D[i].x, (int)pPoints2D[i].y, cColor);
                }
            }

            delete[] pPoints2D;
        }

        for (int n = nNumHypotheses - 1; n >= 0; n--)
        {
            cColor = colors[aHypotheses[n]->nHypothesisNumber];
            sprintf(pcN, "%d", aHypotheses[n]->nHypothesisNumber);
            Vec2d vCenter2d;
            calibration->WorldToImageCoordinates(aHypotheses[n]->vCenter, vCenter2d, false);

            cvCircle(m_pIplImageLeft, cvPoint((int)vCenter2d.x, (int)vCenter2d.y), 4, cColor);
            cvCircle(m_pIplImageLeft, cvPoint((int)vCenter2d.x, (int)vCenter2d.y), 7, cColor);
            cvPutText(m_pIplImageLeft, pcN, cvPoint((int)vCenter2d.x, (int)vCenter2d.y), &cFont1, cvScalar(0, 0, 255));
            cvPutText(m_pIplImageLeft, pcN, cvPoint((int)vCenter2d.x, (int)vCenter2d.y), &cFont2, cColor);
        }

        delete[] pcN;
    }





#ifndef OLP_USE_ARMAR3_ARMAR3_4

    if (bMakeScreenshot)
    {
        COLPTools::SetNumberInFileName(screenshotFileName, screenshotCounter);
        screenshotImage->SaveToFile(screenshotFileName.c_str());

        if (nNumHypotheses > 0)
        {
            COLPTools::CreateSegmentationProbabilityMap(aHypotheses[0], calibration, segmentationImage);
        }
        else
        {
            ImageProcessor::Zero(segmentationImage);
        }

        COLPTools::SetNumberInFileName(segmentationImageFileName, screenshotCounter);
        segmentationImage->SaveToFile(segmentationImageFileName.c_str());

        for (int i = 0; i < OLP_IMG_WIDTH * OLP_IMG_HEIGHT; i++)
        {
            segmentedCameraImage->pixels[3 * i] = pByteImageColorLeft->pixels[3 * i] * segmentationImage->pixels[i] / 255;
            segmentedCameraImage->pixels[3 * i + 1] = pByteImageColorLeft->pixels[3 * i + 1] * segmentationImage->pixels[i] / 255;
            segmentedCameraImage->pixels[3 * i + 2] = pByteImageColorLeft->pixels[3 * i + 2] * segmentationImage->pixels[i] / 255;
        }

        COLPTools::SetNumberInFileName(segmentedCameraImageFileName, screenshotCounter);
        segmentedCameraImage->SaveToFile(segmentedCameraImageFileName.c_str());

        screenshotCounter++;
    }

#endif



    if (pResultImageLeft)
    {
        for (int j = 0; j < OLP_IMG_HEIGHT; j++)
        {
            for (int i = 0; i < OLP_IMG_WIDTH; i++)
            {
                pResultImageLeft->pixels[3 * (OLP_IMG_WIDTH * j + i) + 0] = ((uchar*)(m_pIplImageLeft->imageData + j * m_pIplImageLeft->widthStep))[i * m_pIplImageLeft->nChannels + 2];
                pResultImageLeft->pixels[3 * (OLP_IMG_WIDTH * j + i) + 1] = ((uchar*)(m_pIplImageLeft->imageData + j * m_pIplImageLeft->widthStep))[i * m_pIplImageLeft->nChannels + 1];
                pResultImageLeft->pixels[3 * (OLP_IMG_WIDTH * j + i) + 2] = ((uchar*)(m_pIplImageLeft->imageData + j * m_pIplImageLeft->widthStep))[i * m_pIplImageLeft->nChannels + 0];
            }
        }
    }

    if (pResultImageRight)
    {
        for (int j = 0; j < OLP_IMG_HEIGHT; j++)
        {
            for (int i = 0; i < OLP_IMG_WIDTH; i++)
            {
                pResultImageRight->pixels[3 * (OLP_IMG_WIDTH * j + i) + 0] = ((uchar*)(m_pOldVisualizationImageIpl->imageData + j * m_pOldVisualizationImageIpl->widthStep))[i * m_pOldVisualizationImageIpl->nChannels + 2];
                pResultImageRight->pixels[3 * (OLP_IMG_WIDTH * j + i) + 1] = ((uchar*)(m_pOldVisualizationImageIpl->imageData + j * m_pOldVisualizationImageIpl->widthStep))[i * m_pOldVisualizationImageIpl->nChannels + 1];
                pResultImageRight->pixels[3 * (OLP_IMG_WIDTH * j + i) + 2] = ((uchar*)(m_pOldVisualizationImageIpl->imageData + j * m_pOldVisualizationImageIpl->widthStep))[i * m_pOldVisualizationImageIpl->nChannels + 0];
            }
        }
    }

#ifdef OLP_SHOW_RESULT_IMAGES

    // Display the image
    if (bConfirmedHypotheses)
    {
        //cvShowImage("Confirmed hypotheses", m_pIplImageLeft);
        //cvShowImage("Hypotheses (left)", m_pOldVisualizationImageIpl);
    }
    else
    {
        //cvShowImage("Hypotheses (left)", m_pIplImageLeft);
    }

    //cvWaitKey(OLP_WAITING_TIME_VISUALISATION);
    //int nKey = cvWaitKey(OLP_WAITING_TIME_VISUALISATION);
    //ARMARX_VERBOSE_S << "Key: %d\n", nKey);
    //if (nKey != -1)
    //{
    //#if defined OLP_HUMAN_PUSHES
    //  ARMARX_VERBOSE_S << "\n\n\n   ---  Waiting for push - press a key when done   ---\n\n\n\n");
    //  cvWaitKey(30000);
    //#endif
    //return false;
    //}
    //else return true;
#endif

    return true;
}

void CHypothesisVisualization::RefreshVisualization(bool bConfirmedHypotheses)
{
#ifdef OLP_SHOW_RESULT_IMAGES

    // Display the image
    if (bConfirmedHypotheses)
    {
        //cvShowImage("Confirmed hypotheses", m_pIplImageLeft);
        //cvShowImage("Hypotheses (left)", m_pOldVisualizationImageIpl);
    }
    else
    {
        //cvShowImage("Hypotheses (left)", m_pIplImageLeft);
    }

    //cvWaitKey(OLP_WAITING_TIME_VISUALISATION);
#endif
}




void CHypothesisVisualization::MarkConfirmedPoint(CByteImage* pImage, int x, int y, CvScalar cColor)
{
    if (x < 2 || x > OLP_IMG_WIDTH - 3 || y < 3 || y > OLP_IMG_HEIGHT - 3)
    {
        return;
    }

    pImage->pixels[(int)(3 * ((y - 2)*pImage->width + x) + 0)] = (char)cColor.val[2];
    pImage->pixels[(int)(3 * ((y - 2)*pImage->width + x) + 1)] = (char)cColor.val[1];
    pImage->pixels[(int)(3 * ((y - 2)*pImage->width + x) + 2)] = (char)cColor.val[0];

    pImage->pixels[(int)(3 * ((y - 1)*pImage->width + x) + 0)] = (char)cColor.val[2];
    pImage->pixels[(int)(3 * ((y - 1)*pImage->width + x) + 1)] = (char)cColor.val[1];
    pImage->pixels[(int)(3 * ((y - 1)*pImage->width + x) + 2)] = (char)cColor.val[0];

    pImage->pixels[(int)(3 * (y * pImage->width + x) + 0)] = (char)cColor.val[2];
    pImage->pixels[(int)(3 * (y * pImage->width + x) + 1)] = (char)cColor.val[1];
    pImage->pixels[(int)(3 * (y * pImage->width + x) + 2)] = (char)cColor.val[0];

    pImage->pixels[(int)(3 * ((y + 1)*pImage->width + x) + 0)] = (char)cColor.val[2];
    pImage->pixels[(int)(3 * ((y + 1)*pImage->width + x) + 1)] = (char)cColor.val[1];
    pImage->pixels[(int)(3 * ((y + 1)*pImage->width + x) + 2)] = (char)cColor.val[0];

    pImage->pixels[(int)(3 * ((y + 2)*pImage->width + x) + 0)] = (char)cColor.val[2];
    pImage->pixels[(int)(3 * ((y + 2)*pImage->width + x) + 1)] = (char)cColor.val[1];
    pImage->pixels[(int)(3 * ((y + 2)*pImage->width + x) + 2)] = (char)cColor.val[0];

    pImage->pixels[(int)(3 * (y * pImage->width + x - 2) + 0)] = (char)cColor.val[2];
    pImage->pixels[(int)(3 * (y * pImage->width + x - 2) + 1)] = (char)cColor.val[1];
    pImage->pixels[(int)(3 * (y * pImage->width + x - 2) + 2)] = (char)cColor.val[0];

    pImage->pixels[(int)(3 * (y * pImage->width + x - 1) + 0)] = (char)cColor.val[2];
    pImage->pixels[(int)(3 * (y * pImage->width + x - 1) + 1)] = (char)cColor.val[1];
    pImage->pixels[(int)(3 * (y * pImage->width + x - 1) + 2)] = (char)cColor.val[0];

    pImage->pixels[(int)(3 * (y * pImage->width + x + 1) + 0)] = (char)cColor.val[2];
    pImage->pixels[(int)(3 * (y * pImage->width + x + 1) + 1)] = (char)cColor.val[1];
    pImage->pixels[(int)(3 * (y * pImage->width + x + 1) + 2)] = (char)cColor.val[0];

    pImage->pixels[(int)(3 * (y * pImage->width + x + 2) + 0)] = (char)cColor.val[2];
    pImage->pixels[(int)(3 * (y * pImage->width + x + 2) + 1)] = (char)cColor.val[1];
    pImage->pixels[(int)(3 * (y * pImage->width + x + 2) + 2)] = (char)cColor.val[0];
}



void CHypothesisVisualization::MarkUnconfirmedPoint(CByteImage* pImage, int x, int y, CvScalar cColor)
{
    if (x < 2 || x > OLP_IMG_WIDTH - 3 || y < 3 || y > OLP_IMG_HEIGHT - 3)
    {
        return;
    }

    pImage->pixels[(int)(3 * (y * pImage->width + x) + 0)] = (char)cColor.val[2];
    pImage->pixels[(int)(3 * (y * pImage->width + x) + 1)] = (char)cColor.val[1];
    pImage->pixels[(int)(3 * (y * pImage->width + x) + 2)] = (char)cColor.val[0];

}

