/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include "ObjectLearningByPushingDefinitions.h"
#include "ObjectHypothesis.h"

// OpenCV
#include <opencv2/opencv.hpp>

//IVT
#include <Image/IplImageAdaptor.h>

class CCalibration;
class CByteImage;


class CHypothesisVisualization
{
public:
    CHypothesisVisualization(CCalibration* calibration);
    ~CHypothesisVisualization(void);

    bool VisualizeHypotheses(const CByteImage* pByteImageColorLeft, const CByteImage* pByteImageColorRight,  const CObjectHypothesisArray& aHypotheses,
                             const CSIFTFeatureArray& aAllPoints, std::vector<CMSERDescriptor3D*>& aAllMSERs, std::vector<CMSERDescriptor3D*>& aCorrespondingMSERs,
                             bool bConfirmedHypotheses, CByteImage* pResultImageLeft = NULL, CByteImage* pResultImageRight = NULL,
                             const bool bMakeScreenshot = false, const bool bIsLeftImage = true);

    void RefreshVisualization(bool bConfirmedHypotheses);

private:

    void MarkConfirmedPoint(CByteImage* pImage, int x, int y, CvScalar cColor);
    void MarkUnconfirmedPoint(CByteImage* pImage, int x, int y, CvScalar cColor);

    CCalibration* calibration;
    IplImage* m_pIplImageLeft, *m_pIplImageRight, *m_pIplImageLeftRectified, *m_pIplImageRightRectified, *m_pIplImageDisparity, *m_pOldVisualizationImageIpl;

    CvScalar* colors;

    CByteImage* screenshotImage, *segmentationImage, *segmentedCameraImage;
    std::string screenshotFileName, segmentationImageFileName, segmentedCameraImageFileName;
    int screenshotCounter;
};
