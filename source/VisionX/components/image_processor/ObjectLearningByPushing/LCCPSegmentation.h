/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include "ObjectLearningByPushingDefinitions.h"

#ifdef OLP_USE_LCCP

#include "ObjectHypothesis.h"

// IVT
#include <Math/Math3d.h>


namespace pcl
{
    class PointXYZRGBA;
}


class LCCPSegmentationWrapper
{
public:
    LCCPSegmentationWrapper(const float voxelResolution, const float seedResolution, const float colorImportance, const float spatialImportance, const float normalImportance,
                            const bool useSingleCamTransform, const float concavityToleranceThreshold, const float smoothnessThreshold, const unsigned int minSegmentSize, const bool useExtendedConvexity,
                            const bool useSanityCriterion, const float maxZ);

    ~LCCPSegmentationWrapper();

    void CreateHypothesesFromLCCPSegments(const std::vector<CHypothesisPoint*>& inputPointCloud, const int maxNumHypotheses, CVec3dArray*& hypothesesFromLCCP, int& numFoundSegments);

    static pcl::PointXYZRGBA ConvertHypothesisPointToPCL(const CHypothesisPoint* point);

private:

    float voxelResolution;
    float seedResolution;
    float colorImportance;
    float spatialImportance;
    float normalImportance;
    bool useSingleCamTransform;
    float concavityToleranceThreshold;
    float smoothnessThreshold;
    unsigned int minSegmentSize;
    bool useExtendedConvexity;
    bool useSanityCriterion;
    float maxZ;
};

#endif // OLP_USE_LCCP

