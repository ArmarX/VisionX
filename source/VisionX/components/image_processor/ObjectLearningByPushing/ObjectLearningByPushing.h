/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <map>
#include <string>
#include <set>


// Core
#include <RobotAPI/libraries/core/LinkedPose.h>
#include <ArmarXCore/observers/Observer.h>
#include <ArmarXCore/observers/variant/ChannelRef.h>

#include <RobotAPI/interface/visualization/DebugDrawerInterface.h>

#include <RobotAPI/libraries/core/remoterobot/RemoteRobot.h>

// MemoryX
#include <MemoryX/interface/observers/ObjectMemoryObserverInterface.h>

// VisionX
#include <VisionX/components/pointcloud_core/PointCloudAndImageProcessor.h>
#include <VisionX/interface/components/ObjectLearningByPushing.h>
#include <VisionX/interface/components/PointCloudAndImageAndCalibrationProviderInterface.h>
#include <VisionX/interface/components/RGBDImageProvider.h>


// IVT
#include <Math/Math3d.h>
#include <Math/Math2d.h>


#include "ObjectLearningByPushingDefinitions.h"
#include "ObjectHypothesis.h"
#include "HypothesisVisualization.h"

#include <mutex>


// forward declarations
class CHypothesisGeneration;
class CStereoCalibration;
class CCalibration;
class CFeatureCalculation;
class CByteImage;
class CGaussBackground;
class CommunicationWithRobotArmarX;
class CDataToRobot;
class CDataFromRobot;



namespace visionx
{

    class ObjectLearningByPushingPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        ObjectLearningByPushingPropertyDefinitions(std::string prefix):
            ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("ImageProviderAdapterName", "Armar3ImageProvider", "Ice Adapter name of the image provider");
            defineOptionalProperty<std::string>("PointCloudProviderAdapterName", "PointCloudProvider", "Ice Adapter name of the point cloud provider");
            defineOptionalProperty<std::string>("RobotStateProxyName", "RobotStateComponent", "Ice Adapter name of the robot state proxy");
            defineOptionalProperty<std::string>("CameraFrameName", "EyeLeftCamera", "Name of the robot state frame of the primary camera");

            defineOptionalProperty<std::string>("DebugDrawerTopicName", "DebugDrawerUpdates", "name of DebugDrawer topic");
        }
    };


    /**
     * ObjectLearningByPushing is the vision part of the approach for interactive object segmentation. It is used by the statechart defined in
     * (how do I reference that here?). Initially, object hypotheses are created based on the stereocamera images. One of these hypotheses is
     * then pushed. The resulting motion is used to segment the one or more objects that moved from the rest of the scene. The segmented objects
     * are represented in the form of RGBD point clouds. (can I cite a publication or link to a website here?)
     *
     * \componentproperties
     * \prop VisionX.RobotHandLocalizationWithFingertips.ImageProviderAdapterName: Name of the
     *       image provider that delivers the camera images.
     * \prop VisionX.RobotHandLocalizationWithFingertips.RobotStateProxyName: Name of the robot state
     *       proxy used to obtain the current robot state.
     * \prop VisionX.RobotHandLocalizationWithFingertips.CameraFrameName: Name of the robot model frame of the primary camera
     *
     */

    class ObjectLearningByPushing:
        virtual public visionx::PointCloudAndImageProcessor,
        virtual public visionx::ObjectLearningByPushingInterface
    {
    public:

        /**
        * @see Component::getDefaultName()
        */
        std::string getDefaultName() const override
        {
            return "ObjectLearningByPushing";
        }


        /**
        * Creates the initial object hypotheses.
        */
        void CreateInitialObjectHypotheses(const ::Ice::Current& c = Ice::emptyCurrent) override;

        /**
        * Validates the initial object hypotheses after the first push.
        */
        void ValidateInitialObjectHypotheses(const ::Ice::Current& c = Ice::emptyCurrent) override;

        /**
        * Re-validates the confirmed object hypotheses after the second and later pushes.
        */
        void RevalidateConfirmedObjectHypotheses(const ::Ice::Current& c = Ice::emptyCurrent) override;

        /**
        * Returns the confirmed points constituting the object hypothesis. If several confirmed objects are available,
        * the one containing the biggest number of confirmed points is returned.
        */
        visionx::types::PointList getObjectHypothesisPoints(const ::Ice::Current& c = Ice::emptyCurrent) override;


        visionx::types::PointList getScenePoints(const ::Ice::Current& c = Ice::emptyCurrent) override;


        armarx::Vector3BasePtr getUpwardsVector(const ::Ice::Current& c = Ice::emptyCurrent) override;


        std::string getReferenceFrameName(const ::Ice::Current& c = Ice::emptyCurrent) override;


        /**
        * Returns the last transformation that the preferred object hypothesis underwent.
        */
        armarx::PoseBasePtr getLastObjectTransformation(const ::Ice::Current& c = Ice::emptyCurrent) override;

        /**
        * Returns the last transformation that the preferred object hypothesis underwent.
        */
        void recognizeObject(const std::string& objectName, const ::Ice::Current& c = Ice::emptyCurrent) override;


        enum OLPControlMode
        {
            eNoHypotheses = 0,
            eCreatingInitialHypotheses = 1,
            eHasInitialHypotheses = 2,
            eValidatingInitialHypotheses = 3,
            eHasConfirmedHypotheses = 4,
            eRevalidatingConfirmedHypotheses = 5,
            eQuit = 42
        };

    protected:
        // inherited from PointCloudAndImageProcessor
        void onInitPointCloudAndImageProcessor() override;
        void onConnectPointCloudAndImageProcessor() override;
        void onExitPointCloudAndImageProcessor() override;

        void process() override;


        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override
        {
            return armarx::PropertyDefinitionsPtr(new ObjectLearningByPushingPropertyDefinitions(getConfigIdentifier()));
        }

    private:

        void CreateInitialObjectHypothesesInternal(CByteImage* pImageGreyLeft, CByteImage* pImageGreyRight, CByteImage* pImageColorLeft, CByteImage* pImageColorRight, int nImageNumber);

        bool ValidateInitialObjectHypothesesInternal(CByteImage* pImageGreyLeft, CByteImage* pImageGreyRight, CByteImage* pImageColorLeft, CByteImage* pImageColorRight, int nImageNumber);

        bool RevalidateConfirmedObjectHypothesesInternal(CByteImage* pImageGreyLeft, CByteImage* pImageGreyRight, CByteImage* pImageColorLeft, CByteImage* pImageColorRight, int nImageNumber);



        bool SaveHistogramOfConfirmedHypothesis(std::string sObjectName, int nDescriptorNumber = 0);
        void RecognizeHypotheses(CByteImage* pImageColorLeft, const std::string objectName = "");


        void VisualizeHypotheses(CByteImage* pImageGreyLeft, CByteImage* pImageGreyRight, CByteImage* pImageColorLeft, CByteImage* pImageColorRight, bool bShowConfirmedHypotheses,
                                 CByteImage* pResultImageLeft = NULL, CByteImage* pResultImageRight = NULL, bool bMakeScreenshot = false);
        void RefreshVisualization(bool bConfirmedHypotheses)
        {
            m_pHypothesisVisualization->RefreshVisualization(bConfirmedHypotheses);
        }


        void UpdateDataFromRobot();

        void ApplyHeadMotionTransformation(Mat3d mRotation, Vec3d vTranslation);

        void SetHeadToPlatformTransformation(Vec3d vTranslation, Mat3d mRotation, bool bResetOldTransformation = false);


        int GetNumberOfNonconfirmedHypotheses()
        {
            return m_pObjectHypotheses->GetSize();
        }


        Vec3d GetObjectPosition(float& fObjectExtent, bool bPreferCentralObject = true);

        void GetHypothesisBoundingBox(int& nMinX, int& nMaxX, int& nMinY, int& nMaxY);

        void GetHypothesisPrincipalAxesAndBoundingBox(Vec3d& vPrincipalAxis1, Vec3d& vPrincipalAxis2, Vec3d& vPrincipalAxis3,
                Vec3d& vEigenValues, Vec3d& vMaxExtentFromCenter,
                Vec2d& vBoundingBoxLU, Vec2d& vBoundingBoxRU, Vec2d& vBoundingBoxLL, Vec2d& vBoundingBoxRL);

        void ReportObjectPositionInformationToObserver();


        void LoadAndFuseObjectSegmentations(std::string sObjectName);


        void SwapAllPointsArraysToOld();


        CObjectHypothesis* SelectPreferredHypothesis(std::vector<CHypothesisPoint*>*& pPoints, const bool bPreferCentralObject = true);


        void convertFileOLPtoPCL(std::string filename, bool includeCandidatePoints = false);

        void BoundingBoxInForegroundImage(CByteImage* image, int minX, int maxX, int minY, int maxY);



        // VisionX framework
        std::string imageProviderName, pointcloudProviderName, robotStateProxyName, cameraFrameName;
        armarx::RobotStateComponentInterfacePrx robotStateProxy;
        ImageProviderInterfacePrx imageProviderProxy;
        // CapturingPointCloudAndImageAndCalibrationProviderInterfacePrx pointcloudProviderProxy;
        RGBDPointCloudProviderInterfacePrx pointcloudProviderProxy;
        bool connected = false;


        ObjectLearningByPushingListenerPrx listener;


        OLPControlMode currentState;

        CFeatureCalculation* m_pFeatureCalculation;
        CHypothesisGeneration* m_pHypothesisGeneration;
        CHypothesisVisualization* m_pHypothesisVisualization;

        CCalibration* calibration;

        bool m_bMakeIntermediateScreenshots;
        std::string m_sScreenshotPath;

        CByteImage* colorImageLeft, *colorImageRight, *greyImageLeft, *greyImageRight, *colorImageLeftOld, *resultImageLeft, *resultImageRight, *tempResultImage;
        CByteImage** cameraImages, ** resultImages;

        pcl::PointCloud<pcl::PointXYZRGBA>::Ptr pointCloudPtr;

        Transformation3d tHeadToPlatformTransformation;

        int iterationCounter;

        CObjectHypothesisArray* m_pObjectHypotheses, *m_pConfirmedHypotheses, *m_pInitialHypothesesAtLocalMaxima;

        CSIFTFeatureArray* m_pAllNewSIFTPoints;
        CSIFTFeatureArray* m_pAllOldSIFTPoints;
        std::vector<CMSERDescriptor3D*>* m_pAllNewMSERs;
        std::vector<CMSERDescriptor3D*>* m_pAllOldMSERs;
        std::vector<CMSERDescriptor3D*>* m_pCorrespondingMSERs;
        std::vector<CHypothesisPoint*>* m_pAllNewDepthMapPoints;
        std::vector<CHypothesisPoint*>* m_pAllOldDepthMapPoints;

        CGaussBackground* m_pGaussBackground;

        Transformation3d m_tHeadToPlatformTransformation, m_tHeadToPlatformTransformationOld;
        Vec3d upwardsVector;

        // debug
        CByteImage* m_pSegmentedBackgroundImage;
        CByteImage* m_pDisparityImage;

        std::mutex processingLock;



        armarx::DebugDrawerInterfacePrx debugDrawer;
        VirtualRobot::RobotPtr localRobot;

    };

}

