/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "ObjectLearningByPushingObserver.h"


#include <ArmarXCore/observers/checks/ConditionCheckEquals.h>
#include <RobotAPI/libraries/core/FramedPose.h>

namespace visionx
{
    void ObjectLearningByPushingObserver::onInitObserver()
    {
        usingTopic("ObjectLearningByPushing");
    }

    void ObjectLearningByPushingObserver::onConnectObserver()
    {
        offerChannel("objectHypotheses", "Gives information about having initial or validated hypotheses.");
        armarx::Variant defaultValue(0);
        offerDataFieldWithDefault("objectHypotheses", "initialHypothesesCreated", defaultValue, "Is 1 when initial object hypotheses are available, 0 if not, -1 if the creation failed.");
        offerDataFieldWithDefault("objectHypotheses", "hypothesesValidated", defaultValue, "Is 1 when object hypotheses were validated, 0 if not, -1 if the validation failed.");

        offerChannel("objectHypothesisPosition", "Gives information about position and extent of the object hypothesis for pushing or grasping.");
        offerDataField("objectHypothesisPosition", "objectPosition", armarx::VariantType::FramedPosition, "Position of the center of the object hypothesis");
        offerDataField("objectHypothesisPosition", "objectExtent", armarx::VariantType::Float, "Estimated maximal extent of the object from its center");
        offerDataField("objectHypothesisPosition", "principalAxis1", armarx::VariantType::Vector3, "First (i.e. longest) principal axis of the object hypothesis point cloud");
        offerDataField("objectHypothesisPosition", "principalAxis2", armarx::VariantType::Vector3, "Second principal axis of the object hypothesis point cloud");
        offerDataField("objectHypothesisPosition", "principalAxis3", armarx::VariantType::Vector3, "Third principal axis of the object hypothesis point cloud");
        offerDataField("objectHypothesisPosition", "eigenValues", armarx::VariantType::Vector3, "Eigenvalues of the object hypothesis point cloud, i.e. variance (or std. dev.?) along the principal axes");

        offerConditionCheck("equals", new armarx::ConditionCheckEquals());
    }



    void ObjectLearningByPushingObserver::reportInitialObjectHypothesesCreated(bool hypothesesCreated, const Ice::Current&)
    {
        if (hypothesesCreated)
        {
            setDataField("objectHypotheses", "initialHypothesesCreated", 1);
            ARMARX_IMPORTANT << "Initial hypotheses created";
        }
        else
        {
            setDataField("objectHypotheses", "initialHypothesesCreated", -1);
            ARMARX_VERBOSE << "No initial object hypotheses available";
        }

        updateChannel("objectHypotheses");
    }




    void ObjectLearningByPushingObserver::reportObjectHypothesesValidated(bool hypothesesValidated, const Ice::Current&)
    {
        if (hypothesesValidated)
        {
            setDataField("objectHypotheses", "hypothesesValidated", 1);
            ARMARX_IMPORTANT << "Hypotheses were validated";
        }
        else
        {
            setDataField("objectHypotheses", "hypothesesValidated", -1);
            ARMARX_VERBOSE << "No confirmed object hypotheses available";
        }

        updateChannel("objectHypotheses");
    }




    void ObjectLearningByPushingObserver::resetHypothesesStatus(const Ice::Current&)
    {
        setDataField("objectHypotheses", "initialHypothesesCreated", 0);
        setDataField("objectHypotheses", "hypothesesValidated", 0);

        updateChannel("objectHypotheses");
    }




    void ObjectLearningByPushingObserver::reportObjectHypothesisPosition(const::armarx::FramedPositionBasePtr& objectPosition, Ice::Float objectExtent,
            const::armarx::Vector3BasePtr& principalAxis1, const::armarx::Vector3BasePtr& principalAxis2,
            const::armarx::Vector3BasePtr& principalAxis3, const::armarx::Vector3BasePtr& eigenValues, const Ice::Current&)
    {
        setDataField("objectHypothesisPosition", "objectPosition", armarx::Variant(objectPosition));
        setDataField("objectHypothesisPosition", "objectExtent", armarx::Variant(objectExtent));
        setDataField("objectHypothesisPosition", "principalAxis1", armarx::Variant(principalAxis1));
        setDataField("objectHypothesisPosition", "principalAxis2", armarx::Variant(principalAxis2));
        setDataField("objectHypothesisPosition", "principalAxis3", armarx::Variant(principalAxis3));
        setDataField("objectHypothesisPosition", "eigenValues", armarx::Variant(eigenValues));

        updateChannel("objectHypothesisPosition");

        ARMARX_VERBOSE << "Object hypothesis position updated";
    }

}
