/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once


#include "ObjectHypothesis.h"

// IVT
#include <Math/Math3d.h>

#include <cstdio>
#include <string>
#include <vector>


class CByteImage;
class CCalibration;
class CRFCSignatureFeatureEntry;


class CObjectRecognition
{
public:
    static void FindPossibleObjectLocations(const CObjectHypothesis* pHypothesis, const CByteImage* pHSVImage, const std::vector<CHypothesisPoint*>& aScenePoints,
                                            const CCalibration* calibration, std::vector<Vec3d>& aPossibleLocations, const int desiredNumberOfLocations = 0);

    static void FindSimilarRegionsWithHueAndSaturationHistogram(const CByteImage* pHSVImage, const std::vector<float>& aHueHistogram,
            const std::vector<float>& aSaturationHistogram, CByteImage* pProbabilityImage);

    static void SaveObjectDescriptorRGBD(const CObjectHypothesis* pHypothesis, const std::string sObjectName, const int nDescriptorNumber = 0);
    static bool LoadObjectDescriptorRGBD(const std::string sObjectName, const int nDescriptorNumber, CObjectHypothesis*& pHypothesis);

    static void SaveObjectDescriptorPCD(const CObjectHypothesis* pHypothesis, const std::string sObjectName, const int nDescriptorNumber = 0);

    static void FindObjectRGBD(const CObjectHypothesis* pHypothesis, const CByteImage* pHSVImage, const std::vector<CHypothesisPoint*>& aScenePoints,
                               const CCalibration* calibration, const Vec3d upwardsVector, Vec3d& vPosition, Mat3d& mOrientation, float& distance,
                               float& fProbability, CByteImage* pResultImage = NULL);

    static void FindAllObjectsRGBD(const CByteImage* pHSVImage, const CByteImage* pRGBImage, const std::vector<CHypothesisPoint*>& aScenePoints,
                                   const CCalibration* calibration, const Vec3d upwardsVector, std::vector<std::string>& aNames, std::vector<Vec3d>& aPositions,
                                   std::vector<Mat3d>& aOrientations, std::vector<float>& aProbabilities);

    static void FindObjectWithManyDescriptorsRGBD(const CByteImage* pHSVImage, const CByteImage* pRGBImage, const std::vector<CHypothesisPoint*>& aScenePoints,
            const CCalibration* calibration, const Vec3d upwardsVector, const std::string objectName, const int numDescriptors);


    static void SaveObjectDescriptorRFCH(const CByteImage* pRGBImage, const CByteImage* pObjectMask, const std::string sObjectName, const int nDescriptorNumber = 0);
    static void LoadObjectDescriptorRFCH(const std::string sObjectName, const int nDescriptorNumber, CRFCSignatureFeatureEntry*& pFeatureDescriptor);

    static void FindObjectRFCH(CRFCSignatureFeatureEntry* pFeatureDescriptor, CByteImage* pRGBImage, const std::vector<CHypothesisPoint*>& aScenePoints,
                               const CCalibration* calibration, Vec3d& vPosition, Mat3d& mOrientation, float& fProbability, CByteImage* pResultImage = NULL);
    static void FindAllObjectsRFCH(const CByteImage* pRGBImage, const CCalibration* calibration, const std::vector<CHypothesisPoint*>& aScenePoints,
                                   std::vector<std::string>& aNames, std::vector<Vec3d>& aPositions, std::vector<Mat3d>& aOrientations, std::vector<float>& aProbabilities);


};

