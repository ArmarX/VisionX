
# Libs required for the tests
SET(LIBS ${LIBS} ArmarXCore OpenCVImageStabilizer)
 
armarx_add_test(OpenCVImageStabilizerTest OpenCVImageStabilizerTest.cpp "${LIBS}")