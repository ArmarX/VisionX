armarx_component_set_name("LegacyRGBDOpenPoseEstimation")

set(COMPONENT_LIBS
    ArmarXCoreInterfaces
    ArmarXCore
    RobotAPICore
    RobotAPIComponentPlugins
    VisionXCore
    VisionXTools
    VisionXInterfaces
    VisionXArViz
    RGBDOpenPoseEstimation
)

set(SOURCES
    ./LegacyRGBDOpenPoseEstimation.cpp
)
set(HEADERS
    ./LegacyRGBDOpenPoseEstimation.h
)

armarx_add_component("${SOURCES}" "${HEADERS}")
armarx_generate_and_add_component_executable(COMPONENT_NAMESPACE armarx)

if (NOT "$ENV{CUDA_10_1_CUDNN_7_6_LD_LIBRARY_PATH}" STREQUAL "")
    target_link_directories(LegacyRGBDOpenPoseEstimationRun PUBLIC "$ENV{CUDA_10_1_CUDNN_7_6_LD_LIBRARY_PATH}")
endif()
