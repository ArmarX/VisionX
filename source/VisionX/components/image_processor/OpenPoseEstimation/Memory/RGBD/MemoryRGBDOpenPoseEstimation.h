/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::RGBDOpenPoseEstimation
 * @author     Fabian Peller <fabian.peller@kit.edu>
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

// ArmarX
#include <VisionX/core/ImageProcessor.h>
#include <VisionX/interface/components/PointCloudAndImageAndCalibrationProviderInterface.h>

namespace visionx
{
    class RGBDOpenPoseEstimation:
        public visionx::ImageProcessor,
        public OpenPose3DDepthImageConverter
    {
    public:
        std::string getDefaultName() const override
        {
            return "RGBDOpenPoseEstimation";
        }

        virtual armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

    protected:
        virtual void onInitImageProcessor() override;
        virtual void onConnectImageProcessor()  override;
        virtual void onDisconnectImageProcessor() override;
        virtual void onExitImageProcessor() override;
        virtual void process() override;

    private:
        std::string providerName = "";

        // ImageBuffer und ImageInformations
        CByteImage** imageBuffer;
        visionx::ImageProviderInfo imageProviderInfo;
        armarx::MetaInfoSizeBasePtr imageMetaInfo;
        unsigned int numImages;

        // ErrorCounters
        std::uint64_t timeoutCounter2d{0};
        std::uint64_t readErrorCounter2d{0};
        std::uint64_t sucessCounter2d{0};
    };
}
