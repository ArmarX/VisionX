/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::RGBDOpenPoseEstimation
 * @author     Fabian Peller <fabian.peller@kit.edu>
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

// ArmarX
#include <RobotAPI/libraries/armem/client/plugins/PluginUser.h>
#include <VisionX/core/ImageProcessor.h>
#include <VisionX/interface/components/PointCloudAndImageAndCalibrationProviderInterface.h>

#include <VisionX/libraries/OpenPose/RGBDOpenPoseEstimation/RGBDOpenPoseEstimationComponent.h>

#include <RobotAPI/libraries/armem/core/MemoryID.h>
#include <RobotAPI/libraries/armem/client/Writer.h>

namespace armarx
{
    class RGBDPoseEstimationWithMemoryWriter :
        public RGBDOpenPoseEstimationComponentPluginUser,
        public visionx::ImageProcessor,
        public armarx::armem::ClientPluginUser
    {
    public:

        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;

        /// Get the component's default name.
        static std::string GetDefaultName();

        virtual armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        void reportEntities() override;

    protected:
        virtual void onInitImageProcessor() override;
        virtual void onConnectImageProcessor()  override;
        virtual void onDisconnectImageProcessor() override;
        virtual void onExitImageProcessor() override;
        virtual void process() override;

    private:

        static const std::string defaultName;

        struct Properties
        {
            std::string providerName = "";
            bool enableMemory = true;
            armarx::armem::MemoryID humanMemory{"Human/Pose"};
        } properties;

        armarx::armem::client::Writer humanMemoryWriter;
    };
}
