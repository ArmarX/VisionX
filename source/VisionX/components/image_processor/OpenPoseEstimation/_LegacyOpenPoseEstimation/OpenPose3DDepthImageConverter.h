/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::OpenPoseEstimation
 * @author     Stefan Reither ( stef dot reither at web dot de )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

// STD/STL

// IVT
#include <ivt/Calibration/Calibration.h>

// ArmarX
#include <VisionX/core/ImageProcessor.h>

// OpenPose stuff
#include "OpenPoseEstimation.h"

namespace visionx
{

    class OpenPose3DDepthImageConverter:
            OpenPoseEstimation
    {
    public:

    protected:
        virtual void setupPropertyDefinitions(armarx::PropertyDefinitionsPtr& def) override;
        virtual void setupLocalVariables() override;
        virtual void destroyLocalVariables() override;
        void calculate3DEntitiesFrom2DEntitiesInBuffer() const;

        CByteImage* VisualizeTransparentImageMask(const CByteImage& maskedInputImage, int brightnessIncrease, const CByteImage& inputImage);

    private:
        int getMedianDepthFromImage(int x, int y, int radius) const;

    protected:
        // Depth image info
        unsigned int radius = 10;
        unsigned int maxDepth = 3000;
        unsigned int maxDepthDifference = 700;

        // Image calibration
        const CCalibration* calibration = nullptr;
        std::string cameraNodeName = "";

        // ImageBuffer und ImageInformations
        CByteImage* maskedrgbImageBuffer;

        CByteImage* depthImageBuffer;
        std::mutex depthImageBufferMutex;
    };
}
