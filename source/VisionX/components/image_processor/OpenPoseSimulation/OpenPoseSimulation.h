/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::OpenPoseEstimation
 * @author     Stefan Reither ( stef dot reither at web dot de )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/services/tasks/RunningTask.h>

#include <RobotAPI/components/RobotState/RobotStateComponent.h>
#include <RobotAPI/libraries/RobotAPIComponentPlugins/ArVizComponentPlugin.h>

#include <VirtualRobot/Robot.h>

#include <VisionX/interface/components/OpenPoseEstimationInterface.h>
#include <SimoxUtility/threads/CountingSemaphore.h>


namespace armarx
{

    /**
     * @class OpenPoseEstimationPropertyDefinitions
     * @brief
     */
    class OpenPoseSimulationPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        OpenPoseSimulationPropertyDefinitions(std::string prefix);
    };


    /**
     * @defgroup Component-OpenPoseEstimation OpenPoseEstimation
     * @ingroup VisionX-Components
     * A description of the component OpenPoseEstimation.
     *
     * @class OpenPoseEstimation
     * @ingroup Component-OpenPoseEstimation
     * @brief Brief description of class OpenPoseEstimation.
     *
     * Detailed description of class OpenPoseEstimation.
     */
    class OpenPoseSimulation :
        public armarx::Component,
        public OpenPoseEstimationInterface,
        virtual public armarx::ArVizComponentPluginUser
    {
    public:

        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override
        {
            return "OpenPoseSimulation";
        }

        void start(const Ice::Current& = Ice::emptyCurrent)  override;
        void stop(const Ice::Current& = Ice::emptyCurrent)  override;
        void start3DPoseEstimation(const Ice::Current& = Ice::emptyCurrent)  override;
        void stop3DPoseEstimation(const Ice::Current& = Ice::emptyCurrent)  override;

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

    protected:
        /**
         * @see armarx::ManagedIceObject::onInitComponent()
         */
        void onInitComponent() override;

        /**
         * @see armarx::ManagedIceObject::onConnectComponent()
         */
        void onConnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onDisconnectComponent()
         */
        void onDisconnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onExitComponent()
         */
        void onExitComponent() override;


        //
        void reportImageAvailable(const ::std::string&, const ::Ice::Current& = ::Ice::emptyCurrent) override {};

    protected:
        //Texting interface
        void onMessage(const Texting::TextMessage& text, const Ice::Current& = Ice::emptyCurrent) override;

    private:
        RunningTask<OpenPoseSimulation>::pointer_type runTask;
        std::atomic<bool> running2D;
        std::atomic<bool> running3D;
        void run();

        // Topics
        OpenPose2DListenerPrx listener2DPrx;
        OpenPose3DListenerPrx listener3DPrx;

        // Robot
        RobotStateComponentInterfacePrx robotStateInterface;
        VirtualRobot::RobotPtr localRobot;

        std::string layerName = "OpenPoseSimulation";
    };
}
