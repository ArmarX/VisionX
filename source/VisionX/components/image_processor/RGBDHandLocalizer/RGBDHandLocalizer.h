/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::RGBDHandLocalizer
 * @author     Markus Grotz ( markus dot grotz at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/observers/variant/TimestampVariant.h>

#include <RobotAPI/libraries/core/FramedPose.h>
#include <RobotAPI/components/RobotState/RobotStateComponent.h>

#include <MemoryX/interface/workingmemory/WorkingMemoryUpdaterBase.h>
#include <MemoryX/core/entity/ProbabilityMeasures.h>
#include <MemoryX/interface/observers/ObjectMemoryObserverInterface.h>

#include <VisionX/core/ImageProcessor.h>
#include <VisionX/components/pointcloud_core/PointCloudAndImageProcessor.h>
#include <VisionX/interface/components/ObjectLocalizerInterfaces.h>
#include <VisionX/tools/TypeMapping.h>
#include <VisionX/tools/ImageUtil.h>
#include <VisionX/components/pointcloud_core/PCLUtilities.h>

#include <Eigen/Geometry>

#include <RobotAPI/interface/visualization/DebugDrawerInterface.h>
#include <RobotAPI/libraries/core/remoterobot/RemoteRobot.h>

#include <opencv2/opencv.hpp>
#include <Image/IplImageAdaptor.h>
#include <Image/ImageProcessor.h>
#include <pcl/filters/crop_box.h>
#include "bloblabeler.h"
#include <math.h>

namespace visionx
{
    /**
     * @class RGBDHandLocalizerPropertyDefinitions
     * @brief
     */
    class RGBDHandLocalizerPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        RGBDHandLocalizerPropertyDefinitions(std::string prefix):
            armarx::ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("providerName", "OpenNIPointCloudProvider", "");
            defineOptionalProperty<std::string>("RobotStateComponentProxyName", "RobotStateComponent", "");
            defineOptionalProperty<std::string>("HandNameInRobotModel", "TCP R", "");
            defineOptionalProperty<std::string>("HandNameInMemory", "handright3aRGBD", "");
            defineOptionalProperty<std::string>("SensorFrameName", "DepthCamera", "");
            defineOptionalProperty<std::string>("RightHandNameViewSelection", "handright3a", "Name of the normal hand");
            defineOptionalProperty<std::string>("MarkerName", "Marker R", "");
            defineOptionalProperty<std::string>("DebugDrawerTopicName", "DebugDrawerUpdates", "Name of the debug drawer topic that should be used");

            defineOptionalProperty<bool>("PrimitiveCalibrationOnStart", true, "If true the component calibrates itself in a primitive way.");
            defineOptionalProperty<float>("MarkerRadiusMM", 10, "");

            defineOptionalProperty<float>("uncertaintyMM", 100, "");
            //Green
            defineOptionalProperty<int>("hueMin", 48, "");
            defineOptionalProperty<int>("hueMax", 90, "");

            defineOptionalProperty<int>("satMin", 50, "");
            defineOptionalProperty<int>("satMax", 255, "");

            defineOptionalProperty<int>("valMin", 105, "");
            defineOptionalProperty<int>("valMax", 255, "");
        }
    };

    /**
     * @defgroup Component-RGBDHandLocalizer RGBDHandLocalizer
     * @ingroup VisionX-Components
     * A description of the component RGBDHandLocalizer.
     *
     * @class RGBDHandLocalizer
     * @ingroup Component-RGBDHandLocalizer
     * @brief Brief description of class RGBDHandLocalizer.
     *
     * Detailed description of class RGBDHandLocalizer.
     */
    class RGBDHandLocalizer :

        virtual public PointCloudAndImageProcessor,
        virtual public ObjectLocalizerPointCloudAndImageInterface
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override
        {
            return "RGBDHandLocalizer";
        }

        memoryx::ObjectLocalizationResultList localizeObjectClasses(const memoryx::ObjectClassNameList&, const Ice::Current&) override;

    protected:

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        void process() override;

        void onInitPointCloudAndImageProcessor() override;
        void onConnectPointCloudAndImageProcessor() override;
        void onDisconnectPointCloudAndImageProcessor() override;
        void onExitPointCloudAndImageProcessor() override;

    private:

        CByteImage** images;
        int numImages;
        std::string providerName;
        armarx::RobotStateComponentInterfacePrx robotStatePrx = nullptr;
        armarx::FramedPosition getFramedPosition(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr, int x, int y);
        armarx::TimestampVariantPtr timestamp;
        armarx::FramedPosePtr realHandPose = nullptr;
        CByteImage** result;

        int calibrationCounter = 0;
        Eigen::Vector3f calibrationSum = Eigen::Vector3f::Zero();

        std::mutex positionLock;
        armarx::DebugDrawerInterfacePrx debugDrawerTopic;
        pcl::CropBox<pcl::PointXYZRGBA> cropper;

        std::string sensorFrameName;
        std::string markerFrameName;
        std::string handFrameName;

        // PointCloudAndImageProcessor interface
        void calcPositions(cv::Mat& binaryImage, pcl::PointCloud<pcl::PointXYZRGBA>::Ptr pointcloud, std::vector<Eigen::Vector3f>& medianCoordinates);
        void cropFilter(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr input, pcl::PointCloud<pcl::PointXYZRGBA>::Ptr output, float uncertaintyMM, Eigen::Vector3f guessMM);

    };
}

