/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "bloblabeler.h"
#include <stdio.h>
BlobLabeler::BlobLabeler()
{


}

void BlobLabeler::labelBlobs(cv::Mat& binaryImage, cv::Mat& outputImage, std::vector<std::vector<cv::Point> >& blobList)
{

    blobList.clear();
    int width = binaryImage.cols;
    int heigth = binaryImage.rows;
    int blobIndex = 1;


    binaryImage.convertTo(outputImage, CV_16UC1);


    for (uint16_t& pixel : cv::Mat_<uint16_t>(outputImage))
    {
        if (pixel > 0)
        {
            pixel = 65535;
        }
    }





    for (int y = 0; y < heigth; y++)
    {
        for (int x = 0; x < width; x++)
        {
            cv::Point currentPixel(x, y);

            bool isWhite = (outputImage.at<uint16_t>(currentPixel) == 65535);
            std::vector<cv::Point> pointlist;
            if (isWhite)
            {
                this->markBlob(outputImage, currentPixel, blobIndex, pointlist);
                blobList.push_back(pointlist);
                blobIndex++;
                if (blobIndex >= 65535)
                {
                    return;
                }
            }

        }
    }

}

void BlobLabeler::markBlob(cv::Mat& image, cv::Point currentPixel, uint16_t color, std::vector<cv::Point>& pointList)
{

    int x = currentPixel.x;
    int y = currentPixel.y;
    int width = image.cols;
    int heigth = image.rows;
    image.at<uint16_t>(currentPixel) = color;

    for (int j = y - 1; j <= y + 1; j++)
    {
        for (int i = x - 1; i <= x + 1; i++)
        {


            bool validPoint = i >= 0 && j >= 0 && i <= width && j <= heigth;

            if (validPoint)
            {
                cv::Point neighbor(i, j);
                uint16_t* neighborValue = &image.at<uint16_t>(neighbor);
                bool isWhite = *neighborValue == 65535;
                if (isWhite)
                {
                    cv::Point neighbor(i, j);
                    image.at<uint16_t>(neighbor) = color;
                    //std::cout << i << " " << j << " " << image.at<uint16_t>(neighbor) <<"\n";
                    pointList.push_back(neighbor);
                    markBlob(image, neighbor, color, pointList);
                }

            }

        }
    }
}
