armarx_component_set_name("SimpleEpisodicMemoryImageConnector")

find_package(Simox ${ArmarX_Simox_VERSION} QUIET)
armarx_build_if(Simox_FOUND "Simox ${ArmarX_Simox_VERSION} or later not available")
if (Simox_FOUND)
    include_directories(${Simox_INCLUDE_DIRS})
endif()

find_package(Eigen3 3.2.0 QUIET)
armarx_build_if(Eigen3_FOUND "Eigen 3.2.0 or later not available")
if (Eigen3_FOUND)
    include_directories(${Eigen3_INCLUDE_DIRS})
endif()

find_package(IVT COMPONENTS ivt ivtopencv QUIET)
armarx_build_if(IVT_FOUND "IVT not available")
if(IVT_FOUND)
    include_directories(${IVT_INCLUDE_DIRS})
endif()

find_package(OpenCV QUIET)
armarx_build_if(OpenCV_FOUND "OpenCV not available")
if(OpenCV_FOUND)
    include_directories(${OpenCV_INCLUDE_DIRS})
endif()

set(COMPONENT_LIBS
    ArmarXCoreInterfaces
    ArmarXCore
    MemoryXCore
    MemoryXInterfaces
    VisionXCore
    VisionXTools
    VisionXInterfaces
    ivt ivtvideocapture ivtopencv
    opencv_core
    opencv_imgcodecs
    opencv_imgproc
    ${Simox_LIBS}
    )

set(SOURCES SimpleEpisodicMemoryImageConnector.cpp)
set(HEADERS SimpleEpisodicMemoryImageConnector.h)

armarx_add_component("${SOURCES}" "${HEADERS}")

armarx_generate_and_add_component_executable(COMPONENT_NAMESPACE visionx APPLICATION_APP_SUFFIX)
