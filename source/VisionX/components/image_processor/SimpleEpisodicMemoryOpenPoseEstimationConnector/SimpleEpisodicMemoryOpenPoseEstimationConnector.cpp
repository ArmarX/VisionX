/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::SimpleEpisodicMemoryImageConnector
 * @author     Fabian Peller ( fabian dot peller-konrad at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "SimpleEpisodicMemoryOpenPoseEstimationConnector.h"


// STD/STL

// Simox
#include <VirtualRobot/Nodes/RobotNode.h>
#include <VirtualRobot/XML/RobotIO.h>

// IVT

// ArmarX
#include <RobotAPI/libraries/core/FramedPose.h>

using namespace visionx;

armarx::PropertyDefinitionsPtr
SimpleEpisodicMemoryOpenPoseEstimationConnector::createPropertyDefinitions()
{
    armarx::PropertyDefinitionsPtr def{new armarx::ComponentPropertyDefinitions{getConfigIdentifier()}};

    def->topic<armarx::OpenPose3DListener>("OpenPoseEstimation3D");
    return def;
}

void SimpleEpisodicMemoryOpenPoseEstimationConnector::onInitComponent()
{
    usingProxy("SimpleEpisodicMemory");
}

void SimpleEpisodicMemoryOpenPoseEstimationConnector::onConnectComponent()
{
    getProxy(m_simple_episodic_memory, m_simple_episodic_memory_proxy_name);
}

void SimpleEpisodicMemoryOpenPoseEstimationConnector::report3DKeypoints(const armarx::Keypoint3DMapList& kpml, long timestamp, const Ice::Current&)
{
    if (kpml.empty())
    {
        return;
    }
    IceUtil::Time time = IceUtil::Time::microSeconds(timestamp);

    for (unsigned int i = 0; i < kpml.size(); ++i)
    {
        memoryx::Body25HumanPoseEvent human_pose_event;
        human_pose_event.receivedInMs = time.toMilliSecondsDouble();

        for (const auto& [key, value] : kpml[i])
        {
            memoryx::HumanKeypoint kp;
            if (key == "Background")
            {
                continue;
            }

            kp.label = value.label;
            kp.x = value.x;
            kp.y = value.y;
            kp.z = value.z;
            kp.globalX = value.globalX;
            kp.globalY = value.globalY;
            kp.globalZ = value.globalZ;
            kp.confidence = value.confidence;
            human_pose_event.keypoints[key] = kp;
        }
        m_simple_episodic_memory->registerHumanPoseEvent(human_pose_event);
    }

}



