/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <string>

#include <Image/ByteImage.h>

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/services/tasks/RunningTask.h>
#include <ArmarXCore/core/services/tasks/PeriodicTask.h>

#include <VisionX/core/ImageProcessor.h>
#include <VisionX/interface/components/StreamProvider.h>

extern "C" {
#include <x264.h>
#include <libswscale/swscale.h>
}



#define NUMBER_OF_COMPRESSIONRATES 3 //should hold the number of compression rates (the compression rates  are defined in the enum ::Stream::CompressionRate)


namespace Stream
{
    /**
     * This class realizes the StreamProviderImageProcessorInterface-SliceInterface and is a subclass of visionx::ImageProcessor.
     * On tablet-side the methods of this class can be called through an Ice-Proxy.
     */
    class StreamProviderI :
        public StreamProviderImageProcessorInterface,
        public visionx::ImageProcessor
    {

    public:
        class StreamProviderPropertyDefinitions :
            public visionx::ImageProcessorPropertyDefinitions
        {
        public:
            StreamProviderPropertyDefinitions(std::string prefix);
        };


        /// @see PropertyUser::createPropertyDefinitions()
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override
        {
            return armarx::PropertyDefinitionsPtr(
                       new StreamProviderPropertyDefinitions(
                           getConfigIdentifier()));
        }
        StreamProviderI();

        void stopCapture(const ::Ice::Current& = Ice::emptyCurrent) override;
        /** starts the capture for the given source */
        bool startCapture(const ::Ice::Current& = Ice::emptyCurrent) override;

        void getImageInformation(int& imageWidth, int& imageHeight, int& imageType,  const Ice::Current& c = Ice::emptyCurrent) override;
        int getNumberOfImages(const ::Ice::Current& = Ice::emptyCurrent) override;
        void setCompressionRate(::Stream::CompressionRate = COMPRESSIONHIGH, const ::Ice::Current& = Ice::emptyCurrent) override;
        CodecType getCodecType(const Ice::Current&) override;


        /* Inherited from ImageProcessor. */
        void onInitImageProcessor() override;
        void onConnectImageProcessor() override;
        virtual void onDisConnectImageProcessor();
        void onExitImageProcessor() override;
        void process() override;

        /// Executes a single cycle of the encoding process.
        void step(const Ice::Current& = Ice::emptyCurrent);


        std::string getDefaultName() const override;

        void onStartRecording(const ::Ice::Current& = Ice::emptyCurrent) override;



    private:

        // Pushes the given image into the given appsrc, helper method for pushNewImages().
        void calculateFps();  // Calcuates the current fps for the streamSources (as (80% old:20% new) weighted medium).
        armarx::PeriodicTask<StreamProviderI>::pointer_type fpsCalculator; // Task calcualting the fps.


        bool capturing = false;
        std::string imageProviderProxyName;
        visionx::ImageProviderInterfacePrx imageProviderProxy;
        visionx::ImageFormatInfo imageFormat;
        int numberImages;
        int imgWidth;
        int imgHeight;
        int imgType;
        int encodedImgHeight;

        CByteImage** ppInputImages, * pImageForEncoder;
        using CByteImagePtr = std::shared_ptr<CByteImage>;
        using ImageContainer = std::vector<CByteImagePtr>;

        x264_param_t param;
        float fps;
        x264_t* encoder;
        x264_picture_t pic_in, pic_out;
        SwsContext* convertCtx;
        int frameCounter = 0;
        Stream::StreamListenerInterfacePrx listener;
    };
    using StreamProviderIPtr = IceInternal::Handle<StreamProviderI>;
}
