/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::ValveAttention
 * @author     Markus Grotz ( markus dot grotz at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once



#include "hog.h"
#include "choughcirclesdemo.h"
#include <ArmarXCore/core/Component.h>


#include <RobotAPI/interface/core/RobotState.h>
#include <RobotAPI/interface/components/ViewSelectionInterface.h>

#include <RobotAPI/libraries/core/FramedPose.h>

#include <RobotAPI/components/EarlyVisionGraph/GraphPyramidLookupTable.h>
#include <RobotAPI/components/EarlyVisionGraph/IntensityGraph.h>
#include <RobotAPI/components/EarlyVisionGraph/MathTools.h>


#include <VisionX/core/ImageProcessor.h>
#include <VisionX/components/pointcloud_core/PointCloudAndImageProcessor.h>

#include <VisionX/tools/TypeMapping.h>
#include <VisionX/tools/ImageUtil.h>


#include <opencv2/opencv.hpp>

#include <Image/IplImageAdaptor.h>

#include <mutex>

namespace armarx
{
    /**
     * @class ValveAttentionPropertyDefinitions
     * @brief
     */
    class ValveAttentionPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        ValveAttentionPropertyDefinitions(std::string prefix):
            armarx::ComponentPropertyDefinitions(prefix)
        {
            //defineRequiredProperty<std::string>("PropertyName", "Description");
            //defineOptionalProperty<std::string>("PropertyName", "DefaultValue", "Description");
            defineOptionalProperty<std::string>("RobotStateComponentName", "RobotStateComponent", "Name of the robot state component that should be used");
            defineOptionalProperty<std::string>("ViewSelectionName", "ViewSelection", "Name of the ViewSelection component that should be used");
            defineOptionalProperty<std::string>("HeadFrameName", "Head Base", "Name of the frame of the head base in the robot model");
            defineOptionalProperty<std::string>("providerName", "ImageProvider", "Name of the image provider that should be used");

            defineOptionalProperty<std::string>("TrainingData", "/common/homes/students/zhu/home/", "Path to the training data set");
        }
    };

    /**
     * @defgroup Component-ValveAttention ValveAttention
     * @ingroup VisionX-Components
     * A description of the component ValveAttention.
     *
     * @class ValveAttention
     * @ingroup Component-ValveAttention
     * @brief Brief description of class ValveAttention.
     *
     * Detailed description of class ValveAttention.
     */
    class ValveAttention :
        virtual public visionx::PointCloudAndImageProcessor
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override
        {
            return "ValveAttention";
        }

    protected:


        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        void process() override;

        void onInitPointCloudAndImageProcessor() override;
        void onConnectPointCloudAndImageProcessor() override;
        void onDisconnectPointCloudAndImageProcessor() override;
        void onExitPointCloudAndImageProcessor() override;

    private:

        void addSaliencyRecursive(const int currentNodeIndex, std::vector<bool>& visitedNodes, const float saliency, const TSphereCoord objectSphereCoord, const float maxDistanceOnArc);


        std::mutex mutex;

        RobotStateComponentInterfacePrx robotStateComponent;
        ViewSelectionInterfacePrx viewSelection;

        VirtualRobot::RobotPtr robot;


        std::string headFrameName;
        CIntensityGraph* saliencyEgosphereGraph;
        CGraphPyramidLookupTable* graphLookupTable;

        std::string providerName;
        int numImages, numResultImages;
        CByteImage** images;
        CByteImage** result;
        HoG* hog;
    };
}

