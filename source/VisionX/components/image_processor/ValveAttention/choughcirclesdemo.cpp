#include "choughcirclesdemo.h"

CHoughCircles::CHoughCircles(int CannyLowThreshold, int CannyHighThreshold, int CirclesToExtract, int minRadius, int maxRadius)
{
    m_nCannyLowThreshold = CannyLowThreshold;
    m_nCannyHighThreshold = CannyHighThreshold;
    m_nCirclesToExtract = CirclesToExtract;
    m_nMinRadius = minRadius;
    m_nMaxRadius = maxRadius;
}

//origin image should be gray and then blurred by gauss filter
void CHoughCircles::HoughSaliency(CByteImage* origin, CByteImage* saliencyImage, int sampleWindowsize, int width, int height, int windowCenterX, int windowCenterY)
{
    CVec3dArray resultListCircles(50);
    CDynamicArrayTemplate<int> resultHits(50);
    CVec2dArray edgePoints(10000), edgeDirections(10000);
    CByteImage visualizationImage(sampleWindowsize, sampleWindowsize, CByteImage::eRGB24);

    // detect edges with Canny edge detector
    ImageProcessor::Canny(origin, edgePoints, edgeDirections, m_nCannyLowThreshold, m_nCannyHighThreshold);

    // detect circles with Hough transform
    ImageProcessor::HoughTransformCircles(edgePoints, edgeDirections, sampleWindowsize, sampleWindowsize, m_nMinRadius, m_nMaxRadius, m_nCirclesToExtract, 1, resultListCircles, resultHits, &visualizationImage);

    Vec3d circle = resultListCircles[0];

    for (int i = 0; i < resultListCircles.GetSize(); i++)
    {
        circle = resultListCircles[i];
        //std::cout << "the center of the circle is:" << circle.x << " " << circle.y << std::endl;
        circle.x += windowCenterX - sampleWindowsize / 2;
        circle.y += windowCenterY - sampleWindowsize / 2;
        if (circle.x >= 0 && circle.y >= 0 && circle.x < width && circle.y < height)
        {
            saliencyImage->pixels[(int)circle.y * width + (int)circle.x]++;
        }
    }

}

void CHoughCircles::openCVHoughSaliency(CByteImage* origin, CByteImage* saliencyImage, int sampleWindowsize, int width, int height, int windowCenterX, int windowCenterY)
{
    cv::Mat src(origin->height, origin->width, CV_8UC1);
    std::vector<cv::Vec3f> resultListCircles;
    std::string path;

    for (int i = 0; i < src.rows; i++)
    {
        for (int j = 0; j < src.cols; j++)
        {
            src.at<uint8_t>(i, j) = origin->pixels[i * src.cols + j];

        }
    }

    cv::HoughCircles(src, resultListCircles, cv::HOUGH_GRADIENT, 1, src.rows / 8, m_nCannyHighThreshold, 20, m_nMinRadius, m_nMaxRadius);
    if (!resultListCircles.size())
    {
        return;
    }
    cv::Vec3d circle = resultListCircles[0];
    for (size_t i = 0; i < resultListCircles.size(); i++)
    {
        circle = resultListCircles[i];
        circle[0] += windowCenterX - sampleWindowsize / 2;
        circle[1] += windowCenterY - sampleWindowsize / 2;
        if (circle[0] > 0 && circle[1] > 0 && circle[0] < width - 1 && circle[1] < height - 1)
        {
            for (int j = -1; j <= 1; j++)
            {
                for (int k = -1; k <= 1; k++)
                {
                    saliencyImage->pixels[((int)circle[1] + j) * width + ((int)circle[0] + k)] = 255;
                }
            }
        }
    }
}

