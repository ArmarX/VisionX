/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "VisualContactDetectionObserver.h"

#include <ArmarXCore/observers/checks/ConditionCheckUpdated.h>
#include <ArmarXCore/observers/checks/ConditionCheckEquals.h>

namespace visionx
{
    void VisualContactDetectionObserver::onInitObserver()
    {
        usingTopic("VisualContactDetection");


        offerConditionCheck("update", new armarx::ConditionCheckUpdated());
        offerConditionCheck("equals", new armarx::ConditionCheckEquals());
    }



    void VisualContactDetectionObserver::onConnectObserver()
    {
        offerChannel("collisionDetector", "Indicates whether the robot hand is colliding with an object.");
        armarx::Variant defaultValue(0);
        offerDataFieldWithDefault("collisionDetector", "collisionDetected", defaultValue, "Is 1 when a collision between hand and object was detected, 0 if not.");

    }



    void VisualContactDetectionObserver::reportContactDetected(bool collisionDetected, const Ice::Current&)
    {
        if (collisionDetected)
        {
            setDataField("collisionDetector", "collisionDetected", 1);
            ARMARX_IMPORTANT << "Collision detected!";
        }
        else
        {
            setDataField("collisionDetector", "collisionDetected", 0);
        }

        updateChannel("collisionDetector");
    }
}
