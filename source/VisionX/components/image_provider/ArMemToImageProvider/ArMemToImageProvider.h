/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::ArMemToImageProvider
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <mutex>

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/interface/observers/ObserverInterface.h>

#include <RobotAPI/libraries/armem/client/plugins.h>

#include <VisionX/core/ImageProcessor.h>
#include <VisionX/libraries/armem_images_core/ImagesProperties.h>
#include <VisionX/libraries/armem_images_core/ArMemToImage.h>


namespace visionx
{

    /**
     * @defgroup Component-ArMemToImageProvider ArMemToImageProvider
     * @ingroup VisionX-Components
     * A description of the component ArMemToImageProvider.
     *
     * @class ArMemToImageProvider
     * @ingroup Component-ArMemToImageProvider
     * @brief Brief description of class ArMemToImageProvider.
     *
     * Detailed description of class ArMemToImageProvider.
     */
    class ArMemToImageProvider :
        virtual public armarx::Component
        , virtual public armarx::armem::ListeningClientPluginUser
    {
    public:

        std::string getDefaultName() const override;

    protected:

        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        void onInitComponent() override;
        void onConnectComponent() override;
        void onDisconnectComponent() override;
        void onExitComponent() override;


    private:

        void enableResultImages(size_t numberImages, ImageDimension imageDimension, ImageType imageType, const std::string& name);
        void fetchUpdates(const armarx::armem::MemoryID& entityID, const std::vector<armarx::armem::MemoryID>& updatedSnapshotIDs);


    private:

        visionx::armem_images::ArMemToImage armemToImage;
        std::mutex armemToImageMutex;

        armarx::DebugObserverInterfacePrx debugObserver;
        IceInternal::Handle<ResultImageProvider> resultImageProvider;

        struct Properties
        {
            visionx::armem_images::ImagesProperties images;

            std::string providerSegmentName = "ProviderSegmentName";
            std::string resultImagesName = "(auto)";
            Eigen::Vector2i fallbackDims { 640, 480 };
        };
        Properties p;

    };
}
