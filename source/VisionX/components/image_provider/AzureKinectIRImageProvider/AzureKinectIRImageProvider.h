/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::AzureKinectIRImageProvider
 * @author     Christoph Pohl <christoph.pohl@kit.edu>
 * @author     Christian R. G. Dreher <c.dreher@kit.edu>
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once

#include <chrono>
#include <condition_variable>
#include <mutex>

#include <Eigen/Core>

#include <opencv2/opencv.hpp>

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/services/tasks/RunningTask.h>
#include <ArmarXCore/core/time/TimeUtil.h>
#include <ArmarXCore/interface/observers/ObserverInterface.h>

#include <RobotAPI/interface/visualization/DebugDrawerInterface.h>

#include <VisionX/components/pointcloud_core/CapturingPointCloudProvider.h>
#include <VisionX/components/pointcloud_provider/ImageToPointCloud/DepthImageUtils.h>
#include <VisionX/interface/components/RGBDImageProvider.h>
#include <VisionX/core/CapturingImageProvider.h>

#include <k4a/k4a.h>
#include <k4a/k4a.hpp>


namespace visionx
{
    /// @class AzureKinectIRImageProviderPropertyDefinitions
    class AzureKinectIRImageProviderPropertyDefinitions:
        public visionx::CapturingPointCloudProviderPropertyDefinitions
    {
    public:
        AzureKinectIRImageProviderPropertyDefinitions(std::string prefix);
    };


    /**
     * @defgroup Component-AzureKinectIRImageProvider AzureKinectIRImageProvider
     * @ingroup VisionX-Components
     * Provides support for Intel RealSense cameras for ArmarX.
     *
     * @class AzureKinectIRImageProvider
     * @ingroup Component-AzureKinectIRImageProvider
     * @brief Brief description of class AzureKinectIRImageProvider.
     */
    class AzureKinectIRImageProvider :
        virtual public visionx::StereoCalibrationCaptureProviderInterface,
        virtual public visionx::CapturingImageProvider
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        virtual std::string getDefaultName() const override;

        std::string getReferenceFrame(const Ice::Current& current) override;

        StereoCalibration getStereoCalibration(const Ice::Current& current) override;

        bool getImagesAreUndistorted(const Ice::Current& current) override;

    protected:

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        virtual armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        // ManagedIceObject interface

    protected:
        bool hasSharedMemorySupport(const Ice::Current& c = Ice::emptyCurrent) override
        {
            return true;
        }

        void onInitCapturingImageProvider() override;

        void onExitCapturingImageProvider() override;

        void onStartCapture(float frames_per_second) override;

        void onStopCapture() override;

        bool capture(void** pp_image_buffers) override;

        // Gets the dimensions of the color images that the color camera will produce for a
        // given color resolution.
        static inline std::pair<int, int> GetColorDimensions(const k4a_color_resolution_t resolution)
        {
            switch (resolution)
            {
                case K4A_COLOR_RESOLUTION_720P:
                    return { 1280, 720 };
                case K4A_COLOR_RESOLUTION_2160P:
                    return { 3840, 2160 };
                case K4A_COLOR_RESOLUTION_1440P:
                    return { 2560, 1440 };
                case K4A_COLOR_RESOLUTION_1080P:
                    return { 1920, 1080 };
                case K4A_COLOR_RESOLUTION_3072P:
                    return { 4096, 3072 };
                case K4A_COLOR_RESOLUTION_1536P:
                    return { 2048, 1536 };

                default:
                    throw std::logic_error("Invalid color dimensions value!");
            }
        }

        // Gets the dimensions of the depth images that the depth camera will produce for a
        // given depth mode.
        static inline std::pair<int, int> GetDepthDimensions(const k4a_depth_mode_t depth_mode)
        {
            switch (depth_mode)
            {
                case K4A_DEPTH_MODE_NFOV_2X2BINNED:
                    return { 320, 288 };
                case K4A_DEPTH_MODE_NFOV_UNBINNED:
                    return { 640, 576 };
                case K4A_DEPTH_MODE_WFOV_2X2BINNED:
                    return { 512, 512 };
                case K4A_DEPTH_MODE_WFOV_UNBINNED:
                    return { 1024, 1024 };
                case K4A_DEPTH_MODE_PASSIVE_IR:
                    return { 1024, 1024 };

                default:
                    throw std::logic_error("Invalid depth dimensions value!");
            }
        }

        static std::string VersionToString(const k4a_version_t& version)
        {
            std::stringstream s;
            s << version.major << "." << version.minor << "." << version.iteration;
            return s.str();
        }

    private:
        visionx::CByteImageUPtr resultIRImage;

        k4a::device device;
        k4a_device_configuration_t config;
        k4a::calibration k4aCalibration;
        k4a::transformation transformation;
        std::pair<int, int> depthDim;

    };
}
