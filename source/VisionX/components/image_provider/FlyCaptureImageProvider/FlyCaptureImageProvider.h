/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::FlyCaptureImageProvider
 * @author     Markus Grotz ( markus dot grotz at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <ArmarXCore/core/Component.h>

#include <VisionX/core/CapturingImageProvider.h>

#include <flycapture/FlyCapture2.h>

#include <VisionX/interface/components/Calibration.h>

#include <Image/ImageProcessor.h>

#include <Calibration/Undistortion.h>
#include <Calibration/Rectification.h>


namespace armarx
{

    /**
     * @class FlyCaptureImageProviderPropertyDefinitions
     * @brief
     */
    class FlyCaptureImageProviderPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        FlyCaptureImageProviderPropertyDefinitions(std::string prefix):
            armarx::ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("serialNumbers", "10121969 10121966 10121963 10121968", "camera serial numbers");

            defineRequiredProperty<std::string>("CalibrationFile", "Camera calibration file");

            defineOptionalProperty<visionx::ImageDimension>("dimensions", visionx::ImageDimension(1600, 1200), "")
            .map("640x480", visionx::ImageDimension(640, 480))
            .map("1600x1200", visionx::ImageDimension(1600, 1200));

            defineOptionalProperty<float>("FrameRate", 7.5f, "Frames per second")
            .setMatchRegex("\\d+(.\\d*)?")
            .setMin(0.0f)
            .setMax(60.0f);

            defineOptionalProperty<bool>("UndistortImages", true, "Perform undistortion of the images.");
            defineOptionalProperty<bool>("RectifyImages", true, "Perform rectification of the images.");

            defineOptionalProperty<std::string>("ReferenceFrameName", "EyeLeftCamera", "Optional reference frame name.");

            defineOptionalProperty<float>("Exposure", 300, "Exposure value. Negative values can be used to enable auto exposure.");
            defineOptionalProperty<float>("Shutter", 20, "Shutter value. Negative values can be used to enable auto mode.");
            defineOptionalProperty<float>("Gain", 0.0, "Gain value. Negative values can be used to enable auto mode.");
        }
    };

    /**
     * @class FlyCaptureImageProvider
     *
     * @ingroup VisionX-Components
     * @brief A brief description
     *
     *
     * Detailed Description
     */
    class FlyCaptureImageProvider :
        virtual public visionx::CapturingImageProvider
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override
        {
            return "FlyCaptureImageProvider";
        }

    protected:

        void onInitCapturingImageProvider() override;

        void onExitCapturingImageProvider() override;

        void onStartCapture(float frameRate) override;

        void onStopCapture() override;

        bool capture(void** ppImageBuffers) override;

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        CUndistortion* undistortion;
        CRectification* rectification;

        bool undistortImages;
        bool rectifyImages;

        std::string calibrationFileName;

    private:

        FlyCapture2::BusManager busManager;
        std::vector<FlyCapture2::Camera*> cameras;

        std::vector<std::string> serialNumbers;

        std::vector<FlyCapture2::Image*> colorImages;

        CByteImage** cameraImages;
    };


    class FlyCaptureStereoCameraProvider:
        virtual public FlyCaptureImageProvider,
        virtual public visionx::StereoCalibrationCaptureProviderInterface
    {

    public:


        void onInitCapturingImageProvider() override;

        visionx::StereoCalibration getStereoCalibration(const Ice::Current& c = Ice::emptyCurrent) override
        {
            return stereoCalibration;
        }

        bool getImagesAreUndistorted(const Ice::Current& c = Ice::emptyCurrent) override
        {
            return undistortImages;
        }

        std::string getReferenceFrame(const Ice::Current& c = Ice::emptyCurrent) override
        {
            return getProperty<std::string>("ReferenceFrameName").getValue();
        }


    private:

        visionx::StereoCalibration stereoCalibration;

    };
}

