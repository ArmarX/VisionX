armarx_component_set_name("ResultImageFuser")
set(COMPONENT_LIBS ArmarXCore VisionXCore VisionXInterfaces)
set(SOURCES ResultImageFuser.cpp)
set(HEADERS ResultImageFuser.h)
armarx_add_component("${SOURCES}" "${HEADERS}")

# add unit tests
add_subdirectory(test)

armarx_generate_and_add_component_executable(APPLICATION_APP_SUFFIX)
