/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::ResultImageFuser
 * @author     Markus Grotz ( markus dot grotz at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ResultImageFuser.h"


using namespace armarx;
using namespace visionx;



void ResultImageFuser::onInitCapturingImageProvider()
{
    numImages = getProperty<int>("numImages").getValue();
    height = getProperty<int>("height").getValue();
    width = getProperty<int>("width").getValue();

    colorMask = getProperty<Eigen::Vector3i>("colorMask").getValue();

    ARMARX_INFO << "color mask is r:" << colorMask(0) << " g:" << colorMask(1) << " b:" << colorMask(2);

    setImageFormat(visionx::ImageDimension(width, height), visionx::eRgb);
    setNumberImages(numImages);

    bytesPerPixel = getImageFormat().bytesPerPixel;

    pollImagesTask = new PeriodicTask<ResultImageFuser>(this, &ResultImageFuser::pollImageProviders, 50);
}

void ResultImageFuser::onExitCapturingImageProvider()
{

}



void ResultImageFuser::onStartCapture(float frameRate)
{
    pollImagesTask->start();

    if (imageSources.size() == 0)
    {
        std::vector<std::string> imageProviders = getProperty<std::vector<std::string>>("imageProviders").getValue();
        setResultImageProviders(imageProviders);
    }
}

void ResultImageFuser::onStopCapture()
{
    pollImagesTask->stop();
}

bool ResultImageFuser::capture(void** ppImages)
{
    usleep(10000);

    const size_t imageSize = width * height * bytesPerPixel;
    CByteImage* resultImage =  visionx::tools::createByteImage(getImageFormat(), visionx::eRgb);

    bool imageCaptured = false;

    for (auto& any : imageSources)
    {

        std::string proxyName = any.first;

        if (!imageAvailable[proxyName])
        {
            continue;
        }
        else
        {
            imageAvailable[proxyName] = false;
        }

        ImageProviderInterfacePrx providerPrx;

        try
        {
            providerPrx = getProxy<ImageProviderInterfacePrx>(proxyName, false);
        }
        catch (...)
        {
            ARMARX_WARNING << "provider" << proxyName << " is no longer here.";
            continue;
        }



        CByteImage** providerImages = any.second;
        for (int n = 0; n < numImages; n++)
        {
            CByteImage* image = providerImages[n];

            for (int j = 0; j < height; j++)
            {
                for (int i = 0; i < width; i++)
                {

                    int r = image->pixels[3 * (j * width + i) + 0];
                    int g = image->pixels[3 * (j * width + i) + 1];
                    int b = image->pixels[3 * (j * width + i) + 2];

                    if (!(r == colorMask(0) && g == colorMask(1) && b == colorMask(2)))
                    {
                        resultImage->pixels[3 * (j * width + i) + 0] = r;
                        resultImage->pixels[3 * (j * width + i) + 1] = g;
                        resultImage->pixels[3 * (j * width + i) + 2] = b;
                    }
                }
            }
        }

        imageCaptured = true;
    }

    {
        armarx::SharedMemoryScopedWriteLockPtr lock = getScopedWriteLock();

        for (int i = 0; i < numImages; i++)
        {
            memcpy(ppImages[i], resultImage->pixels, imageSize);
        }
    }

    return imageCaptured;
}



void ResultImageFuser::setResultImageProviders(std::vector<std::string> imageProviders)
{
    imageSources.clear();
    //    removeProxyDependency()

    for (std::string& proxyName : imageProviders)
    {

        ImageProviderInterfacePrx providerPrx;

        try
        {
            providerPrx = getProxy<ImageProviderInterfacePrx>(proxyName, false);
        }
        catch (...)
        {
            ARMARX_WARNING << "provider" << proxyName << " is no longer here.";
            continue;
        }

        const int providerNumImages = providerPrx->getNumberImages();
        ImageFormatInfo imageFormat = providerPrx->getImageFormat();

        assert(imageFormat.dimension.width == width);
        assert(imageFormat.dimension.height == height);
        assert(imageFormat.bytesPerPixel == bytesPerPixel);
        assert(numImages <= providerNumImages);

        CByteImage** images = new CByteImage*[providerNumImages];

        for (int n = 0; n < numImages; n++)
        {
            // todo visionx::eRgb might be wrong
            images[n] = visionx::tools::createByteImage(imageFormat, visionx::eRgb);
        }

        imageSources[proxyName] = images;
        imageAvailable[proxyName] = false;
    }
}



void ResultImageFuser::pollImageProviders()
{
    std::unique_lock lock(imageMutex);

    for (auto& any : imageSources)
    {
        std::string proxyName = any.first;

        ImageProviderInterfacePrx providerPrx;

        try
        {
            providerPrx = getProxy<ImageProviderInterfacePrx>(proxyName, false);
        }
        catch (...)
        {
            ARMARX_WARNING << "provider" << proxyName << " is no longer here.";
            continue;
        }

        CByteImage** bufferImages = any.second;
        ImageFormatInfo imageFormat = providerPrx->getImageFormat();

        armarx::Blob images = providerPrx->getImages();

        size_t imageSize = width * height * imageFormat.bytesPerPixel;

        for (int i = 0; i < numImages; i++)
        {
            memcpy(bufferImages[i]->pixels, &images[imageSize * i], imageSize);
        }

        imageAvailable[proxyName] = true;
    }
}

PropertyDefinitionsPtr ResultImageFuser::createPropertyDefinitions()
{
    return PropertyDefinitionsPtr(new ResultImageFuserPropertyDefinitions(
                                      getConfigIdentifier()));
}

