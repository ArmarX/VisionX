#/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2017, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "VideoFileImageProvider.h"
#include <opencv2/imgproc/imgproc.hpp>
#include <ArmarXCore/core/time/TimeUtil.h>
#include <Calibration/Calibration.h>
#include <ArmarXCore/core/exceptions/user/NotImplementedYetException.h>
namespace visionx
{

    VideoFileImageProvider::VideoFileImageProvider()
    {

    }

} // namespace visionx


std::string visionx::VideoFileImageProvider::getDefaultName() const
{
    return "VideoFileImageProvider";
}



void visionx::VideoFileImageProvider::onInitCapturingImageProvider()
{
    ARMARX_INFO << "Opening video file " << getProperty<std::string>("VideoFilePath").getValue();

    if (!openVideo())
    {
        throw armarx::LocalException("Failed to open Videofile ") << getProperty<std::string>("VideoFilePath").getValue();
    }
    setNumberImages(1);
    this->frameRate =  capturer.get(cv::CAP_PROP_FPS);
    ARMARX_INFO << "Video Size: " << capturer.get(cv::CAP_PROP_FRAME_WIDTH) << "x" << capturer.get(cv::CAP_PROP_FRAME_HEIGHT);
    setImageFormat(ImageDimension(capturer.get(cv::CAP_PROP_FRAME_WIDTH), capturer.get(cv::CAP_PROP_FRAME_HEIGHT)), visionx::eRgb, visionx::eBayerPatternGr);
    setImageSyncMode(visionx::eFpsSynchronization);
}

void visionx::VideoFileImageProvider::onExitCapturingImageProvider()
{
    capturer.release();

}

bool visionx::VideoFileImageProvider::capture(void** ppImageBuffers)
{
    //    TIMING_START(capture);
    auto result = capturer.read(image);
    if (!result && capturer.isOpened() && getProperty<bool>("LoopVideo").getValue())
    {
        capturer.release();
        openVideo();
        result = capturer.read(image);
    }
    //    TIMING_END(capture);
    if (result && sharedMemoryProvider)
    {
        Ice::Byte* pixels = static_cast<Ice::Byte*>(image.data);
        //        TIMING_START(conversion);

        cv::cvtColor(image, image, cv::COLOR_RGB2BGR);
        //        TIMING_END(conversion);
        //        TIMING_START(mutex);
        armarx::SharedMemoryScopedWriteLockPtr lock(sharedMemoryProvider->getScopedWriteLock());
        //        TIMING_END(mutex);
        //        TIMING_START(copy);
        memcpy(ppImageBuffers[0], pixels, image.cols * image.rows * image.channels());
        //        TIMING_END(copy);

    }

    //    TIMING_END(capture);
    return result;
}

bool visionx::VideoFileImageProvider::openVideo()
{
    auto result = capturer.open(getProperty<std::string>("VideoFilePath").getValue());
    //    CCalibration::LoadCameraParameters();
    return result;
}




void visionx::VideoFileImageProvider::onStartCapture(float framesPerSecond)
{
    ARMARX_INFO << __FUNCTION__;
    //    capturer.set(CV_CAP_PROP_FPS, framesPerSecond);

}

void visionx::VideoFileImageProvider::onStopCapture()
{
}


armarx::PropertyDefinitionsPtr visionx::VideoFileImageProvider::createPropertyDefinitions()
{
    return armarx::PropertyDefinitionsPtr(new VideoFileImageProviderPropertyDefinitions(getConfigIdentifier()));
}


visionx::MonocularCalibration visionx::VideoFileImageProvider::getCalibration(const Ice::Current&)
{
    throw armarx::exceptions::user::NotImplementedYetException("Calibration NYI");
}
