#/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2017, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "WebCamImageProvider.h"
#include <opencv2/imgproc/imgproc.hpp>
#include <ArmarXCore/core/time/TimeUtil.h>
#include <ArmarXCore/core/time/TimeUtil.h>
#include <Calibration/Calibration.h>
#include <ArmarXCore/core/exceptions/user/NotImplementedYetException.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <VisionX/tools/ImageUtil.h>
namespace visionx
{
    WebCamImageProvider::WebCamImageProvider()
    {

    }




    std::string visionx::WebCamImageProvider::getDefaultName() const
    {
        return "WebCamImageProvider";
    }


    std::vector<ImageDimension> visionx::WebCamImageProvider::getSupportedResolutions(cv::VideoCapture& camera)
    {
        std::vector<ImageDimension> supportedVideoResolutions;
        const ImageDimension CommonResolutions[] =
        {
            ImageDimension(320,  240),
            ImageDimension(640,  480),
            ImageDimension(800,  600),
            ImageDimension(768,  576),
            ImageDimension(1024, 768),
            ImageDimension(1280, 720),
            ImageDimension(1280, 960),
            ImageDimension(1280, 1024),
            ImageDimension(1600, 1200),
            ImageDimension(1920, 1080),
            ImageDimension(1920, 1200),
            ImageDimension(2560, 1440),
            ImageDimension(3849, 2160)
        };
        int nbTests = sizeof(CommonResolutions) / sizeof(CommonResolutions[0]);

        for (int i = 0; i < nbTests; i++)
        {
            ImageDimension test = CommonResolutions[i];

            // try to set resolution
            camera.set(cv::CAP_PROP_FRAME_WIDTH, test.width);
            camera.set(cv::CAP_PROP_FRAME_HEIGHT, test.height);
            camera.set(cv::CAP_PROP_AUTOFOCUS, 0);

            double width = camera.get(cv::CAP_PROP_FRAME_WIDTH);
            double height = camera.get(cv::CAP_PROP_FRAME_HEIGHT);
            if (test.width == width && test.height == height)
            {
                supportedVideoResolutions.push_back(test);
            }
        }

        return supportedVideoResolutions;
    }


    void visionx::WebCamImageProvider::onInitCapturingImageProvider()
    {
        ARMARX_INFO << "Opening camera";
        capturer.open(getProperty<int>("DeviceNumber").getValue());
        ARMARX_CHECK_EXPRESSION(capturer.isOpened());
        //    CCalibration::LoadCameraParameters();
        setNumberImages(1);
        auto dim = getProperty<ImageDimension>("ImageResolution").getValue();
        if (dim.width == 0)
        {
            auto supportedResolutions = getSupportedResolutions(capturer);
            ARMARX_INFO << "Supported resolutions: " << supportedResolutions;
            ARMARX_CHECK_POSITIVE(supportedResolutions.size());
            capturer.set(cv::CAP_PROP_FRAME_WIDTH, supportedResolutions.rbegin()->width);
            capturer.set(cv::CAP_PROP_FRAME_HEIGHT, supportedResolutions.rbegin()->height);
            ARMARX_INFO << "Using max resolution found for camera: " << capturer.get(cv::CAP_PROP_FRAME_WIDTH) << "x" << capturer.get(cv::CAP_PROP_FRAME_HEIGHT);
        }
        else
        {
            capturer.set(cv::CAP_PROP_FRAME_WIDTH, dim.width);
            capturer.set(cv::CAP_PROP_FRAME_HEIGHT, dim.height);
            ARMARX_INFO << "Using resolution " << capturer.get(cv::CAP_PROP_FRAME_WIDTH) << "x" << capturer.get(cv::CAP_PROP_FRAME_HEIGHT);
        }
        //    setImageFormat(ImageDimension(dim.width, dim.height), visionx::eRgb, visionx::eBayerPatternGr);
        setImageFormat(ImageDimension(capturer.get(cv::CAP_PROP_FRAME_WIDTH), capturer.get(cv::CAP_PROP_FRAME_HEIGHT)), visionx::eRgb, visionx::eBayerPatternGr);
        if (getProperty<float>("FPS").isSet())
        {
            setImageSyncMode(visionx::eFpsSynchronization);
            frameRate = getProperty<float>("FPS").getValue();
            ARMARX_INFO << "Using " << frameRate << " FPS";
        }
        else
        {
            setImageSyncMode(visionx::eCaptureSynchronization);
        }
    }

    void visionx::WebCamImageProvider::onExitCapturingImageProvider()
    {
        capturer.release();

    }

    bool visionx::WebCamImageProvider::capture(void** ppImageBuffers)
    {
        //    TIMING_START(capture);
        auto result = capturer.read(image);
        //    TIMING_END(capture);
        auto tmpSharedMemoryProvider = sharedMemoryProvider;
        if (result && tmpSharedMemoryProvider)
        {
            Ice::Byte* pixels = static_cast<Ice::Byte*>(image.data);
            //        TIMING_START(conversion);
            updateTimestamp(armarx::TimeUtil::GetTime().toMicroSeconds());
            cv::cvtColor(image, image, cv::COLOR_RGB2BGR);
            //        TIMING_END(conversion);
            //        TIMING_START(mutex);
            armarx::SharedMemoryScopedWriteLockPtr lock(tmpSharedMemoryProvider->getScopedWriteLock());
            //        TIMING_END(mutex);
            //        TIMING_START(copy);
            memcpy(ppImageBuffers[0], pixels, image.cols * image.rows * image.channels());
            //        TIMING_END(copy);

        }
        //    TIMING_END(capture);
        return result;
    }




    void visionx::WebCamImageProvider::onStartCapture(float framesPerSecond)
    {
        ARMARX_INFO << __FUNCTION__;
        //    capturer.set(CV_CAP_PROP_FPS, framesPerSecond);

    }

    void visionx::WebCamImageProvider::onStopCapture()
    {
    }


    armarx::PropertyDefinitionsPtr visionx::WebCamImageProvider::createPropertyDefinitions()
    {
        return armarx::PropertyDefinitionsPtr(new WebCamImageProviderPropertyDefinitions(getConfigIdentifier()));
    }


    visionx::MonocularCalibration visionx::WebCamImageProvider::getCalibration(const Ice::Current&)
    {
        throw armarx::exceptions::user::NotImplementedYetException("Calibration NYI");
    }


    std::vector<imrec::ChannelPreferences>
    WebCamImageProvider::getImageRecordingChannelPreferences(const Ice::Current&)
    {
        ARMARX_TRACE;

        imrec::ChannelPreferences cp;
        cp.requiresLossless = false;
        cp.name = "rgb";
        return {cp};
    }
}
