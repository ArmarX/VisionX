/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2017, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <VisionX/core/CapturingImageProvider.h>
#include <VisionX/interface/components/Calibration.h>
#include <opencv2/highgui/highgui.hpp>


namespace visionx
{
    class WebCamImageProviderPropertyDefinitions : public armarx::ComponentPropertyDefinitions
    {
    public:
        WebCamImageProviderPropertyDefinitions(std::string prefix) : ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<int>("DeviceNumber", 0, "Number of the ID as it openCV expects (usually 0 if only one device exists).");
            defineOptionalProperty<float>("FPS", 30.0f, "If not set, FPS is limited by device.");

            defineOptionalProperty<ImageDimension>("ImageResolution", ImageDimension(0, 0), "Image resolution. If not set, maximum is used.")
            .setCaseInsensitive(true)
            .map("320x240",     ImageDimension(320,  240))
            .map("640x480",     ImageDimension(640,  480))
            .map("800x600",     ImageDimension(800,  600))
            .map("768x576",     ImageDimension(768,  576))
            .map("1024x768",    ImageDimension(1024, 768))
            .map("1280x720",    ImageDimension(1280, 960))
            .map("1280x960",    ImageDimension(1280, 960))
            .map("1280x1024",   ImageDimension(1280, 960))
            .map("1600x1200",   ImageDimension(1600, 1200))
            .map("1920x1080",   ImageDimension(1920, 1080))
            .map("1920x1200",   ImageDimension(1920, 1200))
            .map("none",        ImageDimension(0, 0));
        }
    };


    class WebCamImageProvider :
    //        public MonocularCalibrationCapturingProviderInterface,
        public CapturingImageProvider
    {
    public:
        WebCamImageProvider();

        // ManagedIceObject interface
    protected:
        std::string getDefaultName() const override;

        // CapturingImageProviderInterface interface
    public:
        // CapturingImageProvider interface
    protected:
        void onInitCapturingImageProvider() override;
        void onExitCapturingImageProvider() override;
        bool capture(void** ppImageBuffers) override;
        cv::VideoCapture capturer;
        cv::Mat image;

        // CapturingImageProvider interface
    protected:
        void onStartCapture(float framesPerSecond) override;
        void onStopCapture() override;

        // PropertyUser interface
    public:
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        // MonocularCalibrationCapturingProviderInterface interface
    public:
        MonocularCalibration getCalibration(const Ice::Current&);
        std::vector<ImageDimension> getSupportedResolutions(cv::VideoCapture& camera);
        std::vector<imrec::ChannelPreferences> getImageRecordingChannelPreferences(const Ice::Current&) override;
    protected:
        MonocularCalibration calibration;
    };

} // namespace visionx

