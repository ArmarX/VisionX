#include "X11ScreenCaptureImageProvider.h"

#include <ArmarXCore/core/time.h>

#include <VisionX/tools/ImageUtil.h>


namespace visionx
{

    const std::string X11ScreenCaptureImageProvider::defaultName = "X11ScreenCapture";

    X11ScreenCaptureImageProvider::X11ScreenCaptureImageProvider()
    {
        clock = armarx::Clock(armarx::ClockType::Realtime);
    }


    std::string
    visionx::X11ScreenCaptureImageProvider::getDefaultName() const
    {
        return defaultName;
    }


    std::string
    visionx::X11ScreenCaptureImageProvider::GetDefaultName()
    {
        return defaultName;
    }


    void
    visionx::X11ScreenCaptureImageProvider::onInitCapturingImageProvider()
    {
        ARMARX_INFO << "Initializing X11...";

        display = XOpenDisplay(nullptr);

        ARMARX_CHECK_GREATER_EQUAL(properties.screenNumber, 0);
        ARMARX_CHECK_LESS(properties.screenNumber, XScreenCount(display));
        window = RootWindow(display, properties.screenNumber);

        setNumberImages(1);

        //    setImageFormat(ImageDimension(dim.width, dim.height), visionx::eRgb, visionx::eBayerPatternGr);
        setImageFormat(ImageDimension(properties.width, properties.height),
                       visionx::eRgb,
                       visionx::eBayerPatternGr);

        out = cv::Mat(properties.height, properties.width, CV_8UC3);

        setImageSyncMode(visionx::eFpsSynchronization);
        frameRate = properties.fps;
        ARMARX_INFO << "Using " << frameRate << " FPS";
    }


    void
    visionx::X11ScreenCaptureImageProvider::onExitCapturingImageProvider()
    {
        if (ximg != nullptr)
        {
            XDestroyImage(ximg);
        }

        XCloseDisplay(display);
    }


    bool
    visionx::X11ScreenCaptureImageProvider::capture(void** ppImageBuffers)
    {
        if (ximg != nullptr)
        {
            XDestroyImage(ximg);
        }

        ximg = XGetImage(display,
                         window,
                         properties.x,
                         properties.y,
                         properties.width,
                         properties.height,
                         AllPlanes,
                         ZPixmap);
        ximg_cv = cv::Mat(properties.height, properties.width, CV_8UC4, ximg->data);

        auto tmpSharedMemoryProvider = sharedMemoryProvider;
        if (tmpSharedMemoryProvider)
        {
            Ice::Byte* pixels = static_cast<Ice::Byte*>(out.data);
            updateTimestamp(clock.now().toMicroSecondsSinceEpoch());
            cv::cvtColor(ximg_cv, out, cv::COLOR_RGBA2BGR);
            armarx::SharedMemoryScopedWriteLockPtr lock(
                tmpSharedMemoryProvider->getScopedWriteLock());
            memcpy(ppImageBuffers[0], pixels, out.cols * out.rows * out.channels());
        }

        return true;
    }


    void
    visionx::X11ScreenCaptureImageProvider::onStartCapture(float framesPerSecond)
    {
        properties.fps = static_cast<int>(framesPerSecond);
    }


    void
    visionx::X11ScreenCaptureImageProvider::onStopCapture()
    {
        ;
    }


    armarx::PropertyDefinitionsPtr
    visionx::X11ScreenCaptureImageProvider::createPropertyDefinitions()
    {
        armarx::PropertyDefinitionsPtr defs =
            new armarx::ComponentPropertyDefinitions(getConfigIdentifier());

        defs->optional(properties.screenNumber, "screen.number", "Screen number to record. This does not correspond to "
                                                                 "the monitor count, as this is transparent to X. You "
                                                                 "don't want to change this in most cases.");
        defs->optional(properties.fps, "fps", "Frames per second.");
        defs->optional(properties.x, "region.x", "X coordinate of the region to record.");
        defs->optional(properties.y, "region.y", "Y coordinate of the region to record.");
        defs->optional(properties.width, "region.width", "Width of the region to record.");
        defs->optional(properties.height, "region.height", "Height of the region to record.");

        return defs;
    }


    std::vector<imrec::ChannelPreferences>
    X11ScreenCaptureImageProvider::getImageRecordingChannelPreferences(const Ice::Current&)
    {
        ARMARX_TRACE;

        imrec::ChannelPreferences cp;
        cp.requiresLossless = false;
        cp.name = "screen";
        return {cp};
    }
} // namespace visionx
