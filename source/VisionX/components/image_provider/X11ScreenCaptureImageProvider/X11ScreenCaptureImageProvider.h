#pragma once

#include <opencv2/opencv.hpp>

#include <ArmarXCore/core/time/Clock.h>

#include <VisionX/core/CapturingImageProvider.h>

// Include X last.
#include <X11/Xlib.h>
#include <X11/Xutil.h>


namespace visionx
{

    class X11ScreenCaptureImageProvider : public CapturingImageProvider
    {
    public:
        X11ScreenCaptureImageProvider();

        // ManagedIceObject interface
    protected:
        std::string getDefaultName() const override;
        static std::string GetDefaultName();

        // CapturingImageProviderInterface interface
    public:
        // CapturingImageProvider interface
    protected:
        void onInitCapturingImageProvider() override;
        void onExitCapturingImageProvider() override;
        bool capture(void** ppImageBuffers) override;

        // CapturingImageProvider interface
    protected:
        void onStartCapture(float framesPerSecond) override;
        void onStopCapture() override;

        // PropertyUser interface
    public:
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        std::vector<imrec::ChannelPreferences>
        getImageRecordingChannelPreferences(const Ice::Current&) override;

    private:
        static const std::string defaultName;

        struct Properties
        {
            int screenNumber = 0;
            int x = 0;
            int y = 0;
            int width = 1920;
            int height = 1080;
            int fps = 30;
        } properties;

        armarx::Clock clock;

        Window window;
        Display* display;
        XImage* ximg = nullptr;
        cv::Mat ximg_cv;
        cv::Mat out;
    };

} // namespace visionx
