/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    VisionX::Component
* @author     Kai Welke <welke at kit dot edu>
* @copyright  2013 Humanoids Group, HIS, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "ColorMarkerObjectLocalizer.h"

// Core
#include <ArmarXCore/core/system/ArmarXDataPath.h>

// RobotState
#include <RobotAPI/libraries/core/Pose.h>
#include <RobotAPI/libraries/core/remoterobot/RemoteRobot.h>

// IVT
#include <Image/ByteImage.h>
#include <DataProcessing/RANSAC.h>
#include <SegmentableRecognition/SegmentableDatabase.h>
#include <Image/ImageProcessor.h>
#include <Math/Math3d.h>

// MemoryX
#include <MemoryX/libraries/helpers/ObjectRecognitionHelpers/ObjectRecognitionWrapper.h>
#include <MemoryX/core/MemoryXCoreObjectFactories.h>

// This file must be included after all Ice-related headers when Ice 3.5.x is used (e.g. with Ubuntu 14.04 or Debian 7.0)!
// GLContext.h indirectly includes X.h which does "#define None", colliding with IceUtil::None.
#include <gui/GLContext.h>

using namespace armarx;
using namespace visionx;
using namespace memoryx;
using namespace memoryx::EntityWrappers;


ColorMarkerObjectLocalizer::ColorMarkerObjectLocalizer()
{
}

ColorMarkerObjectLocalizer::~ColorMarkerObjectLocalizer()
{

}

void ColorMarkerObjectLocalizer::onInitObjectLocalizerProcessor()
{
    usingProxy("RobotStateComponent");
}

void ColorMarkerObjectLocalizer::onConnectObjectLocalizerProcessor()
{
    RobotStateComponentInterfacePrx robotStateComponent = getProxy<RobotStateComponentInterfacePrx>("RobotStateComponent");
    remoteRobot.reset(new RemoteRobot(robotStateComponent->getSynchronizedRobot()));
}

void ColorMarkerObjectLocalizer::onExitObjectLocalizerProcessor()
{

}

bool ColorMarkerObjectLocalizer::initRecognizer()
{
    ARMARX_VERBOSE << "Initializing ColorMarkerObjectLocalizer";

    minPixelsPerRegion = getProperty<float>("MinPixelsPerRegion").getValue();
    numObjectMarker = getProperty<int>("NumObjectMarker").getValue();
    markeredObjectName = getProperty<std::string>("MarkeredObjectName").getValue();
    maxEpipolarDistance = getProperty<float>("MaxEpipolarDistance").getValue();

    hue = getProperty<int>("Hue").getValue();
    hueTol = getProperty<int>("HueTolerance").getValue();
    minS = getProperty<int>("MinSaturation").getValue();
    maxS = getProperty<int>("MaxSaturation").getValue();
    minV = getProperty<int>("MinValue").getValue();
    maxV = getProperty<int>("MaxValue").getValue();
    objectMarkerColor = CColorParameterSet::Translate(getProperty<std::string>("MarkerColor").getValue().c_str());


    if (!segmentableRecognition)
    {
        colorParameters.reset(new CColorParameterSet());

        colorParameters->SetColorParameters(objectMarkerColor, hue, hueTol, minS, maxS, minV, maxV);

        ARMARX_VERBOSE << "Startup step 1";

        segmentableRecognition.reset(new CSegmentableRecognition());

        ARMARX_VERBOSE << "Startup step 2";

        // create context
        ImageFormatInfo imageFormat = getImageFormat();
        contextGL.reset(new CGLContext());

        ARMARX_VERBOSE << "Startup step 3";

        contextGL->CreateContext(imageFormat.dimension.width, imageFormat.dimension.height);

        ARMARX_VERBOSE << "Startup step 4";

        contextGL->MakeCurrent();

        m_pOpenGLVisualizer.reset(new COpenGLVisualizer());
        m_pOpenGLVisualizer->InitByCalibration(getStereoCalibration()->GetRightCalibration());

        ARMARX_VERBOSE << "Startup step 5";

        // init segmentable recognition
        bool success = segmentableRecognition->InitWithoutDatabase(colorParameters.get(), getStereoCalibration());

        if (!success)
        {
            throw armarx::LocalException("Could not initialize segmentable object database.");
        }

        ARMARX_VERBOSE << "ColorMarkerObjectLocalizer is initialized";

        return success;
    }

    ARMARX_VERBOSE << "ColorMarkerObjectLocalizer is initialized";

    return true;
}



memoryx::ObjectLocalizationResultList ColorMarkerObjectLocalizer::localizeObjectClasses(const std::vector<std::string>& objectClassNames, CByteImage** cameraImages, armarx::MetaInfoSizeBasePtr imageMetaInfo, CByteImage** resultImages)
{

    contextGL->MakeCurrent();

    Object3DList objectList;

    ARMARX_VERBOSE << "Localizing everything at once";

    segmentableRecognition->DoRecognition(cameraImages, resultImages, minPixelsPerRegion, true, maxEpipolarDistance, objectMarkerColor, false, getImagesAreUndistorted());

    objectList = segmentableRecognition->GetObject3DList();

    ARMARX_INFO << "Found " << objectList.size() << " objects";

    memoryx::ObjectLocalizationResultList resultList;
    // help for problem analysis
    visualizeResults(objectList, resultImages);
    std::string refFrame = getProperty<std::string>("ReferenceFrameName").getValue();
    std::string camFrame("EyeLeftCamera");
    memoryx::MultivariateNormalDistributionPtr cPositionNoise;
    Eigen::Vector3f objectPosition(0.0, 0.0, 0.0);
    Eigen::Matrix3f objectOrientation;
    bool bChairLocalized = false;

    if (objectList.size() == numObjectMarker)
    {


        CVec3dArray allMarkerPositions;


        for (Object3DList::iterator iter = objectList.begin() ; iter != objectList.end() ; iter++)
        {
            // assure instance belongs to queried class

            //if (iter->localizationValid)
            {
                // assemble result

                // position and orientation

                std::cout << iter->world_point.x << " " << iter->world_point.y << " " << iter->world_point.z << std::endl;
                std::cout << iter->pose.translation.x << " " << iter->pose.translation.y << " " << iter->pose.translation.z << std::endl;
                Eigen::Vector3f markerPosition(iter->pose.translation.x, iter->pose.translation.y, iter->pose.translation.z);
                objectPosition += markerPosition;
                Vec3d singleMarkerPosition;
                singleMarkerPosition.x = markerPosition(0);
                singleMarkerPosition.y = markerPosition(1);
                singleMarkerPosition.z = markerPosition(2);
                allMarkerPositions.AddElement(singleMarkerPosition);
            }

        }

        objectPosition /= numObjectMarker;

        std::vector<int> markerPlacementIndex(numObjectMarker);
        float min1Distance = 0;

        for (int i = 0; i < allMarkerPositions.GetSize(); i++)
        {


            for (int j = 0; j < allMarkerPositions.GetSize(); j++)
            {
                float temp1Distance = 0;

                if (j == i)
                {
                    continue;
                }

                temp1Distance += (allMarkerPositions[i].x - allMarkerPositions[j].x) * (allMarkerPositions[i].x - allMarkerPositions[j].x);
                temp1Distance += (allMarkerPositions[i].y - allMarkerPositions[j].y) * (allMarkerPositions[i].y - allMarkerPositions[j].y);
                temp1Distance += (allMarkerPositions[i].z - allMarkerPositions[j].z) * (allMarkerPositions[i].z - allMarkerPositions[j].z);
                temp1Distance = sqrt(temp1Distance);

                if (min1Distance == 0 || temp1Distance < min1Distance)
                {
                    std::cout << "minDist " <<  min1Distance << std::endl;
                    min1Distance = temp1Distance;

                    for (int k = 0; k < allMarkerPositions.GetSize(); k++)
                    {
                        markerPlacementIndex[k] = 0;
                    }

                    markerPlacementIndex[i] = 1;
                    markerPlacementIndex[j] = 1;
                }

            }

        }

        Vec3d minMeanPosition;
        minMeanPosition.x = 0.0;
        minMeanPosition.y = 0.0;
        minMeanPosition.z = 0.0;

        Vec3d otherMeanPosition;
        otherMeanPosition.x = 0.0;
        otherMeanPosition.y = 0.0;
        otherMeanPosition.z = 0.0;

        for (int i = 0; i < allMarkerPositions.GetSize(); i++)
        {
            std::cout << markerPlacementIndex[i] << " ";

            if (markerPlacementIndex[i] == 1)
            {
                minMeanPosition.x += allMarkerPositions[i].x;
                minMeanPosition.y += allMarkerPositions[i].y;
                minMeanPosition.z += allMarkerPositions[i].z;
            }
            else
            {
                otherMeanPosition.x += allMarkerPositions[i].x;
                otherMeanPosition.y += allMarkerPositions[i].y;
                otherMeanPosition.z += allMarkerPositions[i].z;
            }
        }

        std::cout << std::endl;
        minMeanPosition.x /= 2.0;
        minMeanPosition.y /= 2.0;
        minMeanPosition.z /= 2.0;
        otherMeanPosition.x /= float(numObjectMarker - 2);
        otherMeanPosition.y /= float(numObjectMarker - 2);
        otherMeanPosition.z /= float(numObjectMarker - 2);
        std::cout << "minMean " << minMeanPosition.x << " " << minMeanPosition.y << " " << minMeanPosition.z << std::endl;
        std::cout << "oterhMean " << otherMeanPosition.x << " " << otherMeanPosition.y << " " << otherMeanPosition.z << std::endl;
        Vec3d ny, nz, nx;
        //Vec3d u1, u2;
        //Math3d::SubtractVecVec(allMarkerPositions[1], allMarkerPositions[0], u1);
        //Math3d::SubtractVecVec(allMarkerPositions[2], allMarkerPositions[0], u2);
        //Math3d::CrossProduct(u1, u2, ny);
        ny.x = minMeanPosition.x - otherMeanPosition.x;
        ny.z = minMeanPosition.z - otherMeanPosition.z;


        //interpret camera coordinate in chair
        nz.x = 0.0;
        nz.y = -1.0;
        nz.z = 0.0;
        //nx.x = -1.0;nx.y = 0.0;nx.z = 0.0;
        //ny.x = 0.0;ny.y = 0.0;ny.z = -1.0;
        //std::cout << "nx " << nx.x << " " << nx.y << " " << nx.z << std::endl;
        std::cout << "ny " << ny.x << " " << ny.y << " " << ny.z << std::endl;
        //std::cout << "nz " << nz.x << " " << nz.y << " " << nz.z << std::endl;
        Math3d::NormalizeVec(ny);
        Math3d::CrossProduct(ny, nz, nx);
        std::cout << "objectPosition" << std::endl;
        std::cout << objectPosition << std::endl;
        objectOrientation << nx.x, ny.x, nz.x, nx.y, ny.y, nz.y, nx.z, ny.z, nz.z;
        std::cout << "objectOrientation" << std::endl;
        std::cout << objectOrientation << std::endl;
        cPositionNoise = calculateLocalizationUncertainty(objectList[0].region_left.centroid, objectList[0].region_right.centroid);;
        bChairLocalized = true;
        ARMARX_VERBOSE << "Finished localizing " << markeredObjectName << " in " << refFrame;
    }
    else if (objectList.size() == numObjectMarker - 1)
    {



        CVec3dArray allMarkerPositions;

        for (Object3DList::iterator iter = objectList.begin() ; iter != objectList.end() ; iter++)
        {
            // assure instance belongs to queried class

            //if (iter->localizationValid)
            {
                // assemble result

                // position and orientation

                std::cout << iter->world_point.x << " " << iter->world_point.y << " " << iter->world_point.z << std::endl;
                std::cout << iter->pose.translation.x << " " << iter->pose.translation.y << " " << iter->pose.translation.z << std::endl;
                Eigen::Vector3f markerPosition(iter->pose.translation.x, iter->pose.translation.y, iter->pose.translation.z);
                objectPosition += markerPosition;
                Vec3d singleMarkerPosition;
                singleMarkerPosition.x = markerPosition(0);
                singleMarkerPosition.y = markerPosition(1);
                singleMarkerPosition.z = markerPosition(2);
                allMarkerPositions.AddElement(singleMarkerPosition);
            }

        }

        objectPosition /= numObjectMarker;


        Vec3d u1, u2, ny, nz, nx, zAxis;
        Math3d::SubtractVecVec(allMarkerPositions[1], allMarkerPositions[0], u1);
        Math3d::SubtractVecVec(allMarkerPositions[2], allMarkerPositions[0], u2);
        Math3d::CrossProduct(u1, u2, ny);
        zAxis.x = 0;
        zAxis.y = 0;
        zAxis.z = 1;

        if (Math3d::Angle(zAxis, ny) < M_PI)
        {
            ny.x = -ny.x;
            ny.y = -ny.y;
            ny.z = -ny.z;
        }


        //interpret camera coordinate in chair
        nz.x = 0.0;
        nz.y = -1.0;
        nz.z = 0.0;
        //nx.x = -1.0;nx.y = 0.0;nx.z = 0.0;
        //ny.x = 0.0;ny.y = 0.0;ny.z = -1.0;
        //std::cout << "nx " << nx.x << " " << nx.y << " " << nx.z << std::endl;
        std::cout << "ny " << ny.x << " " << ny.y << " " << ny.z << std::endl;
        //std::cout << "nz " << nz.x << " " << nz.y << " " << nz.z << std::endl;
        Math3d::NormalizeVec(ny);
        Math3d::CrossProduct(ny, nz, nx);
        std::cout << "objectPosition" << std::endl;
        std::cout << objectPosition << std::endl;
        objectOrientation << nx.x, ny.x, nz.x, nx.y, ny.y, nz.y, nx.z, ny.z, nz.z;
        std::cout << "objectOrientation" << std::endl;
        std::cout << objectOrientation << std::endl;
        cPositionNoise = calculateLocalizationUncertainty(objectList[0].region_left.centroid, objectList[0].region_right.centroid);;
        bChairLocalized = true;
        ARMARX_VERBOSE << "Finished localizing " << markeredObjectName << " in " << refFrame;
    }
    else if (objectList.size() == numObjectMarker + 1)
    {



        CVec3dArray allMarkerPositions;

        for (Object3DList::iterator iter = objectList.begin() ; iter != objectList.end() ; iter++)
        {
            // assure instance belongs to queried class

            //if (iter->localizationValid)
            {
                // assemble result

                // position and orientation

                std::cout << iter->world_point.x << " " << iter->world_point.y << " " << iter->world_point.z << std::endl;
                std::cout << iter->pose.translation.x << " " << iter->pose.translation.y << " " << iter->pose.translation.z << std::endl;
                Eigen::Vector3f markerPosition(iter->pose.translation.x, iter->pose.translation.y, iter->pose.translation.z);
                objectPosition += markerPosition;
                Vec3d singleMarkerPosition;
                singleMarkerPosition.x = markerPosition(0);
                singleMarkerPosition.y = markerPosition(1);
                singleMarkerPosition.z = markerPosition(2);
                allMarkerPositions.AddElement(singleMarkerPosition);
            }

        }

        objectPosition /= numObjectMarker;

        std::vector<int> markerPlacementIndex(numObjectMarker);
        float min1Distance = 0;

        for (int i = 0; i < allMarkerPositions.GetSize(); i++)
        {


            for (int j = 0; j < allMarkerPositions.GetSize(); j++)
            {
                float temp1Distance = 0;

                if (j == i)
                {
                    continue;
                }

                temp1Distance += (allMarkerPositions[i].x - allMarkerPositions[j].x) * (allMarkerPositions[i].x - allMarkerPositions[j].x);
                temp1Distance += (allMarkerPositions[i].y - allMarkerPositions[j].y) * (allMarkerPositions[i].y - allMarkerPositions[j].y);
                temp1Distance += (allMarkerPositions[i].z - allMarkerPositions[j].z) * (allMarkerPositions[i].z - allMarkerPositions[j].z);
                temp1Distance = sqrt(temp1Distance);

                if (min1Distance == 0 || temp1Distance < min1Distance)
                {
                    std::cout << "minDist " <<  min1Distance << std::endl;
                    min1Distance = temp1Distance;

                    for (int k = 0; k < allMarkerPositions.GetSize(); k++)
                    {
                        markerPlacementIndex[k] = 0;
                    }

                    markerPlacementIndex[i] = 1;
                    markerPlacementIndex[j] = 1;
                }

            }

        }

        Vec3d minMeanPosition;
        minMeanPosition.x = 0.0;
        minMeanPosition.y = 0.0;
        minMeanPosition.z = 0.0;
        CVec3dArray otherMarkerPositions;
        Vec3d otherMeanPosition;
        otherMeanPosition.x = 0.0;
        otherMeanPosition.y = 0.0;
        otherMeanPosition.z = 0.0;

        for (int i = 0; i < allMarkerPositions.GetSize(); i++)
        {
            std::cout << markerPlacementIndex[i] << " ";

            if (markerPlacementIndex[i] == 1)
            {
                minMeanPosition.x += allMarkerPositions[i].x;
                minMeanPosition.y += allMarkerPositions[i].y;
                minMeanPosition.z += allMarkerPositions[i].z;
            }
            else
            {
                otherMarkerPositions.AddElement(allMarkerPositions[i]);
            }
        }

        std::cout << std::endl;
        minMeanPosition.x /= 2.0;
        minMeanPosition.y /= 2.0;
        minMeanPosition.z /= 2.0;
        otherMeanPosition.x /= float(numObjectMarker - 2);
        otherMeanPosition.y /= float(numObjectMarker - 2);
        otherMeanPosition.z /= float(numObjectMarker - 2);
        std::cout << "minMean " << minMeanPosition.x << " " << minMeanPosition.y << " " << minMeanPosition.z << std::endl;
        std::cout << "oterhMean " << otherMeanPosition.x << " " << otherMeanPosition.y << " " << otherMeanPosition.z << std::endl;
        Vec3d u1, u2, ny, nz, nx, zAxis;
        Math3d::SubtractVecVec(otherMarkerPositions[1], otherMarkerPositions[0], u1);
        Math3d::SubtractVecVec(otherMarkerPositions[2], otherMarkerPositions[0], u2);
        Math3d::CrossProduct(u1, u2, ny);
        zAxis.x = 0;
        zAxis.y = 0;
        zAxis.z = 1;
        std::cout << "ny " << ny.x << " " << ny.y << " " << ny.z << std::endl;

        if (Math3d::Angle(zAxis, ny) > M_PI)
        {
            ny.x = -ny.x;
            ny.y = -ny.y;
            ny.z = -ny.z;
        }


        //interpret camera coordinate in chair
        nz.x = 0.0;
        nz.y = -1.0;
        nz.z = 0.0;
        //nx.x = -1.0;nx.y = 0.0;nx.z = 0.0;
        //ny.x = 0.0;ny.y = 0.0;ny.z = -1.0;
        //std::cout << "nx " << nx.x << " " << nx.y << " " << nx.z << std::endl;
        std::cout << "ny " << ny.x << " " << ny.y << " " << ny.z << std::endl;
        //std::cout << "nz " << nz.x << " " << nz.y << " " << nz.z << std::endl;
        Math3d::NormalizeVec(ny);
        Math3d::CrossProduct(ny, nz, nx);
        std::cout << "objectPosition" << std::endl;
        std::cout << objectPosition << std::endl;
        objectOrientation << nx.x, ny.x, nz.x, nx.y, ny.y, nz.y, nx.z, ny.z, nz.z;
        std::cout << "objectOrientation" << std::endl;
        std::cout << objectOrientation << std::endl;
        cPositionNoise = calculateLocalizationUncertainty(objectList[0].region_left.centroid, objectList[0].region_right.centroid);;
        bChairLocalized = true;
        ARMARX_VERBOSE << "Finished localizing " << markeredObjectName << " in " << refFrame;
    }
    else
    {
        ARMARX_VERBOSE << "No object localized. Go Closer. " ;


    }

    if (bChairLocalized)
    {
        memoryx::ObjectLocalizationResult result;
        VirtualRobot::RobotNodePtr rnBase = remoteRobot->getRobotNode(refFrame);
        VirtualRobot::RobotNodePtr rnCam = remoteRobot->getRobotNode(camFrame);
        auto agentName = remoteRobot->getName();
        FramedPositionPtr tmpPosition = new armarx::FramedPosition(objectPosition, camFrame, agentName);
        FramedOrientationPtr tmpOrientation = new armarx::FramedOrientation(objectOrientation, camFrame, agentName);
        FramedPosePtr tmpPose = new armarx::FramedPose(tmpOrientation->toEigen(), tmpPosition->toEigen(), camFrame, agentName);
        tmpPose->changeFrame(remoteRobot, refFrame);
        Eigen::Vector3f zAxisChair(0.0, 0.0, 1.0);
        Eigen::Vector3f yAxisChair((tmpPose->toEigen())(0, 1), (tmpPose->toEigen())(1, 1), (tmpPose->toEigen())(2, 1));
        Eigen::Vector3f xAxisChair = yAxisChair.cross(zAxisChair);
        yAxisChair = xAxisChair.cross(zAxisChair);
        Eigen::Matrix4f objectPoseCorrected = tmpPose->toEigen();

        objectPoseCorrected(2, 3) = 50.0;
        objectPoseCorrected(1, 3) += 400.0;
        objectPoseCorrected.block(0, 0, 3, 1) = xAxisChair;
        objectPoseCorrected.block(0, 2, 3, 1) = zAxisChair;
        objectPoseCorrected.block(0, 1, 3, 1) = yAxisChair;

        result.position = new armarx::FramedPosition(objectPoseCorrected, refFrame, agentName);
        result.orientation = new armarx::FramedOrientation(objectPoseCorrected, refFrame, agentName);

        // calculate recognition certainty
        result.recognitionCertainty = 1.0;

        result.objectClassName = markeredObjectName;
        resultList.push_back(result);
    }

    return resultList;
}



void ColorMarkerObjectLocalizer::visualizeResults(const Object3DList& objectList, CByteImage** resultImages)
{
    m_pOpenGLVisualizer->ActivateShading(true);
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);


    m_pOpenGLVisualizer->SetProjectionMatrix(getStereoCalibration()->GetRightCalibration());

    m_pOpenGLVisualizer->Clear();


    for (int i = 0; i < (int) objectList.size(); i++)
    {
        const Object3DEntry& entry = objectList.at(i);
        CSegmentableDatabase::DrawObjectFromFile(m_pOpenGLVisualizer.get(), entry.sOivFilePath.c_str(), entry.pose);
    }

    const int nImageIndex =  1;

    if (resultImages && resultImages[nImageIndex])
    {
        CByteImage tempImage(resultImages[nImageIndex]);
        m_pOpenGLVisualizer->GetImage(&tempImage);
        ::ImageProcessor::FlipY(&tempImage, &tempImage);
        const int nBytes = 3 * tempImage.width * tempImage.height;
        const unsigned char* pixels = tempImage.pixels;
        unsigned char* output = resultImages[nImageIndex]->pixels;

        for (int i = 0; i < nBytes; i += 3)
            if (pixels[i])
            {
                const unsigned char g = pixels[i];
                output[i] = g;
                output[i + 1] = g;
                output[i + 2] = g;
            }
    }
}



bool ColorMarkerObjectLocalizer::addObjectClass(const memoryx::EntityPtr& objectClassEntity, const memoryx::GridFileManagerPtr& fileManager)
{
    return true;

}
