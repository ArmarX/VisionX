/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::FaceRecognition
 * @author     Markus Grotz ( markus dot grotz at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <VisionX/components/object_perception/ObjectLocalizerProcessor.h>

#include <opencv2/core/core.hpp>
//#include <opencv2/contrib/contrib.hpp>
#include <opencv2/face.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/objdetect/objdetect.hpp>

#include <Image/StereoMatcher.h>


namespace armarx
{
    /**
     * @class FaceRecognitionPropertyDefinitions
     * @brief
     */
    class FaceRecognitionPropertyDefinitions:
        public visionx::ObjectLocalizerProcessorPropertyDefinitions
    {
    public:
        FaceRecognitionPropertyDefinitions(std::string prefix):
            visionx::ObjectLocalizerProcessorPropertyDefinitions(prefix)
        {
            defineOptionalProperty<double>("scaleFactor", 1.02, "scale factor for the HAAR classifier");
            defineOptionalProperty<int>("minNeighbors", 2, "number of neighbors for the HAAR classifier");
            defineOptionalProperty<std::string>("trainingDataPath", "VisionX/examples/faces", "folder containing the training images");
            defineOptionalProperty<std::string>("classifierFileName", "VisionX/examples/haarcascades/haarcascade_frontalface_alt.xml", "path to the HAAR classifier");
        }
    };

    /**
     * @defgroup Component-FaceRecognition FaceRecognition
     * @ingroup VisionX-Components
     * A description of the component FaceRecognition.
     *
     * @class FaceRecognition
     * @ingroup Component-FaceRecognition
     * @brief Brief description of class FaceRecognition.
     *
     * Detailed description of class FaceRecognition.
     */
    class FaceRecognition :
        virtual public visionx::ObjectLocalizerProcessor
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override
        {
            return "FaceRecognition";
        }

    protected:

        /**
         * @see ObjectLocalizerProcessor::onInitObjectLocalizerProcessor()
         */
        void onInitObjectLocalizerProcessor() override;

        /**
         * @see ObjectLocalizerProcessor::onConnectObjectLocalizerProcessor()
         */
        void onConnectObjectLocalizerProcessor() override;

        /**
         * @see ObjectLocalizerProcessor::onExitObjectLocalizerProcessor()
         */
        void onExitObjectLocalizerProcessor() override;

        /**
         * Add a memory entity representing the hand marker in order to set its properties
         *
         * @param objectClassEntity entity containing all information available for the object class
         * @param fileManager GridFileManager required to read files associated to prior knowledge from the database.
         *
         * @return success of adding this entity
         */
        memoryx::ObjectLocalizationResultList localizeObjectClasses(const std::vector<std::string>& objectClassNames, CByteImage** cameraImages, armarx::MetaInfoSizeBasePtr imageMetaInfo, CByteImage** resultImages) override;

        /**
         * Initializes segmentable recognition
         *
         * @return success
         */
        bool initRecognizer() override;

        /**
         * Add a memory entity representing the hand marker in order to set its properties
         *
         * @param objectClassEntity entity containing all information available for the object class
         * @param fileManager GridFileManager required to read files associated to prior knowledge from the database.
         *
         * @return success of adding this entity
         */
        bool addObjectClass(const memoryx::EntityPtr& objectClassEntity, const memoryx::GridFileManagerPtr& fileManager) override;

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

    private:

        cv::Ptr<cv::face::FaceRecognizer> model;

        cv::CascadeClassifier classifier;

        std::map<int, std::string> labels;

        double scaleFactor;
        int minNeighbors;

        cv::Size faceImageSize;

        CStereoMatcher* stereoMatcher;

    };
}

