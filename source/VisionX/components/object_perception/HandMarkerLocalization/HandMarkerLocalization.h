/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::Component
 * @author     Kai Welke (welke at kit dot edu)
 * @date       2013
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// VisionX
#include <VisionX/components/object_perception/ObjectLocalizerProcessor.h>

#include <VirtualRobot/VirtualRobot.h>

// forward declarations
class CByteImage;
class CVisualTargetLocator;
class CColorParameterSet;

namespace visionx
{
    class HandMarkerLocalizationPropertyDefinitions:
        public ObjectLocalizerProcessorPropertyDefinitions
    {
    public:
        HandMarkerLocalizationPropertyDefinitions(std::string prefix):
            ObjectLocalizerProcessorPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("ColorParameterFile", "VisionX/examples/colors.txt", "The color parameter file configures the colors used for segmentable recognition (usually colors.txt)");
            defineOptionalProperty<bool>("ShowSegmentedResultImages", true, "Decide whether to calculate color-segmented images with the marker color and show them as result images. Causes small additional computational effort.");
            defineOptionalProperty<std::string>("TCPNameLeft", "TCP L", "Name of the TCP of the left robot hand.");
            defineOptionalProperty<std::string>("TCPNameRight", "TCP R", "Name of the TCP of the right robot hand.");
            defineOptionalProperty<std::string>("MarkerNameLeft", "Marker L", "Name of the marker ball at the left robot hand.");
            defineOptionalProperty<std::string>("MarkerNameRight", "Marker R", "Name of the marker ball at the right robot hand.");
            defineOptionalProperty<bool>("UseVision", true, "You can switch off the use of the camera images. The returned pose will then just be taken from the forward kinematics.");
            defineOptionalProperty<std::string>("RobotStateComponentName", "RobotStateComponent", "Name of the RobotStateComponent that should be used");
            defineOptionalProperty<float>("MaximalAcceptedDistanceFromForwardKinematics", 200, "Maximal distance that the localization result may have from the position determined by forward kinematics. If it is further away, it is discarded..");
        }
    };


    /**
     * HandMarkerLocalization uses the CVisualTargetLocator of IVT in order to recognize and localize the marker balls at the hands of the robot.
     * The returned hand pose takes the offset from marker to TCP into account, which is defined in the robot model file.
     * The marker color is read from PriorKnowledge and CommonStorage via MemoryX.
     * The hand localization is invoked automatically by the working memory if the object has been requested there.
     */
    class HandMarkerLocalization:
        virtual public ObjectLocalizerProcessor
    {
    public:

        HandMarkerLocalization();

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override
        {
            return armarx::PropertyDefinitionsPtr(new HandMarkerLocalizationPropertyDefinitions(getConfigIdentifier()));
        }

        /**
        * @see Component::getDefaultName()
        */
        std::string getDefaultName() const override
        {
            return "HandMarkerLocalization";
        }

    protected:
        /**
         * @see ObjectLocalizerProcessor::onInitObjectLocalizerProcessor()
         */
        void onInitObjectLocalizerProcessor() override;

        /**
         * Initialize stuff here?
         *
         * @see ObjectLocalizerProcessor::onConnectObjectLocalizerProcessor()
         */
        void onConnectObjectLocalizerProcessor() override;

        /**
         * @see ObjectLocalizerProcessor::onExitObjectLocalizerProcessor()
         */
        void onExitObjectLocalizerProcessor() override {}

        /**
         * Initializes segmentable recognition
         *
         * @return success
         */
        bool initRecognizer() override;

        /**
         * Add a memory entity representing the hand marker in order to set its properties
         *
         * @param objectClassEntity entity containing all information available for the object class
         * @param fileManager GridFileManager required to read files associated to prior knowledge from the database.
         *
         * @return success of adding this entity
         */
        bool addObjectClass(const memoryx::EntityPtr& objectClassEntity, const memoryx::GridFileManagerPtr& fileManager) override;

        /**
         * localize one or both hand markers
         *
         * @param objectClassNames is ignored here
         * @param cameraImages the two input images
         * @param resultImages the two result images. are provided if result images are enabled.
         *
         * @return list of object instances
         */
        memoryx::ObjectLocalizationResultList localizeObjectClasses(const std::vector<std::string>& objectClassNames, CByteImage** cameraImages, armarx::MetaInfoSizeBasePtr imageMetaInfo, CByteImage** resultImages) override;


    private:

        void drawCrossInImage(CByteImage* image, Eigen::Vector3f point, bool leftCamera);


        // decides whether images or forward kinematic will be used
        bool useVision;

        // locator for the tracking balls
        CVisualTargetLocator* visualTargetLocator;
        std::map<std::string, ObjectColor> markerColors;

        // remote robot for the orientation
        VirtualRobot::RobotPtr localRobot;
        std::string tcpNameLeft, tcpNameRight, markerNameLeft, markerNameRight;

        // visualization
        bool showSegmentedResultImages;
        CColorParameterSet* colorParameterSet;

        armarx::RobotStateComponentInterfacePrx robotStateComponent;


    };
}

