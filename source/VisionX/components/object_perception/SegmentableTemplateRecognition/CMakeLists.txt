armarx_component_set_name(SegmentableTemplateRecognition)

set(COMPONENT_LIBS VisionXObjectPerception
                   VisionXInterfaces
                   VisionXCore
                   VisionXTools
                   ArmarXCoreInterfaces
                   ArmarXCore
                   RobotAPICore
                   ArmarXCoreObservers
                   MemoryXInterfaces
                   MemoryXCore
                   MemoryXObjectRecognitionHelpers
                   MemoryXEarlyVisionHelpers
)

set(SOURCES SegmentableTemplateRecognition.cpp)
set(HEADERS SegmentableTemplateRecognition.h)
armarx_add_component("${SOURCES}" "${HEADERS}")

set(EXE_SOURCES main.cpp)
armarx_add_component_executable("${EXE_SOURCES}")
