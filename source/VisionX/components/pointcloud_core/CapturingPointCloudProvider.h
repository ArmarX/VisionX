/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::Core
 * @author     David Gonzalez Aguirre (david dot gonzalez at kit dot edu)
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <ArmarXCore/core/services/tasks/RunningTask.h>

#include "PointCloudProvider.h"
#include <VisionX/tools/FPSCounter.h>

#include <mutex>

namespace visionx
{

    class CapturingPointCloudProviderPropertyDefinitions:
        public PointCloudProviderPropertyDefinitions
    {
    public:
        CapturingPointCloudProviderPropertyDefinitions(std::string prefix):
            PointCloudProviderPropertyDefinitions(prefix)
        {
            defineOptionalProperty<float>("framerate", 30.0f, "framerate for the point clouds").setMin(0.0f).setMax(60.0f);
            defineOptionalProperty<bool>("isEnabled", true, "enable the capturing process immediately");
        }
    };



    /**
     * The CapturingPointCloudProvider provides a callback function to trigger
     * the capturing of point clouds with different synchronization modes.
     */
    class CapturingPointCloudProvider :
        virtual public PointCloudProvider,
        virtual public CapturingPointCloudProviderInterface
    {
    public:
        // ================================================================== //
        // == CapturingPointCloudProvider ice interface ========================== //
        // ================================================================== //
        /**
         * Starts point cloud capturing.
         *
         *
         * @throw visionx::PointCloudProviderResolutionNotSupportedException
         * @throw visionx::PointCloudProviderStartingCaptureFailedException
         */
        void startCapture(const Ice::Current& c = Ice::emptyCurrent) override;

        /**
         * Starts point cloud capturing.
         *
         * @param numFrames Automatically stop capture after this amount of captured frames
         *
         * @throw visionx::PointCloudProviderResolutionNotSupportedException
         * @throw visionx::PointCloudProviderStartingCaptureFailedException
         */
        void startCaptureForNumFrames(int numFrames, const Ice::Current& c = Ice::emptyCurrent) override;

        /**
         * Stops point cloud capturing. The Capturing can be started again by re-calling
         * startCapture(...).
         */
        void stopCapture(const Ice::Current& c = Ice::emptyCurrent) override;


        /**
         * set a new frame rate
         *
         * @param framesPerSecond Frames per second to capture.
         * @throw visionx::PointCloudProviderFrameRateNotSupportedException
         */
        void changeFrameRate(Ice::Float framesPerSecond, const Ice::Current& c = Ice::emptyCurrent) override;


        bool isCaptureEnabled(const Ice::Current& c = Ice::emptyCurrent) override;


    protected:

        // ================================================================== //
        // == Interface of PointCloudProvider ================================= //
        // ================================================================== //
        /**
         * This is called when the Component::onInitComponent() is called.
         *
         * Implement this method in the PointCloudProvider in order to setup its
         * parameters. Use this to set the Point cloud format.
         */
        virtual void onInitCapturingPointCloudProvider() = 0;

        /**
         * This is called when the Component::onConnectComponent() setup is called
         */
        virtual void onStartCapturingPointCloudProvider() { }

        /**
         * This is called when the Component::onExitComponent() setup is called
         *
         * Implement this method in the PointCloudProvider in order clean up right
         * before terminating.
         */
        virtual void onExitCapturingPointCloudProvider() = 0;

        /**
         * This is called when the point cloud provider capturing has been started.
         *
         * Implement this method in the PointCloudProvider in order to reset
         * prvider's state.
         *
         * @param framesPerSecond capturing fps
         *
         * @throw visionx::PointCloudProviderResolutionNotSupportedException
         * @throw visionx::PointCloudProviderFrameRateNotSupportedException
         * @throw visionx::PointCloudProviderStartingCaptureFailedException
         */
        virtual void onStartCapture(float framesPerSecond) = 0;

        /**
         * This is called when the point cloud provider capturing has been stopped.
         *
         * Implement this method in the PointCloudProvider in order to clean up after stopping.
         */
        virtual void onStopCapture() = 0;

        /**
         * Main capturing function.
         *
         * @param ppPointCloudBuffers    Point cloud pointer array where the captured point clouds need to be copied into
         */
        virtual bool doCapture() = 0;

        // ================================================================== //
        // == Utility methods for PointCloudProviders ============================ //
        // ================================================================== //
        /**
         * Sets the point cloud synchronization mode
         *
         * @param pointCloudSyncMode point cloud synchronization mode
         */
        void setPointCloudSyncMode(ImageSyncMode pointCloudSyncMode);

        /**
         * Returns the point cloud sync mode
         */
        ImageSyncMode getPointCloudSyncMode();


        // ================================================================== //
        // == RunningComponent implementation =============================== //
        // ================================================================== //
        /**
         * @see Component::onInitComponent()
         */
        void onInitPointCloudProvider() override;

        /**
         * @see Component::onConnectComponent()
         */
        void onConnectPointCloudProvider() override;

        /**
         * @see Component::onExitComponent()
         */
        void onExitPointCloudProvider() override;

        /**
         * @see capture method issued by RunningTask
         */
        virtual void capture();

    protected:
        /**
         * Capture thread
         */
        armarx::RunningTask<CapturingPointCloudProvider>::pointer_type captureTask;

        /**
         * Point cloud synchronization information
         */
        ImageSyncMode pointCloudSyncMode;

        /**
         * mutex for capturing for proper exit
         */
        mutable std::mutex captureMutex;

        /**
         * Required frame rate
         */
        float frameRate;

        /**
         * FPS manager
         */
        FPSCounter fpsCounter;

        /**
         * Indicates that capturing is enabled and running
         */
        std::atomic_bool captureEnabled;

        /**
         * Number of frames to capture
         */
        int numFramesToCapture;
    };
}

