/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::Tools
 * @author     Markus Grotz ( markus dot grotz at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <map>
#include <tuple>
#include <iostream>
#include <type_traits>

#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/common/io.h>
#include <pcl/io/pcd_io.h>

#include <pcl/common/colors.h>

#include <VisionX/interface/core/DataTypes.h>

// Some code was moved to: (do not include here)
// #include <VisionX/libraries/PointCloudTools/segments.h>


namespace visionx::tools
{

    template <typename PointCloudPtrT>
    std::tuple<uint8_t, uint8_t, uint8_t> colorizeSegment(PointCloudPtrT& segment)
    {
        const uint8_t r = rand() % 255;
        const uint8_t g = rand() % 255;
        const uint8_t b = rand() % 255;

        for (auto& p : segment->points)
        {
            p.r = r;
            p.g = g;
            p.b = b;
        }

        return std::make_tuple(r, g, b);
    }


    inline void colorizeLabeledPointCloud(pcl::PointCloud<pcl::PointXYZL>::Ptr sourceCloudPtr,
                                          pcl::PointCloud<pcl::PointXYZRGBA>::Ptr targetCloudPtr)
    {
        pcl::copyPointCloud(*sourceCloudPtr, *targetCloudPtr);

        std::map<uint32_t, float> colorMap;

        for (size_t i = 0; i < sourceCloudPtr->points.size(); i++)
        {
            pcl::PointXYZL p = sourceCloudPtr->points[i];

            if (!colorMap.count(p.label))
            {
                pcl::RGB c = pcl::GlasbeyLUT::at(p.label % pcl::GlasbeyLUT::size());
                colorMap.insert(std::make_pair(p.label, c.rgb));
            }

            targetCloudPtr->points[i].rgb = colorMap[p.label];
        }
    }
}
namespace visionx::tools::detail
{
    // Need a seperate method to be callable from pcl::PointCloud<>::Ptr versions.
    /**
     * @brief Fill `labelIndicesMap` with indices of each segment's points.
     * @param labeledCloud A labeled point cloud.
     * @param labeledPointMap The map to fill (label -> point indicies).
     * @param excludeZero If true, points with label == 0 are ignored.
     */
    template <class PointCloudT>
    void fillLabelMap(const PointCloudT& labeledCloud, std::map<uint32_t, pcl::PointIndices>& labelIndicesMap, bool excludeZero)
    {
        for (size_t i = 0; i < labeledCloud.points.size(); i++)
        {
            uint32_t currentLabel = labeledCloud.points[i].label;

            if (excludeZero && currentLabel == 0)
            {
                continue;
            }

            // With [] operator, std::map default-constructs a value if it is not already
            // present, which is exactly what we need here (i.e. no need to explicitly check
            // key presence).
            labelIndicesMap[currentLabel].indices.push_back(static_cast<int>(i));
        }
    }
}
namespace visionx::tools
{
    template <class T> struct is_shared_ptr : std::false_type {};
    template <class T> struct is_shared_ptr<boost::shared_ptr<T>> : std::true_type {};
    template <class T> struct is_shared_ptr<std::shared_ptr<T>> : std::true_type {};

    // Legacy: Support pcl::PointCloud<>::Ptr as template argument.
    /// @see detail::fillLabelMap()
    template <class LabeledPointCloudPtrT>
    typename std::enable_if<is_shared_ptr<LabeledPointCloudPtrT>::value, void>::type
    fillLabelMap(LabeledPointCloudPtrT labeledCloudPtr,
                      std::map<uint32_t, pcl::PointIndices>& labelIndicesMap,
                      bool excludeZero = true)
    {
        detail::fillLabelMap(*labeledCloudPtr, labelIndicesMap, excludeZero);
    }

    // Version for const pcl::PointCloud<>& .
    /// @see detail::fillLabelMap()
    template <class PointCloudT>
    typename std::enable_if<!is_shared_ptr<PointCloudT>::value, void>::type
    fillLabelMap(const PointCloudT& labeledCloud,
                      std::map<uint32_t, pcl::PointIndices>& labelIndicesMap,
                      bool excludeZero = true)
    {
        detail::fillLabelMap(labeledCloud, labelIndicesMap, excludeZero);
    }

    /// @see detail::fillLabelMap()
    template <class PointCloudT>
    std::map<uint32_t, pcl::PointIndices> getLabelMap(
        const PointCloudT& labeledCloud, bool excludeZero = true)
    {
        std::map<uint32_t, pcl::PointIndices> labelIndicesMap;
        detail::fillLabelMap(labeledCloud, labelIndicesMap, excludeZero);
        return labelIndicesMap;
    }
}

