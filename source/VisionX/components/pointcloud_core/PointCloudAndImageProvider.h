/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::Core
 * @author     Raphael Grimm (raphael dot grimm at kit dot edu)
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <VisionX/interface/components/PointCloudAndImageAndCalibrationProviderInterface.h>

#include <VisionX/core/ImageProvider.h>

#include "PointCloudProvider.h"

namespace visionx
{
    class PointCloudAndImageProvider :
        virtual public PointCloudAndImageProviderInterface,
        virtual public ImageProvider,
        virtual public PointCloudProvider
    {
    protected:
        void onInitComponent() final override
        {
            ImageProvider:: onInitComponent();
            PointCloudProvider:: onInitComponent();
            onInitPointCloudAndImageProvider();
        }
        void onConnectComponent() final override
        {
            ImageProvider:: onConnectComponent();
            PointCloudProvider:: onConnectComponent();
            onConnectPointCloudAndImageProvider();
        }
        void onDisconnectComponent() final override
        {
            ImageProvider:: onDisconnectComponent();
            PointCloudProvider:: onDisconnectComponent();
            onDisconnectPointCloudAndImageProvider();
        }
        void onExitComponent() final override
        {
            ImageProvider:: onExitComponent();
            PointCloudProvider:: onExitComponent();
            onExitPointCloudAndImageProvider();
        }

        // PointCloudProvider interface
    protected:
        void onInitPointCloudProvider() final override {}
        void onExitPointCloudProvider() final override {}
        void onConnectPointCloudProvider() final override {}

        // ImageProvider interface
    protected:
        void onInitImageProvider() final override {}
        void onExitImageProvider() final override {}
        void onConnectImageProvider() final override {}
        void onDisconnectImageProvider() final override {}
    protected:
        virtual void onInitPointCloudAndImageProvider() = 0;
        virtual void onExitPointCloudAndImageProvider() = 0;
        virtual void onConnectPointCloudAndImageProvider() = 0;
        virtual void onDisconnectPointCloudAndImageProvider() = 0;

        // ProviderWithSharedMemorySupportInterface interface
    public:
        bool hasSharedMemorySupport(const Ice::Current& c) override
        {
            return ImageProvider::hasSharedMemorySupport(c);
        }
    };
}
