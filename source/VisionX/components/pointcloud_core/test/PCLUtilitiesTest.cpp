/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::PointCloudFilter
 * @author     Markus Grotz ( markus dot grotz at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE VisionX::ArmarXObjects::PointCloudCore

#define ARMARX_BOOST_TEST

#include <VisionX/Test.h>
#include <VisionX/components/pointcloud_core/PCLUtilities.h>
#include <VisionX/components/pointcloud_core/pcl_point_operators.h>

#include <iostream>


namespace pcl
{
    bool operator==(const pcl::PointIndices& lhs, const pcl::PointIndices& rhs)
    {
        // Do not check header.
        return lhs.indices == rhs.indices;
    }
}

constexpr uint32_t POINT_CLOUD_SIZE = 64;


template <typename PointT>
void setPointXYZ(std::size_t i, PointT& point)
{
    point.x = i;
    point.y = 2 * i + 1;
    point.z = - 0.5f * (i * i);
}

template <typename PointT>
void setPointRGBA(std::size_t i, PointT& point)
{
    point.r = i % 2;
    point.g = i % 10;
    point.b = i % 42;
    point.a = i % 255;
}

template <typename PointT>
void setPointLabel(std::size_t i, PointT& point)
{
    point.label = i % 8;
}

template <typename PointCloudT>
void fillXYZ(PointCloudT& pointCloud)
{
    for (std::size_t i = 0; i < pointCloud.size(); ++i)
    {
        setPointXYZ(i, pointCloud[i]);
    }
}

template <typename PointCloudT>
void fillXYZPoint(PointCloudT& pointCloud)
{
    for (std::size_t i = 0; i < pointCloud.size(); ++i)
    {
        setPointXYZ(i, pointCloud[i].point);
    }
}

template <typename PointCloudT>
void fillRGBA(PointCloudT& pointCloud)
{
    for (std::size_t i = 0; i < pointCloud.size(); ++i)
    {
        setPointRGBA(i, pointCloud[i]);
    }
}

template <typename PointCloudT>
void fillRGBAColor(PointCloudT& pointCloud)
{
    for (std::size_t i = 0; i < pointCloud.size(); ++i)
    {
        setPointRGBA(i, pointCloud[i].color);
    }
}

template <typename PointCloudT>
void fillLabel(PointCloudT& pointCloud)
{
    for (std::size_t i = 0; i < pointCloud.size(); ++i)
    {
        setPointLabel(i, pointCloud[i]);
    }
}


struct Fixture_fillLabelMap
{
    using PointT = pcl::PointXYZL;

    pcl::PointCloud<PointT>::Ptr cloudPtr { new pcl::PointCloud<PointT>(POINT_CLOUD_SIZE, 1)};
    pcl::PointCloud<PointT>& cloud { *cloudPtr };

    std::map<uint32_t, pcl::PointIndices> labelMap;

    std::size_t numZeros = 0;

    Fixture_fillLabelMap()
    {
        fillXYZ(cloud);
        fillLabel(cloud);

        // Count zero labels.
        long nZeros = std::count_if(cloud.begin(), cloud.end(),
                                    [](const auto & p)
        {
            return p.label == 0;
        });
        // Check that there are any, otherwise this test would be ill-formed.
        BOOST_REQUIRE_GT(numZeros, 0);
        this->numZeros = static_cast<std::size_t>(nZeros);
    }

};


BOOST_FIXTURE_TEST_SUITE(Test_fillLabelMap, Fixture_fillLabelMap)


BOOST_AUTO_TEST_CASE(test_fillLabelMap_includeZero)
{
    const bool excludeZero = false;
    visionx::tools::fillLabelMap(cloud, labelMap, excludeZero);

    // Check that zeros are included.
    BOOST_CHECK_GT(labelMap.count(0), 0);

    // Check that all points are included and correctly assigned.
    std::size_t totalSize = 0;
    for (const auto& [label, indices] : labelMap)
    {
        totalSize += indices.indices.size();
        for (const auto& index : indices.indices)
        {
            BOOST_CHECK_EQUAL(cloud[static_cast<std::size_t>(index)].label, label);
        }
    }

    BOOST_CHECK_EQUAL(totalSize, cloud.size());
}


BOOST_AUTO_TEST_CASE(test_fillLabelMap_excludeZero)
{
    const bool excludeZero = true;
    visionx::tools::fillLabelMap(cloud, labelMap, excludeZero);

    // Check that zeros are not included.
    BOOST_CHECK_EQUAL(labelMap.count(0), 0);

    // Check that all points (except 0s) are included and correctly assigned.
    std::size_t totalSize = 0;
    for (const auto& [label, indices] : labelMap)
    {
        totalSize += indices.indices.size();
        for (const auto& index : indices.indices)
        {
            BOOST_CHECK_EQUAL(cloud[static_cast<std::size_t>(index)].label, label);
        }
    }

    BOOST_CHECK_EQUAL(totalSize + numZeros, cloud.size());
}


/// Test whether callable with pointer type and yields same result.
BOOST_AUTO_TEST_CASE(test_fillLabelMap_callable_with_pointer)
{
    const bool excludeZero = true;
    visionx::tools::fillLabelMap(cloud, labelMap, excludeZero);

    std::map<uint32_t, pcl::PointIndices> labelMapPtr;
    visionx::tools::fillLabelMap(cloudPtr, labelMapPtr, excludeZero);

    BOOST_CHECK_EQUAL_COLLECTIONS(labelMap.begin(), labelMap.end(),
                                  labelMapPtr.begin(), labelMapPtr.end());
}



BOOST_AUTO_TEST_SUITE_END()
