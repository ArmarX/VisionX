//
// Created by christoph on 05.06.19.
//

#ifndef ROBDEKON_COMMON_H
#define ROBDEKON_COMMON_H

#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <VisionX/libraries/PointCloudGraph/PointXYZRGBLNormal.h>
#include <VisionX/libraries/PointCloudGraph/point_cloud_graph.h>

namespace boost
{
    struct vertex_timestamp_t
    {
        typedef vertex_property_tag kind;
    };
    struct vertex_confidence_t
    {
        typedef vertex_property_tag kind;
    };

}

namespace armarx
{
    typedef pcl::PointXYZRGBL PointT;
    typedef pcl::PointXYZRGBLNormal PointWithNormalT;
    typedef pcl::PointCloud<PointT> PointCloudT;
    typedef pcl::PointCloud<PointWithNormalT> PointCloudWithNormalT;


    typedef pcl::graph::point_cloud_graph
    <PointWithNormalT,
    boost::vecS,
    boost::undirectedS,
    // vertex properties
    boost::property<boost::vertex_color_t, uint32_t,
    boost::property<boost::vertex_timestamp_t, double,
    boost::property<boost::vertex_confidence_t, float>>>,
    // edge properties
    boost::property<boost::edge_weight_t, float,
    boost::property<boost::edge_index_t, int> > > CloudGraphWithTimestamp;
    typedef pcl::graph::point_cloud_graph
    <PointWithNormalT,
    boost::vecS,
    boost::undirectedS,
    // vertex properties
    boost::property<boost::vertex_color_t, uint32_t>,
    // edge properties
    boost::property<boost::edge_weight_t, float,
    boost::property<boost::edge_index_t, int> > > CloudGraph;
    typedef boost::subgraph<CloudGraph> Graph;
    typedef boost::subgraph<CloudGraphWithTimestamp> GraphWithTimestamp;

    typedef boost::shared_ptr<Graph> GraphPtr;
    typedef boost::shared_ptr<const Graph> GraphConstPtr;
    typedef boost::reference_wrapper<Graph> GraphRef;
    typedef boost::shared_ptr<GraphWithTimestamp> GraphWithTimestampPtr;
    typedef boost::shared_ptr<const GraphWithTimestamp> GraphWithTimestampConstPtr;
    typedef boost::reference_wrapper<GraphWithTimestamp> GraphWithTimestampRef;

    typedef typename boost::graph_traits<Graph>::vertex_descriptor VertexId;
    typedef typename boost::graph_traits<GraphWithTimestamp>::vertex_descriptor VertexWTsId;
    typedef typename boost::graph_traits<Graph>::edge_descriptor EdgeId;
    typedef typename boost::graph_traits<Graph>::vertex_iterator VertexIterator;
    typedef typename boost::graph_traits<Graph>::edge_iterator EdgeIterator;
    typedef typename boost::graph_traits<Graph>::adjacency_iterator AdjacencyIterator;
    typedef boost::property_map<CloudGraphWithTimestamp, boost::vertex_timestamp_t>::type TimestampMap;
    typedef boost::property_map<CloudGraphWithTimestamp, boost::vertex_confidence_t>::type ConfidenceMap;
}

#endif //ROBDEKON_COMMON_H
