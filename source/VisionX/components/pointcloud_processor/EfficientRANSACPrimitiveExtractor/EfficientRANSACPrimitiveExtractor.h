/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::EfficientRANSACPrimitiveExtractor
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <ArmarXCore/util/CPPUtility/TripleBuffer.h>
#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/interface/observers/ObserverInterface.h>
#include <ArmarXCore/libraries/ArmarXCoreComponentPlugins/DebugObserverComponentPlugin.h>

#include <ArmarXGui/libraries/ArmarXGuiComponentPlugins/RemoteGuiComponentPlugin.h>

#include <RobotAPI/libraries/RobotAPIComponentPlugins/RobotStateComponentPlugin.h>
#include <RobotAPI/libraries/RobotAPIComponentPlugins/ArVizComponentPlugin.h>

#include <VisionX/components/pointcloud_core/PointCloudProcessor.h>
#include <VisionX/interface/core/PointCloudProcessorInterface.h>

namespace armarx
{
    /**
     * @class EfficientRANSACPrimitiveExtractorPropertyDefinitions
     * @brief Property definitions of `EfficientRANSACPrimitiveExtractor`.
     */
    class EfficientRANSACPrimitiveExtractorPropertyDefinitions :
        public visionx::PointCloudProcessorPropertyDefinitions
    {
    public:
        EfficientRANSACPrimitiveExtractorPropertyDefinitions(std::string prefix);
    };



    /**
     * @defgroup Component-EfficientRANSACPrimitiveExtractor EfficientRANSACPrimitiveExtractor
     * @ingroup VisionX-Components
     * A description of the component EfficientRANSACPrimitiveExtractor.
     *
     * @class EfficientRANSACPrimitiveExtractor
     * @ingroup Component-EfficientRANSACPrimitiveExtractor
     * @brief Brief description of class EfficientRANSACPrimitiveExtractor.
     *
     * Detailed description of class EfficientRANSACPrimitiveExtractor.
     */
    class EfficientRANSACPrimitiveExtractor :
        virtual public visionx::PointCloudProcessor,
        virtual public RemoteGuiComponentPluginUser,
        virtual public RobotStateComponentPluginUser,
        virtual public DebugObserverComponentPluginUser,
        virtual public plugins::ArVizComponentPluginUser
    {
    public:

        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;


    protected:
        RemoteGui::WidgetPtr buildGui();
        void processGui(RemoteGui::TabProxy& prx);
        void process() override;
        template<class PointType> void process();

        void onInitPointCloudProcessor() override;
        void onConnectPointCloudProcessor() override;
        void onDisconnectPointCloudProcessor() override;
        void onExitPointCloudProcessor() override;

        /// @see PropertyUser::createPropertyDefinitions()
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

    private:
        //provider data
        std::string                                 _pointCloudProviderName;
        visionx::PointCloudProviderInfo             _pointCloudProviderInfo;
        visionx::PointCloudProviderInterfacePrx     _pointCloudProvider;
        std::string                                 _pointCloudProviderRefFrame;

        //other
        RobotStateComponentPlugin::RobotData        _localRobot;

        struct Config
        {
            Config() {}

            int outlierMeanK = 50;
            float outlierStddevMulThresh = 1.0;

            float ransacEpsilon = 25;
            float ransacNormalThresh = 0.9f;
            int ransacMinSupport = 100;
            float ransacBitmapEpsilon = 50;
            float ransacProbability = 0.001f;
            int ransacMaxruntimeMs = 15000;
        };
        TripleBuffer<Config> _cfgBuf;
    };
}
