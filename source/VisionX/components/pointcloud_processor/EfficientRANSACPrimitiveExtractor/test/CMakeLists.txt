
# Libs required for the tests
SET(LIBS ${LIBS} ArmarXCore EfficientRANSACPrimitiveExtractor)
 
armarx_add_test(EfficientRANSACPrimitiveExtractorTest EfficientRANSACPrimitiveExtractorTest.cpp "${LIBS}")
