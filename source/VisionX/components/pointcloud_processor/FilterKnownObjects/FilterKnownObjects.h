/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::FilterKnownObjects
 * @author     Markus Grotz ( markus dot grotz at kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <pcl/point_types.h>

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/interface/observers/ObserverInterface.h>

#include <VisionX/components/pointcloud_core/PointCloudProcessor.h>
#include <RobotAPI/libraries/ArmarXObjects/plugins/ObjectPoseClientPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiComponentPlugins/LightweightRemoteGuiComponentPlugin.h>
#include <RobotAPI/libraries/RobotAPIComponentPlugins/ArVizComponentPlugin.h>



#include <pcl/filters/crop_box.h>

namespace armarx
{
    /**
     * @defgroup Component-FilterKnownObjects FilterKnownObjects
     * @ingroup VisionX-Components
     * A description of the component FilterKnownObjects.
     *
     * @class FilterKnownObjects
     * @ingroup Component-FilterKnownObjects
     * @brief Brief description of class FilterKnownObjects.
     *
     * Detailed description of class FilterKnownObjects.
     */
    class FilterKnownObjects :
        virtual public visionx::PointCloudProcessor, 
        virtual public armarx::ObjectPoseClientPluginUser,
        virtual public armarx::LightweightRemoteGuiComponentPluginUser,
        virtual public armarx::ArVizComponentPluginUser
    {
        /// The used point type.
        using PointT = pcl::PointXYZRGBL;

    public:

        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;

        static std::string GetDefaultName()
        {
            return "FilterKnownObjects";
        }


    protected:

        /// @see PropertyUser::createPropertyDefinitions()
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        /// @see visionx::PointCloudProcessor::onInitPointCloudProcessor()
        void onInitPointCloudProcessor() override;

        /// @see visionx::PointCloudProcessor::onConnectPointCloudProcessor()
        void onConnectPointCloudProcessor() override;

        /// @see visionx::PointCloudProcessor::onDisconnectPointCloudProcessor()
        void onDisconnectPointCloudProcessor() override;

        /// @see visionx::PointCloudProcessor::onExitPointCloudProcessor()
        void onExitPointCloudProcessor() override;

        /// @see visionx::PointCloudProcessor::process()
        void process() override;


        /// This function should be called once in onConnect() or when you
        /// need to re-create the Remote GUI tab.
        void createRemoteGuiTab();

        /// After calling `RemoteGui_startRunningTask`, this function is
        /// called periodically in a separate thread. If you update variables,
        /// make sure to synchronize access to them.
        void RemoteGui_update() override;





    private:

        pcl::CropBox<PointT> filter;

        struct Properties
        {
            std::string objectInstanceMemoryName = "Object";
        

            float resizeOBBFactor = 10.0;

            bool visualization = false;

        };
        Properties properties;
        std::mutex propertiesMutex;

        // Tab shown in the Remote GUI.
        struct RemoteGuiTab : armarx::RemoteGui::Client::Tab
        {
            //armarx::RemoteGui::Client::LineEdit boxLayerName;
            //armarx::RemoteGui::Client::IntSpinBox numBoxes;

            armarx::RemoteGui::Client::Button extractGrasps;
        };
        RemoteGuiTab tab;


        std::mutex arvizMutex;



    };
}

