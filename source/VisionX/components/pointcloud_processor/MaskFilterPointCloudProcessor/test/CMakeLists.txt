
# Libs required for the tests
SET(LIBS ${LIBS} ArmarXCore MaskFilterPointCloudProcessor)
 
armarx_add_test(MaskFilterPointCloudProcessorTest MaskFilterPointCloudProcessorTest.cpp "${LIBS}")