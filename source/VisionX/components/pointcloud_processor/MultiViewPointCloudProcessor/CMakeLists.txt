armarx_component_set_name("MultiViewPointCloudProcessor")

set(COMPONENT_LIBS
    ArmarXCoreComponentPlugins
    ArmarXGuiComponentPlugins
    RobotAPIComponentPlugins
    VisionXPointCloudTools
    VisionXPointCloud
    ${PCL_FILTERS_LIBRARY}
)

set(SOURCES MultiViewPointCloudProcessor.cpp)
set(HEADERS MultiViewPointCloudProcessor.h)

armarx_add_component("${SOURCES}" "${HEADERS}")

# add unit tests
add_subdirectory(test)

armarx_generate_and_add_component_executable(APPLICATION_APP_SUFFIX)
