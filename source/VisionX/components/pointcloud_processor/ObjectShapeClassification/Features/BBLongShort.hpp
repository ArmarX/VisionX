/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <vector>
#include <limits>
#include <math.h>

#include "BBFeature.hpp"

class BBLongShortRatio : public BBFeature
{
public:
    BBLongShortRatio() {}
    BBLongShortRatio(const std::vector<Eigen::Vector3f>& points)
        : BBFeature(points)
    {
        m_name = "BBLongShortRatio";

        m_longShortRatio = m_longSide / m_shortSide;
    }

    BBLongShortRatio(const std::pair<std::string, std::vector<Eigen::Vector3f>>& points)
        : BBFeature(points)
    {
        m_name = "BBLongShortRatio";

        m_longShortRatio = m_longSide / m_shortSide;
    }

    double longShortRatio() const
    {
        return m_longShortRatio;
    }

    FeaturePtr calculate(const Points& points)
    {
        return FeaturePtr(new BBLongShortRatio(points));
    }
    FeaturePtr calculate(const TaggedPoints& points)
    {
        return FeaturePtr(new BBLongShortRatio(points));
    }

    double compare(const Feature& other) const
    {
        const BBLongShortRatio* casted = dynamic_cast<const BBLongShortRatio*>(&other);

        if (casted)
        {
            return std::pow(m_longShortRatio - casted->longShortRatio(), 2.0);
        }
        else
        {
            return std::numeric_limits<double>::max();
        }
    }

    virtual void serialize(const ObjectSerializerBasePtr& serializer, const Ice::Current&) const
    {
        AbstractObjectSerializerPtr obj = AbstractObjectSerializerPtr::dynamicCast(serializer);

        AbstractObjectSerializerPtr featureObj;

        //Check if object already has a "features" field, else create a new one
        if (obj->hasElement(FEATURE_FIELD_NAME))
        {
            featureObj = obj->getElement(FEATURE_FIELD_NAME);
            featureObj->setDouble(m_name, m_longShortRatio);
        }
        else
        {
            featureObj = obj->createElement();
            featureObj->setDouble(m_name, m_longShortRatio);
            obj->setElement(FEATURE_FIELD_NAME, featureObj);
        }
    }

    virtual void deserialize(const ObjectSerializerBasePtr& serializer, const Ice::Current&)
    {
        AbstractObjectSerializerPtr obj = AbstractObjectSerializerPtr::dynamicCast(serializer);
        AbstractObjectSerializerPtr featureObj = obj->getElement(FEATURE_FIELD_NAME);
        //Now copy the result array from the DB
        m_longShortRatio = featureObj->getDouble(m_name);
    }

    virtual std::ostream& output(std::ostream& out) const
    {
        return out << m_longShortRatio;
    }

private:
    double m_longShortRatio;
};

