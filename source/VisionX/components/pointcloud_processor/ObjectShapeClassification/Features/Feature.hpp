/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <string>
#include <Eigen/Core>
#include <iostream>
#include <limits>
#include <utility>
#include <memory>

#include <VisionX/interface/components/ObjectShapeClassification.h>

#include <ArmarXCore/observers/AbstractObjectSerializer.h>
#include <ArmarXCore/interface/observers/Serialization.h>
#include <ArmarXCore/observers/variant/Variant.h>

using namespace visionx;
using namespace armarx;

const std::string FEATURE_FIELD_NAME = "features";

class Feature : public FeatureBase
{
public:
    using Points = std::vector<Eigen::Vector3f>;
    using TaggedPoints = std::pair<std::string, Points>;
    using FeaturePtr = std::shared_ptr<Feature>;

    virtual std::string name()
    {
        return m_name;
    }
    virtual FeaturePtr calculate(const Points& points) = 0;
    virtual FeaturePtr calculate(const TaggedPoints& points)
    {
        return calculate(points.second);
    }
    virtual double compare(const Feature&) const = 0;

    virtual void serialize(const ObjectSerializerBasePtr& serializer, const Ice::Current&) const = 0;
    virtual void deserialize(const ObjectSerializerBasePtr& serializer, const Ice::Current&) = 0;

    //TODO: properly implement this functions
    virtual VariantDataClassPtr clone(const Ice::Current&) const
    {
        return VariantDataClassPtr();
    }
    virtual std::string output(const Ice::Current&) const
    {
        return "Feature";
    }
    virtual VariantTypeId getType(const Ice::Current&) const
    {
        return 0;
    }
    virtual bool validate(const Ice::Current&)
    {
        return true;
    }

    virtual std::ostream& output(std::ostream& out) const = 0;
    friend std::ostream& operator<<(std::ostream& os, const Feature& f);

protected:
    std::string m_name;
};

using FeaturePtr = std::shared_ptr<Feature>;

std::ostream& operator<<(std::ostream& os, const Feature& f)
{
    return f.output(os);
}

