#pragma once

#include <functional>
#include <Eigen/Core>
#include <vector>
#include <string>
#include <map>
#include <iostream>

using TaggedPoints = std::pair<std::string, std::vector<Eigen::Vector3f>>;

template <typename R>
std::function<R(TaggedPoints)> memoized(R(*fn)(const std::vector<Eigen::Vector3f>&))
{
    /*static*/ std::map<std::string, R> table;
    return [table, fn](const TaggedPoints & points) mutable -> R
    {
        auto argt = points.first;
        auto memoized = table.find(argt);

        if (memoized == table.end())
        {
            auto result = fn(points.second);
            table[argt] = result;
            return result;
        }
        else
        {
            return memoized->second;
        }
    };
}

