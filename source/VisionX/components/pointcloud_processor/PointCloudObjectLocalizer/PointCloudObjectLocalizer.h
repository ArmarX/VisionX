/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::PointCloudObjectLocalizer
 * @author     Markus Grotz ( markus dot grotz at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <Eigen/Core>

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/system/ArmarXDataPath.h>


#include <RobotAPI/interface/core/PoseBase.h>
#include <RobotAPI/libraries/core/FramedPose.h>

#include <RobotAPI/interface/visualization/DebugDrawerInterface.h>

#include <MemoryX/interface/core/ProbabilityMeasures.h>
#include <MemoryX/core/MemoryXCoreObjectFactories.h>

#include <VisionX/components/pointcloud_core/PointCloudProcessor.h>
#include <VisionX/components/pointcloud_core/PCLUtilities.h>
#include <VisionX/interface/components/SimpleLocation.h>

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpedantic"
#include <pcl/point_types.h>

#include <pcl/common/transforms.h>

#include <pcl/io/pcd_io.h>

#include <pcl/filters/filter.h>
#include <pcl/filters/passthrough.h>
#include <pcl/filters/approximate_voxel_grid.h>

#include <pcl/features/normal_3d.h>
#include <pcl/features/normal_3d_omp.h>
#include <pcl/features/fpfh_omp.h>
#include <pcl/features/shot_omp.h>
#include <pcl/search/kdtree.h>
#include <pcl/kdtree/kdtree_flann.h>

#include <pcl/registration/icp.h>
#include <pcl/registration/gicp6d.h>

#include <pcl/correspondence.h>
#include <pcl/recognition/cg/geometric_consistency.h>
#pragma GCC diagnostic pop

#include <limits>

namespace visionx
{


    using PointT = pcl::PointXYZRGBA;
    using PointL = pcl::PointXYZRGBL;
    using PointD = pcl::FPFHSignature33;
    //using PointD = pcl::SHOT352;

    /**
     * @class PointCloudObjectLocalizerPropertyDefinitions
     * @brief
     */
    class PointCloudObjectLocalizerPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        PointCloudObjectLocalizerPropertyDefinitions(std::string prefix):
            armarx::ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("agentName", "Armar3", "Name of the agent for which the sensor values are provided");
            defineOptionalProperty<std::string>("modelFileName", "VisionX/examples/point_cloud_models/wateringcan2.pcd", "path to the model to match");
            defineOptionalProperty<std::string>("providerName", "PointCloudSegmenterResult", "name of the point cloud provider");
            defineOptionalProperty<double>("recGCSize", 5.0f, "the consensus set resolution");
            defineOptionalProperty<int>("recGCThreshold", 20, "minimum cluster size");
            defineOptionalProperty<float>("leafSize", 6.0f, "the voxel grid leaf size");
            defineOptionalProperty<float>("sceneLeafSize", 3.0f, "the voxel grid leaf size");
            defineOptionalProperty<bool>("icpOnly", true, "");
            defineOptionalProperty<std::string>("DebugDrawerTopic", "DebugDrawerUpdates", "Name of the DebugDrawerTopic");
            defineOptionalProperty<std::string>("sourceNodeName", armarx::GlobalFrame, "the robot node to use as source coordinate system for the captured point clouds");
            defineOptionalProperty<std::string>("objectClassName", "wateringcan", "Name of the objectClass this component localizes.");

        }
    };

    /**

      http://pointclouds.org/documentation/tutorials/global_hypothesis_verification.php
     * @class PointCloudObjectLocalizer
     *
     * @ingroup VisionX-Components
     * @brief A brief description
     *
     *
     * Detailed Description
     */
    class PointCloudObjectLocalizer :
        virtual public armarx::SimpleLocationInterface,
        virtual public PointCloudProcessor
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override
        {
            return "PointCloudObjectLocalizer";
        }


        void getLocation(armarx::FramedOrientationBasePtr& orientation, armarx::FramedPositionBasePtr& position, const Ice::Current& c = Ice::emptyCurrent) override;

        memoryx::ObjectLocalizationResultList localizeObjectClasses(const memoryx::ObjectClassNameList& objectClassNames, const Ice::Current& c = Ice::emptyCurrent) override;

    protected:
        /**
         * @see visionx::PointCloudProcessor::onInitPointCloudProcessor()
         */
        void onInitPointCloudProcessor() override;

        /**
         * @see visionx::PointCloudProcessor::onConnectPointCloudProcessor()
         */
        void onConnectPointCloudProcessor() override;

        /**
         * @see visionx::PointCloudProcessor::onDisconnectPointCloudProcessor()
         */
        void onDisconnectPointCloudProcessor() override;

        /**
         * @see visionx::PointCloudProcessor::onExitPointCloudProcessor()
         */
        void onExitPointCloudProcessor() override;

        /**
         * @see visionx::PointCloudProcessor::process()
         */
        void process() override;

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;


    private:
        std::mutex pointCloudMutex;

        bool icpOnly;

        std::string agentName;

        std::string providerName;
        std::string sourceNodeName;

        pcl::NormalEstimationOMP<PointT, pcl::Normal> normalEstimation;
        pcl::FPFHEstimationOMP<PointT, pcl::Normal, PointD> featureEstimation;
        //pcl::SHOTEstimationOMP<PointT, pcl::Normal, PointD> featureEstimation;

        pcl::KdTreeFLANN<PointD> matchSearchTree;
        pcl::GeometricConsistencyGrouping<PointT, PointT> clusterer;
        pcl::IterativeClosestPoint<PointT, PointT> icp;

        //pcl::GeneralizedIterativeClosestPoint6D icp;

        pcl::PointCloud<PointD>::Ptr modelDescriptors;
        pcl::PointCloud<PointT>::Ptr modelKeypoints;

        pcl::ApproximateVoxelGrid<PointT> grid;

        std::mutex localizeMutex;

        armarx::FramedPosePtr pose;

        armarx::DebugDrawerInterfacePrx debugDrawerPrx;

    };
}

