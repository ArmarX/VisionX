armarx_component_set_name("PointCloudRecorder")
set(COMPONENT_LIBS
    RobotAPICore
    VisionXCore
    VisionXPointCloud
)
set(SOURCES PointCloudRecorder.cpp)
set(HEADERS PointCloudRecorder.h)
armarx_add_component("${SOURCES}" "${HEADERS}")

armarx_generate_and_add_component_executable(APPLICATION_APP_SUFFIX)
