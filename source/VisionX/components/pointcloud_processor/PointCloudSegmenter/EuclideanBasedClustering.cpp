/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX
 * @author     Eren Aksoy ( eren dot aksoy at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "EuclideanBasedClustering.h"



EuclideanBasedClustering::EuclideanBasedClustering()
{
    labeledCloud.reset(new pcl::PointCloud<pcl::PointXYZRGBL>());
}

pcl::PointCloud<pcl::PointXYZRGBL>::Ptr& EuclideanBasedClustering::GetLabeledPointCloud(pcl::PointCloud<PointO>::Ptr& CloudPtr)
{

    pcl::PointCloud<PointO>::Ptr filteredCloudPtr(new pcl::PointCloud<PointO>);
    pcl::search::KdTree<PointO>::Ptr tree(new pcl::search::KdTree<PointO> ());
    pcl::ModelCoefficients BackGroundCoefficients;

    std::cout << " inputCloudPtr size: " << CloudPtr->points.size() << std::endl;

    // apply pass through filtering in z axis
    pcl::PassThrough<PointO> pass;
    pass.setInputCloud(CloudPtr);
    pass.setFilterFieldName("z");
    pass.setFilterLimits(0.0, 1700);
    pass.filter(*filteredCloudPtr);

    std::cout  << " filteredCloudPtr size: " << filteredCloudPtr->points.size() << std::endl;

    // normal estimation
    pcl::NormalEstimation<PointO, pcl::Normal> ne;
    ne.setSearchMethod(tree);
    ne.setKSearch(20);  //Use 20 nearest neighbors

    // extract background
    pcl::SACSegmentationFromNormals<PointO, pcl::Normal> segmentor;
    segmentor.setOptimizeCoefficients(true);
    segmentor.setModelType(pcl::SACMODEL_NORMAL_PLANE);
    segmentor.setMethodType(pcl::SAC_RANSAC);
    segmentor.setProbability(0.99);
    segmentor.setEpsAngle(pcl::deg2rad(1.0));
    segmentor.setRadiusLimits(0.0, 50.0);
    segmentor.setMaxIterations(100);
    segmentor.setDistanceThreshold(10); // Points at less than 1cm over the plane are part of the table


    for (int i = 0; i < 4; i++)
    {
        pcl::PointIndices::Ptr inliers(new pcl::PointIndices);
        pcl::PointCloud<pcl::Normal>::Ptr normals(new pcl::PointCloud<pcl::Normal>);

        ne.setInputCloud(filteredCloudPtr);
        ne.compute(*normals);

        segmentor.setInputCloud(filteredCloudPtr);
        segmentor.setInputNormals(normals);
        segmentor.segment(*inliers, BackGroundCoefficients);

        if (inliers->indices.size() > 10000)
        {
            // Extract the planar inliers from the input cloud
            pcl::ExtractIndices<PointO> extract;
            extract.setInputCloud(filteredCloudPtr);
            extract.setIndices(inliers);
            extract.setNegative(true);
            extract.filter(*filteredCloudPtr);
        }
        else
        {
            break;
        }
    }

    std::cout  << " filteredCloudPtr size after plane removal: " << filteredCloudPtr->points.size() << std::endl;


    // clustering based on euclidean distances
    tree->setInputCloud(filteredCloudPtr);
    std::vector<pcl::PointIndices> ObjectClusters;
    pcl::EuclideanClusterExtraction<PointO> ec;
    ec.setClusterTolerance(50);
    ec.setMinClusterSize(50);
    ec.setMaxClusterSize(1000000);
    ec.setInputCloud(filteredCloudPtr);
    ec.setSearchMethod(tree);
    ec.extract(ObjectClusters);

    std::cout << " Number of ObjectClusters: " << ObjectClusters.size() <<  std::endl;

    labeledCloud->points.clear();
    int label = 0;

    for (std::vector<pcl::PointIndices>::const_iterator it = ObjectClusters.begin(); it != ObjectClusters.end(); ++it)
    {
        if (it->indices.size() < 3000)
        {
            continue;
        }
        label++;
        std::cout << " ObjectClusters size: " << it->indices.size() <<  std::endl;
        for (std::vector<int>::const_iterator pit = it->indices.begin(); pit != it->indices.end(); ++pit)
        {
            pcl::PointXYZRGBL currPoint;

            currPoint.x = filteredCloudPtr->points[*pit].x;
            currPoint.y = filteredCloudPtr->points[*pit].y;
            currPoint.z = filteredCloudPtr->points[*pit].z;
            currPoint.rgba = filteredCloudPtr->points[*pit].rgba;
            currPoint.label = label;

            labeledCloud->points.push_back(currPoint);
        }
    }


    labeledCloud->height = 1;
    labeledCloud->width = labeledCloud->points.size();

    return labeledCloud;
}



