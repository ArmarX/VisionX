/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * package    navigation::dynamic_distance_to_obstacle_costmap_provider
 * author     Fabian Reister ( fabian dot reister at kit dot edu )
 * date       2022
 * copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *            GNU General Public License
 */


#pragma once

#include <RobotAPI/interface/armem/client/MemoryListenerInterface.ice>
#include <VisionX/interface/components/PointCloudToArViz.ice>

module visionx {  module components {  module point_cloud_to_arviz 
{

    interface ComponentInterface extends visionx::PointCloudToArVizInterface, armarx::armem::client::MemoryListenerInterface, visionx::PointCloudProcessorInterface
    {
	      // Define your interface here.
    };

};};};
