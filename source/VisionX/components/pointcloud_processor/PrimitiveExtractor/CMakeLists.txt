armarx_component_set_name("PrimitiveExtractor")

find_package(AffordanceKit QUIET)
armarx_build_if(AffordanceKit_FOUND "AffordanceKit not available")

set(COMPONENT_LIBS
    VisionXPointCloud
    MemoryXCore
    MemoryXMemoryTypes
    RobotAPICore
    AffordanceKitArmarX
)
set(SOURCES PrimitiveExtractor.cpp PrimitiveFusion.cpp PrimitiveFilter.cpp)
set(HEADERS PrimitiveExtractor.h   PrimitiveFusion.h   PrimitiveFilter.h)
armarx_add_component("${SOURCES}" "${HEADERS}")

armarx_generate_and_add_component_executable(APPLICATION_APP_SUFFIX)

if(AffordanceKit_FOUND)
    target_link_libraries(PrimitiveExtractor PRIVATE AffordanceKit::AffordanceKit)
endif()
