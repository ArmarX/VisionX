/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    SceneUnderstanding::ArmarXObjects::UserAssistedSegmenter
 * @author     Rainer Kartmann ( rainer dot kartmann at student dot kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/services/tasks/PeriodicTask.h>

#include <VisionX/components/pointcloud_core/PointCloudProcessor.h>
#include <VisionX/interface/components/UserAssistedSegmenter.h>


namespace visionx
{
    /// UserAssistedSegmenterPropertyDefinitions
    class UserAssistedSegmenterPropertyDefinitions :
        public visionx::PointCloudProcessorPropertyDefinitions
    {
    public:
        UserAssistedSegmenterPropertyDefinitions(std::string prefix);
    };


    /**
     * @defgroup Component-UserAssistedSegmenter UserAssistedSegmenter
     * @ingroup VisionX-Components
     *
     * UserAssistedSegmenter is used to manually group segments of an over-
     * segmentation (e.g. LCCP, Region-Growing) to new segments.
     *
     *
     * @class UserAssistedSegmenter
     * @ingroup Component-UserAssistedSegmenter
     * @brief Brief description of class UserAssistedSegmenter.
     *
     * Detailed description of class UserAssistedSegmenter.
     */
    class UserAssistedSegmenter :
        virtual public visionx::PointCloudProcessor,
        virtual public visionx::UserAssistedSegmenterInterface
    {
        using PointL = pcl::PointXYZRGBL;
        using PointCloudL = pcl::PointCloud<PointL>;
        using PointCloudLPtr = PointCloudL::Ptr;


    public:

        /// @see armarx::ManagedIceObject::getDefaultName()
        virtual std::string getDefaultName() const override;

        // UserAssistedSegmenterInterface interface
        virtual void publishSegmentation(const visionx::ColoredLabeledPointCloud& pointCloud,
                                         const Ice::Current& = Ice::emptyCurrent) override;

        void provideResultPointCloud();


    protected:

        /// @see visionx::PointCloudProcessor::onInitPointCloudProcessor()
        virtual void onInitPointCloudProcessor() override;

        /// @see visionx::PointCloudProcessor::onConnectPointCloudProcessor()
        virtual void onConnectPointCloudProcessor() override;

        /// @see visionx::PointCloudProcessor::onDisconnectPointCloudProcessor()
        virtual void onDisconnectPointCloudProcessor() override;

        /// @see visionx::PointCloudProcessor::onExitPointCloudProcessor()
        virtual void onExitPointCloudProcessor() override;

        /// @see visionx::PointCloudProcessor::process()
        virtual void process() override;


    private:

        std::string resultPointCloudName;

        float publishFrequency;

        visionx::UserAssistedSegmenterListenerPrx updatesListener;

        PointCloudLPtr resultPointCloud;

        armarx::PeriodicTask<UserAssistedSegmenter>::pointer_type provideTask;


    protected:

        /// @see PropertyUser::createPropertyDefinitions()
        virtual armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

    };
}

