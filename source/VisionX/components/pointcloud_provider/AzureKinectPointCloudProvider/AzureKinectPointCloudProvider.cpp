/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::AzureKinectPointCloudProvider
 * @author     Mirko Wächter
 * @author     Christian R. G. Dreher <c.dreher@kit.edu>
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "AzureKinectPointCloudProvider.h"
#include "ArmarXCore/core/logging/Logging.h"


// STD/STL
#include <cstdio>
#include <functional>

// IVT
#include <Image/ImageProcessor.h>
#include <Calibration/Calibration.h>

// ArmarX
#include <ArmarXCore/core/time/TimeUtil.h>
#include <ArmarXCore/core/time/ScopedStopWatch.h>
#include <ArmarXCore/core/time/StopWatch.h>
#include <ArmarXCore/observers/variant/Variant.h>
#include <ArmarXCore/util/CPPUtility/trace.h>

// VisionX
#include <VisionX/tools/ImageUtil.h>
#include <VisionX/tools/TypeMapping.h>
#include <VisionX/libraries/imrec/helper.h>

#include <opencv2/core/hal/interface.h>
#include <utility>

// OpenCV
#include <opencv2/imgproc/imgproc.hpp>

namespace
{
    /**
     * @brief Converts a k4a image to an IVT image.
     * @param color_image Reference to a k4a image containing the color data that should be converted.
     * @param result IVT image the converted image should be stored in. The size of this image and the color_image need to match.
     */
    [[maybe_unused]] void
    k4aToIvtImage(const k4a::image& color_image, ::CByteImage& result)
    {
        auto cw = static_cast<unsigned int>(color_image.get_width_pixels());
        auto ch = static_cast<unsigned int>(color_image.get_height_pixels());
        ARMARX_CHECK_EQUAL(static_cast<unsigned int>(result.width), cw);
        ARMARX_CHECK_EQUAL(static_cast<unsigned int>(result.height), ch);


        auto color_buffer = color_image.get_buffer();
        auto rgb_buffer_ivt = result.pixels;

        // Index in the IVT image. This value will be increased by 3 per pixel.
        int index_ivt = 0;

        // Index in the k4a image. This value increases by 4 per pixel.
        int index_k4a = 0;

        for (unsigned int y = 0; y < ch; ++y)
        {
            for (unsigned int x = 0; x < cw; ++x)
            {
                rgb_buffer_ivt[index_ivt] = color_buffer[index_k4a + 2];
                rgb_buffer_ivt[index_ivt + 1] = color_buffer[index_k4a + 1];
                rgb_buffer_ivt[index_ivt + 2] = color_buffer[index_k4a + 0];
                index_ivt += 3;
                index_k4a += 4;
            }
        }
    }

    std::function<void(armarx::Duration)>
    createSwCallback(
        armarx::ManagedIceObject* obj,
        const std::string& description)
    {
        return [ = ](armarx::Duration duration)
        {
            std::string name = "duration " + description + " in [ms]";
            obj->setMetaInfo(name, new armarx::Variant{duration.toMilliSecondsDouble()});
        };
    }

}

namespace visionx
{

    AzureKinectPointCloudProviderPropertyDefinitions::AzureKinectPointCloudProviderPropertyDefinitions(
        std::string prefix) :
        visionx::CapturingPointCloudProviderPropertyDefinitions(std::move(prefix))
    {
        defineOptionalProperty<k4a_color_resolution_t>("ColorResolution", K4A_COLOR_RESOLUTION_720P,
                "Resolution of the RGB camera image.")
        .map("0", K4A_COLOR_RESOLUTION_OFF)       /** Color camera will be turned off */
        .map("720", K4A_COLOR_RESOLUTION_720P)    /** 1280x720  16:9 */
        .map("1080", K4A_COLOR_RESOLUTION_1080P)  /** 1920x1080 16:9 */
        .map("1440", K4A_COLOR_RESOLUTION_1440P)  /** 2560x1440 16:9 */
        .map("1536", K4A_COLOR_RESOLUTION_1536P)  /** 2048x1536 4:3  */
        .map("2160", K4A_COLOR_RESOLUTION_2160P)  /** 3840x2160 16:9 */
        .map("3072", K4A_COLOR_RESOLUTION_3072P); /** 4096x3072 4:3  */
        defineOptionalProperty<k4a_depth_mode_t>("DepthMode", K4A_DEPTH_MODE_NFOV_UNBINNED,
                "Resolution/mode of the depth camera image.")
        .setCaseInsensitive(true)
        .map("OFF", K4A_DEPTH_MODE_OFF)
        .map("NFOV_2X2BINNED", K4A_DEPTH_MODE_NFOV_2X2BINNED)
        .map("NFOV_UNBINNED", K4A_DEPTH_MODE_NFOV_UNBINNED)
        .map("WFOV_2X2BINNED", K4A_DEPTH_MODE_WFOV_2X2BINNED)
        .map("WFOV_UNBINNED", K4A_DEPTH_MODE_WFOV_UNBINNED)
        .map("PASSIVE_IR", K4A_DEPTH_MODE_PASSIVE_IR);

        defineOptionalProperty<float>("MaxDepth",
                                      6000.0,
                                      "Max. allowed depth value in mm. Depth values above this threshold will be set to nan.");

        defineOptionalProperty<float>("CaptureTimeOffset",
                                      16.0f,
                                      "In Milliseconds. Time offset between capturing the image on the hardware and receiving the image in this process.",
                                      armarx::PropertyDefinitionBase::eModifiable);
    }

    armarx::PropertyDefinitionsPtr AzureKinectPointCloudProvider::createPropertyDefinitions()
    {
        armarx::PropertyDefinitionsPtr defs(
            new AzureKinectPointCloudProviderPropertyDefinitions(getConfigIdentifier()));

        defs->optional(enableColorUndistortion,
                       "EnableColorUndistortion",
                       "Undistort the color images using the full 8 radial and tangential distortion parameters provided by the Azure Kinect.\n"
                       "This can help for processing tasks which cannot handle radial parameters k3-k6.\n"
                       "Note that this drastically reduces the FPS (to something like 3).");
        defs->optional(externalCalibrationFilePath, "ExternalCalibrationFilePath", "Path to an optional external"
                       " calibration file, which has a"
                       " camera matrix and distortion"
                       " parameters.");
        defs->optional(mDeviceId, "device_id", "ID of the device.");

        return defs;
    }

    void
    AzureKinectPointCloudProvider::onInitImageProvider()
    {
        config = K4A_DEVICE_CONFIG_INIT_DISABLE_ALL;
        auto fps = getProperty<float>("framerate").getValue();
        if (fps == 5.0f)
        {
            config.camera_fps = K4A_FRAMES_PER_SECOND_5;
        }
        else if (fps == 15.0f)
        {
            config.camera_fps = K4A_FRAMES_PER_SECOND_15;
        }
        else if (fps == 30.0f)
        {
            config.camera_fps = K4A_FRAMES_PER_SECOND_30;
        }
        else
        {
            throw armarx::LocalException("Invalid framerate: ") << fps << " - Only framerates 5, 15 and 30 are "
                    << "supported by Azure Kinect.";
        }

        config.depth_mode = getProperty<k4a_depth_mode_t>("DepthMode");
        config.color_format = K4A_IMAGE_FORMAT_COLOR_BGRA32;
        config.color_resolution = getProperty<k4a_color_resolution_t>("ColorResolution");

        // This means that we'll only get captures that have both color and depth images, so we don't
        // need to check if the capture contains a particular type of image.
        config.synchronized_images_only = true;

        // Set number of images per frame.
        setNumberImages(2);

        auto depth_dim = GetDepthDimensions(config.depth_mode);
        auto color_dim = GetColorDimensions(config.color_resolution);

        ARMARX_INFO << "Depth image size: " << depth_dim.first << "x" << depth_dim.second;
        ARMARX_INFO << "Color image size: " << color_dim.first << "x" << color_dim.second;

        alignedDepthImage = k4a::image::create(
                                K4A_IMAGE_FORMAT_DEPTH16,
                                color_dim.first,
                                color_dim.second,
                                color_dim.first * 2 * static_cast<int32_t>(sizeof(uint8_t)));

        setImageFormat(
            visionx::ImageDimension(color_dim.first, color_dim.second),
            visionx::eRgb,
            visionx::eBayerPatternGr);

        resultDepthImage.reset(visionx::tools::createByteImage(getImageFormat(), visionx::eRgb));
        resultColorImage = std::make_unique<CByteImage>(color_dim.first, color_dim.second, CByteImage::eRGB24);

        xyzImage = k4a::image::create(
                       K4A_IMAGE_FORMAT_CUSTOM,
                       color_dim.first,
                       color_dim.second,
                       color_dim.first * 3 * static_cast<int32_t>(sizeof(int16_t)));

        pointcloud = std::make_unique<pcl::PointCloud<CloudPointType>>();

    }

    void
    AzureKinectPointCloudProvider::onConnectImageProvider()
    {
        ARMARX_DEBUG << "Connecting " << getName();

        pointcloudTask = armarx::RunningTask<AzureKinectPointCloudProvider>::pointer_type(
                             new armarx::RunningTask<AzureKinectPointCloudProvider>(
                                 this,
                                 &AzureKinectPointCloudProvider::runPointcloudPublishing));
        pointcloudTask->start();

        ARMARX_VERBOSE << "Connected " << getName();
    }

    void
    AzureKinectPointCloudProvider::onDisconnectImageProvider()
    {
        ARMARX_DEBUG << "Disconnecting " << getName();

        // Stop task
        {
            std::lock_guard<std::mutex> lock(pointcloudProcMutex);
            ARMARX_DEBUG << "Stopping pointcloud processing thread...";
            const bool WAIT_FOR_JOIN = false;
            pointcloudTask->stop(WAIT_FOR_JOIN);
            pointcloudProcSignal.notify_all();
            ARMARX_DEBUG << "Waiting for pointcloud processing thread to stop...";
            pointcloudTask->waitForStop();
            ARMARX_DEBUG << "Pointcloud processing thread stopped";
        }

        ARMARX_DEBUG << "Disconnected " << getName();
    }

    void
    AzureKinectPointCloudProvider::onInitCapturingPointCloudProvider()
    {
        this->setPointCloudSyncMode(eCaptureSynchronization);
        ARMARX_TRACE;
    }

    void
    AzureKinectPointCloudProvider::onExitCapturingPointCloudProvider()
    {
        ARMARX_INFO << "Closing Azure Kinect device";
        device.close();
        ARMARX_INFO << "Closed Azure Kinect device";
    }

    void
    AzureKinectPointCloudProvider::onStartCapture(float /* framesPerSecond */)
    {
        ARMARX_TRACE;

        cloudFormat = getPointCloudFormat();

        // Check for devices.
        const uint32_t DEVICE_COUNT = k4a::device::get_installed_count();
        if (DEVICE_COUNT == 0)
        {
            getArmarXManager()->asyncShutdown();
            throw armarx::LocalException("No Azure Kinect devices detected!");
        }

        ARMARX_TRACE;

        device = k4a::device::open(static_cast<uint32_t>(mDeviceId));
        ARMARX_DEBUG << "Opened device id #" << mDeviceId << " with serial number "
                     << device.get_serialnum() << ".";

        ARMARX_TRACE;

        auto depthDim = GetDepthDimensions(config.depth_mode);
        auto colorDim = GetColorDimensions(config.color_resolution);
        (void) depthDim;
        (void) colorDim;

        k4aCalibration = device.get_calibration(config.depth_mode, config.color_resolution);
        ARMARX_INFO
                << "Color camera calibration:" << "\n"
                << "cx: " << k4aCalibration.color_camera_calibration.intrinsics.parameters.param.cx << "\n"
                << "cy: " << k4aCalibration.color_camera_calibration.intrinsics.parameters.param.cy << "\n"
                << "fx: " << k4aCalibration.color_camera_calibration.intrinsics.parameters.param.fx << "\n"
                << "fy: " << k4aCalibration.color_camera_calibration.intrinsics.parameters.param.fy << "\n"
                << "k1: " << k4aCalibration.color_camera_calibration.intrinsics.parameters.param.k1 << "\n"
                << "k2: " << k4aCalibration.color_camera_calibration.intrinsics.parameters.param.k2 << "\n"
                << "p1: " << k4aCalibration.color_camera_calibration.intrinsics.parameters.param.p1 << "\n"
                << "p2: " << k4aCalibration.color_camera_calibration.intrinsics.parameters.param.p2 << "\n"
                << "k3: " << k4aCalibration.color_camera_calibration.intrinsics.parameters.param.k3 << "\n"
                << "k4: " << k4aCalibration.color_camera_calibration.intrinsics.parameters.param.k4 << "\n"
                << "k5: " << k4aCalibration.color_camera_calibration.intrinsics.parameters.param.k5 << "\n"
                << "k6: " << k4aCalibration.color_camera_calibration.intrinsics.parameters.param.k6;


        auto c = getStereoCalibration(Ice::Current());
        CCalibration* c_left = tools::convert(c.calibrationLeft);
        c_left->PrintCameraParameters();
        delete c_left;
        CCalibration* c_right = tools::convert(c.calibrationRight);
        c_right->PrintCameraParameters();
        delete c_right;

        transformation = k4a::transformation(k4aCalibration);

        ARMARX_TRACE;
	    device.set_color_control(K4A_COLOR_CONTROL_BRIGHTNESS, K4A_COLOR_CONTROL_MODE_MANUAL, 128);

        device.start_cameras(&config);

        ARMARX_TRACE;

        setMetaInfo("serialNumber", new armarx::Variant(device.get_serialnum()));
        setMetaInfo("rgbVersion", new armarx::Variant(VersionToString(device.get_version().rgb)));
        setMetaInfo("depthVersion", new armarx::Variant(VersionToString(device.get_version().depth)));
        setMetaInfo("depthSensorVersion", new armarx::Variant(VersionToString(device.get_version().depth_sensor)));
        setMetaInfo("audioVersion", new armarx::Variant(VersionToString(device.get_version().audio)));

        ARMARX_TRACE;


        // Color image calibration
        {
             // Load intrinsics from camera
            const k4a_calibration_camera_t calibration{
                k4aCalibration.color_camera_calibration};
            const k4a_calibration_intrinsic_parameters_t::_param param =
                calibration.intrinsics.parameters.param;
          
            // // Scale intrinsics according to image scale
            // param.fx *= image_scale_;
            // param.fy *= image_scale_;

            // Calculate distortion map
            cv::Mat1f camera_matrix(3, 3);
            camera_matrix << param.fx, 0, param.cx, 0, param.fy, param.cy, 0, 0, 1;
            cv::Mat1f new_camera_matrix(3, 3);
            new_camera_matrix << param.fx, 0, param.cx, 0, param.fx,
                param.cy, 0, 0, 1;
            cv::Mat1f distortion_coeff(1, 8);
            distortion_coeff << param.k1, param.k2, param.p1, param.p2, param.k3,
                param.k4, param.k5, param.k6;
            cv::Mat map1, map2, map3;
            cv::initUndistortRectifyMap(
                camera_matrix, distortion_coeff, cv::Mat{}, new_camera_matrix,
                cv::Size{calibration.resolution_width, calibration.resolution_height}, CV_32FC1, map1, map2);
            cv::convertMaps(map1, map2, colorDistortionMap, map3, CV_16SC2, true);
        }
    }

    void
    AzureKinectPointCloudProvider::onStopCapture()
    {
        // pass
    }

    bool
    AzureKinectPointCloudProvider::doCapture()
    {
        using armarx::core::time::ScopedStopWatch;
        using armarx::core::time::StopWatch;

        ScopedStopWatch sw_total{::createSwCallback(this, "doCapture")};

        k4a::capture capture;
        const std::chrono::milliseconds TIMEOUT{1000};

        StopWatch sw_get_capture;
        bool status = false;
        try
        {
            status = device.get_capture(&capture, TIMEOUT);
        }
        catch (const std::exception&)
        {
            ARMARX_WARNING << "Failed to get capture from device (#"
                           << ++mDiagnostics.num_crashes
                           << ").  Restarting camera.";
            StopWatch sw;
            device.stop_cameras();
            device.start_cameras(&config);
            ARMARX_INFO << "Restarting took " << sw.stop() << ".";
            return false;
        }

        if (status)
        {
            ::createSwCallback(this, "waiting for get_capture")(sw_get_capture.stop());

            setMetaInfo("temperature", new armarx::Variant(capture.get_temperature_c()));

            const k4a::image DEPTH_IMAGE = capture.get_depth_image();
            const k4a::image COLOR_IMAGE = capture.get_color_image();

            auto real_time = IceUtil::Time::now();
            auto monotonic_time = IceUtil::Time::now(IceUtil::Time::Monotonic);
            auto clock_diff = real_time - monotonic_time;

            auto image_monotonic_time = IceUtil::Time::microSeconds(
                                            std::chrono::duration_cast<std::chrono::microseconds>(
                                                DEPTH_IMAGE.get_system_timestamp()).count());
            long offset = long(getProperty<float>("CaptureTimeOffset").getValue() * 1000.0f);

            imagesTime = image_monotonic_time + clock_diff - IceUtil::Time::microSeconds(offset);

            setMetaInfo("image age in [ms]",
                        new armarx::Variant{(real_time - imagesTime).toMilliSecondsDouble()});

            //  This function assumes that the image is made of depth pixels (i.e. uint16_t's),
            //  which is only true for IR/depth images.
            const k4a_image_format_t IMAGE_FORMAT = DEPTH_IMAGE.get_format();
            if (IMAGE_FORMAT != K4A_IMAGE_FORMAT_DEPTH16 && IMAGE_FORMAT != K4A_IMAGE_FORMAT_IR16)
            {
                throw std::logic_error("Attempted to colorize a non-depth image!");
            }

            if (enableColorUndistortion)
            {
                // only the color image needs to be rectified
                cv::Mat temp_image;

                cv::cvtColor(
                    cv::Mat{cv::Size{COLOR_IMAGE.get_width_pixels(), COLOR_IMAGE.get_height_pixels()}, CV_8UC4,
                            (void *)COLOR_IMAGE.get_buffer(),
                            cv::Mat::AUTO_STEP},
                    temp_image, cv::COLOR_RGBA2RGB);

                cv::Mat cv_color_image_undistorted(COLOR_IMAGE.get_width_pixels(), COLOR_IMAGE.get_height_pixels(), CV_8UC3);

                cv::remap(temp_image, cv_color_image_undistorted, colorDistortionMap, cv::Mat{}, cv::INTER_NEAREST,
                        cv::BORDER_CONSTANT);

                visionx::imrec::convert(cv_color_image_undistorted, *resultColorImage);
            }
            else
            {
                // Convert the k4a image to an IVT image.
                ScopedStopWatch sw{::createSwCallback(this, "convert k4a image to IVT")};
                ::k4aToIvtImage(COLOR_IMAGE, *resultColorImage);
            }


            // Provide data for pointcloud processing thread and signal to start processing.
            {
                ScopedStopWatch sw{::createSwCallback(this, "transform depth image to camera")};
                // Acquire lock and write data needed by pointcloud thread (i.e.,
                // alignedDepthImageScaled and depthImageReady).
                {
                    std::lock_guard lock{pointcloudProcMutex};
                    transformation.depth_image_to_color_camera(DEPTH_IMAGE, &alignedDepthImage);

                    // Signal that point cloud processing may proceed and reset processed flag.
                    depthImageReady = true;
                }

                ARMARX_DEBUG << "Notifying pointcloud thread.";
                pointcloudProcSignal.notify_one();
            }


            // Prepare result depth image.
            {
                ScopedStopWatch sw{::createSwCallback(this, "prepare result depth image")};

                const int DW = alignedDepthImage.get_width_pixels();
                const int DH = alignedDepthImage.get_height_pixels();

                ARMARX_CHECK_EXPRESSION(resultDepthImage);
                ARMARX_CHECK_EQUAL(resultDepthImage->width, DW);
                ARMARX_CHECK_EQUAL(resultDepthImage->height, DH);

                auto result_depth_buffer = resultDepthImage->pixels;
                const auto* depth_buffer =
                    reinterpret_cast<const uint16_t*>(alignedDepthImage.get_buffer());

                int index = 0;
                int index_2 = 0;
                for (int y = 0; y < DH; ++y)
                {
                    for (int x = 0; x < DW; ++x)
                    {
                        uint16_t depth_value = depth_buffer[index_2];
                        visionx::tools::depthValueToRGB(
                            depth_value,
                            result_depth_buffer[index],
                            result_depth_buffer[index + 1],
                            result_depth_buffer[index + 2]);
                        index += 3;
                        index_2 += 1;
                    }
                }
            }

            // Broadcast RGB-D image.
            {
                ScopedStopWatch sw{::createSwCallback(this, "broadcast RGB-D image")};
                CByteImage* images[2] = {resultColorImage.get(), resultDepthImage.get()};
                provideImages(images, imagesTime);
            }

            // Wait until `alignedDepthImage` was processed and can be overridden again.
            {
                std::unique_lock signal_lock{pointcloudProcMutex};
                ARMARX_DEBUG << "Capturing thread waiting for signal...";
                pointcloudProcSignal.wait(signal_lock, [&]
                { return depthImageProcessed; });
                ARMARX_DEBUG << "Capturing thread received signal.";
            }

            return true;
        }
        else
        {
            ARMARX_INFO << deactivateSpam(30) << "Did not get frame until timeout of " << TIMEOUT;

            return false;
        }

    }

    void
    AzureKinectPointCloudProvider::runPointcloudPublishing()
    {
        ARMARX_DEBUG << "Started pointcloud processing task.";

        IceUtil::Time time;

        // Main pointcloud processing loop.
        while (not pointcloudTask->isStopped())
        {
            // Wait for data.
            std::unique_lock signal_lock{pointcloudProcMutex};
            ARMARX_DEBUG << "Pointcloud thread waiting for signal...";
            pointcloudProcSignal.wait(signal_lock,
                                      [&]
            { return pointcloudTask->isStopped() or depthImageReady; });
            ARMARX_DEBUG << "Pointcloud thread received signal.";

            // Assess timings, reset flags.
            const IceUtil::Time TIMESTAMP = imagesTime;
            depthImageReady = false;
            depthImageProcessed = false;

            // Transform depth image to pointcloud.
            {
                time = armarx::TimeUtil::GetTime(armarx::TimeMode::SystemTime);
                transformation.depth_image_to_point_cloud(alignedDepthImage,
                        K4A_CALIBRATION_TYPE_COLOR,
                        &xyzImage);
                ARMARX_DEBUG << "Transforming depth image to point cloud took "
                             << armarx::TimeUtil::GetTimeSince(time, armarx::TimeMode::SystemTime);
            }

            // Construct PCL pointcloud.
            {
                time = armarx::TimeUtil::GetTime(armarx::TimeMode::SystemTime);

                ARMARX_CHECK_EXPRESSION(pointcloud);

                pointcloud->width = static_cast<uint32_t>(alignedDepthImage.get_width_pixels());
                pointcloud->height = static_cast<uint32_t>(alignedDepthImage.get_height_pixels());

                pointcloud->is_dense = false;
                pointcloud->points.resize(pointcloud->width * pointcloud->height);

                auto k4a_cloud_buffer = reinterpret_cast<const int16_t*>(xyzImage.get_buffer());
                auto color_buffer = resultColorImage->pixels;

                size_t index = 0;
                float max_depth = getProperty<float>("MaxDepth").getValue();
                for (auto& p : pointcloud->points)
                {
                    p.r = color_buffer[index];
                    p.x = k4a_cloud_buffer[index];

                    p.g = color_buffer[++index];
                    p.y = k4a_cloud_buffer[index];

                    p.b = color_buffer[++index];
                    auto z = k4a_cloud_buffer[index++];

                    p.z = z <= max_depth && z != 0 ? z : std::numeric_limits<float>::quiet_NaN();
                }

                ARMARX_DEBUG << "Constructing point cloud took "
                             << armarx::TimeUtil::GetTimeSince(time, armarx::TimeMode::SystemTime);
            }

            // Notify capturing thread that data was processed and may be overridden.
            depthImageProcessed = true;
            signal_lock.unlock();
            ARMARX_DEBUG << "Notifying capturing thread...";
            pointcloudProcSignal.notify_one();

            // Broadcast PCL pointcloud.
            {
                time = armarx::TimeUtil::GetTime(armarx::TimeMode::SystemTime);
                pointcloud->header.stamp = static_cast<unsigned long>(TIMESTAMP.toMicroSeconds());
                providePointCloud(pointcloud);
                ARMARX_DEBUG << "Broadcasting pointcloud took "
                             << armarx::TimeUtil::GetTimeSince(time, armarx::TimeMode::SystemTime);
            }
        }

        ARMARX_DEBUG << "Stopped pointcloud processing task.";
    }

    std::string
    AzureKinectPointCloudProvider::getDefaultName() const
    {
        return "AzureKinectPointCloudProvider";
    }

    void
    AzureKinectPointCloudProvider::onInitComponent()
    {
        ImageProvider::onInitComponent();
        CapturingPointCloudProvider::onInitComponent();
    }

    void
    AzureKinectPointCloudProvider::onConnectComponent()
    {
        ImageProvider::onConnectComponent();
        CapturingPointCloudProvider::onConnectComponent();
    }

    void
    AzureKinectPointCloudProvider::onDisconnectComponent()
    {
        captureEnabled = false;

        ImageProvider::onDisconnectComponent();
        CapturingPointCloudProvider::onDisconnectComponent();
    }

    void
    AzureKinectPointCloudProvider::onExitComponent()
    {
        ImageProvider::onExitComponent();
        CapturingPointCloudProvider::onExitComponent();
    }

    StereoCalibration
    AzureKinectPointCloudProvider::getStereoCalibration(const Ice::Current&)
    {
        using namespace Eigen;
        using Matrix3FRowMajor = Matrix<float, 3, 3, StorageOptions::RowMajor>;
        using visionx::tools::convertEigenMatToVisionX;

        // TODO: use the externally used cameraMatrix and distCoeffs
        const auto convert_calibration = [](const k4a_calibration_camera_t& k4a_calib,
                                            float scale = 1.f)
        {
            MonocularCalibration monocular_calibration;

            const k4a_calibration_intrinsic_parameters_t& params = k4a_calib.intrinsics.parameters;
            monocular_calibration.cameraParam.principalPoint =
            {
                params.param.cx * scale,
                params.param.cy* scale
            };
            monocular_calibration.cameraParam.focalLength =
            {
                params.param.fx * scale,
                params.param.fy* scale
            };
            // TODO: Figure out convertions.  IVT (Calibration.h) expects 4 parameters:
            //  - The first radial lens distortion parameter.
            //  - The second radial lens distortion parameter.
            //  - The first tangential lens distortion parameter.
            //  - The second tangential lens distortion parameter.
            // However, the Kinect offers k1-k6 radial distortion coefficients and 2 p1-p2
            // tangential distortion parameters, which means that k3-k6 are unused.
            // It is even unclear whether this is correct now, as previously it was a vector of all
            // 6 k-params, which resulted in failed assertions in TypeMapping.cpp in the function
            // CStereoCalibration* visionx::tools::convert(const visionx::StereoCalibration& stereoCalibration)
            // lin 314 at time of this commit.
            // See: https://microsoft.github.io/Azure-Kinect-Sensor-SDK/master/structk4a__calibration__intrinsic__parameters__t_1_1__param.html
            monocular_calibration.cameraParam.distortion =
            {
                params.param.k1, params.param.k2,
                params.param.p1, params.param.p2,
            };

            // TODO this needs to be scaled! Or why shouldn't it ?
            monocular_calibration.cameraParam.width = std::floor(float(k4a_calib.resolution_width) * scale);
            monocular_calibration.cameraParam.height = std::floor(float(k4a_calib.resolution_height) * scale);

            const Matrix3FRowMajor rotation =
                Map<const Matrix3FRowMajor> {k4a_calib.extrinsics.rotation};

            monocular_calibration.cameraParam.rotation = convertEigenMatToVisionX(rotation);
            monocular_calibration.cameraParam.translation =
            {
                k4a_calib.extrinsics.translation,
                k4a_calib.extrinsics.translation + 3
            };

            return monocular_calibration;
        };

        StereoCalibration stereo_calibration;

        stereo_calibration.calibrationLeft = convert_calibration(k4aCalibration.color_camera_calibration);
        
        // if the camera images are rectified, the distortion params are 0
        if(enableColorUndistortion)
        {
            auto& colorDistortionParams = stereo_calibration.calibrationLeft.cameraParam.distortion;
            std::fill(colorDistortionParams.begin(), colorDistortionParams.end(), 0);
        }
        
        // the depth image is rectified by default 
        {
            // The depth image has been warped to to color camera frame. See depth_image_to_color_camera() above.
            // Therefore we use the calibration of the color camera (focal length etc) and set the distortion parameters to 0. 
            stereo_calibration.calibrationRight = convert_calibration(k4aCalibration.color_camera_calibration);

            auto& depthDistortionParams = stereo_calibration.calibrationRight.cameraParam.distortion;
            std::fill(depthDistortionParams.begin(), depthDistortionParams.end(), 0);
        }

        stereo_calibration.rectificationHomographyLeft = convertEigenMatToVisionX(Matrix3f::Identity());
        stereo_calibration.rectificationHomographyRight = convertEigenMatToVisionX(Matrix3f::Identity());

        return stereo_calibration;
    }

    bool
    AzureKinectPointCloudProvider::getImagesAreUndistorted(const Ice::Current&)
    {
        return enableColorUndistortion;
    }

    std::string
    AzureKinectPointCloudProvider::getReferenceFrame(const Ice::Current&)
    {
        return getProperty<std::string>("frameName");
    }

    std::vector<imrec::ChannelPreferences>
    AzureKinectPointCloudProvider::getImageRecordingChannelPreferences(const Ice::Current&)
    {
        ARMARX_TRACE;

        imrec::ChannelPreferences rgb;
        rgb.requiresLossless = false;
        rgb.name = "rgb";

        imrec::ChannelPreferences depth;
        depth.requiresLossless = true;
        depth.name = "depth";

        return {rgb, depth};
    }

}
