#include "Stopwatch.h"


namespace stopwatch
{
    Stopwatch::Stopwatch()
    {

    }

    void Stopwatch::reset()
    {
        statsMap.clear();
        currentStarts.clear();
    }

    void Stopwatch::start(const std::string& tag)
    {
        // implicit construction by [] operator
        currentStarts[tag] = Clock::now();
    }

    void Stopwatch::stop(const std::string& tag)
    {
        TimePoint end = Clock::now();

        auto it = currentStarts.find(tag);
        if (it == currentStarts.end())
        {
            return; // not started, ignore
        }

        TimePoint start = it->second;
        Duration elapsed = end - start;

        Stats& stats = statsMap[tag];

        stats.totalExecutionTime += elapsed;
        stats.numExecutions++;

        stats.latestExecutionTime = elapsed;
        stats.averageExecutionTime = stats.totalExecutionTime / stats.numExecutions;
    }

    const Stats& Stopwatch::getStats(const std::string& tag)
    {
        return statsMap[tag];
    }

    std::ostream& operator<<(std::ostream& os, const Stats& stats)
    {
        os << "Total execution time: " << stats.totalExecutionTime.count() << " seconds" << std::endl;
        os << "Total number of executions: " << stats.numExecutions << std::endl;
        os << "Average execution time: " << stats.averageExecutionTime.count() << " seconds" << std::endl;
        return os;
    }

    std::ostream& operator<<(std::ostream& os, const Stopwatch& stats)
    {
        for (auto it : stats.statsMap)
        {
            os << "== Stats for '" << it.first << "' ==" << std::endl;
            os << it.second;
        }
        return os;
    }

}

