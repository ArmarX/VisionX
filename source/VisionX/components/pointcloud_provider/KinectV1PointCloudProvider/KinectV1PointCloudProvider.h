/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::KinectV1PointCloudProvider
 * @author     Christoph Pohl (christoph dot pohl at kit dot edu)
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/system/ArmarXDataPath.h>
#include <VisionX/core/ImageProcessor.h>
#include <VisionX/components/pointcloud_core/CapturingPointCloudProvider.h>

#include <VisionX/interface/components/RGBDImageProvider.h>


#include <pcl/common/transforms.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

#include <Eigen/Geometry>

#include <libfreenect.hpp>
#include "KinectV1Device.hpp"


//#include <Image/IplImageAdaptor.h>

namespace visionx
{


    /**
         * @class KinectV1PointCloudProviderPropertyDefinitions
         * @brief
         */
    class KinectV1PointCloudProviderPropertyDefinitions :
        public CapturingPointCloudProviderPropertyDefinitions
    {
    public:
        KinectV1PointCloudProviderPropertyDefinitions(std::string prefix) :
            CapturingPointCloudProviderPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("ReferenceFrameName", "DepthCamera", "Optional reference frame name.");
        }
    };

    /**
         * @class KinectV1PointCloudProvider
         * @ingroup VisionX-Components
         * @brief A brief description
         *
         * Detailed Description
         */
    class KinectV1PointCloudProvider :
        virtual public RGBDPointCloudProviderInterface,
        virtual public CapturingPointCloudProvider,
        virtual public ImageProvider
    {
    public:
        /**
             * @see armarx::ManagedIceObject::getDefaultName()
             */

        std::string getDefaultName() const override
        {
            return "KinectV1PointCloudProvider";
        }

    protected:
        /**
             * @see visionx::PointCloudProviderBase::onInitPointCloudProvider()
             */
        void onInitCapturingPointCloudProvider() override;

        /**
             * @see visionx::PointCloudProviderBase::onExitPointCloudProvider()
             */
        void onExitCapturingPointCloudProvider() override;

        /**
             * @see visionx::PointCloudProviderBase::onStartCapture()
             */
        void onStartCapture(float frameRate) override;

        /**
             * @see visionx::PointCloudProviderBase::onStopCapture()
             */
        void onStopCapture() override;

        /**
             * @see visionx::PointCloudProviderBase::doCapture()
            */
        bool doCapture() override;

        /**
             * @see visionx::CapturingImageProvider::onInitImageProvider()
             */
        void onInitImageProvider() override;

        /**
             * @see visionx::CapturingImageProvider::onExitImageProvider()
             */
        void onExitImageProvider() override {}

        bool hasSharedMemorySupport(const Ice::Current& c = Ice::emptyCurrent) override
        {
            return true;
        }

        /**
             * @see PropertyUser::createPropertyDefinitions()
             */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        // mixed inherited stuff
        void onInitComponent() override;

        void onConnectComponent() override;

        void onDisconnectComponent() override;

        void onExitComponent() override;

        StereoCalibration getStereoCalibration(const Ice::Current& c = Ice::emptyCurrent) override;

        bool getImagesAreUndistorted(const ::Ice::Current& c = Ice::emptyCurrent) override
        {
            return false;
        }


        std::string getReferenceFrame(const Ice::Current& c = Ice::emptyCurrent) override
        {
            return getProperty<std::string>("ReferenceFrameName");
        };

    private:
        typedef pcl::PointXYZRGBA PointT;

        Freenect::Freenect freenect_;
        KinectV1Device* device_;

        CByteImage** rgbImages_;
        int width_, height_, size_;
        StereoCalibration calibration;
        float cx_, cy_, fx_, fy_;

    };
}
