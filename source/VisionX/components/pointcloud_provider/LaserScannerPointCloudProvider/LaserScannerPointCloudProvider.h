/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::LaserScannerPointCloudProvider
 * @author     Markus Grotz ( markus dot grotz at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <ArmarXCore/core/Component.h>
#include <RobotAPI/interface/units/LaserScannerUnit.h>
#include <VisionX/interface/components/LaserScannerPointCloudProvider.h>
#include <VisionX/components/pointcloud_core/PointCloudProvider.h>

#include <RobotAPI/libraries/core/remoterobot/RemoteRobot.h>

#pragma GCC diagnostic push
#include <pcl/common/transforms.h>
#include <pcl/filters/statistical_outlier_removal.h>
#pragma GCC diagnostic ignored "-Wpedantic"
#pragma GCC diagnostic pop

#include <mutex>
#include <Eigen/Core>
#include <RobotAPI/interface/visualization/DebugDrawerInterface.h>


namespace armarx
{

    /**
     * @class LaserScannerPointCloudProviderPropertyDefinitions
     * @brief
     */
    class LaserScannerPointCloudProviderPropertyDefinitions:
        public visionx::PointCloudProviderPropertyDefinitions
    {
    public:
        LaserScannerPointCloudProviderPropertyDefinitions(std::string prefix):
            visionx::PointCloudProviderPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("LaserScannerUnitName", "LaserScannerSimulation", "The name of the laser scanner provider");
            defineOptionalProperty<std::string>("RobotStateComponentName", "RobotStateComponent", "Name of the robot state component that should be used");
        }
    };

    /**
     * @defgroup Component-LaserScannerPointCloudProvider LaserScannerPointCloudProvider
     * @ingroup VisionX-Components
     * A description of the component LaserScannerPointCloudProvider.
     *
     * @class LaserScannerPointCloudProvider
     * @ingroup Component-LaserScannerPointCloudProvider
     * @brief Brief description of class LaserScannerPointCloudProvider.
     *
     * Detailed description of class LaserScannerPointCloudProvider.
     */
    class LaserScannerPointCloudProvider :
        virtual public visionx::PointCloudProvider,
        virtual public armarx::LaserScannerPointCloudProviderInterface
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override
        {
            return "LaserScannerPointCloudProvider";
        }
        void reportSensorValues(const std::string&, const std::string&, const LaserScan&, const TimestampBasePtr&, const Ice::Current& c = Ice::emptyCurrent) override;


        std::string getReferenceFrame(const Ice::Current& c = Ice::emptyCurrent) override
        {
            return  localRobot->getRootNode()->getName();
        }


    protected:
        void onInitPointCloudProvider() override;
        void onConnectPointCloudProvider() override;
        void onExitPointCloudProvider() override;


        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        visionx::MetaPointCloudFormatPtr getDefaultPointCloudFormat() override;

    private:
        std::map<std::string, std::string> deviceToFrame;
        std::mutex mutex;

        RobotStateComponentInterfacePrx robotStateComponent;
        VirtualRobot::RobotPtr localRobot;
        // LaserScannerUnitInterfacePrx laserScanner;

        std::map<std::string, pcl::PointCloud<pcl::PointXYZ>> clouds;
        pcl::PointCloud<pcl::PointXYZ>::Ptr resultCloud;

        pcl::StatisticalOutlierRemoval<pcl::PointXYZ> filter;

    };
}

