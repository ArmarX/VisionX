armarx_component_set_name("OpenNIPointCloudProvider")

find_package(OpenNI QUIET)
armarx_build_if(OpenNI_FOUND "OpenNI not available")

set(COMPONENT_LIBS
    VisionXPointCloud
    VisionXCore
    ${OpenNI_LIBRARY}
)
set(SOURCES OpenNIPointCloudProvider.cpp)
set(HEADERS OpenNIPointCloudProvider.h)

armarx_add_component("${SOURCES}" "${HEADERS}")
if(OpenNI_FOUND)
    target_include_directories(OpenNIPointCloudProvider SYSTEM PUBLIC ${OpenNI_INCLUDE_DIRS})
endif()

set(EXE_SOURCES main.cpp OpenNIPointCloudProviderApp.h)
armarx_add_component_executable("${EXE_SOURCES}")
