/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::StereoImagePointCloudProvider
 * @author     David Schiebener (schiebener at kit dot edu)
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/system/ArmarXDataPath.h>
#include <VisionX/core/ImageProcessor.h>
#include <VisionX/components/pointcloud_core/CapturingPointCloudProvider.h>
#include <VisionX/interface/components/PointCloudAndImageAndCalibrationProviderInterface.h>


#include <pcl/point_types.h>

#include <vector>
#include <cmath>
#include <climits>
#include <mutex>


class CStereoCalibration;


namespace visionx
{

    /**
         * @class StereoImagePointCloudProviderPropertyDefinitions
         * @brief
         */
    class StereoImagePointCloudProviderPropertyDefinitions:
        public CapturingPointCloudProviderPropertyDefinitions
    {
    public:
        StereoImagePointCloudProviderPropertyDefinitions(std::string prefix):
            CapturingPointCloudProviderPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("ImageProviderAdapterName", "Armar3ImageProvider", "Name of the stereo image provider for the camera images and the calibration");
            defineOptionalProperty<int>("DownsamplingRate", 1, "Create a point only at every n-th pixel");
            defineOptionalProperty<bool>("SmoothDisparity", true, "Smooth the disparity image to filter out noise, but loose sharp edges");
        }
    };

    /**
         * @class StereoImagePointCloudProvider
         * @ingroup VisionX-Components
         * @brief A brief description
         *
         * Detailed Description
         */
    class StereoImagePointCloudProvider :
        virtual public StereoImagePointCloudProviderInterface,
        virtual public CapturingPointCloudProvider,
        virtual public ImageProcessor,
        virtual public ImageProvider
    {
    public:
        /**
             * @see armarx::ManagedIceObject::getDefaultName()
             */
        std::string getDefaultName() const override
        {
            return "StereoImagePointCloudProvider";
        }

        MetaPointCloudFormatPtr getDefaultPointCloudFormat() override;

    protected:
        /**
             * @see visionx::PointCloudProviderBase::onInitPointCloudProvider()
             */
        void onInitCapturingPointCloudProvider() override;

        /**
             * @see visionx::PointCloudProviderBase::onExitPointCloudProvider()
             */
        void onExitCapturingPointCloudProvider() override;

        /**
             * @see visionx::PointCloudProviderBase::onStartCapture()
             */
        void onStartCapture(float frameRate) override;

        /**
             * @see visionx::PointCloudProviderBase::onStopCapture()
             */
        void onStopCapture() override;

        /**
             * @see visionx::PointCloudProviderBase::doCapture()
            */
        bool doCapture() override;

        // ImageProcessor functions
        void onInitImageProcessor() override;
        void onConnectImageProcessor() override;
        void onExitImageProcessor() override;
        void process() override { }

        /**
             * @see visionx::CapturingImageProvider::onInitImageProvider()
             */
        void onInitImageProvider() override;

        /**
             * @see visionx::CapturingImageProvider::onExitImageProvider()
             */
        void onExitImageProvider() override { }


        /**
             * @see PropertyUser::createPropertyDefinitions()
             */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        // mixed inherited stuff
        void onInitComponent() override;
        void onConnectComponent() override;
        void onDisconnectComponent() override;
        void onExitComponent() override;

        bool getImagesAreUndistorted(const ::Ice::Current& c = Ice::emptyCurrent) override
        {
            return imagesAreUndistorted;
        }
        visionx::MonocularCalibration getMonocularCalibration(const ::Ice::Current& c = Ice::emptyCurrent) override;

        bool hasSharedMemorySupport(const Ice::Current& c = Ice::emptyCurrent) override
        {
            return true;
        }


    private:

        ImageProviderInterfacePrx imageProviderPrx;
        std::string providerName;
        CStereoCalibration* stereoCalibration;
        CByteImage** cameraImages;
        CByteImage** cameraImagesGrey;
        CByteImage** resultImages;
        CByteImage* disparityImage, *disparityImageRGB;
        float frameRate;
        int width, height;
        bool imagesAreUndistorted;
        int downsamplingRate;
        bool smoothDisparity;

        std::mutex captureLock;
    };
}
