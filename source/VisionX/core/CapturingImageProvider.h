/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::Core
 * @author     Kai Welke (kai dot welke at kit dot edu)
 * @date       2012
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <ArmarXCore/core/services/tasks/RunningTask.h>

#include <VisionX/core/ImageProvider.h>
#include <VisionX/tools/FPSCounter.h>

#include <mutex>

namespace visionx
{
    class CapturingImageProviderPropertyDefinitions : public armarx::ComponentPropertyDefinitions
    {
    public:
        CapturingImageProviderPropertyDefinitions(std::string prefix):
            armarx::ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<float>("FPS", 30.0f, "Frames per second with that the capture function is called.");
        }

    };

    /**
     * The CapturingImageProvider provides a callback function to trigger
     * the capturing of images with different synchronization modes.
     */
    class CapturingImageProvider :
        virtual public ImageProvider,
        virtual public CapturingImageProviderInterface
    {
    public:
        // ================================================================== //
        // == CapturingImageProvider ice interface ========================== //
        // ================================================================== //
        /**
         * Starts image capturing.
         *
         * @param framesPerSecond Frames per second to capture.
         *
         * @throw visionx::FrameRateNotSupportedException
         * @throw visionx::StartingCaptureFailedException
         */
        void startCapture(float framesPerSecond,
                          const Ice::Current& c = Ice::emptyCurrent) override;

        /**
         * Stops image capturing. The Capturing can be started anew by calling
         * startCapture(...) again.
         */
        void stopCapture(const Ice::Current& c = Ice::emptyCurrent) override;

    protected:

        // ================================================================== //
        // == Interface of ImageProvider ================================= //
        // ================================================================== //
        /**
         * This is called when the Component::onInitComponent() is called.
         *
         * Implement this method in the ImageProvider in order to setup its
         * parameters. Use this to set the image format.
         */
        virtual void onInitCapturingImageProvider() = 0;

        /**
         * This is called when the Component::onConnectComponent() setup is called
         */
        virtual void onStartCapturingImageProvider() { }

        /**
         * This is called when the Component::onExitComponent() setup is called
         *
         * Implement this method in the ImageProvider in order clean up right
         * before terminating.
         */
        virtual void onExitCapturingImageProvider() = 0;

        /**
         * This is called when the image provider capturing has been started.
         *
         * Implement this method in the ImageProvider in order to reset
         * prvider's state.
         *
         * @param framesPerSecond capturing fps
         *
         * @throw visionx::FrameRateNotSupportedException
         * @throw visionx::StartingCaptureFailedException
         */
        virtual void onStartCapture(float framesPerSecond) = 0;

        /**
         * This is called when the image provider capturing has been stopped.
         *
         * Implement this method in the ImageProvider in order to clean up after
         * stopping.
         */
        virtual void onStopCapture() = 0;

        /**
         * Main capturing function.
         *
         * @param ppImageBuffers    Image pointer array where the captured images
         *                          need to be copied into
         */
        virtual bool capture(void** ppImageBuffers) = 0;

        // ================================================================== //
        // == Utility methods for ImageProviders ============================ //
        // ================================================================== //
        /**
         * Sets the image synchronization mode
         *
         * @param imageSyncMode image synchronization mode
         */
        void setImageSyncMode(ImageSyncMode imageSyncMode);

        /**
         * Returns the image sync mode
         */
        ImageSyncMode getImageSyncMode();

        /**
         * Retrieve scoped lock for writing to the memory.
         *
         * @return the scoped lock
         */
        armarx::SharedMemoryScopedWriteLockPtr getScopedWriteLock()
        {
            return sharedMemoryProvider->getScopedWriteLock();
        }

        // ================================================================== //
        // == RunningComponent implementation =============================== //
        // ================================================================== //
        /**
         * @see Component::onInitComponent()
         */
        void onInitImageProvider() override;

        /**
         * @see Component::onConnectComponent()
         */
        void onConnectImageProvider() override;

        /**
         * @see Component::onExitComponent()
         */
        void onExitImageProvider() override;

        /**
         * @see capture method issued by RunningTask
         */
        virtual void capture();

    protected:
        /**
         * Capture thread
         */
        armarx::RunningTask<CapturingImageProvider>::pointer_type captureTask;

        /**
         * Image synchronization information
         */
        ImageSyncMode imageSyncMode;

        /**
         * mutex for capturing for proper exit
         */
        std::mutex  captureMutex;
        /**
         * Required frame rate
         */
        float frameRate;

        /**
         * FPS manager
         */
        FPSCounter fpsCounter;

        /**
         * Indicates that capturing is enabled and running
         */
        bool captureEnabled;
    };
}

