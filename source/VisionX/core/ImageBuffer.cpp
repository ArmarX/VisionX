#include "ImageBuffer.h"

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include <VisionX/core/ImageProcessor.h>
#include <VisionX/tools/ImageUtil.h>


namespace visionx
{

    ImageBuffer::~ImageBuffer()
    {
        if (images)
        {
            for (size_t i = 0 ; i < numImages; i++)
            {
                delete images[i];
            }
            delete[] images;
        }
    }

    void ImageBuffer::allocate(const visionx::ImageProviderInfo& providerInfo)
    {
        ARMARX_CHECK_NULL(images) << "Images have already been allocated.";

        numImages = providerInfo.numberImages;

        images = new CByteImage*[numImages];
        for (size_t i = 0 ; i < numImages; i++)
        {
            images[i] = visionx::tools::createByteImage(providerInfo);
        }
    }

    void ImageBuffer::update(std::function<void (CByteImage**)> setImagesFn)
    {
        std::scoped_lock lock(mutex);
        ARMARX_CHECK_NOT_NULL(images) << "Images must be allocated before calling update().";
        setImagesFn(images);
        imagesSeq++;
    }
}
