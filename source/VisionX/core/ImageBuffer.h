#pragma once

#include <functional>
#include <mutex>


class CByteImage;
namespace visionx
{
    class ImageProviderInfo;
}

namespace visionx
{

    /**
     * @brief Handler of image buffer for ImageProcessors.
     *
     * How to call allocate (in `onConnectImageProcessor()`):
     *
     * @code
     * visionx::ImageProviderInfo imageProviderInfo = getImageProvider(providerName);
     *
     * imageBuffer.allocate(providerInfo);
     * @endcode
     *
     *
     * How to call update (in `process()`):
     *
     * @code
     * int numReceived = 0;
     *
     * imageBuffer.update([this, &numReceived](CByteImage** images)
     * {
     *      numReceived = getImages(images);
     * });
     * @endcode
     */
    class ImageBuffer
    {
    public:

        ~ImageBuffer();

        void allocate(const visionx::ImageProviderInfo& providerInfo);

        void update(std::function<void(CByteImage**)> setImagesFn);


        mutable std::mutex mutex;

        CByteImage** images = nullptr;
        /// Update this each time you update images.
        long imagesSeq = 0;


    private:

        unsigned int numImages = 0;

    };

}
