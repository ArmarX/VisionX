/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::Core
 * @author     Jan Issac (jan dot issac at gmx dot net)
 * @author     Kai Welke (kai dot welke at kit dot edu)
 * @date       2011
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once

// STD/STL
#include <deque>
#include <mutex>
#include <unordered_map>

// OpenCV
#include <opencv2/core/mat.hpp>

// IVT
#include <Image/ByteImage.h>
#include <Image/FloatImage.h>

// ArmarX
#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/services/sharedmemory/IceSharedMemoryProvider.h>
#include <ArmarXCore/core/time/forward_declarations.h>
#include <ArmarXCore/util/tasks.h>

#include <RobotAPI/interface/units/UnitInterface.h>

#include <VisionX/interface/core/CompressedImageProviderInterface.h>
#include <VisionX/interface/core/DataTypes.h>
#include <VisionX/interface/core/ImageProcessorInterface.h>
#include <VisionX/libraries/imrec.h>


namespace visionx
{
    using CByteImageUPtr = std::unique_ptr<CByteImage>;
    using CByteImageUPtrVec = std::vector<std::unique_ptr<CByteImage>>;
    // ====================================================================== //
    // == class ImageProvider declaration =============================== //
    // ====================================================================== //
    /**
     * ImageProvider abstract class defines a component which provide images
     * via ice or shared memory.     *
     */
    class ImageProvider :
        virtual public armarx::Component,
        virtual public CompressedImageProviderInterface
    {
    public:
        // ================================================================== //
        // == ImageProvider ice interface =================================== //
        // ================================================================== //
        /**
         * Retrieve images via Ice. Bypasses the IcesharedMemoryProvider
         */
        armarx::Blob getImages(const Ice::Current& c = Ice::emptyCurrent) override;

        armarx::Blob getImagesAndMetaInfo(armarx::MetaInfoSizeBasePtr&, const Ice::Current&) override;


        /**
         * Returns the entire image format info struct via Ice
         */
        ImageFormatInfo getImageFormat(const Ice::Current& c = Ice::emptyCurrent) override;

        /**
         * Retrieve number of images handled by this provider
         */
        int getNumberImages(const Ice::Current& c = Ice::emptyCurrent) override;


        bool hasSharedMemorySupport(const Ice::Current& c = Ice::emptyCurrent) override
        {
            return true;
        }

    protected:
        // ================================================================== //
        // == Interface of ImageProvider ================================= //
        // ================================================================== //
        /**
         * This is called when the Component::onInitComponent() is called.
         *
         * Implement this method in the ImageProvider in order to setup its
         * parameters. Use this to set the image format.
         */
        virtual void onInitImageProvider() = 0;

        /**
         * This is called when the Component::onConnectComponent() setup is called
         */
        virtual void onConnectImageProvider() { }
        virtual void onDisconnectImageProvider() { }

        /**
         * This is called when the Component::onExitComponent() setup is called
         *
         * Implement this method in the ImageProvider in order clean up right
         * before terminating.
         */
        virtual void onExitImageProvider() = 0;

        // ================================================================== //
        // == Utility methods for ImageProviders ============================ //
        // ================================================================== //
        /**
         * Sets the number of images on each capture
         *
         * @param numberImages number of images on each capture cycle
         */
        void setNumberImages(int numberImages);

        /**
         * Sets the image basic format data.
         *
         * @param imageDimension    size of image
         *
         * @param imageType         Image type
         *
         * @param bayerPatternType  Bayer Pattern type if using BayerPattern as
         *                          image type
         */
        void setImageFormat(
            ImageDimension imageDimension,
            ImageType imageType,
            BayerPatternType bayerPatternType = visionx::eBayerPatternRg);

        /**
         * send images raw. Take care to fit imageFormat. Timestamp is used to call updateTimestamp(), when the mutex is locked (only done when timestamp > 0)
         */
        void provideImages(void** inputBuffers, const IceUtil::Time& imageTimestamp = IceUtil::Time());

        /**
         * send ByteImages. Timestamp is used to call updateTimestamp(), when the mutex is locked (only done when timestamp > 0)
         */
        void provideImages(CByteImage** images, const IceUtil::Time& imageTimestamp = IceUtil::Time());
        void provideImages(const std::vector<CByteImageUPtr >& images, const IceUtil::Time& imageTimestamp = IceUtil::Time());

        /**
         * send FloatImages. Timestamp is used to call updateTimestamp(), when the mutex is locked (only done when timestamp > 0)
         */
        void provideImages(CFloatImage** images, const IceUtil::Time& imageTimestamp = IceUtil::Time());

        bool startImageRecording(const imrec::Config& cfg, const Ice::Current&) override;
        imrec::Status getImageRecordingStatus(const Ice::Current&) override;
        bool stopImageRecording(const Ice::Current&) override;
        std::vector<imrec::ChannelPreferences> getImageRecordingChannelPreferences(const Ice::Current&) override;

        // ================================================================== //
        // == Component implementation =============================== //
        // ================================================================== //
        /**
         * @see Component::onInitComponent()
         */
        void onInitComponent() override;

        /**
         * @see Component::onConnectComponent()
         */
        void onConnectComponent() override;
        void onDisconnectComponent() override;

        /**
         * @see Component::onExitComponent()
         */
        void onExitComponent() override;

        void request(const Ice::Current& c) override { }

        void release(const Ice::Current& c) override { }

        void init(const Ice::Current& c) override { }

        void start(const Ice::Current& c) override { }

        void stop(const Ice::Current& c) override { }

        armarx::UnitExecutionState getExecutionState(const Ice::Current& c) override
        {
            return armarx::eUndefinedUnitExecutionState;
        }

    protected:
        /**
         * Retrieve scoped lock for writing to the memory.
         *
         * @return the scoped lock
         */
        armarx::SharedMemoryScopedWriteLockPtr getScopedWriteLock()
        {
            auto prov = sharedMemoryProvider;
            if (prov)
            {
                return sharedMemoryProvider->getScopedWriteLock();
            }
            else
            {
                return armarx::SharedMemoryScopedWriteLockPtr();
            }
        }

        /**
         * @brief Updates the timestamp of the currently captured image.
         * @param timestamp in Milleseconds since epoch
         */
        void updateTimestamp(Ice::Long timestamp, bool threadSafe = true);
        void updateTimestamp(IceUtil::Time timestamp, bool threadSafe = true);
        void updateTimestamp(const armarx::DateTime& timestamp, bool threadSafe = true);

        void recordImages(const IceUtil::Time& image_timestamp);

        /**
         * Retrieve whether provider is exiting
         *
         * @return exiting status
         */
        bool isExiting()
        {
            return exiting;
        }

        /**
         * Ice proxy of the image processor interface
         */
        ImageProcessorInterfacePrx imageProcessorProxy;

        /**
         * Image buffer memory
         */
        void** imageBuffers;

        /**
         * shared memory provider
        */
        armarx::IceSharedMemoryProvider<unsigned char>::pointer_type sharedMemoryProvider;

        void imageRecordingRunningTask();

    private:
        /**
         * Image information
         */
        ImageFormatInfo imageFormat;

        /**
         * number of images
         */
        int numberImages;

        /**
         * Indicates that exit is in process and thread needs to be stopped
         */
        bool exiting;

        struct CompressionData
        {
            armarx::Blob compressedImage;
            IceUtil::Time imageTimestamp;
        };
        using CompressionDataMap = std::map<std::pair<CompressionType, int>, CompressionData>;
        CompressionDataMap compressionDataMap;
        std::mutex compressionDataMapMutex;

        struct RecordingProperties
        {
            std::mutex callMutex;
            imrec::Config config;
            armarx::RunningTask<ImageProvider>::pointer_type runningTask;
            std::unordered_map<int, visionx::imrec::Recording> channelRecordings;
            std::mutex statusMutex;
            imrec::Status status;
            std::condition_variable cv;
            std::mutex bufferMutex;
            std::deque<std::tuple<std::chrono::microseconds, std::unordered_map<int, CByteImage*>>> buffer;
        };

        RecordingProperties rec;

        // CompressedImageProviderInterface interface
    public:
        armarx::Blob getCompressedImagesAndMetaInfo(CompressionType, Ice::Int compressionQuality, armarx::MetaInfoSizeBasePtr& info, const Ice::Current&) override;
    };
}

