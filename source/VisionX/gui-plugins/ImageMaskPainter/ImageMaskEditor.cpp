#include <QWidget>
#include <QMouseEvent>
#include <QPainter>
#include <QPen>
#include <QBrush>

#include "ImageMaskEditor.h"

ImageMaskEditor::ImageMaskEditor(QSize size, QWidget* parent)
    : QWidget(parent)
{
    setMouseTracking(true);
    resizeImage(size);
}

ImageMaskEditor::ImageMaskEditor(int width, int height, QWidget* parent)
    : ImageMaskEditor({width, height}, parent)
{}

bool ImageMaskEditor::openMaskImage(const QString& fileName)
{
    QImage loadedImage;
    if (!loadedImage.load(fileName))
    {
        return false;
    }
    setMaskImage(loadedImage);
    return true;
}

bool ImageMaskEditor::openBackgroundImage(const QString& fileName)
{
    QImage loadedImage;
    if (!loadedImage.load(fileName))
    {
        return false;
    }
    setBackgroundImage(loadedImage);
    return true;
}

bool ImageMaskEditor::saveMaskImage(const QString& fileName, const char* fileFormat)
{
    if (maskImage().save(fileName, fileFormat))
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool ImageMaskEditor::saveBackgroundImage(const QString& fileName, const char* fileFormat)
{
    if (backgroundImage().save(fileName, fileFormat))
    {
        return true;
    }
    else
    {
        return false;
    }
}

const QColor& ImageMaskEditor::maskColor() const
{
    return _penColor;
}

const QColor& ImageMaskEditor::transparentColor() const
{
    return _maskTransparentColor;
}

int ImageMaskEditor::penWidth() const
{
    return _penWidth;
}

const QImage& ImageMaskEditor::backgroundImage() const
{
    return _backgroundImage;
}

QImage ImageMaskEditor::maskImage() const
{
    QImage i = _maskImage;
    for (int x = 0; x < i.width(); ++x)
    {
        for (int y = 0; y < i.height(); ++y)
        {
            QColor color {i.pixel(x, y)};
            if (color.alpha() == 0)
            {
                color = _maskTransparentColor;
            }
            else
            {
                color.setAlpha(255);
            }
            i.setPixel(x, y, color.rgba());
        }
    }
    return i;
}

QSize ImageMaskEditor::imageSize() const
{
    return _backgroundImage.size();
}

QSize ImageMaskEditor::sizeHint() const
{
    return imageSize();
}

void ImageMaskEditor::setMaskColor(const QColor& newColor)
{
    _penColor = newColor;
    _penColor.setAlpha(255); // prevent blending of masks
}

void ImageMaskEditor::setMaskColor(int r, int g, int b)
{
    _penColor.setRed(r);
    _penColor.setGreen(g);
    _penColor.setBlue(b);
    _penColor.setAlpha(255); // prevent blending of masks
}

void ImageMaskEditor::setMaskColor(qreal r, qreal g, qreal b)
{
    _penColor.setRedF(r);
    _penColor.setGreenF(g);
    _penColor.setBlueF(b);
    _penColor.setAlpha(255); // prevent blending of masks
}


void ImageMaskEditor::setTransparentColor(const QColor& newColor)
{
    _maskTransparentColor = newColor;
    _maskTransparentColor.setAlpha(255); // prevent blending of masks
}

void ImageMaskEditor::setTransparentColor(int r, int g, int b)
{
    _maskTransparentColor.setRed(r);
    _maskTransparentColor.setGreen(g);
    _maskTransparentColor.setBlue(b);
    _maskTransparentColor.setAlpha(255); // prevent blending of masks
}

void ImageMaskEditor::setTransparentColor(qreal r, qreal g, qreal b)
{
    _maskTransparentColor.setRedF(r);
    _maskTransparentColor.setGreenF(g);
    _maskTransparentColor.setBlueF(b);
    _maskTransparentColor.setAlpha(255); // prevent blending of masks
}

void ImageMaskEditor::clearMaskImage()
{
    _maskImage.fill(qRgba(0, 0, 0, 0));
    update();
}

void ImageMaskEditor::setPenWidth(int newWidth)
{
    _penWidth = std::max(1, newWidth);
}

void ImageMaskEditor::setMaskAlpha(int newAlpha)
{
    _maskAlpha = std::max(1, std::min(255, newAlpha));
    update();
}

void ImageMaskEditor::setImageSize(QSize size, bool doNotShrink)
{
    if (doNotShrink)
    {
        size = imageSize().expandedTo(size);
    }
    setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    setFixedSize(size);
    resizeImage(&_backgroundImage, size, Qt::white);
    resizeImage(&_maskImage, size, _maskTransparentColor);
    update();
}

void ImageMaskEditor::resizeImage(QSize size, bool doNotShrink)
{
    setImageSize(size, doNotShrink);
}

void ImageMaskEditor::setMaskImage(const QImage& i)
{
    _maskImage = i.copy();
    resizeImage(i.size(), true);
    update();
}

void ImageMaskEditor::setBackgroundImage(const QImage& i)
{
    _backgroundImage = i.copy();
    resizeImage(i.size(), false);
    _maskModified = true;
    update();
}

void ImageMaskEditor::mousePressEvent(QMouseEvent* event)
{
    if (event->button() == Qt::LeftButton)
    {
        _lastPoint = event->pos();
        drawOnMask(_lastPoint);
        _drawing = true;
    }
}

void ImageMaskEditor::mouseMoveEvent(QMouseEvent* event)
{
    if ((event->buttons() & Qt::LeftButton) && _drawing)
    {
        drawOnMask(event->pos());
    }
    else if (_drawPenCircle)
    {
        int rad = (_penWidth / 2) + 2;
        QPoint border{rad, rad};
        update(QRect(event->pos() - border, event->pos() + border));
    }
}

void ImageMaskEditor::mouseReleaseEvent(QMouseEvent* event)
{
    if (event->button() == Qt::LeftButton && _drawing)
    {
        drawOnMask(event->pos());
        _drawing = false;
    }
}

void ImageMaskEditor::paintEvent(QPaintEvent* event)
{
    QPainter painter(this);
    QRect dirtyRect = event->rect();
    painter.drawImage(dirtyRect, _backgroundImage, dirtyRect);
    painter.setCompositionMode(QPainter::CompositionMode_SourceOver);
    for (
        int x = std::max(0, dirtyRect.x());
        x < std::min(dirtyRect.x() + dirtyRect.width(), _maskImage.width());
        ++x
    )
    {
        for (
            int y = std::max(0, dirtyRect.y());
            y < std::min(dirtyRect.y() + dirtyRect.height(), _maskImage.height()) ;
            ++y
        )
        {
            QColor clr {_maskImage.pixel(x, y)};
            if (
                clr.red()   == _maskTransparentColor.red()   &&
                clr.green() == _maskTransparentColor.green() &&
                clr.blue()  == _maskTransparentColor.blue()
            )
            {
                clr = QColor(0, 0, 0, 0);
            }
            else if (clr.alpha() != 0)
            {
                clr.setAlpha(_maskAlpha);
            }
            _maskImage.setPixel(x, y, clr.rgba());
        }
    }
    painter.drawImage(dirtyRect, _maskImage, dirtyRect);
    if (_maskModified)
    {
        _maskModified = false;
        emit maskUpdated();
        if (!_drawing)
        {
            emit maskUpdateFinished();
        }
    }
    //draw brush circle
    if (_drawPenCircle)
    {
        const auto pos = mapFromGlobal(QCursor::pos());
        if (pos.x() >= 0 && pos.x() < width() && pos.y() >= 0 && pos.y() < height())
        {
            painter.setBrush(Qt::NoBrush);
            {
                QPen pen(Qt::white);
                pen.setWidth(2);
                painter.setPen(pen);
                painter.drawEllipse(QPointF(pos), _penWidth / 2.0, _penWidth / 2.0);
            }
            {
                QPen pen(Qt::black);
                pen.setWidth(2);
                painter.setPen(pen);
                painter.drawEllipse(QPointF(pos), _penWidth / 2.0 - 2, _penWidth / 2.0 - 2);
            }
        }
    }
}

void ImageMaskEditor::drawOnMask(const QPoint& endPoint)
{
    QPainter painter(&_maskImage);
    if (_lastPoint == endPoint)
    {
        if (_penWidth > 1)
        {
            painter.setPen(Qt::NoPen);
            painter.setBrush(_penColor);
            const auto w = _penWidth / 2.0;
            painter.drawEllipse(QPointF(_lastPoint), w, w);
        }
        else
        {
            painter.setPen(_penColor);
            painter.drawPoint(_lastPoint); // fix for w = 1
        }
    }
    else
    {
        painter.setPen(QPen(_penColor, _penWidth, Qt::SolidLine, Qt::RoundCap,
                            Qt::RoundJoin));
        painter.drawLine(_lastPoint, endPoint);
    }

    int rad = (_penWidth / 2) + 2;
    update(QRect(_lastPoint, endPoint).normalized()
           .adjusted(-rad, -rad, +rad, +rad));
    _lastPoint = endPoint;
    _maskModified = true;
}

void ImageMaskEditor::resizeImage(QImage* image, const QSize& newSize, const QColor& fillcolor)
{
    if (image->size() == newSize)
    {
        return;
    }

    QImage newImage(newSize, QImage::Format_ARGB32);
    newImage.fill(fillcolor);
    QPainter painter(&newImage);
    painter.drawImage(QPoint(0, 0), *image);
    *image = newImage;
}

void ImageMaskEditor::setPenCircleVisible(bool b)
{
    _drawPenCircle = b;
}
