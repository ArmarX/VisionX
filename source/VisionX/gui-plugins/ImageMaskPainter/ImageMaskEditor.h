#pragma once

#include <QColor>
#include <QImage>
#include <QPoint>
#include <QWidget>

class ImageMaskEditor : public QWidget
{
    Q_OBJECT

public:
    ImageMaskEditor(QSize size, QWidget* parent = nullptr);

    ImageMaskEditor(int width = 64, int height = 64, QWidget* parent = nullptr);

    bool openMaskImage(const QString& fileName);
    bool openBackgroundImage(const QString& fileName);

    bool saveMaskImage(const QString& fileName, const char* fileFormat);
    bool saveBackgroundImage(const QString& fileName, const char* fileFormat);

    const QColor& maskColor() const;
    const QColor& transparentColor() const;
    int penWidth() const;

    const QImage& backgroundImage() const;
    QImage maskImage() const;

    QSize imageSize() const;

    QSize sizeHint() const override;

public slots:
    void setMaskColor(const QColor& newColor);
    void setMaskColor(int r, int g, int b);
    void setMaskColor(qreal r, qreal g, qreal b);
    void setTransparentColor(const QColor& newColor);
    void setTransparentColor(int r, int g, int b);
    void setTransparentColor(qreal r, qreal g, qreal b);
    void clearMaskImage();
    void setPenWidth(int newWidth);
    void setMaskAlpha(int newAlpha);
    void setImageSize(QSize size, bool doNotShrink = false);
    void resizeImage(QSize size, bool doNotShrink = false);
    void setMaskImage(const QImage& i);
    void setBackgroundImage(const QImage& i);
    void setPenCircleVisible(bool b = true);

signals:
    void maskUpdated();
    void maskUpdateFinished();
    void arrowDrawn(const QPoint& arrowStart, const QPoint& arrowEnd);

protected:
    void mousePressEvent(QMouseEvent* event) override;
    void mouseMoveEvent(QMouseEvent* event) override;
    void mouseReleaseEvent(QMouseEvent* event) override;
    void paintEvent(QPaintEvent* event) override;

private:
    void drawOnMask(const QPoint& endPoint);
    void resizeImage(QImage* image, const QSize& newSize, const QColor& fillcolor);

    bool    _drawing {false};
    bool    _maskModified {false};
    bool    _drawPenCircle {false};

    int    _penWidth {1};
    QColor _penColor {Qt::red};

    QColor _maskTransparentColor {Qt::black};
    QImage _maskImage;
    int    _maskAlpha {0};

    QImage _backgroundImage;

    QPoint _lastPoint;
};
