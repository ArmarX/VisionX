/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::gui-plugins::ImageMaskPainterWidgetController
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <ArmarXCore/core/system/ImportExportComponent.h>

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>
#include <ArmarXGui/libraries/SimpleConfigDialog/SimpleConfigDialog.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/widgets/IceProxyFinder.h>

#include <VisionX/core/ImageProcessor.h>
#include <VisionX/interface/core/ImageProviderInterface.h>
#include <VisionX/interface/components/Calibration.h>

#include <VisionX/gui-plugins/ImageMaskPainter/ui_ImageMaskPainterWidget.h>
#include "ImageMaskEditor.h"

namespace armarx
{
    /**
    \page VisionX-GuiPlugins-ImageMaskPainter ImageMaskPainter
    \brief The ImageMaskPainter allows visualizing ...

    \image html ImageMaskPainter.png
    The user can

    API Documentation \ref ImageMaskPainterWidgetController

    \see ImageMaskPainterGuiPlugin
    */

    /**
     * \class ImageMaskPainterWidgetController
     * \brief ImageMaskPainterWidgetController brief one line description
     *
     * Detailed description
     */
    class ImageMaskPainterWidgetController:
        public armarx::ArmarXComponentWidgetControllerTemplate < ImageMaskPainterWidgetController >,
        virtual public visionx::ImageProcessor
    {
        Q_OBJECT
    public:
        /// Controller Constructor
        explicit ImageMaskPainterWidgetController();

        /// Controller destructor
        virtual ~ImageMaskPainterWidgetController();

        /**
         * @see ArmarXWidgetController::loadSettings()
         */
        void loadSettings(QSettings* settings) override;

        /**
         * @see ArmarXWidgetController::saveSettings()
         */
        void saveSettings(QSettings* settings) override;


        /**
         * Returns the Widget name displayed in the ArmarXGui to create an
         * instance of this class.
         */
        static QString GetWidgetName();

        QPointer<QDialog> getConfigDialog(QWidget* parent) override;
    protected:
        void onInitImageProcessor() override;
        void onConnectImageProcessor() override;
        void onExitImageProcessor()    override {}
        void process() override;
        void timerEvent(QTimerEvent*) override;

    private slots:
        void updatePenColor();
        void imageIndexChanged(int i);
        void maskUpdated();
    private:
        /// Widget Form
        Ui::ImageMaskPainterWidget                          _widget;
        //mask
        ImageMaskEditor*                                    _maskEditor{nullptr};

        std::string                                         _imageProviderName;
        visionx::ImageProviderInfo                          _imageProviderInfo;
        visionx::ImageProviderInterfacePrx                  _imageProvider;
        std::atomic_int                                     _imageIndex{0};
        std::atomic_int                                     _numberOfImages{1};
        std::string                                         _imageProviderReferenceFrame;
        visionx::StereoCalibration                          _imageProviderCalibration;
        bool                                                _imageProviderAreImagesUndistorted;

        QPointer<SimpleConfigDialog>                        _dialog;

        std::vector<visionx::CByteImageUPtr>                _providerImagesOwner;
        std::vector<void*>                                  _providerImages;
        QImage                                              _currentImage;
        std::mutex                                          _currentImageMutex;
        std::atomic_bool                                    _currentImageDirty{false};

        bool                                                _maskSet = false;
        visionx::CByteImageUPtr                             _resultImage;

        // ArmarXWidgetController interface
    public:
        void configured() override;
    };
}


