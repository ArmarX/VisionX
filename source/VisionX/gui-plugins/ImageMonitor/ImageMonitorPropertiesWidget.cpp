/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Kai Welke ( welke at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/


#include <VisionX/gui-plugins/ImageMonitor/ImageMonitorPropertiesWidget.h>


// STD/STL
#include <map>
#include <string>

// Qt
#include <QApplication>

// ArmarXGui
#include <ArmarXGui/libraries/ArmarXGuiBase/widgets/IceProxyFinder.h>


using namespace armarx;


namespace visionx
{
    ImageMonitorPropertiesWidget::ImageMonitorPropertiesWidget()
    {
        ui.setupUi(this);
        proxyFinder = new armarx::IceProxyFinder<ImageProviderInterfacePrx>(this);
        proxyFinder->setSearchMask("*Provider|*Result");
        ui.horizontalLayout->addWidget(proxyFinder);

        connect(proxyFinder, SIGNAL(validProxySelected(QString)), this, SLOT(onValidProxySelected(QString)));
        connect(ui.spinBoxDepthImageIndex, SIGNAL(valueChanged(int)), this, SLOT(refreshRecordingWidgets()));
        connect(ui.comboBoxCompressionType, SIGNAL(currentIndexChanged(int)), this, SLOT(compressionTypeChanged(int)));

        // Initialise options for frame rate dropdown
        this->ui.comboBoxFrameRate->addItem("Source framerate", -1);
        for (unsigned int i = 30; i > 0; i -= 5)
        {
            this->ui.comboBoxFrameRate->addItem(QString::number(i) + " fps", static_cast<int>(i));
        }
        this->ui.comboBoxFrameRate->addItem("1 fps", 1);

        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        this->setSizePolicy(sizePolicy);
    }

    ImageMonitorPropertiesWidget::~ImageMonitorPropertiesWidget()
    {
        // pass
    }

    void ImageMonitorPropertiesWidget::update(ImageMonitorProperties properties, IceManagerPtr iceManager)
    {
        this->iceManager = iceManager;
        proxyFinder->setIceManager(iceManager, proxyFinder->getSelectedProxyName().isEmpty());
        proxyFinder->setDefaultSelectedProxy(QString::fromStdString(properties.providerName));

        // select framerate
        this->ui.comboBoxFrameRate->setCurrentIndex(this->ui.comboBoxFrameRate->findData(properties.frameRate));

        ui.doubleSpinBoxBufferFps->setValue(static_cast<double>(properties.bufferFps));
        ui.spinBoxImageBuffer->setValue(properties.imageBufferSize);

        QStringList l;
        for (auto number : properties.imagesToShow)
        {
            l << QString::number(number);
        }
        ui.lineEditImagesToShow->setText(l.join(","));

        ui.spinBoxMaxDepth->setValue(properties.maxDepthmm);
        ui.spinBoxDepthImageIndex->setValue(properties.depthImageIndex);

        ui.comboBoxCompressionType->setCurrentIndex((int)properties.compressionType);
        //        compressionTypeChanged((int)properties.compressionType);
        ui.spinBoxCompressionQuality->setValue(properties.compressionQuality);
    }

    ImageMonitorProperties ImageMonitorPropertiesWidget::getProperties()
    {
        ImageMonitorProperties properties;

        // retrieve provider name
        QString text = proxyFinder->getSelectedProxyName();
        std::string providerName = text.toStdString();
        properties.providerName = providerName;

        // set framerate
        properties.frameRate = this->ui.comboBoxFrameRate->itemData(this->ui.comboBoxFrameRate->currentIndex()).toInt();

        // buffer properties
        properties.imageBufferSize = ui.spinBoxImageBuffer->value();
        properties.bufferFps = static_cast<float>(ui.doubleSpinBoxBufferFps->value());
        QStringList list = ui.lineEditImagesToShow->text().split(",");

        for (QString& s : list)
        {
            bool ok;
            auto value = s.toULong(&ok);
            if (ok)
            {
                properties.imagesToShow.insert(value);
            }
        }
        for (auto i : properties.imagesToShow)
        {
            ARMARX_INFO_S << i;
        }
        properties.depthImageIndex = ui.spinBoxDepthImageIndex->value();
        properties.maxDepthmm = ui.spinBoxMaxDepth->value();

        properties.compressionType = static_cast<CompressionType>(ui.comboBoxCompressionType->currentIndex());
        properties.compressionQuality = ui.spinBoxCompressionQuality->value();

        return properties;
    }

    void
    ImageMonitorPropertiesWidget::onValidProxySelected(const QString& proxyName)
    {
        try
        {
            // Try to get the object proxy "objPrx" represented by "proxyName"
            IceGrid::AdminPrx admin = this->iceManager->getIceGridSession()->getAdmin();
            Ice::Identity objectIceId = Ice::stringToIdentity(proxyName.toStdString());
            visionx::ImageProviderInterfacePrx imageProviderPrx = visionx::ImageProviderInterfacePrx::checkedCast(admin->getObjectInfo(objectIceId).proxy);
            unsigned int numImages = static_cast<unsigned int>(imageProviderPrx->getNumberImages());
            this->numImageSources = numImages;
        }
        catch (...)
        {
            // pass
        }
    }

    void ImageMonitorPropertiesWidget::compressionTypeChanged(int index)
    {
        ui.spinBoxCompressionQuality->setEnabled(index != 0);
        if (index == 1)
        {
            ui.spinBoxCompressionQuality->setValue(9);
        }
        else if (index == 2)
        {
            ui.spinBoxCompressionQuality->setValue(95);
        }
    }
}
