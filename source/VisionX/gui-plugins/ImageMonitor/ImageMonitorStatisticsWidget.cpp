/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Kai Welke ( welke at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "ImageMonitorStatisticsWidget.h"
#include "VisionX/tools/TypeMapping.h"

using namespace armarx;

namespace visionx
{
    ImageMonitorStatisticsWidget::ImageMonitorStatisticsWidget()
    {
        ui.setupUi(this);
    }

    ImageMonitorStatisticsWidget::~ImageMonitorStatisticsWidget()
    {

    }

    void ImageMonitorStatisticsWidget::update(std::string providerName, ImageProviderInfo providerInfo, ImageTransferStats transferStats)
    {
        // provider info
        ui.providerNameLabel->setText(QString(providerName.c_str()));
        QString numberImages = QString("%1").arg(providerInfo.numberImages);
        ui.numberImagesLabel->setText(numberImages);
        QString imageSize = QString("%1x%2").arg(providerInfo.imageFormat.dimension.width).arg(providerInfo.imageFormat.dimension.height);
        ui.imageSizeLabel->setText(imageSize);

        std::string imageTypeName = visionx::tools::imageTypeToTypeName(providerInfo.imageFormat.type);
        ui.imageFormatLabel->setText(QString(imageTypeName.c_str()));

        // broadcast statistics
        ui.bcFpsLabel->setText(QString("%1").arg(transferStats.imageProviderFPS.getFPS()));
        ui.bcCycleMeanLabel->setText(QString("%1").arg(transferStats.imageProviderFPS.getMeanCycleTimeMS()));
        ui.bcCycleMinLabel->setText(QString("%1").arg(transferStats.imageProviderFPS.getMinCycleTimeMS()));
        ui.bcCycleMaxLabel->setText(QString("%1").arg(transferStats.imageProviderFPS.getMaxCycleTimeMS()));

        // polling statistics
        ui.plFpsLabel->setText(QString("%1").arg(transferStats.pollingFPS.getFPS()));
        ui.plCycleMeanLabel->setText(QString("%1").arg(transferStats.pollingFPS.getMeanCycleTimeMS()));
        ui.plCycleMinLabel->setText(QString("%1").arg(transferStats.pollingFPS.getMinCycleTimeMS()));
        ui.plCycleMaxLabel->setText(QString("%1").arg(transferStats.pollingFPS.getMaxCycleTimeMS()));
    }
}
