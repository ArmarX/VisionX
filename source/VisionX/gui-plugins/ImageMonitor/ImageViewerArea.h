/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Kai Welke ( welke at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

// ArmarX includes
#include <QWidget>

#include <ArmarXCore/core/logging/Logging.h>

#include "Image/ByteImage.h"
#include <Math/Math2d.h>

#include <mutex>

namespace visionx
{

    class ImageViewerArea :
        public QWidget,
        public armarx::Logging
    {
        Q_OBJECT
    public:
        ImageViewerArea(QWidget* parent = 0);
        ~ImageViewerArea() override;

        void setImages(int numberImages, CByteImage** images, IceUtil::Time imageTimestamp, IceUtil::Time receiveTimestamp);
        Vec2d getScaledImageDimensions();

        IceUtil::Time getTimeDisplayed() const;

        IceUtil::Time getDisplayDelay() const;

    public slots:

    signals:
    protected slots:
        void updateImage(long timeReceived);
    protected:
        void paintEvent(QPaintEvent* pPaintEvent) override;

        int getNumberImages();

    private:
        int inputWidth;
        int inputHeight;
        int numberImages;
        IceUtil::Time timeDisplayed;

        unsigned char* buffer;
        std::mutex bufferMutex;
        QImage scaledImage;
        std::mutex imageMutex;
        IceUtil::Time imageTimestamp;
        IceUtil::Time receiveTimestamp;
        IceUtil::Time displayDelay;
    };
}


