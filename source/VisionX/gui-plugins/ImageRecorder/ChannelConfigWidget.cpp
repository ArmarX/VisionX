/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::gui-plugins::ImageRecorder
 * @author     Christian R. G. Dreher <c.dreher@kit.edu>
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include <VisionX/gui-plugins/ImageRecorder/ChannelConfigWidget.h>


// Qt
#include <QStandardItemModel>

// Simox
#include <SimoxUtility/algorithm/string/string_tools.h>


visionx::ChannelConfigWidget::ChannelConfigWidget(QWidget* parent, const visionx::imrec::ChannelConfig& channel_config, const visionx::imrec::ChannelPreferences& channel_prefs)
    : QWidget(parent),
      m_channel_name{channel_config.name},
      m_channel_prefs{channel_prefs}
{
    m_widget.setupUi(this);

    const QString q_channel_name = QString::fromStdString(channel_config.name);
    m_widget.channels_config->setTitle(QString{"Channel: "} + q_channel_name);
    m_widget.channels_config->setChecked(not channel_config.disabled);
    m_widget.name->setText(q_channel_name);

    for (const auto& [format, reg_entry] : visionx::imrec::getFormatsMap())
    {
        m_widget.file_format->addItem(QString::fromStdString(reg_entry.hr_name));
    }
    const QString q_format_str = QString::fromStdString(imrec::format2hrstr(imrec::str2format(channel_config.format)));
    m_widget.file_format->setCurrentText(q_format_str);

    update_file_formats(channel_prefs.requiresLossless);
    set_visible_file_fps(m_widget.file_format->currentText());

    connect(m_widget.name, SIGNAL(textEdited(QString)), this, SLOT(channel_name_changed(QString)));
    connect(m_widget.file_format, SIGNAL(currentTextChanged(QString)), this, SLOT(file_format_changed(QString)));

    m_widget.lossless->setChecked(channel_prefs.requiresLossless);
    connect(m_widget.lossless, SIGNAL(stateChanged(int)), this, SLOT(lossless_state_changed(int)));
}


visionx::ChannelConfigWidget::~ChannelConfigWidget()
{

}


const std::string&
visionx::ChannelConfigWidget::getChannelName() const
{
    return m_channel_name;
}


void
visionx::ChannelConfigWidget::confirmChannelName(const std::string& new_name)
{
    m_widget.channels_config->setTitle(QString::fromStdString("Channel: " + new_name));
    m_channel_name = new_name;
}


void
visionx::ChannelConfigWidget::rejectChannelName(const std::string& new_name)
{
    // TODO: Display fancy graphics.
    ARMARX_WARNING << "Invalid name '" << new_name << "' for channel (already taken).";
}


void
visionx::ChannelConfigWidget::setChannelPreferences(const imrec::ChannelPreferences& channel_prefs)
{
    update_file_formats(channel_prefs.requiresLossless);
}


visionx::imrec::ChannelConfig
visionx::ChannelConfigWidget::toChannelConfig(bool disabled) const
{
    imrec::ChannelConfig cc;
    cc.name = m_channel_name;
    cc.disabled = disabled or not m_widget.channels_config->isChecked();
    const imrec::Format format = imrec::str2format(m_widget.file_format->currentText().toStdString());
    cc.format = imrec::format2hrstr(format);
    cc.fps = m_widget.file_fps->value();
    return cc;
}


void
visionx::ChannelConfigWidget::update_file_formats(bool require_lossless)
{
    QStandardItemModel* model = qobject_cast<QStandardItemModel*>(m_widget.file_format->model());
    for (int i = 0; i < m_widget.file_format->count(); ++i)
    {
        QStandardItem* item = model->item(i);
        const imrec::Format format = imrec::str2format(item->text().toStdString());
        const bool disabled = require_lossless and not imrec::isFormatLossless(format);
        item->setFlags(disabled ? item->flags() & ~Qt::ItemIsEnabled : item->flags() | Qt::ItemIsEnabled);
    }

    // If require_lossless is set and current selected value is lossy, choose bmp image sequence.
    if (require_lossless)
    {
        const std::string format_str = m_widget.file_format->currentText().toStdString();
        const imrec::Format format = imrec::str2format(format_str);
        if (not imrec::isFormatLossless(format))
        {
            const QString q_format_str = QString::fromStdString(imrec::format2hrstr(imrec::Format::bmp_img_seq));
            m_widget.file_format->setCurrentText(q_format_str);
        }
    }
}


void
visionx::ChannelConfigWidget::set_visible_file_fps(const QString& q_format_str)
{
    const std::string format_str = q_format_str.toStdString();
    const imrec::Format format = imrec::str2format(format_str);
    set_visible_file_fps(not imrec::isFormatDynamicFramerate(format));
}


void
visionx::ChannelConfigWidget::set_visible_file_fps(bool visible)
{
    m_widget.file_fps->setVisible(visible);
    m_widget.label_file_fps->setVisible(visible);
}


void
visionx::ChannelConfigWidget::channel_name_changed(const QString& q_new_name)
{
    const std::string new_name = simox::alg::trim_copy(q_new_name.toStdString());
    const std::string old_name = m_channel_name;
    if (new_name != old_name)
    {
        emit channelRenamed(old_name, new_name);
    }
}


void
visionx::ChannelConfigWidget::lossless_state_changed(int state)
{
    update_file_formats(state == Qt::Checked);
}


void
visionx::ChannelConfigWidget::file_format_changed(const QString& new_format_str)
{
    set_visible_file_fps(new_format_str);
}
