/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::gui-plugins::ImageRecorder
 * @author     Christian R. G. Dreher <c.dreher@kit.edu>
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include <VisionX/gui-plugins/ImageRecorder/ImageProviderConfigWidget.h>


// Qt
#include <QIcon>
#include <QProgressBar>

// Simox
#include <SimoxUtility/algorithm/string/string_tools.h>

// ArmarX
#include <ArmarXCore/core/logging/Logging.h>
#include <VisionX/gui-plugins/ImageRecorder/ChannelConfigWidget.h>
#include <VisionX/gui-plugins/ImageRecorder/ImageRecorderWidgetController.h>


namespace
{
    struct Progress
    {
        int min;
        int max;
        int val;
    };

    const Progress invalid_progress{0, 1, -1};
    const Progress indefinite_progress{0, 0, 0};

    void set_progress(QProgressBar* progress_bar, const Progress& progress)
    {
        progress_bar->setMinimum(progress.min);
        progress_bar->setMaximum(progress.max);
        progress_bar->setValue(progress.val);
    }
}


visionx::ImageProviderConfigWidget::ImageProviderConfigWidget(QWidget* parent, armarx::IceManagerPtr iceman, const std::string& image_provider_name, const visionx::imrec::ChannelConfigs& channel_configs)
    : QWidget(parent),
      m_iceman{iceman},
      m_image_provider_name{image_provider_name}
{
    m_widget.setupUi(this);
    connect(this, &ImageProviderConfigWidget::imageProviderStateChanged, this, &ImageProviderConfigWidget::state_changed);
    connect(m_widget.remove_image_provider, &QPushButton::clicked, this, &ImageProviderConfigWidget::image_provider_removed);

    const QString q_image_provider_name = QString::fromStdString(m_image_provider_name);
    m_widget.configs->setTitle(QString{"Image provider: "} + q_image_provider_name);

    // Replace or remove dummy values from the UI designer.
    set_progress(m_widget.write_progress, ::invalid_progress);
    m_widget.ice_proxy_finder_placeholder->deleteLater();  // Remove placeholder.

    // Timer for blink animation while recording/writing.
    m_blink_timer = new QTimer(this);
    m_blink_timer->setSingleShot(false);
    m_blink_timer->setInterval(750);
    connect(m_blink_timer, &QTimer::timeout, this, &ImageProviderConfigWidget::blink);

    // Ice proxy finder to select a recording image provider.
    m_ice_proxy_finder = new armarx::IceProxyFinder<RecordingImageProviderInterfacePrx>(this);
    m_widget.ice_proxy_finder->layout()->addWidget(m_ice_proxy_finder);
    m_ice_proxy_finder->setIceManager(iceman);
    m_ice_proxy_finder->setDefaultSelectedProxyAsItem(q_image_provider_name);
    m_ice_proxy_finder->setSearchMask("*");
    connect(m_ice_proxy_finder->getProxyNameComboBox(), SIGNAL(editTextChanged(QString)), this, SLOT(image_provider_renamed(QString)));

    // Initialise UI according to channel settings and channel preferences.
    std::vector<imrec::ChannelPreferences> channel_prefs_list;
    std::vector<imrec::ChannelPreferences> reported_channel_prefs_list = get_channel_prefs();
    const bool use_default_channel_prefs = reported_channel_prefs_list.size() != channel_configs.size();
    for (unsigned int i = 0; i < channel_configs.size(); ++i)
    {
        imrec::ChannelPreferences channel_prefs;
        if (use_default_channel_prefs)
        {
            channel_prefs.name = "c" + std::to_string(i);
            channel_prefs.requiresLossless = false;
        }
        else
        {
            channel_prefs = reported_channel_prefs_list.at(i);
        }
        channel_prefs_list.push_back(channel_prefs);
    }

    build_ui_from_configs(channel_configs, channel_prefs_list);
}


visionx::ImageProviderConfigWidget::~ImageProviderConfigWidget()
{

}


void
visionx::ImageProviderConfigWidget::confirmImageProviderName(const std::string& new_name)
{
    m_widget.configs->setTitle(QString::fromStdString("Image provider: " + new_name));
    m_ice_proxy_finder->setDefaultSelectedProxyAsItem(QString::fromStdString(new_name));
    m_image_provider_name = new_name;
    update_channel_list();
}


void
visionx::ImageProviderConfigWidget::rejectImageProviderName(const std::string& new_name)
{
    // TODO: Display fancy graphics.
    ARMARX_WARNING << "Invalid name '" << new_name << "' for image provider (already taken).";
}


std::vector<visionx::imrec::ChannelConfig>
visionx::ImageProviderConfigWidget::toChannelConfigs() const
{
    const bool disabled = false /*not m_widget.configs->isChecked()*/ ;
    std::vector<visionx::imrec::ChannelConfig> ccs;
    std::transform(m_channel_config_widgets.begin(), m_channel_config_widgets.end(),
                   std::back_inserter(ccs), [&](const auto * ccw)
    {
        return ccw->toChannelConfig(disabled);
    });
    return ccs;
}


void
visionx::ImageProviderConfigWidget::setImageProviderStatus(std::optional<visionx::imrec::Status> status)
{
    const std::optional<imrec::State> old_state = m_current_state;
    std::optional<imrec::State> new_state;
    if (status.has_value())
    {
        new_state = status.value().type;
    }

    Progress progress;

    if (status.has_value() and status.value().type != imrec::State::offline)
    {
        const imrec::Status s = status.value();

        if (s.type == imrec::State::ready and s.framesWritten == 0 and s.framesBuffered == 0)
        {
            progress = ::invalid_progress;
        }
        else if (s.type == imrec::State::scheduled)
        {
            progress = ::indefinite_progress;
        }
        else
        {
            progress.min = 0;
            progress.max = s.framesWritten + s.framesBuffered;
            progress.val = s.framesWritten;
        }
    }
    else
    {
        progress = ::invalid_progress;
    }

    set_progress(m_widget.write_progress, progress);

    m_current_state = new_state;
    if (old_state != new_state)
    {
        emit imageProviderStateChanged(old_state, new_state);
    }
}


void
visionx::ImageProviderConfigWidget::setViewMode(ViewMode viewMode)
{
    switch (viewMode)
    {
        case ViewMode::Full:
            m_widget.line->show();
            m_widget.channel_configs->show();
            m_widget.buttons->show();
            m_widget.label_name->show();
            m_widget.ice_proxy_finder->show();
            break;
        case ViewMode::StatusOnly:
            m_widget.line->hide();
            m_widget.channel_configs->hide();
            m_widget.buttons->hide();
            m_widget.label_name->hide();
            m_widget.ice_proxy_finder->hide();
            break;
    }
}


void
visionx::ImageProviderConfigWidget::build_ui_from_configs(const std::vector<imrec::ChannelConfig>& configs, const std::vector<imrec::ChannelPreferences>& prefs_list)
{
    ARMARX_CHECK(configs.size() == prefs_list.size());

    for (ChannelConfigWidget* config_widget : m_channel_config_widgets)
    {
        m_widget.channel_configs->layout()->removeWidget(config_widget);
        config_widget->deleteLater();
    }

    for (unsigned int i = 0; i < configs.size(); ++i)
    {
        add_channel_conf_widget(configs.at(i), prefs_list.at(i));
    }
}


void
visionx::ImageProviderConfigWidget::add_channel_conf_widget(const imrec::ChannelConfig& channel_config, const imrec::ChannelPreferences& channel_prefs)
{
    // Assert that there's no channel config with the same name in the list.
    ARMARX_CHECK(is_channel_name_unique(channel_config.name));

    ChannelConfigWidget* ccw = new ChannelConfigWidget{m_widget.channel_configs, channel_config, channel_prefs};

    m_channel_config_widgets.push_back(ccw);
    m_widget.channel_configs->layout()->addWidget(ccw);

    connect(ccw, &ChannelConfigWidget::channelRenamed, this, &ImageProviderConfigWidget::channel_renamed);
}


void
visionx::ImageProviderConfigWidget::update_channel_list()
{
    std::vector<imrec::ChannelPreferences> prefs_list = get_channel_prefs();

    if (prefs_list.empty())
    {
        return;
    }

    const bool image_provider_changed = m_channel_config_widgets.size() != prefs_list.size();
    if (image_provider_changed)
    {
        // Delete all widgets and instantiate new ones.
        for (ChannelConfigWidget* ccw : m_channel_config_widgets)
        {
            m_widget.channel_configs->layout()->removeWidget(ccw);
            ccw->deleteLater();
        }
        m_channel_config_widgets.clear();

        // Build new UI.
        std::vector<imrec::ChannelConfig> ccs;
        for (const imrec::ChannelPreferences& prefs : prefs_list)
        {
            imrec::ChannelConfig cc;
            cc.disabled = false;
            cc.name = prefs.name;
            cc.format = imrec::format2str(imrec::Format::bmp_img_seq);
            ccs.push_back(cc);
        }
        build_ui_from_configs(ccs, prefs_list);
    }
}


std::vector<visionx::imrec::ChannelPreferences>
visionx::ImageProviderConfigWidget::get_channel_prefs()
{
    try
    {
        return m_iceman->getProxy<RecordingImageProviderInterfacePrx>(m_image_provider_name, "")->getImageRecordingChannelPreferences();
    }
    catch (...)
    {
        return {};
    }
}


bool
visionx::ImageProviderConfigWidget::is_channel_name_unique(const std::string& name)
{
    return std::find_if(m_channel_config_widgets.begin(), m_channel_config_widgets.end(), [&](const auto & e)
    {
        return e->getChannelName() == name;
    }) == m_channel_config_widgets.end();
}


void
visionx::ImageProviderConfigWidget::state_changed(std::optional<imrec::State>, std::optional<imrec::State> new_state)
{
    if (new_state.has_value() and (new_state.value() == imrec::State::running or new_state.value() == imrec::State::writing))
    {
        if (new_state.value() == imrec::State::running)
        {
            m_current_icon_name = "imrec_running";
        }
        else
        {
            m_current_icon_name = "imrec_write";
        }
        m_blink_timer->start();
    }
    else
    {
        m_blink_timer->stop();
    }

    QString icon_name;

    if (new_state.has_value())
    {
        switch (new_state.value())
        {
            case imrec::State::ready:
                m_widget.status->setText("ready");
                icon_name = ":/icons/imrec_ready.ico";
                break;
            case imrec::State::scheduled:
                m_widget.status->setText("scheduled");
                icon_name = ":/icons/imrec_scheduled.ico";
                break;
            case imrec::State::running:
                m_widget.status->setText("recording");
                icon_name = ":/icons/imrec_running.ico";
                break;
            case imrec::State::stopping:
                m_widget.status->setText("stopping");
                icon_name = ":/icons/imrec_stopping.ico";
                break;
            case imrec::State::writing:
                m_widget.status->setText("writing");
                icon_name = ":/icons/imrec_write.ico";
                break;
            case imrec::State::offline:
                m_widget.status->setText("offline");
                icon_name = ":/icons/imrec_offline.ico";
                break;
        }
    }
    else
    {
        m_widget.status->setText("not configured");
        icon_name = ":/icons/imrec_not_configured.ico";
    }

    QIcon icon;
    icon.addFile(icon_name, QSize(), QIcon::Normal, QIcon::On);
    m_widget.icon_placeholder->setPixmap(icon.pixmap(20, 20));
}


void
visionx::ImageProviderConfigWidget::blink()
{
    QString icon_name;
    if (m_icon_blink)
    {
        icon_name = QString::fromStdString(":/icons/" + m_current_icon_name + ".ico");
    }
    else
    {
        icon_name = QString::fromStdString(":/icons/" + m_current_icon_name + "_dark.ico");
    }
    m_icon_blink = not m_icon_blink;

    QIcon icon;
    icon.addFile(icon_name, QSize(), QIcon::Normal, QIcon::On);
    m_widget.icon_placeholder->setPixmap(icon.pixmap(20, 20));
}


void
visionx::ImageProviderConfigWidget::channel_renamed(const std::string& old_name, const std::string& new_name)
{
    auto it = std::find_if(m_channel_config_widgets.begin(), m_channel_config_widgets.end(), [&](const auto & c)
    {
        return c->getChannelName() == old_name;
    });
    if (it == m_channel_config_widgets.end())
    {
        ARMARX_ERROR << "Internal error, a channel was renamed which is not known to parent "
                     << "widget.  Old name: '" << old_name << "'.  New name: '" << new_name << "'.";
        return;
    }
    ChannelConfigWidget* ccw = *it;
    is_channel_name_unique(new_name) ? ccw->confirmChannelName(new_name) : ccw->rejectChannelName(new_name);
}


void
visionx::ImageProviderConfigWidget::image_provider_renamed(const QString& q_new_name)
{
    const std::string new_name = simox::alg::trim_copy(q_new_name.toStdString());
    const std::string old_name = m_image_provider_name;

    if (not q_new_name.isEmpty())
    {
        if (new_name != old_name)
        {
            emit imageProviderRenamed(old_name, new_name);
        }
    }
    else
    {
        m_ice_proxy_finder->setDefaultSelectedProxy(QString::fromStdString(old_name));
    }
}


void
visionx::ImageProviderConfigWidget::image_provider_removed()
{
    emit imageProviderRemoved(m_image_provider_name);
}
