/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::gui-plugins::ImageRecorder
 * @author     Christian R. G. Dreher <c.dreher@kit.edu>
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


// STD/STL
#include <optional>
#include <string>

// Qt
#include <QPointer>
#include <QTimer>
#include <QWidget>

// ArmarX
#include <ArmarXCore/core/IceManager.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/widgets/IceProxyFinder.h>
#include <VisionX/gui-plugins/ImageRecorder/ui_ImageProviderConfigWidget.h>
#include <VisionX/interface/core/ImageProviderInterface.h>
#include <VisionX/libraries/imrec.h>


namespace visionx
{

    class ChannelConfigWidget;
    class ImageRecorderWidgetController;

    enum class ViewMode
    {
        Full,
        StatusOnly
    };

    class ImageProviderConfigWidget : public QWidget
    {

        Q_OBJECT

    private:

        Ui::image_provider_config_widget m_widget;
        QPointer<armarx::IceProxyFinder<RecordingImageProviderInterfacePrx>> m_ice_proxy_finder;
        QPointer<QTimer> m_blink_timer;
        armarx::IceManagerPtr m_iceman;
        std::string m_image_provider_name;
        std::vector<ChannelConfigWidget*> m_channel_config_widgets;
        bool m_icon_blink = false;
        std::string m_current_icon_name;
        std::optional<imrec::State> m_current_state;
        imrec::Status m_current_status;

    public:

        ImageProviderConfigWidget(QWidget* parent, armarx::IceManagerPtr iceman, const std::string& image_provider_name, const visionx::imrec::ChannelConfigs& channel_configs);

        virtual ~ImageProviderConfigWidget();

        void confirmImageProviderName(const std::string& new_name);

        void rejectImageProviderName(const std::string& new_name);

        std::vector<visionx::imrec::ChannelConfig> toChannelConfigs() const;

        void setImageProviderStatus(std::optional<visionx::imrec::Status> status);

        void setViewMode(ViewMode viewMode);

    private:

        void build_ui_from_configs(const std::vector<imrec::ChannelConfig>& configs, const std::vector<imrec::ChannelPreferences>& prefs_list);

        void add_channel_conf_widget(const imrec::ChannelConfig& channel_config, const imrec::ChannelPreferences& channel_prefs);

        void update_channel_list();

        std::vector<imrec::ChannelPreferences> get_channel_prefs();

        bool is_channel_name_unique(const std::string& name);

    signals:

        void imageProviderRenamed(const std::string& old_name, const std::string& new_name);

        void imageProviderRemoved(const std::string& name);

        void imageProviderStateChanged(std::optional<imrec::State> old_state, std::optional<imrec::State> new_state);

    private slots:

        void state_changed(std::optional<imrec::State> old_state, std::optional<imrec::State> new_state);

        void blink();

        void channel_renamed(const std::string& old_name, const std::string& new_name);

        void image_provider_renamed(const QString& name);

        void image_provider_removed();

    };

}
