/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::gui-plugins::ImageRecorder
 * @author     Christian R. G. Dreher <c.dreher@kit.edu>
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include <VisionX/gui-plugins/ImageRecorder/ImageRecorderWidgetController.h>


// STD/STL
#include <thread>

// Qt
#include <QApplication>
#include <QClipboard>

// ArmarX
#include <VisionX/components/ImageRecordingManager/json_conversions.h>
#include <VisionX/libraries/imrec.h>


namespace
{
    static const std::string config_key_imrecman = "imrecman_name";
    static const std::string config_key_start_in = "start_in";
}


visionx::ImageRecorderWidgetController::ImageRecorderWidgetController()
{
    ARMARX_TRACE;

    using this_class = ImageRecorderWidgetController;

    m_widget.setupUi(getWidget());
    m_poll_timer = new QTimer(getWidget());
    m_poll_timer->setSingleShot(false);
    m_poll_timer->setInterval(50);
    uninit_gui();

    connect(m_poll_timer, &QTimer::timeout, this, &this_class::poll_statuses);
    connect(m_widget.add_image_provider, &QPushButton::clicked, this, &this_class::add_image_provider);
    connect(m_widget.copy_config_json, &QPushButton::clicked, this, &this_class::copy_config_json_to_clipboard);
    connect(m_widget.config_get, &QPushButton::clicked, this, &this_class::get_config_from_imrecman);
    connect(m_widget.config_set, &QPushButton::clicked, this, &this_class::set_config_at_imrecman);
    connect(m_widget.start_in, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, &this_class::update_start_rec_label);
    connect(m_widget.start_rec, &QPushButton::clicked, this, &this_class::start_recordings);
    connect(m_widget.stop_rec, &QPushButton::clicked, this, &this_class::stop_recordings);
    connect(m_widget.edit_mode, &QCheckBox::stateChanged, this, &this_class::edit_mode_changed);
    connect(this, &this_class::component_connected, this, &this_class::init_gui);
    connect(this, &this_class::component_connected, this, &this_class::get_config_from_imrecman);
    connect(this, &this_class::component_connected, this, &this_class::update_start_rec_label);
    connect(this, &this_class::component_disconnected, this, &this_class::uninit_gui);

    edit_mode_changed(m_widget.edit_mode->checkState());
}


void
visionx::ImageRecorderWidgetController::onInitComponent()
{
    ARMARX_TRACE;

    if (not m_imrecman_name.empty())
    {
        usingProxy(m_imrecman_name);
    }
}


void
visionx::ImageRecorderWidgetController::onConnectComponent()
{
    ARMARX_TRACE;

    getProxy(m_imrecman, m_imrecman_name);

    emit component_connected();
}


void
visionx::ImageRecorderWidgetController::onDisconnectComponent()
{
    ARMARX_TRACE;

    emit component_disconnected();
}


void
visionx::ImageRecorderWidgetController::onExitComponent()
{
    ARMARX_TRACE;

    // pass
}


void
visionx::ImageRecorderWidgetController::loadSettings(QSettings* settings)
{
    ARMARX_TRACE;

    m_imrecman_name = settings->value(QString::fromStdString(::config_key_imrecman), "ImageRecordingManager").toString().toStdString();
    m_widget.start_in->setValue(settings->value(QString::fromStdString(::config_key_start_in), 1.0).toDouble());
}


void
visionx::ImageRecorderWidgetController::saveSettings(QSettings* settings)
{
    ARMARX_TRACE;

    settings->setValue(QString::fromStdString(::config_key_imrecman), QString::fromStdString(m_imrecman_name));
    settings->setValue(QString::fromStdString(::config_key_start_in), m_widget.start_in->value());
}


QPointer<QDialog>
visionx::ImageRecorderWidgetController::getConfigDialog(QWidget* parent)
{
    ARMARX_TRACE;

    if (not m_config_dialog)
    {
        m_config_dialog = new armarx::SimpleConfigDialog{parent};
        m_config_dialog->addProxyFinder<ImageRecordingManagerInterfacePrx>({::config_key_imrecman, "Image recording manager", "*"});
    }
    return qobject_cast<QDialog*>(m_config_dialog);
}


void
visionx::ImageRecorderWidgetController::configured()
{
    ARMARX_TRACE;

    if (m_config_dialog)
    {
        m_imrecman_name = m_config_dialog->getProxyName(::config_key_imrecman);
    }
}


void
visionx::ImageRecorderWidgetController::build_ui_from_config(const std::map<std::string, imrec::Config>& configs)
{
    ARMARX_TRACE;

    for (const auto& [image_provider_name, config_widget] : m_config_widgets)
    {
        m_widget.image_providers->layout()->removeWidget(config_widget);
        config_widget->deleteLater();
    }
    m_config_widgets.clear();

    if (not configs.empty())
    {
        const imrec::Config& ref_config = configs.begin()->second;
        m_widget.pp_package->setText(QString::fromStdString(ref_config.location.package));
        m_widget.pp_path->setText(QString::fromStdString(ref_config.location.path));
        m_widget.recording_name->setText(QString::fromStdString(ref_config.name));
    }

    for (const auto& [image_provider_name, config] : configs)
    {
        add_image_provider_conf_widget(image_provider_name, config);
    }
}


void
visionx::ImageRecorderWidgetController::update_statuses(const std::map<std::string, visionx::imrec::Status>& statuses)
{
    ARMARX_TRACE;

    // Upate statuses of known image providers.
    for (const auto& [image_provider_name, status] : statuses)
    {
        if (m_config_widgets.find(image_provider_name) != m_config_widgets.end())
        {
            m_config_widgets.at(image_provider_name)->setImageProviderStatus(status);
        }
    }

    // Reset statuses of those image providers which are unknown to the recording manager yet
    // configured in the GUI.
    std::optional<imrec::Status> empty_status;
    for (const auto& [image_provider_name, config_widget] : m_config_widgets)
    {
        if (statuses.find(image_provider_name) == statuses.end())
        {
            m_config_widgets.at(image_provider_name)->setImageProviderStatus(empty_status);
        }
    }
}


void
visionx::ImageRecorderWidgetController::add_image_provider_conf_widget(const std::string& image_provider_name, const imrec::Config& config)
{
    ARMARX_TRACE;

    ImageProviderConfigWidget* cw = new ImageProviderConfigWidget{m_widget.image_providers, getIceManager(), image_provider_name, config.channelConfigs};

    m_config_widgets[image_provider_name] = cw;
    m_widget.image_providers->layout()->addWidget(cw);

    connect(cw, &ImageProviderConfigWidget::imageProviderRenamed, this, &ImageRecorderWidgetController::rename_image_provider);
    connect(cw, &ImageProviderConfigWidget::imageProviderRemoved, this, &ImageRecorderWidgetController::remove_image_provider);

    if (m_widget.auto_sync->isChecked())
    {
        set_config_at_imrecman();
    }
}


std::map<std::string, visionx::imrec::Config>
visionx::ImageRecorderWidgetController::to_config_map()
{
    ARMARX_TRACE;

    const IceUtil::Time start_in = IceUtil::Time::secondsDouble(m_widget.start_in->value());
    const IceUtil::Time start_at = IceUtil::Time::now() + start_in;

    std::map<std::string, visionx::imrec::Config> configs;
    visionx::imrec::Config config_template;
    config_template.location.package = m_widget.pp_package->text().toStdString();
    config_template.location.path = m_widget.pp_path->text().toStdString();
    config_template.name = m_widget.recording_name->text().toStdString();
    config_template.startTimestamp = start_at.toMicroSeconds();
    if (config_template.name.empty())
    {
        config_template.name = "rec_%TIMESTAMP%";
    }

    for (const auto& [name, config_widget] : m_config_widgets)
    {
        config_template.channelConfigs = config_widget->toChannelConfigs();
        configs[name] = config_template;
    }
    return configs;
}


void
visionx::ImageRecorderWidgetController::init_gui()
{
    ARMARX_TRACE;

    const bool enabled = true;
    m_widget.start_rec->setEnabled(enabled);
    m_widget.stop_rec->setEnabled(enabled);
    m_widget.config_get->setEnabled(enabled);
    m_widget.config_set->setEnabled(enabled);
    m_poll_timer->start();
}


void
visionx::ImageRecorderWidgetController::uninit_gui()
{
    ARMARX_TRACE;

    const bool enabled = false;
    m_widget.start_rec->setEnabled(enabled);
    m_widget.stop_rec->setEnabled(enabled);
    m_widget.config_get->setEnabled(enabled);
    m_widget.config_set->setEnabled(enabled);
    m_poll_timer->stop();
}


void
visionx::ImageRecorderWidgetController::poll_statuses()
{
    ARMARX_TRACE;

    try
    {
        if (m_imrecman->isConfigured())
        {
            update_statuses(m_imrecman->getRecordingStatuses());
        }
    }
    catch (...)
    {
        // pass
    }
}


void
visionx::ImageRecorderWidgetController::get_config_from_imrecman()
{
    ARMARX_TRACE;

    try
    {
        if (m_imrecman->isConfigured())
        {
            build_ui_from_config(m_imrecman->getConfiguration());
        }
    }
    catch (...)
    {
        // pass
    }
}


void
visionx::ImageRecorderWidgetController::set_config_at_imrecman()
{
    ARMARX_TRACE;

    try
    {
        m_imrecman->configureRecordings(to_config_map());
    }
    catch (...)
    {
        // pass
    }
}


void
visionx::ImageRecorderWidgetController::add_image_provider()
{
    ARMARX_TRACE;

    visionx::imrec::Config config;
    add_image_provider_conf_widget("image_provider_" + std::to_string(++m_image_provider_ctr), config);
}


void
visionx::ImageRecorderWidgetController::copy_config_json_to_clipboard()
{
    ARMARX_TRACE;

    nlohmann::json j = to_config_map();
    const std::string config_map_json = j.dump();
    QClipboard* clipboard = QGuiApplication::clipboard();
    clipboard->setText(QString::fromStdString(config_map_json));
}


void
visionx::ImageRecorderWidgetController::update_start_rec_label()
{
    ARMARX_TRACE;

    QString text = "Start recordings in ";
    text += m_widget.start_in->cleanText();
    text += " sec";
    m_widget.start_rec->setText(text);
}


void
visionx::ImageRecorderWidgetController::start_recordings()
{
    ARMARX_TRACE;

    try
    {
        m_imrecman->configureRecordings(to_config_map());
        m_imrecman->startRecordings();
    }
    catch (...)
    {
        // pass
    }
}


void
visionx::ImageRecorderWidgetController::stop_recordings()
{
    ARMARX_TRACE;

    try
    {
        auto fn = [&]
        {
            m_imrecman->stopRecordings();
        };
        std::thread t{fn};
        t.detach();
    }
    catch (...)
    {
        // pass
    }
}


void
visionx::ImageRecorderWidgetController::edit_mode_changed(const int state)
{
    const ViewMode viewMode = state == Qt::CheckState::Checked ? ViewMode::Full : ViewMode::StatusOnly;
    for (const auto& [image_provider_name, config_widget] : m_config_widgets)
    {
        config_widget->setViewMode(viewMode);
    }

    switch (viewMode)
    {
        case ViewMode::Full:
            m_widget.config_get->setEnabled(true);
            m_widget.config_set->setEnabled(true);
            m_widget.add_image_provider->show();
            break;
        case ViewMode::StatusOnly:
            m_widget.config_get->setEnabled(false);
            m_widget.config_set->setEnabled(false);
            m_widget.add_image_provider->hide();
            break;
    }
}


void
visionx::ImageRecorderWidgetController::rename_image_provider(const std::string& old_name, const std::string& new_name)
{
    ARMARX_TRACE;

    if (m_config_widgets.find(new_name) == m_config_widgets.end())
    {
        auto node_handler = m_config_widgets.extract(old_name);
        node_handler.key() = new_name;
        m_config_widgets.insert(std::move(node_handler));
        m_config_widgets.at(new_name)->confirmImageProviderName(new_name);

        if (m_widget.auto_sync->isChecked())
        {
            set_config_at_imrecman();
        }
    }
    else
    {
        m_config_widgets.at(new_name)->rejectImageProviderName(new_name);
    }
}


void
visionx::ImageRecorderWidgetController::remove_image_provider(const std::string& name)
{
    ARMARX_TRACE;

    ImageProviderConfigWidget* config_widget = m_config_widgets.at(name);
    m_widget.image_providers->layout()->removeWidget(config_widget);
    config_widget->deleteLater();
    m_config_widgets.erase(m_config_widgets.find(name));

    if (m_widget.auto_sync->isChecked())
    {
        set_config_at_imrecman();
    }
}
