/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::gui-plugins::ImageRecorder
 * @author     Christian R. G. Dreher <c.dreher@kit.edu>
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


// STD/STL


// Qt
#include <QDialog>
#include <QPointer>
#include <QString>
#include <QTimer>

// ArmarX
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>
#include <ArmarXGui/libraries/SimpleConfigDialog/SimpleConfigDialog.h>
#include <VisionX/components/ImageRecordingManager/ImageRecordingManagerInterface.h>
#include <VisionX/gui-plugins/ImageRecorder/ui_ImageRecorderWidget.h>
#include <VisionX/gui-plugins/ImageRecorder/ImageProviderConfigWidget.h>


namespace visionx
{

    class ImageProviderConfigWidget;

    class ImageRecorderWidgetController :
        public armarx::ArmarXComponentWidgetControllerTemplate<ImageRecorderWidgetController>
    {

        Q_OBJECT

    private:

        QPointer<QTimer> m_poll_timer;

        QPointer<armarx::SimpleConfigDialog> m_config_dialog;

        Ui::image_recorder_widget m_widget;

        std::string m_imrecman_name;

        ImageRecordingManagerInterfacePrx m_imrecman;

        std::map<std::string, ImageProviderConfigWidget*> m_config_widgets;

        unsigned int m_image_provider_ctr = 0;

    public:

        ImageRecorderWidgetController();

        void onInitComponent() override;

        void onConnectComponent() override;

        void onDisconnectComponent() override;

        void onExitComponent() override;

        static QString GetWidgetName()
        {
            return "VisionX.ImageRecorder";
        }

        static QIcon GetWidgetIcon()
        {
            return QIcon {"://icons/image_recorder.svg"};
        }

        static QIcon GetWidgetCategoryIcon()
        {
            return QIcon {"://icons/eye.svg"};
        }

        void loadSettings(QSettings* settings) override;

        void saveSettings(QSettings* settings) override;

        QPointer<QDialog> getConfigDialog(QWidget* parent) override;

        void configured() override;

        //QPointer<QWidget> getCustomTitlebarWidget(QWidget* parent) override;

        void build_ui_from_config(const std::map<std::string, imrec::Config>& configs);

        void update_statuses(const std::map<std::string, visionx::imrec::Status>& statuses);

        void add_image_provider_conf_widget(const std::string& image_provider_name, const imrec::Config& config);

        std::map<std::string, imrec::Config> to_config_map();

    signals:

        void component_connected();

        void component_disconnected();

    private slots:

        void init_gui();

        void uninit_gui();

        void poll_statuses();

        void get_config_from_imrecman();

        void set_config_at_imrecman();

        void add_image_provider();

        void remove_image_provider(const std::string& name);

        void copy_config_json_to_clipboard();

        void update_start_rec_label();

        void start_recordings();

        void stop_recordings();

        void edit_mode_changed(int state);

        void rename_image_provider(const std::string& old_name, const std::string& new_name);

    };
}
