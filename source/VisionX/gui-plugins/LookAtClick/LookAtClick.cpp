#include <iostream>

#include <QMouseEvent>
#include <QPainter>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/logging/Logging.h>


#include "LookAtClick.h"

LookAtClick::LookAtClick(QWidget* parent) : QWidget(parent)
{
    setMouseTracking(true);
    setCursor(Qt::PointingHandCursor);
    this->installEventFilter(this);
    std::cout << "mousetracking activated!\n";
}
void LookAtClick::drawBackground(QImage& image)
{
    _backgroundImage = image.copy();
    setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    setFixedSize(image.size());
    QImage newImage(image.size(), QImage::Format_ARGB32);
    newImage.fill(Qt::white);
    QPainter painter(&newImage);
    painter.drawImage(QPoint(0, 0), _backgroundImage);
    _backgroundImage = newImage;
    update();
}

QSize LookAtClick::sizeHint() const
{
    return _backgroundImage.size();
}

bool LookAtClick::eventFilter(QObject* watched, QEvent* event)
{
    if (event->type() == QEvent::MouseButtonPress)
    {
        ARMARX_INFO << "mousePressEvent found";
        mousePressEvent(static_cast<QMouseEvent*>(event));
    }
    return false;
}

void LookAtClick::mousePressEvent(QMouseEvent* event)
{
    ARMARX_INFO << "mousePressEvent triggered!";
    if (event->button() == Qt::LeftButton)
    {
        ARMARX_INFO << "Leftclick detected!";
        _lastPoint = event->pos();
        QPoint offset;
        _center.setX(_backgroundImage.size().rwidth() / 2);
        _center.setY(_backgroundImage.size().rheight() / 2);
        offset.setX(_lastPoint.x() - _center.x());
        offset.setY(_lastPoint.y() - _center.y());
        clickedAt(offset);
    }
}

void LookAtClick::mouseMoveEvent(QMouseEvent* event)
{
    std::cout << "Mouse is moving!\n";
}

void LookAtClick::paintEvent(QPaintEvent* event)
{
    QPainter painter(this);
    QRect dirtyRect = event->rect();
    painter.drawImage(dirtyRect, _backgroundImage, dirtyRect);
}
