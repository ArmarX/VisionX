#pragma once



#include <QWidget>
#include <QImage>
#include <QPoint>

class LookAtClick : public QWidget
{
    Q_OBJECT
public:
    explicit LookAtClick(QWidget* parent = nullptr);
    void drawBackground(QImage& image);
    void process();

    QSize sizeHint() const override;
signals:
    void clickedAt(const QPoint& point);


protected:
    void mousePressEvent(QMouseEvent* event) override;
    void paintEvent(QPaintEvent* event) override;
    void mouseMoveEvent(QMouseEvent* event) override;
    bool eventFilter(QObject* watched, QEvent* event) override;
public slots:

private:
    QPoint _lastPoint;
    QPoint _center;

    QImage _backgroundImage;
};

