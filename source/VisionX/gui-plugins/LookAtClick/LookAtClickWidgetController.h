/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::gui-plugins::LookAtClickWidgetController
 * @author     [Author Name] ( [Author Email] )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <ArmarXCore/core/system/ImportExportComponent.h>

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>
#include <ArmarXGui/libraries/SimpleConfigDialog/SimpleConfigDialog.h>

#include <RobotAPI/interface/units/KinematicUnitInterface.h>
#include <RobotAPI/libraries/RobotAPIComponentPlugins/RobotStateComponentPlugin.h>

#include <VisionX/core/ImageProcessor.h>
#include <VisionX/gui-plugins/LookAtClick/ui_LookAtClickWidget.h>

#include "LookAtClick.h"

namespace armarx
{
    /**
    \page VisionX-GuiPlugins-LookAtClick LookAtClick
    \brief The LookAtClick allows visualizing ...

    \image html LookAtClick.png
    The user can

    API Documentation \ref LookAtClickWidgetController

    \see LookAtClickGuiPlugin
    */

    /**
     * \class LookAtClickWidgetController
     * \brief LookAtClickWidgetController brief one line description
     *
     * Detailed description
     */
    class ARMARXCOMPONENT_IMPORT_EXPORT
        LookAtClickWidgetController:
        public armarx::ArmarXComponentWidgetControllerTemplate < LookAtClickWidgetController >,
        virtual public visionx::ImageProcessor,
        virtual public RobotStateComponentPluginUser
    {
        Q_OBJECT

    public:
        /**
         * Controller Constructor
         */
        explicit LookAtClickWidgetController();

        /**
         * Controller destructor
         */
        virtual ~LookAtClickWidgetController();

        /**
         * @see ArmarXWidgetController::loadSettings()
         */
        void loadSettings(QSettings* settings) override;

        /**
         * @see ArmarXWidgetController::saveSettings()
         */
        void saveSettings(QSettings* settings) override;

        /**
         * Returns the Widget name displayed in the ArmarXGui to create an
         * instance of this class.
         */
        static QString GetWidgetName()
        {
            return "RobotControl.LookAtClick";
        }

        /**
         * \see armarx::Component::onInitImageProcessor()
         */
        void onInitImageProcessor() override;

        /**
         * \see armarx::Component::onConnectImageProcessor()
         */
        void onConnectImageProcessor() override;

        void process();
        QPointer<QDialog> getConfigDialog(QWidget* parent) override;
        void configured() override;


        void timerEvent(QTimerEvent*);

    public slots:
        /* QT slot declarations */
        void clickedAt(const QPoint& point);
    signals:
        /* QT signal declarations */

    protected:
    private:

        void autoDetectRobotSpecificVariables();
        void refreshWidgetData(float* factorPitch, float* factorYaw);
        void determineNewAngles(const QPoint& offset, NameValueMap& jointAngles, NameControlModeMap& jointModes, float factorPitch, float factorYaw);
        /**
         * Widget Form
         */

        Ui::LookAtClickWidget widget;

        LookAtClick* _turnOnClick{nullptr};

        VirtualRobot::RobotPtr                          _robot;

        std::string                                         _pitchJointName;
        std::string                                         _yawJointName;
        float                                               _inverseYaw;
        QPointer<SimpleConfigDialog>                        _dialog;

        KinematicUnitInterfacePrx kinematicUnitInterfacePrx;
        std::string                                         _robotStateUnitComponentName;
        std::string                                         _kinematicUnitInterfaceName;
        visionx::ImageProviderInterfacePrx                  _imageProvider;
        std::string                                         _imageProviderName;
        visionx::ImageProviderInfo                          _imageProviderInfo;
        std::atomic_int                                     _imageIndex{0};
        std::atomic_int                                     _numberOfImages{1};

        std::vector<visionx::CByteImageUPtr>                _providerImagesOwner;
        std::vector<void*>                                  _providerImages;

        QImage                                              _currentImage;
        std::mutex                                          _currentImageMutex;
        std::atomic_bool                                    _currentImageDirty{false};

        // ImageProcessor interface
    protected:
        void onExitImageProcessor() {}
    };
}


