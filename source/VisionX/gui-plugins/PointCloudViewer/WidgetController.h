/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::gui-plugins::WidgetController
 * @author     Philipp Schmidt ( ufedv at student dot kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <mutex>

// QT
#include <QListWidgetItem>
#include <QColorDialog>

// Point Cloud Library
#include <pcl/point_types.h>
#include <pcl/visualization/pcl_visualizer.h>

// ArmarX
#include <Ice/BuiltinSequences.h>
#include <ArmarXCore/core/system/ImportExportComponent.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/widgets/CoinViewer.h>
#include <RobotAPI/interface/visualization/DebugDrawerInterface.h>

// Point Cloud Processor
#include <VisionX/components/pointcloud_core/PointCloudProcessor.h>

// User interface
#include <VisionX/gui-plugins/PointCloudViewer/ui_Widget.h>

#include "Manager.h"
#include "SaveDialog.h"


/* NOTES for developers:
 *
 * The code has two invariancies:
 *
 * 1.: The topic and proxies of a provider are used, exactly if it is listed in the list widget "providerList".
 * 2.: If a provider is checked in the list widget "providerList" it has an entry in the manager
 *     and is shown in the viewer with the next render call renderClouds() with its ID.
 */


namespace visionx
{
    /**
    \page VisionX-GuiPlugins-PointCloudViewer PointCloudViewer
    \brief ...

    API Documentation \ref WidgetController

    \see GuiPlugin
    */

    /**
     * \class WidgetController
     * \brief WidgetController brief one line description
     *
     * Detailed description
     */
    class ARMARXCOMPONENT_IMPORT_EXPORT
        WidgetController:
        public armarx::ArmarXComponentWidgetControllerTemplate<WidgetController>,
        public PointCloudProcessor
    {
        Q_OBJECT

        using PointT = pcl::PointXYZRGBL;

    public:

        /// Controller constructor.
        explicit WidgetController();

        /// Controller destructor
        ~WidgetController() override;

        /// @see ArmarXWidgetController::loadSettings()
        void loadSettings(QSettings* settings) override;

        /// @see ArmarXWidgetController::saveSettings()
        void saveSettings(QSettings* settings) override;

        /// Returns the Widget name displayed in the ArmarXGui to create an instance of this class.
        static QString GetWidgetName()
        {
            return "VisionX.PointCloudViewer";
        }

        /// @see visionx::PointCloudProcessor::onInitPointCloudProcessor()
        void onInitPointCloudProcessor() override;

        /// @see visionx::PointCloudProcessor::onConnectPointCloudProcessor()
        ///
        void onConnectPointCloudProcessor() override;
        /// @see visionx::PointCloudProcessor::onDisconnectPointCloudProcessor()
        void onDisconnectPointCloudProcessor() override;

        /// @see visionx::PointCloudProcessor::onExitPointCloudProcessor()
        void onExitPointCloudProcessor() override;


        /// @see visionx::PointCloudProcessor::process()
        void process() override;

        void postDocking() override;

        void onLockWidget() override;
        void onUnlockWidget() override;

    signals:

        void newPointCloudReceived(QString providerName, CoinPointCloud* cloud);


    public slots:

        void refreshProviderList();
        void showPointCloudInfo(QListWidgetItem* item);
        void pointCloudChecked(QListWidgetItem* item);
        void selectColor();
        void savePointCloud();
        void processPointCloud(QString providerName, CoinPointCloud* cloud);


        void updateProviderList(IceGrid::ObjectInfoSeq objects);


    private slots:

        void setPointSize(double value);
        void disconnectQt();


    private:

        template <class PointCloudT>
        void getPointCloudAs(const std::string& providerName, PointCloudT& targetPointCloud);
        template <class PointCloudT>
        static void storePointCloudAs(const std::string& filename, const PointCloudT& pointCloud);

    private:

        QPointer<QWidget> m_widget;

        /// Widget Form
        Ui::Widget widget;

        /// Cloud Manager
        Manager* manager;

        /// Color dialog
        QColorDialog colorDialog;

        /// Camera reset flag
        bool recenterCamera;

        std::mutex mutex;

        std::atomic<float> pointSize {1};
    };
}

