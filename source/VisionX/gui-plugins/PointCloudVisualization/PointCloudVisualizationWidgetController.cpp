/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * \package    VisionX::gui-plugins::PointCloudVisualizationWidgetController
 * \author     Peter Kaiser ( peter dot kaiser at kit dot edu )
 * \date       2017
 * \copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "PointCloudVisualizationWidgetController.h"

#include <QCheckBox>
#include <QTableWidget>
#include <QTableWidgetItem>
#include <QComboBox>
#include <QLabel>
#include <QMetaObject>

#include <string>

namespace armarx
{
    PointCloudVisualizationWidgetController::PointCloudVisualizationWidgetController()
    {
        widget.setupUi(getWidget());
        updateTimer.setInterval(100);
        updateTimer.setSingleShot(false);
    }


    PointCloudVisualizationWidgetController::~PointCloudVisualizationWidgetController()
    {

    }


    void PointCloudVisualizationWidgetController::loadSettings(QSettings* settings)
    {
        visualizerName = settings->value("visualizerName", "").toString().toStdString();
    }

    void PointCloudVisualizationWidgetController::saveSettings(QSettings* settings)
    {
        settings->setValue("visualizerName", QString::fromStdString(visualizerName));
    }

    QPointer<QDialog> PointCloudVisualizationWidgetController::getConfigDialog(QWidget* parent)
    {
        if (!configDialog)
        {
            configDialog = new PointCloudVisualizationConfigDialog(parent);
        }

        return qobject_cast<PointCloudVisualizationConfigDialog*>(configDialog);
    }

    void PointCloudVisualizationWidgetController::configured()
    {
        visualizerName = configDialog->pointCloudVisualizationProxyFinder->getSelectedProxyName().toStdString();
    }

    void PointCloudVisualizationWidgetController::onInitComponent()
    {
        usingProxy(visualizerName);
    }

    void PointCloudVisualizationWidgetController::onConnectComponent()
    {
        visualizer = getProxy<visionx::PointCloudVisualizationInterfacePrx>(visualizerName);
        if (!visualizer)
        {
            ARMARX_ERROR << "Could not obtain point cloud visualizer proxy";
            return;
        }

        updateProviderList();

        connect(this, SIGNAL(providerListChanged()), this, SLOT(updateProviderTable()));
        connect(&updateTimer, SIGNAL(timeout()), this, SLOT(updateProviderTable()));
        QMetaObject::invokeMethod(&updateTimer, "start", Qt::QueuedConnection);
    }

    void PointCloudVisualizationWidgetController::onDisconnectComponent()
    {
        QMetaObject::invokeMethod(&updateTimer, "stop", Qt::QueuedConnection);
        QObject::disconnect(this, SLOT(updateProviderTable()));
    }

    void PointCloudVisualizationWidgetController::updateProviderList()
    {
        providers.clear();
        providers = visualizer->getAvailableProviders();

        std::sort(providers.begin(), providers.end());

        emit providerListChanged();
    }

    void PointCloudVisualizationWidgetController::updateProviderTable()
    {
        bool rebuild = static_cast<unsigned int>(widget.tableWidgetProviders->rowCount()) != providers.size();
        if (!rebuild)
        {
            for (unsigned int i = 0; i < providers.size(); i++)
            {
                if (static_cast<QCheckBox*>(widget.tableWidgetProviders->cellWidget(static_cast<int>(i), 0))->text().toStdString() != providers[i].name)
                {
                    rebuild = true;
                    break;
                }
            }
        }
        if (rebuild)
        {
            rebuildProviderTable();
        }

        for (unsigned int i = 0; i < providers.size(); i++)
        {
            auto info = visualizer->getCurrentPointCloudVisualizationInfo(providers[i].name);
            static_cast<QCheckBox*>(widget.tableWidgetProviders->cellWidget(static_cast<int>(i), 0))->setCheckState(info.enabled ? Qt::Checked : Qt::Unchecked);

            if (providers[i].type == visionx::ePoints)
            {
                // nothing to update
            }
            else if (providers[i].type == visionx::eColoredPoints || providers[i].type == visionx::eColoredOrientedPoints)
            {
                static_cast<QComboBox*>(widget.tableWidgetProviders->cellWidget(static_cast<int>(i), 1))->setCurrentIndex(info.type == visionx::eColoredPoints ? 1 : 0);
            }
            else if (providers[i].type == visionx::eLabeledPoints)
            {
                static_cast<QComboBox*>(widget.tableWidgetProviders->cellWidget(static_cast<int>(i), 1))->setCurrentIndex(info.type == visionx::eLabeledPoints ? 1 : 0);
            }
            else if (providers[i].type == visionx::eColoredLabeledPoints)
            {
                static_cast<QComboBox*>(widget.tableWidgetProviders->cellWidget(static_cast<int>(i), 1))->setCurrentIndex(info.type == visionx::eLabeledPoints ? 2 : (info.type == visionx::eColoredPoints ? 1 : 0));
            }
            else
            {
                // nothing to update
            }
        }
    }
    void PointCloudVisualizationWidgetController::rebuildProviderTable()
    {
        QObject::disconnect(this, SLOT(providerSelectionChanged()));
        QObject::disconnect(this, SLOT(providerVisualizationTypeChanged(int)));
        widget.tableWidgetProviders->clear();

        widget.tableWidgetProviders->setColumnCount(2);
        widget.tableWidgetProviders->setRowCount(static_cast<int>(providers.size()));

        for (unsigned int i = 0; i < providers.size(); i++)
        {
            QCheckBox* cb = new QCheckBox(QString::fromStdString(providers[i].name));
            connect(cb, SIGNAL(toggled(bool)), this, SLOT(providerSelectionChanged()));
            widget.tableWidgetProviders->setCellWidget(static_cast<int>(i), 0, cb);

            if (providers[i].type == visionx::ePoints)
            {
                widget.tableWidgetProviders->setItem(static_cast<int>(i), 1, new QTableWidgetItem("Plain"));
            }
            else if (providers[i].type == visionx::eColoredPoints || providers[i].type == visionx::eColoredOrientedPoints)
            {
                QComboBox* combo = new QComboBox();
                combo->addItem("Plain");
                combo->addItem("Colors");
                combo->setCurrentIndex(1);
                connect(combo, SIGNAL(currentIndexChanged(int)), this, SLOT(providerVisualizationTypeChanged(int)));
                widget.tableWidgetProviders->setCellWidget(static_cast<int>(i), 1, combo);
            }
            else if (providers[i].type == visionx::eLabeledPoints)
            {
                QComboBox* combo = new QComboBox();
                combo->addItem("Plain");
                combo->addItem("Labels");
                combo->setCurrentIndex(1);
                connect(combo, SIGNAL(currentIndexChanged(int)), this, SLOT(providerVisualizationTypeChanged(int)));
                widget.tableWidgetProviders->setCellWidget(static_cast<int>(i), 1, combo);
            }
            else if (providers[i].type == visionx::eColoredLabeledPoints)
            {
                QComboBox* combo = new QComboBox();
                combo->addItem("Plain");
                combo->addItem("Colors");
                combo->addItem("Labels");
                combo->setCurrentIndex(1);
                connect(combo, SIGNAL(currentIndexChanged(int)), this, SLOT(providerVisualizationTypeChanged(int)));
                widget.tableWidgetProviders->setCellWidget(static_cast<int>(i), 1, combo);
            }
            else
            {
                cb->setEnabled(false);
                widget.tableWidgetProviders->setItem(static_cast<int>(i), 1, new QTableWidgetItem("Unsupported"));
            }
        }
    }

    void PointCloudVisualizationWidgetController::providerSelectionChanged()
    {
        for (unsigned int i = 0; i < providers.size(); i++)
        {
            visionx::PointContentType type;

            QCheckBox* check = dynamic_cast<QCheckBox*>(widget.tableWidgetProviders->cellWidget(i, 0));

            if (dynamic_cast<QComboBox*>(widget.tableWidgetProviders->cellWidget(i, 1)) != nullptr)
            {
                QComboBox* combo = dynamic_cast<QComboBox*>(widget.tableWidgetProviders->cellWidget(i, 1));

                if (combo->currentText() == "Colors")
                {
                    type = visionx::eColoredPoints;
                }
                else if (combo->currentText() == "Labels")
                {
                    type = visionx::eLabeledPoints;
                }
                else
                {
                    type = visionx::ePoints;
                }
            }
            else
            {
                type = providers[i].type;
            }

            visualizer->begin_enablePointCloudVisualization(providers[i].name, type, check->isChecked());
        }
    }

    void PointCloudVisualizationWidgetController::providerVisualizationTypeChanged(int x)
    {
        providerSelectionChanged();
    }
}
