#include "GraphvizLayout.h"

#include <ArmarXGui/gui-plugins/StatechartViewerPlugin/layout/GraphvizConverter.h>

#include <graphviz/cgraph.h>

#include <SimoxUtility/algorithm/string/string_tools.h>

#include <vector>


namespace armarx
{

    namespace
    {

        char* gv_str(const char* c)
        {
            return const_cast<char*>(c);
        }

        char* gv_str(std::string const& s)
        {
            return const_cast<char*>(s.c_str());
        }

        char GRAPHVIZ_ATTR_LABEL[] = "label";
        char GRAPHVIZ_ATTR_POS[] = "pos";
        char GRAPHVIZ_ATTR_WIDTH[] = "width";
        char GRAPHVIZ_ATTR_HEIGHT[] = "height";
        char GRAPHVIZ_ATTR_LABEL_POS[] = "lp";
    }

    GraphvizLayout::GraphvizLayout()
    {
        context = gvContext();
        graph = agopen(gv_str("Graph"), Agdirected, nullptr);


        // Setup properties
        agattr(graph, AGRAPH, (char*)"dpi", (char*)"72"); // needs to be 72 because graphviz's bounding box is always calculated with 72dpi
        // Bounding box of drawing
        // How is this used and specified? In pixels?
        // Do we need it?
        // agattr(graph, AGRAPH, (char*)"bb", (char*)"0,0,1210,744");
        // Maximum drawing size in inches
        // Why would we set that?
        //agattr(graph, AGRAPH, (char*)"size", (char*)"5,5");
        agattr(graph, AGRAPH, (char*)"overlap", (char*)"prism");
        agattr(graph, AGRAPH, (char*)"splines", (char*)"true");
        agattr(graph, AGRAPH, (char*)"pad", (char*)"0.2");
        agattr(graph, AGRAPH, (char*)"nodesep", (char*)"0.4");
        agattr(graph, AGRAPH, (char*)"sep", (char*)"1");
        agattr(graph, AGRAPH, (char*)"overlap_shrink", (char*)"true");
        agattr(graph, AGRAPH, (char*)"rankdir", (char*)"LR");
        agattr(graph, AGRAPH, (char*)"ratio", (char*)"compress");

        agattr(graph, AGNODE, (char*)"margin", (char*)"0,0");
        agattr(graph, AGNODE, (char*)"fontsize", (char*)"28");

        //Set default attributes for future edges
        agattr(graph, AGEDGE, const_cast<char*>("label"), const_cast<char*>(""));
        agattr(graph, AGEDGE, const_cast<char*>("fontsize"), const_cast<char*>("28"));
    }

    GraphvizLayout::~GraphvizLayout()
    {
        if (graph)
        {
            agclose(graph);
        }
        if (context)
        {
            gvFinalize(context);
            gvFreeContext(context);
        }
    }

    void GraphvizLayout::addNode(int id, const std::string& label)
    {
        node_t* node = agnode(graph, gv_str(label), TRUE);
        agset(node, GRAPHVIZ_ATTR_LABEL, gv_str(label));
        id2node.emplace(id, node);
    }

    void GraphvizLayout::addEdge(int sourceID, int targetID, const std::string& label)
    {
        node_t* source = id2node.at(sourceID);
        node_t* target = id2node.at(targetID);
        edge_t* edge = agedge(graph, source, target, gv_str("edge"), TRUE);
        if (label.size() > 0)
        {
            agset(edge, GRAPHVIZ_ATTR_LABEL, gv_str(label));
        }
        id2edge.emplace(std::make_pair(sourceID, targetID), edge);
    }

    GraphvizLayoutedGraph GraphvizLayout::finish(std::string const& savePNG)
    {
        gvLayout(context, graph, "dot");
        attach_attrs(graph);
        if (!savePNG.empty())
        {
            gvRenderFilename(context, graph, "png", gv_str(savePNG));
        }
        gvFreeLayout(context, graph);

        GraphvizLayoutedGraph layouted;

        for (auto& pair : id2node)
        {
            int id = pair.first;
            node_t* node = pair.second;

            std::string labelString = agget(node, GRAPHVIZ_ATTR_LABEL);
            std::string posString = agget(node, GRAPHVIZ_ATTR_POS);
            std::string widthString = agget(node, GRAPHVIZ_ATTR_WIDTH);
            std::string heightString = agget(node, GRAPHVIZ_ATTR_HEIGHT);

            std::vector<std::string> splitPos = simox::alg::split(posString, ",");
            if (splitPos.size() != 2)
            {
                continue;
            }

            float posX = std::stof(splitPos[0]);
            float posY = std::stof(splitPos[1]);
            float width = std::stof(widthString);
            float height = std::stof(heightString);
            // DPI conversion?
            width *= 72.0f;
            height *= 72.0f;
            // Graphviz and Qt have different conventions for position (top-left vs. center)
            posX -= width / 2.0f;
            posY -= height / 2.0f;

            GraphvizLayoutedNode layoutedNode;
            layoutedNode.posX = posX;
            layoutedNode.posY = posY;
            layoutedNode.width = width;
            layoutedNode.height = height;
            layoutedNode.label = labelString;

            layouted.nodes.emplace(id, layoutedNode);
        }

        for (auto& pair : id2edge)
        {
            std::pair<int, int> sourceTarget = pair.first;
            edge_t* edge = pair.second;
            std::string posString = agget(edge, GRAPHVIZ_ATTR_POS);
            std::string label = agget(edge, GRAPHVIZ_ATTR_LABEL);
            std::string labelPos = "";
            if (char* lp = agget(edge, GRAPHVIZ_ATTR_LABEL_POS))
            {
                labelPos = lp;
            }

            SupportPoints spline = armarx::GraphvizConverter::convertToSpline(posString);

            GraphvizLayoutedEdge layoutedEdge;
            layoutedEdge.controlPoints = spline.controlPointList();
            if (spline.endPoint)
            {
                layoutedEdge.endPoint = *spline.endPoint;
            }
            if (spline.startPoint)
            {
                layoutedEdge.startPoint = *spline.startPoint;
            }
            if (label.size() > 0)
            {
                std::vector<std::string> split = simox::alg::split(labelPos, ",");
                if (split.size() == 2)
                {
                    layoutedEdge.label = label;
                    layoutedEdge.labelPosX = std::stof(split[0]);
                    layoutedEdge.labelPosY = std::stof(split[1]);
                }
            }
            layouted.edges.emplace(sourceTarget, layoutedEdge);
        }

        return layouted;
    }



}
