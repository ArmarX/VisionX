#pragma once

#include <graphviz/gvc.h>

#include <QPointF>
#include <QList>

#include <optional>
#include <string>
#include <map>

namespace armarx
{
    struct GraphvizLayoutedNode
    {
        float posX = 0.0f;
        float posY = 0.0f;
        float width = 0.0f;
        float height = 0.0f;
        std::string label;
    };

    struct GraphvizLayoutedEdge
    {
        QList<QPointF> controlPoints;
        std::optional<QPointF> startPoint;
        std::optional<QPointF> endPoint;

        std::string label; // May be empty
        float labelPosX = 0.0f;
        float labelPosY = 0.0f;
    };

    struct GraphvizLayoutedGraph
    {
        std::map<int, GraphvizLayoutedNode> nodes;
        std::map<std::pair<int, int>, GraphvizLayoutedEdge> edges;
    };

    class GraphvizLayout
    {
    public:
        GraphvizLayout();

        GraphvizLayout(GraphvizLayout const&) = delete;

        ~GraphvizLayout();

        void addNode(int id, std::string const& label);

        void addEdge(int sourceID, int targetID, const std::string& label);

        GraphvizLayoutedGraph finish(std::string const& savePNG = "");


    private:
        GVC_t* context = nullptr;
        graph_t* graph = nullptr;

        std::map<int, node_t*> id2node;
        std::map<std::pair<int, int>, edge_t*> id2edge;
    };

}
