/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    SceneUnderstanding::gui-plugins::UserAssistedSegmenterGuiWidgetController
 * @author     Rainer Kartmann ( rainer dot kartmann at student dot kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <VisionX/gui-plugins/UserAssistedSegmenterGui/ui_UserAssistedSegmenterGuiWidget.h>
#include "UserAssistedSegmenterConfigDialog.h"

#include <Eigen/Dense>

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/PointIndices.h>

#include <ArmarXCore/core/system/ImportExportComponent.h>

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>

#include <RobotAPI/libraries/core/visualization/DebugDrawerTopic.h>

#include <VisionX/interface/components/UserAssistedSegmenter.h>

#include "widgets/PointCloudSegmentsTable.h"


namespace visionx
{
    class UserAssistedSegmenterConfigDialog;

    /**
    \page VisionX-GuiPlugins-UserAssistedSegmenterGui UserAssistedSegmenterGui
    \brief The UserAssistedSegmenterGui allows manually grouping segments of
    an input oversegmentation to objects.

    \image html UserAssistedSegmenterGui.png
    The user can see the segment IDs of the input oversegmentation and
    visualize the segments in the Debug Drawer View.
    The user can then choose the target number of objects and assign each
    object the segment IDs of segments which are part of this object.
    Once all objects have been assigned segments, the point cloud can be
    published. Once publushed, it will be provided at constant frequency.

    API Documentation \ref UserAssistedSegmenterGuiWidgetController

    \see UserAssistedSegmenterGuiGuiPlugin
    */

    /**
     * \class UserAssistedSegmenterGuiWidgetController
     * \brief UserAssistedSegmenterGuiWidgetController brief one line description
     *
     * Detailed description
     */
    class ARMARXCOMPONENT_IMPORT_EXPORT
        UserAssistedSegmenterGuiWidgetController :
        public armarx::ArmarXComponentWidgetControllerTemplate<UserAssistedSegmenterGuiWidgetController>,
        public visionx::UserAssistedSegmenterListener
    {
        Q_OBJECT

        using PointT = pcl::PointXYZRGBL;
        using PointCloudT = pcl::PointCloud<PointT>;


    public:

        /// Controller Constructor
        explicit UserAssistedSegmenterGuiWidgetController();

        /// Controller destructor
        virtual ~UserAssistedSegmenterGuiWidgetController() override;


        /// @see ArmarXWidgetController::loadSettings()
        virtual void loadSettings(QSettings* settings) override;

        /// @see ArmarXWidgetController::saveSettings()
        virtual void saveSettings(QSettings* settings) override;


        virtual QPointer<QDialog> getConfigDialog(QWidget* parent) override;

        virtual void configured() override;


        /// Returns the Widget name displayed in the ArmarXGui to create an instance of this class.
        static QString GetWidgetName();


        /// @see armarx::Component::onInitComponent()
        virtual void onInitComponent() override;

        /// @see armarx::Component::onConnectComponent()
        virtual void onConnectComponent() override;


        // UserAssistedSegmenterListener interface
        virtual void reportSegmentation(const visionx::ColoredLabeledPointCloud& pointCloud, const Ice::Current&) override;


    public slots:
        /* QT slot declarations */

        /// @brief Handles an incoming segmented point cloud by reportReceivedSegmentation().
        void onNewSourcePointCloud();

        // Filters
        void onFilterSmallSegmentsNumEdited();
        void updateFilters();

        // Visualization
        void onRadioButtonShowInputToggled(bool checked);
        void onRadioButtonShowResultToggle(bool checked);

        // IDs
        void onShowIDsToggled(bool checked);
        void updateSegmentIDs();

        void clearVisualization();

        // Grouping
        void updateUserGroupingRows(int numRows);
        void onUserGroupingChanged(uint32_t groupID, std::vector<uint32_t> segmentIDs);
        void onTableGroupingCellChanged(int row, int col);

        void publishSegmentation();


    signals:
        /* QT signal declarations */
        void newSourcePointCloud();


    private:

        static std::vector<armarx::DrawColor> makeColorList();


    private:

        void applyFilters(PointCloudT& pointCloud,
                          std::map<uint32_t, pcl::PointIndices>& segmentIndex);

        /// To be called when pointCloud has been modified.
        void updatePointCloud(const PointCloudT& pointCloud,
                              const std::map<uint32_t, pcl::PointIndices>& segmentIndex);

        void computeSegmentIndex(const PointCloudT& pointCloud);
        void computeCenters(const PointCloudT& pointCloud,
                            const std::map<uint32_t, pcl::PointIndices>& segmentIndex);

        void updateTableOverview();
        void drawSegmentIDs(bool onlyIfChecked = false);
        void drawInputPointCloud(bool onlyIfChecked = false);
        void drawResultPointCloud(bool onlyIfChecked = false);

        armarx::DrawColor dcolor(std::size_t id) const;
        QColor qcolor(std::size_t id) const;


    private:

        /// Widget Form
        Ui::UserAssistedSegmenterGuiWidget widget;
        QPointer<UserAssistedSegmenterConfigDialog> configDialog;

        PointCloudSegmentsTable* segmentsOverviewTable;


        std::string userAssistedSegmenterProxyName = "UserAssistedSegmenter";
        std::string userAssistedSegmenterTopicName = "UserAssistedSegmenterUpdates";


        armarx::DebugDrawerTopic drawer;
        visionx::UserAssistedSegmenterInterfacePrx segmenterProxy;


        PointCloudT sourcePointCloud;
        PointCloudT pointCloud;

        std::map<uint32_t, pcl::PointIndices> segmentIndex;
        std::map<uint32_t, Eigen::Vector3f> centers;


        std::vector<std::vector<std::uint32_t>> userGrouping;

        std::string layerPointCloud = "UserAssistedSegmenter_PointCloud";
        std::string layerSegmentIDs = "UserAssistedSegmenter_SegmentIDs";

    };
}

