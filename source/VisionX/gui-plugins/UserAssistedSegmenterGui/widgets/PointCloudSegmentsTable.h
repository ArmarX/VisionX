#pragma once

#include <map>

#include <QTableWidget>

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/PointIndices.h>

#include <RobotAPI/interface/visualization/DebugDrawerInterface.h>


namespace Ui
{
    class PointCloudSegmentsTable;
}


namespace visionx
{

    class PointCloudSegmentsTable : public QTableWidget
    {
        Q_OBJECT

    public:
        using Label = uint32_t;

    public:

        explicit PointCloudSegmentsTable(QWidget* parent = nullptr);
        ~PointCloudSegmentsTable();


        void setData(const pcl::PointCloud<pcl::PointXYZRGBL>& pointCloud, bool excludeZero = false);
        void setData(const std::map<Label, pcl::PointIndices>& segmentMap);


    private:

        Ui::PointCloudSegmentsTable* ui;

    };

}
