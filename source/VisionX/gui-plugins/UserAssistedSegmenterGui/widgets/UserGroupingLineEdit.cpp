#include "UserGroupingLineEdit.h"

#include <sstream>

namespace armarx
{
    UserGroupingLineEdit::UserGroupingLineEdit(
        const std::set<uint32_t>& validSegmentIDs, uint32_t segmentID) :
        groupID(segmentID), validSegmentIDs(validSegmentIDs)
    {
        connect(this, SIGNAL(editingFinished()), this, SLOT(onEditingFinished()));
    }

    uint32_t UserGroupingLineEdit::getGroupID() const
    {
        return groupID;
    }

    void UserGroupingLineEdit::setGroupID(const uint32_t& value)
    {
        groupID = value;
    }

    void UserGroupingLineEdit::onEditingFinished()
    {
        // parse text
        std::vector<uint32_t> segments;

        std::stringstream textStream(text().toStdString());

        uint32_t id;
        while (textStream >> id)
        {
            if (validSegmentIDs.find(id) != validSegmentIDs.end())
            {
                segments.push_back(id);
            }
        }

        emit groupingChanged(groupID, segments);
    }
}
