#pragma once

#include <set>

#include <QLineEdit>


namespace armarx
{

    class UserGroupingLineEdit : public QLineEdit
    {
        Q_OBJECT

    public:

        UserGroupingLineEdit(const std::set<uint32_t>& validSegmentIDs,
                             uint32_t groupID = 0);



        uint32_t getGroupID() const;
        void setGroupID(const uint32_t& value);


    private slots:

        void onEditingFinished();

    signals:

        void groupingChanged(uint32_t groupID, std::vector<uint32_t> segmentIDs);


    private:

        uint32_t groupID;

        const std::set<uint32_t> validSegmentIDs;

    };

}
