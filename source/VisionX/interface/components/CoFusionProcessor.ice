#pragma once

#include <VisionX/interface/core/ImageProcessorInterface.ice>
#include <VisionX/interface/core/PointCloudProviderInterface.ice>

module visionx
{
    interface CoFusionProcessorInterface extends 
            ImageProcessorInterface, CapturingPointCloudProviderInterface
	{
		
	};
};
