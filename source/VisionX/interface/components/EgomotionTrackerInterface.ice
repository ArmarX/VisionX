/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    
 * @author     
 * @date       
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <VisionX/interface/core/ImageProcessorInterface.ice>
#include <RobotAPI/interface/selflocalisation/SelfLocalisationProcess.ice>
#include <VisionX/interface/components/CommonTypes.ice>

module armarx
{    
    enum EgomotionTrackerStatus
    {
	eStopped,
	ePositionInitialisation,
	eTracking,
    eStatusEstimate
    };

    interface EgomotionTrackerInterface extends visionx::ImageProcessorInterface, SelfLocalisationProcessInterface
    {
    void beginLocalisation();
    void beginTracking();
    void beginStatusEstimation();
    LocationEstimate getLocationEstimate();
	PlaneScatteringDirections getPlaneScatteringDirectionsPS();
	PlaneScatteringDirections getPlaneScatteringDirectionsPF();
	ParticleVector getParticlesPS();
	ParticleVector getParticlesPF();
	BoundingBox getBoundingBox();
	int getNumberOfActiveParticles();
	EgomotionTrackerStatus getStatus();
	void setNeckErrorLimits(int rollMin, int rollMax, int pitchMin, int pitchMax, int yawMin, int yawMax);
	void setUseOrientationDifferenceTracking(bool useOrientationDifferenceTracking);
	bool getUseOrientationDifferenceTracking();
	void setUseOrientationDifferenceInitialisation(bool useOrientationDifferenceInitialisation);
	bool getUseOrientationDifferenceInitialisation();
	void setUsePSOptimizationTracking(bool usePSOptimizationTracking);
	bool getUsePSOptimizationTracking();
	void setUseMahalanobisDistanceBasedScatteringReduction(bool useMahalanobisDistanceBasedScatteringReduction);
	bool getUseMahalanobisDistanceBasedScatteringReduction();
	float getNeckErrorRollMin();
	float getNeckErrorRollMax();
	float getNeckErrorPitchMin();
	float getNeckErrorPitchMax();
	float getNeckErrorYawMin();
	float getNeckErrorYawMax();
    void step();
    void setAutomaticExecution(bool automaticExecution);
    void setCycleTrackingStatusEstimation(bool cycleTrackingStatusEstimation);
    };	
};

