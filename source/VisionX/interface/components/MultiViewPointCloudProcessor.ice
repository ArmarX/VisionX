/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    VisionX
* @author     Raphael Grimm (raphael dot grimm at kit dot edu)
* @copyright  2020 Humanoids Group, H2T, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <VisionX/interface/core/PointCloudProcessorInterface.ice>

module visionx
{
    interface MultiViewPointCloudProcessorInterface extends visionx::PointCloudProcessorInterface
    {
        void startRecordingNextViewPoint();
        void stopRecordingViewPoint();
        void clearViewPoints();
        void setMaxNumberOfFramesPerViewPoint(int num);
        void setDownsamplingGridSize(float sz);
        void setFrameRate(float hz);
    };
};



