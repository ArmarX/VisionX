/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    VisionX
* @author     Mirko Waechter
* @copyright  2019 Humanoids Group, HIS, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <Ice/BuiltinSequences.ice>
/**
 * Definition of all generic VisionX interfaces
 */
module visionx
{
         struct LocalizationResult
	{
		Ice::FloatSeq matrix4x4;
		float confidence;
        long timestamp;
	};  
	interface ObjectPoseListenerInterface
	{
		void reportObjectPose(Ice::FloatSeq matrix4x4, float confidence);
	};    

	interface ObjectPoseInterface 
	{
		LocalizationResult getObjectPose(int objectId);
	};   
};


