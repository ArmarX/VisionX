/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    VisionX::OpenCVImageStabilizer
* @author     Stefan Reither
* @date       2018 Humanoids Group, H2T, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <VisionX/interface/core/ImageProcessorInterface.ice>
#include <RobotAPI/interface/visualization/DebugDrawerInterface.ice>
#include <ArmarXCore/interface/core/TextInterface.ice>

module armarx
{    
    interface OpenPoseEstimationInterface extends visionx::ImageProcessorInterface
    {
        void start();
        void stop();
        void start3DPoseEstimation();
        void stop3DPoseEstimation();
    };

    struct Keypoint2D
    {
        int id;
        string label;
        float x;
        float y;
        float confidence;
        armarx::DrawColor24Bit dominantColor;
    };

    dictionary<int, Keypoint2D> Keypoint2DList;
    dictionary<string, Keypoint2D> Keypoint2DMap;

    struct HumanPose2D
    {
        Keypoint2DMap keypointMap;
    }

    dictionary<string, HumanPose2D> HumanPose2DMap;

    struct Keypoint3D
    {
        int id;
        string label;
        float x;
        float y;
        float z;

        float globalX;
        float globalY;
        float globalZ;

        float confidence;
        armarx::DrawColor24Bit dominantColor;
    };

    dictionary<int, Keypoint3D> Keypoint3DList;
    dictionary<string, Keypoint3D> Keypoint3DMap;

    struct HumanPose3D
    {
        Keypoint3DMap keypointMap;
    }

    // This may be a map of human_id->human_pose (right now we cant recognize the human_ids, but it may be nice-to-have in the future, 
    // at least to identify that the update of last iteration and current iteration belong to the same person
    dictionary<string, HumanPose3D> HumanPose3DMap;

    interface OpenPose2DListener
    {
        void report2DKeypoints(HumanPose2DMap list, long timestamp);
        void report2DKeypointsNormalized(HumanPose2DMap list, long timestamp);
    };

    interface OpenPoseHand2DListener
    {
        void report2DHandKeypoints(HumanPose2DMap list, long timestamp);
        void report2DHandKeypointsNormalized(HumanPose2DMap list, long timestamp);
    };

    interface OpenPose3DListener
    {
        void report3DKeypoints(HumanPose3DMap list, long timestamp);
    };

    interface OpenPoseListener extends OpenPose3DListener, OpenPose2DListener
    {
    };
};
