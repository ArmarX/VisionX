/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    RobotComponents::OptokineticReflex
* @author     David Sippel ( uddoe at student dot kit dot edu )
* @date       2015 Humanoids Group, H2T, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once


#include <VisionX/interface/core/ImageProcessorInterface.ice>
#include <VisionX/interface/components/ImageSourceSelectionInterface.ice>


module armarx
{
    interface OpticalFlowListener
    {
        /**
         * @param diffx average optical flow in x direction. unit is rad/s
         * @param diffy average optical flow in y direction. unit is rad/s
         * @param deltaT time between the first and the second image. unit is s
         * @param timestamp when the first image was provided
         */
        void reportNewOpticalFlow(float diffx, float diffy, float deltaT, long timestamp);
    };


    interface OpticalFlowInterface extends visionx::ImageProcessorInterface, armarx::StereoCalibrationUpdateInterface
    {

    };


};


