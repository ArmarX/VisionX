/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    VisionX::Core
* @author     David Schiebener <schiebener at kit dot edu>
* @copyright  2015 Humanoids Group, HIS, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <VisionX/interface/core/ImageProcessorInterface.ice>
#include <VisionX/interface/core/ImageProviderInterface.ice>
#include <VisionX/interface/core/PointCloudProviderInterface.ice>
#include <VisionX/interface/core/PointCloudProcessorInterface.ice>
#include <VisionX/interface/components/Calibration.ice>

/**
 * Definition of all generic VisionX interfaces
 */
module visionx
{
    interface PointCloudAndImageProviderInterface extends PointCloudProviderInterface, CompressedImageProviderInterface
    {
    };

    interface CapturingPointCloudAndImageProviderInterface extends CapturingPointCloudProviderInterface, CompressedImageProviderInterface
    {
    };

    interface CapturingPointCloudAndImageAndCalibrationProviderInterface extends CapturingPointCloudAndImageProviderInterface
    {
        MonocularCalibration getMonocularCalibration();
        bool getImagesAreUndistorted();
    };

    interface CapturingPointCloudAndImageAndStereoCalibrationProviderInterface extends CapturingPointCloudAndImageAndCalibrationProviderInterface
    {
        StereoCalibration getStereoCalibration();
    };

    interface StereoImagePointCloudProviderInterface extends CapturingPointCloudAndImageAndCalibrationProviderInterface, ImageProcessorInterface
    {
    };

    interface PointCloudAndImageProcessorWithStereoCalibrationInterface extends PointCloudAndImageProcessorInterface, StereoCalibrationInterface
    {
    };
};

