/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::ViconStatechartContext
 * @author     Jonas Rauber ( kit at jonasrauber dot de )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotAPI/interface/core/PoseBase.ice>
#include <RobotAPI/interface/units/UnitInterface.ice>
#include <ArmarXCore/interface/core/UserException.ice>

module armarx
{
    /**
     * Will be thrown, if a Vicon error occurs.
     */
    exception ViconException extends UserException
    {
        /**
         * The Vicon error code as defined in the ViconDataStreamSDK.
         * Note that "no error" or "success" is represented with the
         * result code 2. The other result codes are error codes.
         */
        int errorCode;
    };

    /**
     * Will be thrown, if the requested object is occluded.
     */
    exception OccludedException extends UserException
    {
    };

    interface ViconComponentInterface extends SensorActorUnitInterface
    {
        /**
         * requests a frame from vicon
         */
        void getFrame() throws ViconException;

        /**
         * Returns the position of the subjects marker in the coordinate system
         * of the base subject. The base subject must have three markers with
         * the names 1, 2 and 3.
         */
        Vector3Base getRelativePosition(string subjectName,
                                        string markerName,
                                        string baseSubjectName
                                        ) throws ViconException, OccludedException;

        /**
         * Returns the position of the subject's marker in the baseSubject's coordinate system
         * as well as two additional points indicating the subject's orientation in the
         * baseSubject's coordiante system.
         */
        Ice::FloatSeq getRelativePositions(string subjectName,
                                           string baseSubjectName
                                           ) throws ViconException, OccludedException;

        /**
         * Prints all information that can be obtained from vicon.
         */
        void printStatus();

        void startRecording(string customName);

        void stopRecording();
    };
};

