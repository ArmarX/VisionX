/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    VisionX::Core
* @author     Kai Welke <welke at kit dot edu>
* @author     Jan Issac <jan dot issac at gmx dot de>
* @author     David Gonzalez <david dot gonzalez at kit dot edu>
* @copyright  2010 Humanoids Group, HIS, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/interface/core/BasicTypes.ice>
#include <ArmarXCore/interface/core/SharedMemory.ice>
#include <RobotAPI/interface/core/PoseBase.ice>


/**
 * Definition of all generic VisionX interfaces
 */
module visionx
{       
    module types
    {
        /**
         * Encodes if the object has already been searched; if yes, if it has been found or not;
         * and if it has ever been found, if it's still visible.
         */
        enum EObjectFoundState
        {
            eNotYetSearched,
            eSearchedButNotFound,
            eFound,
            eFoundButLost
        };

        /**
         * Encodes which kind of recognizer is used to localize this object
         */
        enum ERecognizerType
        {
            eSegmentable,
            eTextured,
            eRobotHand
        };
        
        enum EMotionModelType
        {
            eMMStaticObject,
            eMMRobotHand
        };

        /**
         * Contains all relevant information about each object. This will be kept in 
         * the object memory and submitted to the memory observer
         */
        struct ObjectInformation
        {
            string objectName;
            armarx::PoseBase objectPose;
            int objectPriority;
            EObjectFoundState foundState;
            float existenceCertainty;
            ERecognizerType recognizerType;
            EMotionModelType motionModelType;
        };

        /**
         * Simple float STL vector
         */ 
        sequence<float> Vec;
        
        /**
         * Simple float matrix which consists of float vectors.
         */
        sequence<Vec> Mat;

		/**
         * List of 3D points.
         */
		sequence<armarx::Vector3Base> PointList;
        
        /**
         * Image reagion definition 
         */
        struct Region
        {
            Ice::IntSeq pixels;
            
            int nPixels;
            
            Vec centroid;
            
            int minX;
            int minY;
            int maxX;
            int maxY;
            
            float ratio;
        };
    };

    /**
     * Provided image types that can be used for the transfer via Ice
     * and shared memory.
     */
    enum ImageType
    {
        eBayerPattern,
        eGrayScale,
        eRgb,
        eFloat1Channel,
        eFloat3Channels,
        //This is added for the transfer of active scans from Asus or Kinekt like cameras
        //THis will be shortly removed
        ePointsScan,
        eColoredPointsScan
    };
    
    enum BayerPatternType
    {
        eBayerPatternBg,
        eBayerPatternGb,
        eBayerPatternGr,
        eBayerPatternRg
    };
    
    /**
     * Supported image transfer modes. While the transfer of 
     * images between different components on different machines
     * is accomplished via Ice, the transfer between two components
     * on the same machine is carried out by shared memory. 
     */ 
    enum ImageTransferMode
    {
        eSharedMemoryTransfer,
        eIceTransfer
    };
    
    /**
     * Image capturing and publishing synchronization modes.
     * Either the capturing synchronization is handled by 
     * the capturer which can be blocking or polling or the
     * synchronization is forced to ensure a specified FPS.
     */
    enum ImageSyncMode
    {
        eCaptureSynchronization,
        eFpsSynchronization
    };
    
    
    /**
     * Dimension of a frame of a video sequence or a single image.
     */
    struct ImageDimension
    {
       int width = 640;
       int height = 480;
    };
    
    /**
     * Image format information struct. This defines all
     * necessary data of the type of provided images of
     * the respective provider.
     */
    struct ImageFormatInfo
    {
        ImageDimension dimension;
        int bytesPerPixel = 1;
        ImageType type = eBayerPattern;
        BayerPatternType bpType = eBayerPatternRg;
    };

    /**
     * Contains definitions for visual perception based on active cameras. This includes ASUS, Kinect and in the future many others.
     */


        /**
        * The type of point content defines whether a point provides information about various cues, namely orientation or color.
        * The tree main sources of information are available per point:
        * [1] The euclidean coordinates of the point in millimeters. These are represented by a float 32 bit value per coordinate. Since the order
        * of the coordinates and other elements is fundamental (when integrating ArmarX with other frameworks) the notation "Pfx" means,
        * Point-float-x coordinate. Thus, the sequence PfxPfyPfz means Point XYZ 96 bits with values in millimeters.
        * [2] The notation "Cbr mean, Color-byte-read channel
        * [3] The notation "Nfx" means Normal-float-x coordinate.
        * Finally, The combination(s) clearly represent the content of each point. Hence, from this definition it is also possible to infer the whole size in bytes of a point.
        */
        enum PointContentType
        {
            /**
            * Euclidean point XYZ \in R³ represented with 3 floats = 12 bytes = 96 bits, no padding added.
            * Total point size: 12 bytes = 96 bits
            */
            ePoints,

            /**
            * Color value RGBA \in N⁴:= [0,..,255]^4 represented with 4 bytes = 32 bits, the value A (Alpha channel) is actually used for padding when the point cloud is directly obtained form the active camera.
            * The further usage may be, containing alpha (transparency) values, or other application specific labels.
            * Euclidean point vector XYZ \in R³ represented with 3 floats = 12 bytes = 96 bits, no padding added.
            * Total point size: 16 bytes = 128 bits
            */
            eColoredPoints,

            /**
            * Euclidean normal vector NXNYNZ \in R³ represented with 3 floats = 12 bytes = 96 bits, no padding added.
            * Euclidean point vector XYZ \in R³ represented with 3 floats = 12 bytes = 96 bits, no padding added.
            * Total point size: 24 bytes = 192 bits
            */
            eOrientedPoints,

            /**
            * Color value RGBA \in N⁴ := [0,..,255]^4 represented with 4 bytes = 32 bits, the value A (Alpha channel) is actually used for padding when the point cloud is directly obtained form the active camera.
            * The further usage may be containing alpha (transparency) values or other labels.
            * Euclidean normal vector NXNYNZ \in R³ represented with 3 floats = 12 bytes = 96 bits, no padding added.
            * Euclidean point vector XYZ \in R³ represented with 3 floats = 12 bytes = 96 bits, no padding added.
            * Total point size: 28 bytes = 224 bits
            */
            eColoredOrientedPoints,

            eLabeledPoints,

            eColoredLabeledPoints,

            eIntensity,
        };


        class MetaPointCloudFormat extends armarx::MetaInfoSizeBase
        {

            int width;
            int height;

            PointContentType type;

            int seq;
            //string frameId;
        };

        /**
        * Color value RGBA \in N⁴ := [0,..,255]^4 represented with 4 bytes = 32 bits. Notice, the alpha channel can also be ignored depending the context.
        * Its inclusion is for 32 bit padding purposes. Namely, the 32 bits are needed for padding while transmitted by ICE. This occurs because the minimal
        * granularity of the data is 4 bytes. Thus, this situation will no affect data structures "X"  which are  (sizeof(X)%4 == 0).
        */
        struct RGBA
        {
            byte r;
            byte g;
            byte b;
            byte a;
        };

        /**
        * Euclidean normal vector NXNYNZ \in R³ represented with 3 floats = 12 bytes = 96 bits, no padding added.
        * For consistency, the magnitude of the vector should be always 1.0f or 0.0f depending on whether the value is known or not.
        * Any other magnitude should be normalized, this enables transparent interoperability between visualizations and calculations.
        * Total normal vector size: 12 bytes = 96 bits
        */
        struct Normal3D
        {
            float nx;
            float ny;
            float nz;
        };

        /**
        * Euclidean point vector PXPYPZ \in R³ represented with 3 floats = 12 bytes = 96 bits, no padding added.
        * Total point vector size: 12 bytes = 96 bits
        */
        struct Point3D
        {
            float x;
            float y;
            float z;
        };

        /**
        * Set of euclidean point vectors PXPYPZ \in R³ each point is represented with 3 floats = 12 bytes = 96 bits, no padding added.
        */
        sequence<Point3D> Point3DList;

        /**
        * OrientedPoint3D, Point with two cues: Orientation and Position:
        * Euclidean normal vector NXNYNZ \in R³ represented with 3 floats = 12 bytes = 96 bits, no padding added. See Normal3D for more information.
        * Euclidean point vector PXPYPZ \in R³ represented with 3 floats = 12 bytes = 96 bits, no padding added. See Point3D for more information.
        * Total oriented point vector size: 24 bytes = 192 bits.
        */
        struct OrientedPoint3D
        {
           Normal3D normal;
           Point3D point;
        };

        /**
        * ColoredPoint3D, Point with two cues: Color and Position:
        * Color value RGBA \in N⁴ := [0,..,255]^4 represented with 4 bytes = 32 bits. Notice, the alpha channel can also be ignored depending the context. See RGBA for more information.
        * Euclidean point vector PXPYPZ \in R³ represented with 3 floats = 12 bytes = 96 bits, no padding added. See Point3D for more information.
        * Total colored point vector size: 16 bytes = 128 bits
        */
        struct ColoredPoint3D
        {
            RGBA    color;
            Point3D point;
        };

        /**
        * A colored point cloud.
        */
        sequence<ColoredPoint3D> ColoredPointCloud;
        sequence<ColoredPointCloud> ColoredPointCloudList;


        /**
        * ColoredLabeledPoint3D, Point with three cues: Color, Position, and label:
        * Color value RGBA \in N⁴ := [0,..,255]^4 represented with 4 bytes = 32 bits. Notice, the alpha channel can also be ignored depending the context. See RGBA for more information.
        * Euclidean point vector PXPYPZ \in R³ represented with 3 floats = 12 bytes = 96 bits, no padding added. See Point3D for more information.
        * Total colored point vector size: 16 bytes = 128 bits
        * Label represents the segment identity
        */
        struct ColoredLabeledPoint3D
        {
            Point3D point;
            RGBA    color;
            int     label;
        };

        /**
        * Set of colored and labeled points. This represents a labeled and colored point cloud.
        */
        sequence<ColoredLabeledPoint3D> ColoredLabeledPointCloud;




        /**
        * LabeledPoint3D, Point with two cues: Position, and label:
        * Euclidean point vector PXPYPZ \in R³ represented with 3 floats = 12 bytes = 96 bits, no padding added. See Point3D for more information.
        * Label represents the segment identity
        */

        struct LabeledPoint3D
        {
            Point3D point;
            int     label;
        };







        /**
        * Set of labeled points. This represents a labeled point cloud.
        */
        sequence<LabeledPoint3D> LabeledCloud;

        /**
        * Set of labeled point clouds. This represents a labeled point cloud vector.
        */
        sequence<LabeledCloud> VectorCloud;


        struct IntensityPoint3D
        {
            Point3D point;
            float  intensity;
        };


         sequence<IntensityPoint3D> IntensityCloud;

        /**
        * PrimitiveData, Extracted Geometric Primitive Results:
        * Set of colored and labeled points. This represents the entire primite inliers.
        * Set of colored and labeled points. This represents the entire grasp points.
        * Simple float matrix which consists of float vectors. This represents plane model coefficients.
        * Simple float matrix which consists of float vectors. This represents cylinder model coefficients.
        * Simple float matrix which consists of float vectors. This represents sphere model coefficients.
        * Set of labeled point clouds. This represents plane inliers as a labeled point cloud vector.
        * Set of labeled point clouds. This represents cylinder inliers as a labeled point cloud vector.
        * Set of labeled point clouds. This represents sphere inliers as a labeled point cloud vector.
        * Set of labeled point clouds. This represents plane grasp points as a labeled point cloud vector.
        * Set of labeled point clouds. This represents cylinder  grasp points as a labeled point cloud vector.
        * Set of labeled point clouds. This represents sphere  grasp points as a labeled point cloud vector.
        */
        struct PrimitiveData
        {
           LabeledCloud entirePrimitiveInliers;
           LabeledCloud entireGraspPoints;

           visionx::types::Mat  planeModelCoefficients;
           visionx::types::Mat  cylinderModelCoefficients;
           visionx::types::Mat  sphereModelCoefficients;

           visionx::types::Vec  circularityProbabilities;

           VectorCloud planeInliers;
           VectorCloud cylinderInliers;
           VectorCloud sphereInliers;

           VectorCloud planeGraspPoints;
           VectorCloud cylinderGraspPoints;
           VectorCloud sphereGraspPoints;

        };

        /**
        * ColoredOrientedPoint3D, Point with three cues: Color, Orientation and Position:
        * Color value RGBA \in N⁴ := [0,..,255]^4 represented with 4 bytes = 32 bits. Notice, the alpha channel can be ignored depending the context. See RGBA for more information.
        * Euclidean normal vector NXNYNZ \in R³ represented with 3 floats = 12 bytes = 96 bits, no padding added. See Normal3D for more information.
        * Euclidean point vector PXPYPZ \in R³ represented with 3 floats = 12 bytes = 96 bits, no padding added. See Point3D for more information.
        * Total colored oriented point vector size: 28 bytes = 224 bits
        */
        struct ColoredOrientedPoint3D
        {
            RGBA color;
            Normal3D normal;
            Point3D point;
        };

        /**
        * Oriented partition plane in 3D.
        * Euclidean normal vector NXNYNZ \in R³ represented with 3 floats = 12 bytes = 96 bits, no padding added.
        * Notice: The normal vector should be always of magnitude 1. If this is not realized, the plane is offset by this magnitude.
        * This should be ensure to keep visualization and calculations consistent.
        * Hesse distance represented by one float = 32 bits.
        * Total oriented partition plane in 3D size: 16 bytes = 128 bits
        */
        struct Plane3D
        {
            Point3D normal;
            float hesseDistance;
        };

        struct BoundingBox2D
        {
            float x;
            float y;
            float w;
            float h;
        };

        /**
         * Axis-aligned bounding box in 3D.
         * The dimensions are as follows:
         * X Axis is the width delimited between [x0,x1] inclusive.
         * Y Axis is the height delimited between [y0,y1] inclusive.
         * Z Axis is the depth delimited between [z0,z1] inclusive.
         * A defined bounding box has fulfills (x1>x0) && (y1>y0) && (z1>z0), thus at leat one point is set.
         * This means, a bounding box with not volume is still valid as long as it contains at leats on single point.
         * An undefined bounding box fulfills (x0>x1) || (y0>y1) || (z0>z1). In this case, this is considered as empty.
         * Each coordinate value is one float 32 bits.
         * Total axis-aligned bounding box in 3D size: 24 bytes = 192 bits
         */
        struct BoundingBox3D
        {
            float x0;
            float y0;
            float z0;
            float x1;
            float y1;
            float z1;
        };
};

