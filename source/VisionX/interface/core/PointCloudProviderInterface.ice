/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    VisionX::Core
* @author     David Gonzalez <david dot gonzalez at kit dot edu>
* @copyright  2014 Humanoids Group, HIS, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/interface/core/UserException.ice>
#include <ArmarXCore/interface/core/SharedMemory.ice>
#include <VisionX/interface/core/DataTypes.ice>
#include <VisionX/interface/core/ImageProviderInterface.ice>
/**
 * Definition of all generic VisionX interfaces
 */
module visionx
{



    /**
     * A point cloud provider component captures sets of points from active cameras, files, ...
     **/
    interface PointCloudProviderInterface extends ProviderWithSharedMemorySupportInterface
    {
        /**
         * Retrives provider's point cloud
         */
        armarx::Blob getPointCloud(out MetaPointCloudFormat info);

        /**
         * Retrives provider's point cloud format information via Ice.
         * See the slice defintion for more detail is DataTypes.ice
         */
        MetaPointCloudFormat getPointCloudFormat();
    };

    interface ImageAndPointCloudProviderInterface extends CompressedImageProviderInterface,PointCloudProviderInterface
    {

    };

    exception PointCloudProviderFrameRateNotSupportedException extends armarx::UserException
    {
        float frameRate;
    };

    exception PointCloudProviderStartingCaptureFailedException extends armarx::UserException
    {

    };

    interface CapturingPointCloudProviderInterface extends PointCloudProviderInterface
    {
        /**
         * Initiate point cloud capturing.
         */
        void startCapture() throws PointCloudProviderStartingCaptureFailedException;
        void startCaptureForNumFrames(int numFrames) throws PointCloudProviderStartingCaptureFailedException;

        /**
         * Suspends point cloud capturing and hence point cloud update notification
         * broadcasts
         */
        void stopCapture();

        /**
         * @param framesPerSecond FPS that has to be ensure if the synchronization
         *        mode is set to eFpsSynchronization, otherwise this won't have any
         *        effect.
        */
        void changeFrameRate(float framesPerSecond) throws PointCloudProviderFrameRateNotSupportedException;


        idempotent bool isCaptureEnabled();
    };
};

