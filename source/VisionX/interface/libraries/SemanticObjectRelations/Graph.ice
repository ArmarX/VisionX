#pragma once

//#include <RobotAPI/interface/core/PoseBase.ice>


module armarx
{
    module semantic
    {
        module data
        {
            struct GraphVertex
            {
                long id = 0;

                string attributes;
            };

            struct GraphEdge
            {
                long sourceID = 0;
                long targetID = 0;

                string attributes;
            };

            sequence<GraphVertex> GraphVertexList;
            sequence<GraphEdge> GraphEdgeList;

            struct Graph
            {
                GraphVertexList vertices;
                GraphEdgeList edges;

                string attributes;
            };

            dictionary<string, data::Graph> GraphMap;
        };
    };
};
