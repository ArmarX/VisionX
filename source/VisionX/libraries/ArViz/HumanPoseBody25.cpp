#include <VisionX/libraries/ArViz/HumanPoseBody25.h>


#include <RobotAPI/components/ArViz/Client/Elements.h>


const std::map<unsigned int, std::string>
armarx::viz::HumanPoseBody25::parts =
{
    {0,  "Nose"},
    {1,  "Neck"},
    {2,  "RShoulder"},
    {3,  "RElbow"},
    {4,  "RWrist"},
    {5,  "LShoulder"},
    {6,  "LElbow"},
    {7,  "LWrist"},
    {8,  "MidHip"},
    {9,  "RHip"},
    {10, "RKnee"},
    {11, "RAnkle"},
    {12, "LHip"},
    {13, "LKnee"},
    {14, "LAnkle"},
    {15, "REye"},
    {16, "LEye"},
    {17, "REar"},
    {18, "LEar"},
    {19, "LBigToe"},
    {20, "LSmallToe"},
    {21, "LHeel"},
    {22, "RBigToe"},
    {23, "RSmallToe"},
    {24, "RHeel"},
    {25, "Background"}
};


const std::vector<std::tuple<float, float, float>>
        armarx::viz::HumanPoseBody25::colors =
{
    {255.f,     0.f,    85.f},
    {255.f,     0.f,     0.f},
    {255.f,    85.f,     0.f},
    {255.f,   170.f,     0.f},
    {255.f,   255.f,     0.f},
    {170.f,   255.f,     0.f},
    {85.f,   255.f,     0.f},
    {0.f,   255.f,     0.f},
    {255.f,     0.f,     0.f},
    {0.f,   255.f,    85.f},
    {0.f,   255.f,   170.f},
    {0.f,   255.f,   255.f},
    {0.f,   170.f,   255.f},
    {0.f,    85.f,   255.f},
    {0.f,     0.f,   255.f},
    {255.f,     0.f,   170.f},
    {170.f,     0.f,   255.f},
    {255.f,     0.f,   255.f},
    {85.f,     0.f,   255.f},
    {0.f,     0.f,   255.f},
    {0.f,     0.f,   255.f},
    {0.f,     0.f,   255.f},
    {0.f,   255.f,   255.f},
    {0.f,   255.f,   255.f},
    {0.f,   255.f,   255.f}
};



const std::vector<std::pair<unsigned int, unsigned int>>
        armarx::viz::HumanPoseBody25::pairs =
{
    {1, 8}, {1, 2}, {1, 5}, {2, 3}, {3, 4}, {5, 6}, {6, 7}, {8, 9}, {9, 10}, {10, 11}, {8, 12},
    {12, 13}, {13, 14}, {1, 0}, {0, 15}, {15, 17}, {0, 16}, {16, 18}, {14, 19}, {19, 20}, {14, 21},
    {11, 22}, {22, 23}, {11, 24}
};


void
armarx::viz::HumanPoseBody25::addPoseToLayer(const armarx::Keypoint3DMap& pose,
        armarx::viz::Layer& layer, const std::string& prefix)
{
    // Segments.
    for (const auto& [index1, index2] : HumanPoseBody25::pairs)
    {
        const std::string name1 = parts.at(index1);
        const std::string name2 = parts.at(index2);

        // Only draw segment if start end end are in keypoint map.
        if (pose.find(name1) == pose.end() or pose.find(name2) == pose.end())
        {
            continue;
        }


        const armarx::Keypoint3D p1 = pose.at(name1);
        const armarx::Keypoint3D p2 = pose.at(name2);

        if (std::isfinite(p1.globalX) && std::isfinite(p1.globalY) && std::isfinite(p1.globalZ)
            && std::isfinite(p2.globalX) && std::isfinite(p2.globalY) && std::isfinite(p2.globalZ))
        {
            const Eigen::Vector3f pos1(p1.globalX, p1.globalY, p1.globalZ);
            const Eigen::Vector3f pos2(p2.globalX, p2.globalY, p2.globalZ);

            if (pos1 == Eigen::Vector3f(0, 0, 0) || pos2 == Eigen::Vector3f(0, 0, 0))
            {
                continue;
            }

            float r, g, b;
            std::tie(r, g, b) = colors.at(index2);
            const armarx::viz::Color color = armarx::viz::Color::fromRGBA(r / 255.0f, g / 255.0f, b / 255.0f, 1.0);

            armarx::viz::Cylinder line = armarx::viz::Cylinder(prefix + "::" + p1.label + "_" + p2.label)
                                         .fromTo(pos1, pos2)
                                         .radius(20)
                                         .color(color);
            layer.add(line);
        }
    }

    // Keypoints.
    for (auto const& [id, name] : parts)
    {

        if (pose.find(name) != pose.end() && name != "Background")
        {
            armarx::Keypoint3D kp = pose.at(name);
            const Eigen::Vector3f pos(kp.globalX, kp.globalY, kp.globalZ);

            float r, g, b;
            std::tie(r, g, b) = colors.at(id);
            const armarx::viz::Color color = armarx::viz::Color::fromRGBA(r / 255.0f, g / 255.0f, b / 255.0f, 1.0);
            armarx::viz::Sphere sphere = armarx::viz::Sphere(prefix + "::" + name)
                                         .position(pos)
                                         .radius(30)
                                         .color(color);
            layer.add(sphere);
        }
    }
}
