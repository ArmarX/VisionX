/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::OpenPoseEstimation
 * @author     Stefan Reither ( stef dot reither at web dot de )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

// STD/STL

// IVT
#include <Calibration/Calibration.h>
#include <Calibration/StereoCalibration.h>


// Base class
#include <VisionX/libraries/OpenPose/MonocularOpenPoseEstimation/OpenPoseEstimationComponent.h>

namespace armarx
{
    class RGBDOpenPoseEstimationComponentPluginUser :
            public OpenPoseEstimationComponentPluginUser
    {
    public:
        using Base = OpenPoseEstimationComponentPluginUser;
        RGBDOpenPoseEstimationComponentPluginUser();

        // helper functions
        void preOnInitImageProcessor() override;
        void preOnConnectImageProcessor() override;
        void postOnConnectImageProcessor() override;
        void preOnDisconnectImageProcessor() override;
        void postOnDisconnectImageProcessor() override;

        void postCreatePropertyDefinitions(PropertyDefinitionsPtr& properties) override;

        // OpenPoseEstimation Interface
        void stop(const Ice::Current& = Ice::emptyCurrent)  override;
        void start3DPoseEstimation(const Ice::Current& = Ice::emptyCurrent)  override;
        void stop3DPoseEstimation(const Ice::Current& = Ice::emptyCurrent)  override;

        void renderOutputImage(const op::Array<float>&) override;
        void reportEntities() override;
        void visualize() override;

    private:
        void maskOutputImageBasedOnDepth();
        int getMedianDepthFromImage(int x, int y, int radius) const;

        void filterToNearest();



    public:
        // Depth image info
        unsigned int radius = 10;
        unsigned int maxDepth = 3000;
        unsigned int maxDepthDifference = 700;

        // Image calibration
        const CCalibration* calibration = nullptr;
        std::string cameraNodeName = "AzureKinectCamera";
        bool useDistortionParameters = false;

        // ImageBuffer und ImageInformations
        CByteImage* maskedrgbImageBuffer;

        CByteImage* depthImageBuffer;
        std::mutex depthImageBufferMutex;
        int brightnessIncrease = 100;

        // Meta (Properties)
        bool reportOnlyNearestPerson = false;

        // Topics
        OpenPose3DListenerPrx listener3DPrx;

        // OpenPose Buffers
        HumanPose3DMap openposeResult3D;

        // Robot
        armarx::RobotStateComponentInterfacePrx robotStateInterface;
        VirtualRobot::RobotPtr localRobot;

        // Threads and program flow information
        std::atomic_bool running3D;
    };
}
