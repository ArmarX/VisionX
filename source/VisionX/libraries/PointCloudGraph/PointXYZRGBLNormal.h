/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ROBDEKON::ArmarXObjects::DenseCRFSegmentationProcessor
 * @author     Christoph Pohl ( christoph dot pohl at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#define PCL_NO_PRECOMPILE
#include <pcl/point_types.h>
#include <pcl/pcl_macros.h>
#include <bitset>
#include <pcl/register_point_struct.h>

// We're doing a lot of black magic with Boost here, so disable warnings in Maintainer mode, as we will never
// be able to fix them anyway
#if defined _MSC_VER
#pragma warning(disable: 4201)
#endif
//#pragma warning(push, 1)
#if defined __GNUC__
#  pragma GCC system_header
#endif

namespace pcl
{
    /** \brief Members: float x, y, z, rgb, uint32_t label, normal[3], curvature
    * \ingroup common
    */
    struct PointXYZRGBLNormal;
}

#include "PointXYZRGBLNormal.hpp"

POINT_CLOUD_REGISTER_POINT_STRUCT(pcl::_PointXYZRGBLNormal,
                                  (float, x, x)
                                  (float, y, y)
                                  (float, z, z)
                                  (uint32_t, rgba, rgba)
                                  (uint32_t, label, label)
                                  (float, normal_x, normal_x)
                                  (float, normal_y, normal_y)
                                  (float, normal_z, normal_z)
                                  (float, curvature, curvature)
                                 )
POINT_CLOUD_REGISTER_POINT_WRAPPER(pcl::PointXYZRGBLNormal, pcl::_PointXYZRGBLNormal)


#if defined _MSC_VER
#pragma warning(default: 4201)
#endif