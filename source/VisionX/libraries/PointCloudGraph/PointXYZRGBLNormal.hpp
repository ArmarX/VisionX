/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ROBDEKON::ArmarXObjects::DenseCRFSegmentationProcessor
 * @author     Christoph Pohl ( christoph dot pohl at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "PointXYZRGBLNormal.h"

#include <Eigen/Core>
#include <ostream>

namespace pcl
{

    struct EIGEN_ALIGN16 _PointXYZRGBLNormal
    {
        PCL_ADD_POINT4D; // This adds the members x,y,z which can also be accessed using the point (which is float[4])
        PCL_ADD_RGB;
        PCL_ADD_NORMAL4D; // This adds the member normal[3] which can also be accessed using the point (which is float[4])
        union
        {
            struct
            {
                uint32_t label;
                float curvature;
            };
            float data_c[4];
        };
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    };

    PCL_EXPORTS std::ostream& operator << (std::ostream& os, const PointXYZRGBLNormal& p);
    struct EIGEN_ALIGN16 PointXYZRGBLNormal : public _PointXYZRGBLNormal
    {
        inline PointXYZRGBLNormal(const _PointXYZRGBLNormal& p)
        {
            x = p.x;
            y = p.y;
            z = p.z;
            data[3] = 1.0f;
            rgba = p.rgba;
            normal_x = p.normal_x;
            normal_y = p.normal_y;
            normal_z = p.normal_z;
            data_n[3] = 0.0f;
            curvature = p.curvature;
            label = p.label;
        }

        inline PointXYZRGBLNormal()
        {
            x = y = z = 0.0f;
            data[3] = 1.0f;
            r = g = b = 0;
            a = 255;
            normal_x = normal_y = normal_z = data_n[3] = 0.0f;
            label = 0;
            curvature = 0;
        }
        inline PointXYZRGBLNormal(uint8_t _r, uint8_t _g, uint8_t _b, uint32_t _label)
        {
            x = y = z = 0.0f;
            data[3] = 1.0f;
            r = _r;
            g = _g;
            b = _b;
            a = 255;
            label = _label;
            normal_x = normal_y = normal_z = data_n[3] = 0.0f;
            curvature = 0;
        }

        friend std::ostream& operator << (std::ostream& os, const PointXYZRGBLNormal& p);
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    };

}