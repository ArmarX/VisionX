#include "AABB.h"


namespace visionx
{
    BoundingBox3D tools::toBoundingBox3D(const Eigen::Vector3f& min, const Eigen::Vector3f& max)
    {
        visionx::BoundingBox3D bb;
        bb.x0 = min.x();
        bb.x1 = max.x();
        bb.y0 = min.y();
        bb.y1 = max.y();
        bb.z0 = min.z();
        bb.z1 = max.z();
        return bb;
    }

    BoundingBox3D tools::toBoundingBox3D(const simox::AxisAlignedBoundingBox& aabb)
    {
        return toBoundingBox3D(aabb.min(), aabb.max());
    }

    simox::AxisAlignedBoundingBox tools::toAABB(const BoundingBox3D& bb)
    {
        return simox::AxisAlignedBoundingBox(bb.x0, bb.x1, bb.y0, bb.y1, bb.z0, bb.z1);
    }
}
