set(LIB_NAME       VisionXPointCloudTools)

armarx_component_set_name("${LIB_NAME}")
armarx_set_target("Library: ${LIB_NAME}")

armarx_build_if(Eigen3_FOUND "Eigen3 not available")
armarx_build_if(PCL_FOUND "PCL not available")

find_package(CGAL QUIET)
# Build also when not found.
# armarx_build_if(CGAL_FOUND "CGAL not available")


set(LIBS
    ${PCL_LIBRARIES}

    ArmarXCoreInterfaces ArmarXCore
    RobotAPICore
    VisionXInterfaces VisionXPointCloud
)

set(LIB_FILES
    PointCloudTools.cpp

    AABB.cpp

    call_with_point_type.cpp
    FramedPointCloud.cpp
    MergedLabeledPointCloud.cpp
    segments.cpp

    OOBB/OOBB.cpp

    plane_fitting_ransac/plane_fitting_ransac.cpp
    PerpendicularPlaneFitting.cpp
    tools.cpp

	  FitKnownRectangleRotationMaxPoints.cpp
)

set(LIB_HEADERS
    PointCloudTools.h

    AABB.h

    call_with_point_type.h
    FramedPointCloud.h
    MergedLabeledPointCloud.h
    segments.h

    crop.h

    OOBB/OOBB.h
    OOBB/OOBB.hpp

    downsampling.h
    downsampling_voxelgrid/downsampling_voxelgrid.hpp

    plane_fitting.h
    plane_fitting_ransac/plane_fitting_ransac.h
    plane_fitting_ransac/plane_fitting_ransac.hpp
    VisitPointLikeContainer.h

    PerpendicularPlaneFitting.h
    tools.h


	  FitKnownRectangleRotationMaxPoints.h
)


armarx_add_library("${LIB_NAME}" "${LIB_FILES}" "${LIB_HEADERS}" "${LIBS}")

if(PCL_FOUND AND Eigen3_FOUND)
    target_include_directories(${LIB_NAME} SYSTEM PUBLIC ${PCL_INCLUDE_DIRS})
    # target_compile_definitions(${LIB_NAME} PUBLIC ${PCL_DEFINITIONS})
    # target_include_directories(${LIB_NAME} SYSTEM PUBLIC ${Eigen3_INCLUDE_DIR})
endif()

if(CGAL_FOUND)
    target_link_libraries(${LIB_NAME} PRIVATE CGAL::CGAL)
    target_compile_definitions(${LIB_NAME} PUBLIC CGAL_FOUND=1)
endif()

# add unit tests
add_subdirectory(test)
