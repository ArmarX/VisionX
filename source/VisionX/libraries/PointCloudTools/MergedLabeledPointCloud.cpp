/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MA_RainerKartmann::ArmarXObjects::PointCloudMerger
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "MergedLabeledPointCloud.h"

#include <pcl/common/io.h>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include <VisionX/components/pointcloud_core/PointCloudConversions.h>

#include "segments.h"


namespace visionx
{

    MergedLabeledPointCloud::MergedLabeledPointCloud() = default;


    void MergedLabeledPointCloud::setInputPointCloud(
        const std::string& name,
        pcl::PointCloud<PointL>::ConstPtr inputCloud,
        bool isLabeled)
    {
        // Todo: replace with real exception.
        ARMARX_CHECK(inputCloud) << "Passed input cloud must not be null (name: " << name << ").";

        if (isLabeled)
        {
            inputLabeledClouds[name] = inputCloud;
            changed = true;
        }
        else
        {
            pcl::PointCloud<PointT>::Ptr unlabeled(new pcl::PointCloud<PointT>);
            pcl::copyPointCloud(*inputCloud, *unlabeled);
            setInputPointCloud(name, unlabeled);
        }
    }

    void MergedLabeledPointCloud::setInputPointCloud(
        const std::string& name,
        pcl::PointCloud<PointL>::ConstPtr inputCloud,
        visionx::PointContentType originalType)
    {
        setInputPointCloud(name, inputCloud, visionx::tools::isLabeled(originalType));
    }

    void MergedLabeledPointCloud::setInputPointCloud(
        const std::string& name, pcl::PointCloud<PointT>::ConstPtr inputCloud)
    {
        // Todo: replace with real exception.
        ARMARX_CHECK(inputCloud) << "Passed input cloud must not be null (name: " << name << ").";

        inputUnlabeledClouds[name] = inputCloud;
        changed = true;
        // Remove potential converted clouds.
        inputLabeledClouds.erase(name);
    }


    auto MergedLabeledPointCloud::getResultPointCloud() -> pcl::PointCloud<PointL>::Ptr
    {
        // Rebuild result cloud if necessary.
        if (changed)
        {
            // Relabel unlabeled point clouds if necessary.
            if (!inputUnlabeledClouds.empty())
            {
                // Get used labels and label unlabeled clouds.
                labelUnlabeledClouds(getUsedLabels());
            }

            mergeLabeledClouds();
            changed = false;
        }

        return resultCloud;
    }


    auto MergedLabeledPointCloud::getUsedLabels() const -> std::set<Label>
    {
        // Get used labels.
        std::set<Label> usedLabels;
        for (const auto& [name, cloud] : inputLabeledClouds)
        {
            for (const auto& point : cloud->points)
            {
                usedLabels.insert(point.label);
            }
        }
        return usedLabels;
    }


    void MergedLabeledPointCloud::labelUnlabeledClouds(const std::set<Label>& usedLabels)
    {
        // Start assigning new labels.
        Label label = 1;
        for (const auto& [name, unlabeled] : inputUnlabeledClouds)
        {
            // Find unused label.
            while (usedLabels.count(label) > 0)
            {
                ++label;
            }

            // Construct labelled cloud.
            pcl::PointCloud<PointL>::Ptr labeled(new pcl::PointCloud<PointL>);
            pcl::copyPointCloud(*unlabeled, *labeled);
            for (auto& point : labeled->points)
            {
                point.label = label;
            }

            // Store labeled cloud (overwriting old relabeled clouds with the same name).
            inputLabeledClouds[name] = labeled;
        }
        // Remove old unlabeled clouds.
        inputUnlabeledClouds.clear();
    }


    void MergedLabeledPointCloud::mergeLabeledClouds()
    {
        // Build result point cloud.
        resultCloud->clear();
        uint64_t timestamp = 0;  // Use highest timestamp for result cloud.
        for (const auto& [name, cloud] : inputLabeledClouds)
        {
            ARMARX_CHECK(cloud);
            if (cloud)
            {
                (*resultCloud) += *cloud;
            }
            timestamp = std::max(timestamp, cloud->header.stamp);
        }
        resultCloud->header.stamp = timestamp;
    }

}
