#pragma once

#include <Eigen/Core>

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/PointIndices.h>
#include <pcl/common/angles.h>

#include <VisionX/libraries/PointCloudTools/plane_fitting_ransac/plane_fitting_ransac.h>


namespace Eigen
{
    using Hyperplane3f = Hyperplane<float, 3>;
}


namespace visionx::tools
{

    class PerpendicularPlaneFitting
    {
    public:
        using PointT = pcl::PointXYZRGBA;

    public:

        double distanceThreshold = 1;
        Eigen::Vector3f normal = Eigen::Vector3f::UnitZ();
        double epsAngle = pcl::deg2rad(10.0);

        int maxIterations = 1000;
        double probability = 0.99;


        std::optional<PlaneFittingResult> fit(pcl::PointCloud<PointT>::ConstPtr input);

    };


}
