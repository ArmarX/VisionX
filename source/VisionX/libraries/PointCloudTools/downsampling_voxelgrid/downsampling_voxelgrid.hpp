#pragma once

#include <pcl/point_cloud.h>
#include <pcl/filters/voxel_grid.h>


namespace visionx::tools
{

    namespace detail
    {

        template <typename PointCloudT, typename IndicesPtrT = pcl::IndicesConstPtr>
        typename PointCloudT::Ptr
        downsampleByVoxelGrid(typename PointCloudT::ConstPtr inputCloud,
                              float leafSize = 5.0,
                              const IndicesPtrT& indices = nullptr)
        {
            using PointT = typename PointCloudT::PointType;

            pcl::VoxelGrid<PointT> vg;

            vg.setLeafSize(leafSize, leafSize, leafSize);

            vg.setInputCloud(inputCloud);
            if (indices)
            {
                vg.setIndices(indices);
            }

            typename PointCloudT::Ptr output(new PointCloudT);
            vg.filter(*output);
            return output;
        }

    }

    template <typename PointCloudT>
    typename PointCloudT::Ptr
    downsampleByVoxelGrid(typename PointCloudT::ConstPtr inputCloud, float leafSize = 5.0,
                          pcl::IndicesConstPtr indices = nullptr)
    {
        return detail::downsampleByVoxelGrid<PointCloudT>(inputCloud, leafSize, indices);
    }
    template <typename PointCloudT>
    typename PointCloudT::Ptr
    downsampleByVoxelGrid(typename PointCloudT::ConstPtr inputCloud, float leafSize = 5.0,
                          pcl::PointIndicesConstPtr indices = nullptr)
    {
        return detail::downsampleByVoxelGrid<PointCloudT>(inputCloud, leafSize, indices);
    }

}
