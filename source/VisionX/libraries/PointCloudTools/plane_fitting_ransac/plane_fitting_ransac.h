#pragma once

#include <optional>

#include <Eigen/Geometry>

#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/PointIndices.h>


// This header exposes minimal includes. For templates, include *.hpp.


namespace Eigen
{
    using Hyperplane3f = Hyperplane<float, 3>;
}


namespace visionx::tools
{

    struct PlaneFittingResult
    {
        pcl::PointIndices::Ptr inliers;
        Eigen::Hyperplane3f plane = Eigen::Hyperplane3f(Eigen::Vector3f::UnitZ(), 0);
    };


#define DECLARE_fitPlaneRansac_PointT(PointT)  \
    std::optional<PlaneFittingResult> fitPlaneRansac(  \
            pcl::PointCloud< PointT >::Ptr cloud, double distanceThreshold = 1.0,  \
            pcl::PointIndices::Ptr indices = nullptr)


    DECLARE_fitPlaneRansac_PointT(pcl::PointXYZ);
    DECLARE_fitPlaneRansac_PointT(pcl::PointXYZRGB);
    DECLARE_fitPlaneRansac_PointT(pcl::PointXYZRGBA);
    DECLARE_fitPlaneRansac_PointT(pcl::PointXYZRGBL);


#undef DECLARE_fitPlaneRansac_PointT

}
