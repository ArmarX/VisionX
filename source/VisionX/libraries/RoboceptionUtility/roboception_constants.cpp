#include "roboception_constants.h"


namespace visionx
{
    const std::map<std::string, float> roboception::DepthQualityToFPS =
    {
        { roboception::GC_DEPTH_QUALITY_LOW, 25.f },
        { roboception::GC_DEPTH_QUALITY_MEDIUM, 15.f },
        { roboception::GC_DEPTH_QUALITY_HIGH, 3.f },
        { roboception::GC_DEPTH_QUALITY_FULL, 0.8f }
    };

}
