#pragma once

#include <map>
#include <string>


namespace visionx::roboception
{
    const char* const GC_COMPONENT_ENABLE = "ComponentEnable";
    const char* const GC_COMPONENT_SELECTOR = "ComponentSelector";
    const char* const GC_EXPOSURE_TIME = "ExposureTime";
    const char* const GC_EXPOSURE_AUTO = "ExposureAuto";
    const char* const GC_EXPOSURE_TIME_AUTO_MAX = "ExposureTimeAutoMax";
    const char* const GC_GAIN = "Gain";
    const char* const GC_DEPTH_QUALITY = "DepthQuality";
    const char* const GC_DEPTH_QUALITY_LOW = "Low";
    const char* const GC_DEPTH_QUALITY_MEDIUM = "Medium";
    const char* const GC_DEPTH_QUALITY_HIGH = "High";
    const char* const GC_DEPTH_QUALITY_FULL = "Full";

    extern const std::map<std::string, float> DepthQualityToFPS;
}
