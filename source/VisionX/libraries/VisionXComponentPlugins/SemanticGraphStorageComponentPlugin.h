#pragma once

#include <ArmarXCore/core/ComponentPlugin.h>

#include <VisionX/interface/libraries/SemanticObjectRelations/GraphStorage.h>

#include <SemanticObjectRelations/RelationGraph/json/AttributedGraph.h>


namespace armarx
{
    namespace plugins
    {

        class SemanticGraphStorageComponentPlugin : public ComponentPlugin
        {
        public:
            using ComponentPlugin::ComponentPlugin;

            void preOnInitComponent() override;

            void preOnConnectComponent() override;

            void postCreatePropertyDefinitions(PropertyDefinitionsPtr& properties) override;

            static constexpr const char* PROPERTY_NAME = "SemanticGraphTopicName";

            armarx::semantic::GraphStorageTopicPrx topic;
        };

    }

}


#include <ArmarXCore/core/ManagedIceObject.h>

namespace armarx
{

    class SemanticGraphStorageComponentPluginUser : virtual public ManagedIceObject
    {
    public:
        SemanticGraphStorageComponentPluginUser();

        armarx::semantic::GraphStorageTopicPrx const& getGraphStorageTopic();

        void storeGraph(std::string const& name, semrel::AttributedGraph const& graph);

    private:
        armarx::plugins::SemanticGraphStorageComponentPlugin* plugin = nullptr;

    };

}
