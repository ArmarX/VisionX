#pragma once

#include <Eigen/Core>


namespace visionx
{

    /**
     * @brief A line of voxels, from start voxel to an end voxel.
     *
     * Voxels are represented by their 3D integer index.
     *
     * Usage:
     *
     * (1) (a) Construct a VoxelLine line.
     *     (b) while !line.finished(), call line.next().
     *
     * (2) Call VoxelLine::getLineVoxels() to get all voxels at once.
     *
     *
     * Uses Bresenham's algorithm for line drawing in 3D (i.e. drawing voxels).
     *
     * Implementation based on:
     * https://www.geeksforgeeks.org/bresenhams-algorithm-for-3-d-line-drawing/
     */
    class VoxelLine
    {

    public:

        /// Construct an empty voxel line.
        VoxelLine();

        /**
         * @brief Construct a voxel line from start to end.
         *
         * @param start the start voxel
         * @param end   the end voxel
         * @param includeStart  if true, the start voxel is included in the result list
         * @param includeEnd    if true, the end voxel is included in the result list
         *
         * If `start` and `end` are equal and any of `includeStart` or
         * `includeEnd` is true, the point is included.
         */
        VoxelLine(const Eigen::Vector3i& start, const Eigen::Vector3i& end,
                  bool includeStart = true, bool includeEnd = true);


        /// Indicate whether there are more voxels on the line.
        bool finished() const;

        /// Get the next voxel.
        /// @throw `std::logic_error` if there are no more voxels (`this->finished()`)
        Eigen::Vector3i next();


        /// Get the voxels indices of the line from start to end.
        /// @see `VoxelLine::VoxelLine()` (constructor)
        static std::vector<Eigen::Vector3i> getLineVoxels(
            const Eigen::Vector3i& start, const Eigen::Vector3i& end,
            bool includeStart = true, bool includeEnd = true);


    private:

        using Index = Eigen::Vector3i::Index;
        static const Eigen::IOFormat iof;


        void init();
        void advance();


        // INPUT
        /// Start voxel.
        Eigen::Vector3i start;
        /// End voxel.
        Eigen::Vector3i end;

        /// Whether to include the start voxel.
        bool includeStart;
        /// Whether to include the end voxel.
        bool includeEnd;


        // COMPUTATION

        Eigen::Vector3i delta;  ///< The cwise absolute (end - start).
        Eigen::Vector3i step;   ///< Step directions (+1/-1).

        Index x;     ///< The driving axis.
        Index y, z;  ///< The two non-driving axes.
        int py, pz;  ///< Slope error terms.

        ///< The current voxel (most recently returned).
        Eigen::Vector3i _next;
        bool _finished = false;

    };

}
