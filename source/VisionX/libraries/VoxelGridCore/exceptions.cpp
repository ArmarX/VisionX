#include "exceptions.h"

#include <sstream>

#include "VoxelGridStructure.h"


namespace visionx::voxelgrid::error
{

    InvalidVoxelDataSize::InvalidVoxelDataSize(std::size_t actual, std::size_t expected) :
        VoxelGridError(makeMsg(actual, expected))
    {}

    std::string InvalidVoxelDataSize::makeMsg(std::size_t actual, std::size_t expected)
    {
        std::stringstream ss;
        ss << "Provided voxel data (" << actual << " voxels) "
           << "does not match structure (" << expected << " voxels).";
        return ss.str();
    }


    InvalidStructure::InvalidStructure(
        const VoxelGridStructure& expected, const VoxelGridStructure& actual,
        const std::string& message) :
        VoxelGridError(makeMsg(expected, actual, message))
    {}

    std::string InvalidStructure::makeMsg(
        const VoxelGridStructure& expected, const VoxelGridStructure& actual,
        const std::string& message)
    {
        std::stringstream ss;
        ss << message << "\n"
           << "  Expected: \n" << expected << "\n"
           << "  but was : \n" << actual;
        return ss.str();
    }

}


