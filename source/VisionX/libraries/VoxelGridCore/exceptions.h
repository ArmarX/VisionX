#pragma once

#include <stdexcept>


namespace visionx::voxelgrid
{
    class VoxelGridStructure;
}


namespace visionx::voxelgrid::error
{

    /**
     * @brief Base class for exceptions in this library.
     */
    class VoxelGridError : public std::runtime_error
    {
    public:
        using std::runtime_error::runtime_error;
    };


    /**
     * @brief Indicates that voxel data with an invalid number of voxels
     * has been passed to a VoxelGrid.
     */
    class InvalidVoxelDataSize : public VoxelGridError
    {
    public:
        InvalidVoxelDataSize(std::size_t actual, std::size_t expected);

    private:
        std::string makeMsg(std::size_t actual, std::size_t expected);

    };

    /**
     * @brief Indicates that a voxel grid structure should have matched
     * another one, but did not.
     */
    class InvalidStructure : public VoxelGridError
    {
    public:
        InvalidStructure(const VoxelGridStructure& expected,
                         const VoxelGridStructure& actual,
                         const std::string& message = "Voxel grid structure does not match.");

    private:
        std::string makeMsg(const VoxelGridStructure& expected,
                            const VoxelGridStructure& actual,
                            const std::string& message);

    };



}


