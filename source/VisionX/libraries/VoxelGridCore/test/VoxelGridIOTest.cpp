/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RelationsForManipulation::ArmarXObjects::SupportVoxelGrid
 * @author     Rainer Kartmann ( rainer dot kartmann at student dot kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE VisionX::ArmarXLibraries::VoxelGridCore

#define ARMARX_BOOST_TEST

#include <VisionX/Test.h>
#include "../io/BinaryIO.h"

#include <iostream>
#include <filesystem>


using namespace visionx::voxelgrid;
namespace fs = std::filesystem;
namespace vg_io = visionx::voxelgrid::io;

using VoxelGridi = VoxelGrid<int>;


struct DetailFixture
{
    DetailFixture()
    {

    }
    ~DetailFixture()
    {
        if (fs::exists(filename))
        {
            fs::remove(filename);
        }
    }

    template <typename T>
    void write(const T& data)
    {
        os.open(filename.string(), std::ofstream::binary);
        vg_io::detail::writeData(os, data);
        os.close();
    }

    template <typename T>
    void read(T& data)
    {
        is.open(filename.string(), std::ofstream::binary);
        vg_io::detail::readData(is, data);
        is.close();
    }

    const fs::path filename = "voxel_grid_io_test_detail.data";

    std::ifstream is;
    std::ofstream os;
};


struct Custom
{
    int i;
    float f;
    Eigen::Vector3i vi;
    Eigen::Vector3f vf;

    bool operator== (const Custom& rhs) const
    {
        return i == rhs.i && fabsf(f - rhs.f) <= 0 && vi == rhs.vi && vf == rhs.vf;
    }
};

std::ostream& operator<< (std::ostream& os, const Custom& rhs)
{
    static const Eigen::IOFormat iof(3, 0, " ", " ", "", "", "[", "]");
    os << "[ i=" << rhs.i << " f=" << rhs.f
       << " vi=" << rhs.vi.format(iof) << " vf=" << rhs.vf.format(iof) << " ]";
    return os;
}


BOOST_FIXTURE_TEST_SUITE(VoxelGridIO_detail_TestSuite, DetailFixture)


// test_write

BOOST_AUTO_TEST_CASE(test_int)
{
    int out = 42, in = 0;
    write(out);
    read(in);
    BOOST_CHECK_EQUAL(in, out);
}


BOOST_AUTO_TEST_CASE(test_float)
{
    float out = 42.5, in = 0;
    write(out);
    read(in);
    BOOST_CHECK_EQUAL(in, out);
}

BOOST_AUTO_TEST_CASE(test_custom)
{
    Custom out {42, 42.5, {1, 2, 3}, {1.5, 2.5, 3.5 }};
    Custom in;

    write(out);
    read(in);
    BOOST_CHECK_EQUAL(in, out);
}

BOOST_AUTO_TEST_SUITE_END()


struct Fixture
{
    Fixture()
    {
        BOOST_TEST_MESSAGE("Grid: " << grid);

        grid.setOrigin({ 2, 0, -6});
        grid.setOrientation(Eigen::Quaternionf(
                                Eigen::AngleAxisf(1.25, Eigen::Vector3f(2, 1, -1).normalized())));

        // fill voxel grid with data
        for (std::size_t i = 0; i < grid.numVoxels(); ++i)
        {
            grid.getVoxel(i) = 200 + 2 * static_cast<int>(i);
        }

        if (fs::exists(filename) && fs::is_regular_file(filename))
        {
            fs::remove(filename);
        }

        if (!fs::exists(dirExist))
        {
            fs::create_directory(dirExist);
        }

        BOOST_CHECK(!fs::exists(dirNonExist));
    }

    ~Fixture()
    {
        if (fs::exists(filename) && fs::is_regular_file(filename))
        {
            fs::remove(filename);
        }

        if (fs::exists(dirExist))
        {
            fs::remove(dirExist);
        }
    }

    const fs::path filename = "voxel_grid_io_test.grid";
    const fs::path dirExist = "voxel_grid_io_test.dir";
    const fs::path dirNonExist = "voxel_grid_io_test_non_exist.dir";

    VoxelGridi grid {{ 16, 1.f }};

};



BOOST_FIXTURE_TEST_SUITE(VoxelGridIOTestSuite, Fixture)


// test_write

BOOST_AUTO_TEST_CASE(test_write_no_fail)
{
    BOOST_CHECK(!fs::exists(filename));

    BOOST_CHECK_NO_THROW(vg_io::BinaryIO::write(grid, filename.string()));

    BOOST_CHECK(fs::exists(filename));
    BOOST_CHECK(fs::is_regular_file(filename));

    BOOST_CHECK_GE(fs::file_size(filename), 0);
}


BOOST_AUTO_TEST_CASE(test_write_on_existent_directory)
{
    BOOST_REQUIRE(fs::exists(dirExist) && fs::is_directory(dirExist));

    BOOST_CHECK_THROW(vg_io::BinaryIO::write(grid, dirExist.string()), std::ios_base::failure);
}

BOOST_AUTO_TEST_CASE(test_write_in_nonexistent_directory)
{
    BOOST_REQUIRE(!fs::exists(dirNonExist));

    BOOST_CHECK_THROW(vg_io::BinaryIO::write(grid, (dirNonExist / filename).string()),
                      std::ios_base::failure);
}


// test_read

BOOST_AUTO_TEST_CASE(test_read_as_param)
{
    vg_io::BinaryIO::write(grid, filename.string());

    VoxelGridi read;
    vg_io::BinaryIO::read(read, filename.string());

    BOOST_CHECK_EQUAL(grid.getVoxelSizes(), read.getVoxelSizes());
    BOOST_CHECK_EQUAL(grid.getOrigin(),     read.getOrigin());
    BOOST_CHECK_EQUAL(grid.getGridSizes(),  read.getGridSizes());

    BOOST_CHECK_EQUAL_COLLECTIONS(grid.begin(), grid.end(),
                                  read.begin(), read.end());
}


BOOST_AUTO_TEST_CASE(test_read_as_return)
{
    vg_io::BinaryIO::write(grid, filename.string());

    VoxelGridi read;
    read = vg_io::BinaryIO::read<VoxelGridi>(filename.string());

    BOOST_CHECK_EQUAL(grid.getVoxelSizes(), read.getVoxelSizes());
    BOOST_CHECK_EQUAL(grid.getOrigin(),     read.getOrigin());
    BOOST_CHECK_EQUAL(grid.getGridSizes(),  read.getGridSizes());

    BOOST_CHECK_EQUAL_COLLECTIONS(grid.begin(), grid.end(),
                                  read.begin(), read.end());
}


BOOST_AUTO_TEST_CASE(test_read_from_directory)
{
    VoxelGridi read;
    BOOST_CHECK_THROW(read = vg_io::BinaryIO::read<VoxelGridi>(dirExist.string()),
                      std::ios_base::failure);
}

BOOST_AUTO_TEST_CASE(test_read_from_nonexistent_file)
{
    VoxelGridi read;
    BOOST_CHECK_THROW(read = vg_io::BinaryIO::read<VoxelGridi>((filename / "non_exist").string()),
                      std::ios_base::failure);
}

BOOST_AUTO_TEST_CASE(test_read_from_nonexistent_dir)
{
    VoxelGridi read;
    BOOST_CHECK_THROW(read = vg_io::BinaryIO::read<VoxelGridi>((dirNonExist / filename).string()),
                      std::ios_base::failure);
}


BOOST_AUTO_TEST_SUITE_END()

