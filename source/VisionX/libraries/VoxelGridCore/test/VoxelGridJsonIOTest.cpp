/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RelationsForManipulation::ArmarXObjects::SupportVoxelGrid
 * @author     Rainer Kartmann ( rainer dot kartmann at student dot kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE VisionX::ArmarXLibraries::VoxelGridCore

#define ARMARX_BOOST_TEST

#include <VisionX/Test.h>
#include "../io/JsonIO.h"

#include <iostream>
#include <filesystem>


using namespace visionx::voxelgrid;
namespace fs = std::filesystem;
namespace vgio = visionx::voxelgrid::io;


namespace custom
{
    struct Custom
    {
        int a;
        float b;
    };

    void to_json(nlohmann::json& j, const Custom& c)
    {
        j["a"] = c.a;
        j["b"] = c.b;
    }
    void from_json(const nlohmann::json& j, Custom& c)
    {
        c.a = j.at("a").get<int>();
        c.b = j.at("b").get<float>();
    }
    bool operator!= (const Custom& lhs, const Custom& rhs)
    {
        return lhs.a != rhs.a || std::abs(lhs.b - rhs.b) > 1e-3f;
    }
    bool operator== (const Custom& lhs, const Custom& rhs)
    {
        return !(lhs != rhs);
    }
    std::ostream& operator<< (std::ostream& os, const Custom& c)
    {
        return os << "(a=" << c.a << " b=" << c.b << ")";
    }
}


struct Voxel
{
    int i;
    std::string s;
    custom::Custom c;

    custom::Custom getC() const
    {
        return c;
    }
    void setC(const custom::Custom& c)
    {
        this->c = c;
    }
};

// Provide to_json/from_json for Voxel

void to_json(nlohmann::json& j, const Voxel& v)
{
    j["i"] = v.i;
    j["s"] = v.s;
    j["c"] = v.c;
}
void from_json(const nlohmann::json& j, Voxel& v)
{
    v.i = j.at("i");
    v.s = j.at("s");
    v.c = j.at("c");
}


struct Fixture
{
    Fixture()
    {
        // Fill data.
        for (std::size_t i = 0; i < grid.numVoxels(); ++i)
        {
            Voxel& voxel = grid.getVoxel(i);
            voxel.i = 2 * static_cast<int>(i) + 1;
            voxel.s = std::to_string(i) + "_foo";
            voxel.c = { static_cast<int>(-i), i * 3.14f };
        }
    }

    VoxelGridStructure structure =
    {
        {2, 3, 4}, {1.0, 2.0, 3.5}, {0, -2, 1.25},
        Eigen::Quaternionf(Eigen::AngleAxisf(1.74f, Eigen::Vector3f::UnitZ()))
    };
    VoxelGrid<Voxel> grid = { structure };
    const VoxelGrid<Voxel>& gridConst = grid;


    static nlohmann::json getI(const Voxel& v)
    {
        return v.i;
    }
    static nlohmann::json getS(const Voxel& v)
    {
        return v.s;
    }
    static nlohmann::json getC(const Voxel& v)
    {
        return v.c;
    }

    static void setI(const nlohmann::json& j, Voxel& v)
    {
        v.i = j;
    }
    static void setS(const nlohmann::json& j, Voxel& v)
    {
        v.s = j.get<std::string>();
    }
    static void setC(const nlohmann::json& j, Voxel& v)
    {
        v.c = j.get<custom::Custom>();
    }

};


BOOST_FIXTURE_TEST_SUITE(VoxelGridJsonIO, Fixture)


BOOST_AUTO_TEST_CASE(test_toJson)
{
    // Pass getters as lambdas.
    const nlohmann::json j = io::JsonIO::toJson(gridConst,
    {
        {
            "i", [](const Voxel & v)
            {
                return v.i;
            }
        },
        {
            "s", [](const Voxel & v)
            {
                return v.s + "_bar";
            }
        },
        {
            "c", [](const Voxel & v)
            {
                return v.c;
            }
        },
    });

    BOOST_TEST_MESSAGE("JSON: \n" << j.dump(2));

    BOOST_CHECK_GT(j.count("structure"), 0);
    BOOST_CHECK_GT(j.count("i"), 0);
    BOOST_CHECK_GT(j.count("s"), 0);
    BOOST_CHECK_GT(j.count("c"), 0);

    BOOST_CHECK(j.at("structure").is_object());
    BOOST_CHECK(j.at("i").is_array());
    BOOST_CHECK(j.at("s").is_array());
    BOOST_CHECK(j.at("c").is_array());

    BOOST_CHECK_EQUAL(j.at("i").size(), grid.numVoxels());
    BOOST_CHECK_EQUAL(j.at("s").size(), grid.numVoxels());
    BOOST_CHECK_EQUAL(j.at("c").size(), grid.numVoxels());


    for (std::size_t i = 0; i < grid.numVoxels(); ++i)
    {
        BOOST_CHECK_EQUAL(j.at("i").at(i).get<int>(), grid[i].i);
        BOOST_CHECK_EQUAL(j.at("s").at(i).get<std::string>(), grid[i].s + "_bar");
        BOOST_CHECK_EQUAL(j.at("c").at(i).get<custom::Custom>(), grid[i].c);
    }
}


BOOST_AUTO_TEST_CASE(test_toJson_fromJson)
{
    // Pass getters as (static) function name.
    const nlohmann::json j = io::JsonIO::toJson(grid,
    {
        {
            "i", Fixture::getI
        },
        {
            "s", Fixture::getS
        },
        {
            "c", Fixture::getC
        },
    });

    BOOST_TEST_MESSAGE("JSON: \n" << j.dump(2));

    VoxelGrid<Voxel> out;
    io::JsonIO::fromJson(j, out,
    {
        {
            "i", Fixture::setI
        },
        {
            "s", Fixture::setS
        },
        {
            "c", Fixture::setC
        },
    });

    BOOST_CHECK_EQUAL(out.numVoxels(), grid.numVoxels());
    BOOST_CHECK_EQUAL(out.getStructure(), grid.getStructure());

    for (std::size_t i = 0; i < grid.numVoxels(); ++i)
    {
        BOOST_CHECK_EQUAL(out[i].i, j.at("i").at(i).get<int>());
        BOOST_CHECK_EQUAL(out[i].s, j.at("s").at(i).get<std::string>());
        BOOST_CHECK_EQUAL(out[i].c, j.at("c").at(i).get<custom::Custom>());

        BOOST_CHECK_EQUAL(out[i].i, grid[i].i);
        BOOST_CHECK_EQUAL(out[i].s, grid[i].s);
        BOOST_CHECK_EQUAL(out[i].c, grid[i].c);
    }
}


BOOST_AUTO_TEST_CASE(test_getter_from_member_variable)
{
    // Pass getters as (static) function name.
    const nlohmann::json j = io::JsonIO::toJson(gridConst,
    {
        {
            "i", io::makeGetter(&Voxel::i)
        },
        {
            "s", io::makeGetter(&Voxel::s)
        },
        {
            "c", io::makeGetter(&Voxel::c)
        },
    });

    if (false)
    {
        BOOST_TEST_MESSAGE("JSON: \n" << j.dump(2));
    }

    VoxelGrid<Voxel> out;
    io::JsonIO::fromJson(j, out,
    {
        {
            "i", io::makeSetter(&Voxel::i)
        },
        {
            "s", io::makeSetter(&Voxel::s)
        },
        {
            "c", io::makeSetter(&Voxel::c)
        },
    });

    BOOST_CHECK_EQUAL(out.numVoxels(), grid.numVoxels());
    BOOST_CHECK_EQUAL(out.getStructure(), grid.getStructure());
}


BOOST_AUTO_TEST_CASE(test_getter_from_member_function)
{
    // Pass getters as (static) function name.
    const nlohmann::json j = io::JsonIO::toJson(gridConst,
    {
        {
            "c", io::makeGetter<Voxel, custom::Custom>(&Voxel::getC)
        },
    });

    if (false)
    {
        BOOST_TEST_MESSAGE("JSON: \n" << j.dump(2));
    }

    VoxelGrid<Voxel> out;
    io::JsonIO::fromJson(j, out,
    {
        {
            "c", io::makeSetter<Voxel, custom::Custom>(&Voxel::setC)
        },
    });

    BOOST_CHECK_EQUAL(out.numVoxels(), grid.numVoxels());
    BOOST_CHECK_EQUAL(out.getStructure(), grid.getStructure());
}



BOOST_AUTO_TEST_CASE(test_toJson_fromJson_voxel_array)
{
    const std::string voxelArrayName = "thevoxels";

    // Pass getters as (static) function name.
    const nlohmann::json j = io::JsonIO::toJson(grid, voxelArrayName);

    BOOST_TEST_MESSAGE("JSON: \n" << j.dump(2));

    BOOST_CHECK_GT(j.count("structure"), 0);
    BOOST_CHECK_GT(j.count(voxelArrayName), 0);

    BOOST_CHECK(j.at("structure").is_object());
    BOOST_CHECK(j.at(voxelArrayName).is_array());

    VoxelGrid<Voxel> out;
    io::JsonIO::fromJson(j, out, voxelArrayName);

    BOOST_CHECK_EQUAL(out.numVoxels(), grid.numVoxels());
    BOOST_CHECK_EQUAL(out.getStructure(), grid.getStructure());

    for (std::size_t i = 0; i < grid.numVoxels(); ++i)
    {
        BOOST_CHECK_EQUAL(out[i].i, grid[i].i);
        BOOST_CHECK_EQUAL(out[i].s, grid[i].s);
        BOOST_CHECK_EQUAL(out[i].c, grid[i].c);
    }
}


BOOST_AUTO_TEST_SUITE_END()
