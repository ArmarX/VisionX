#pragma once

#include "LabelDensity/VoxelGrid.h"
#include "LabelDensity/Visualizer.h"


namespace visionx
{
    using LabelDensityVoxel = voxelgrid::LabelDensity::Voxel;
    using LabelDensityVoxelGrid = voxelgrid::LabelDensity::VoxelGrid;
    using LabelDensityVisualizer = voxelgrid::LabelDensity::Visualizer;
}
