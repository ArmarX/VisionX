/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::VoxelGrids
 * @author     Rainer Kartmann ( rainer dot kartmann at student dot kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE VisionX::ArmarXLibraries::VoxelGrids

#define ARMARX_BOOST_TEST

#include <VisionX/Test.h>
#include "../LabelDensity.h"

#include <iostream>
#include <random>


using namespace visionx::voxelgrid::LabelDensity;
using visionx::voxelgrid::Label;


struct Fixture
{
    std::set<Label> labels { 0, 3, 42 };

    long unsigned int seed = 42;
    std::default_random_engine gen { seed };

    std::discrete_distribution<int> distribOccupied { 3, 2 };  // ( p_free, p_occup )
    std::uniform_int_distribution<std::size_t> distribNum { 0, 10 };

    int gridSize = 3;
    float voxelSize = 1.0;

    VoxelGrid grid {{ gridSize, voxelSize }};
    VoxelGrid gridOut;


    Fixture()
    {
        // Sample random (but fixed) voxel grid.
        for (auto& voxel : grid)
        {
            switch (distribOccupied(gen))
            {
                case 0:  // Free.
                    BOOST_CHECK(voxel.isFree());
                    break;

                case 1:  // Occupied.
                    for (Label label : labels)
                    {
                        voxel[label] = distribNum(gen);
                    }
                    break;
            }
        }

        // Set known values on some voxels.
        grid[0].clear(); // Empty
        grid[1] = { { 0, 10 } };
        grid[2] = { { 3, 20 } };
        grid[3] = { { 0, 5 }, {42, 17} };
        grid[4] = { { 0, 1 }, {3, 4}, {42, 43} };
        grid[5] = { { 0, 0 }, {3, 5}, {42, 0} };
        grid[6] = { { 0, 0 }, {3, 0}, {42, 0} };  // Actually empty.
    }
};


BOOST_FIXTURE_TEST_SUITE(LabelDensityGrid, Fixture)


BOOST_AUTO_TEST_CASE(test_isFree)
{
    BOOST_CHECK(grid[0].isFree());
    BOOST_CHECK(grid[6].isFree());

    BOOST_CHECK(!grid[1].isFree());
    BOOST_CHECK(!grid[2].isFree());
    BOOST_CHECK(!grid[3].isFree());
    BOOST_CHECK(!grid[4].isFree());
}


BOOST_AUTO_TEST_CASE(test_getTotal)
{
    BOOST_CHECK_EQUAL(grid[0].getTotal(), 0);
    BOOST_CHECK_EQUAL(grid[1].getTotal(), 10);
    BOOST_CHECK_EQUAL(grid[2].getTotal(), 20);
    BOOST_CHECK_EQUAL(grid[3].getTotal(), 22);
    BOOST_CHECK_EQUAL(grid[4].getTotal(), 48);
    BOOST_CHECK_EQUAL(grid[5].getTotal(), 5);
    BOOST_CHECK_EQUAL(grid[6].getTotal(), 0);
}

BOOST_AUTO_TEST_CASE(test_getDensity_total_equals_0_or_1)
{
    for (const Voxel& voxel : grid)
    {
        BOOST_TEST_INFO("Voxel: " << voxel);

        const std::map<Label, float> density = voxel.getDensity();
        float sum = 0;
        for (const auto& [label, prop] : density)
        {
            sum += prop;
        }

        if (voxel.isFree())
        {
            BOOST_CHECK_EQUAL(sum, 0.0f);
        }
        else
        {
            BOOST_CHECK_CLOSE(sum, 1.0f, 1e-6f);
        }
    }
}


BOOST_AUTO_TEST_CASE(test_json_io)
{
    nlohmann::json json;

    json = grid;

    BOOST_TEST_MESSAGE("JSON: \n" << json.dump());

    for (Label label : labels)
    {
        BOOST_CHECK_EQUAL(json.count(std::to_string(label)), 1);
    }

    gridOut = json.get<VoxelGrid>();

    BOOST_CHECK_EQUAL(grid.getStructure(), gridOut.getStructure());
    BOOST_CHECK_EQUAL_COLLECTIONS(grid.begin(), grid.end(),
                                  gridOut.begin(), gridOut.end());

}


BOOST_AUTO_TEST_SUITE_END()


