#pragma once


namespace visionx::voxelgrid
{
    /// Type of an object label.
    using Label = uint32_t;
}
