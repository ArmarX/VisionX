/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::armem_images_server
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "aron_conversions.h"
#include "VisionX/libraries/armem_human/types.h"

namespace armarx::armem::server::human
{

}



namespace armarx::armem::human
{
    void fromAron(const ::armarx::human::arondto::Body25Pose3D& dto, HumanPose& bo)
    {
      fromAron(dto.keypoints2D, bo.keypoint2dMap);
      fromAron(dto.keypoints3D, bo.keypoint3dMap);
    }

    void toAron(::armarx::human::arondto::Body25Pose3D& dto, const HumanPose& bo)
    {
      // FIXME implement
    }

    void fromAron(const std::map<std::string, armarx::human::arondto::Keypoint3D>& dto, armarx::armem::human::Keypoint3DIdMap& bo)
    {
        for(const auto& [k, v]: dto)
        {
          Keypoint3D kp;
          fromAron(v, kp);
          bo[k] = kp;
        }
    }

    void fromAron(const std::map<std::string, armarx::human::arondto::Keypoint2D>& dto, armarx::armem::human::Keypoint2DIdMap& bo)
    {
      for(const auto& [k, v]: dto)
        {
          Keypoint2D kp;
          fromAron(v, kp);
          bo[k] = kp;
        }
    }

    void fromAron(const armarx::human::arondto::Keypoint2D& dto, Keypoint2D& bo)
    {
       bo = Keypoint2D
       {
          .label = dto.label,
          .position = Eigen::Vector2f{dto.x, dto.y},
          .confidence = dto.confidence,
          .dominantColor = simox::color::Color{dto.dominantColor.r, dto.dominantColor.g, dto.dominantColor.b}
       };
    }

    void fromAron(const armarx::human::arondto::Keypoint3D& dto, Keypoint3D& bo)
    {
      bo = Keypoint3D
       { 
          .label = dto.label,
          .positionRobot = dto.positionRobot.toEigen(),
          .positionGlobal = dto.positionGlobal.toEigen(),
          .confidence = dto.confidence,
          .dominantColor = simox::color::Color{dto.dominantColor.r, dto.dominantColor.g, dto.dominantColor.b}
       };
    }

}
