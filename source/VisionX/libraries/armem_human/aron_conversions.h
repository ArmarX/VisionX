/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::armem_images_server
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include "VisionX/libraries/armem_human/types.h"
#include <RobotAPI/libraries/aron/core/data/variant/forward_declarations.h>
#include <VisionX/libraries/armem_human/aron/BODY_25Pose.aron.generated.h>
#include <VisionX/interface/components/OpenPoseEstimationInterface.h>


namespace armarx::armem::server::human
{

}

namespace armarx::armem::human
{
    void fromAron(const ::armarx::human::arondto::Body25Pose3D& dto, HumanPose& bo);
    void toAron(::armarx::human::arondto::Body25Pose3D& dto, const HumanPose& bo);

    void fromAron(const armarx::human::arondto::Keypoint2D& dto, Keypoint2D& bo);
    void fromAron(const armarx::human::arondto::Keypoint3D& dto, Keypoint3D& bo);

    void fromAron(const std::map<std::string, armarx::human::arondto::Keypoint3D>& dto, armarx::armem::human::Keypoint3DIdMap& bo);
    void fromAron(const std::map<std::string, armarx::human::arondto::Keypoint2D>& dto, armarx::armem::human::Keypoint2DIdMap& bo);
    
}
