#include "HumanPoseReader.h"

#include <iterator>

#include <RobotAPI/libraries/armem/util/util.h>

#include <VisionX/libraries/armem_human/aron/BODY_25Pose.aron.generated.h>
#include <VisionX/libraries/armem_human/aron_conversions.h>


namespace armarx::armem::human::client
{
    Reader::~Reader() = default;

    armarx::armem::client::query::Builder
    Reader::buildQuery(const Query& query) const
    {
        armarx::armem::client::query::Builder qb;

        armarx::armem::client::query::CoreSegmentSelector& coreSegmentQuery =
            qb.coreSegments().withName(properties().coreSegmentName);

        armarx::armem::client::query::ProviderSegmentSelector& providerQuery =
            [&]() -> armem::client::query::ProviderSegmentSelector&
        {
            if (not query.providerName.empty())
            {
                return coreSegmentQuery.providerSegments().withName(query.providerName);
            }
            return coreSegmentQuery.providerSegments().all();
        }();

        providerQuery.entities()
            .withName("3DDetections")
            .snapshots()
            .beforeOrAtTime(query.timestamp);

        return qb;
    }

    std::string
    Reader::propertyPrefix() const
    {
        return "mem.human.pose.";
    }

    armarx::armem::client::util::SimpleReaderBase::Properties
    Reader::defaultProperties() const
    {
        return {.memoryName = "Human", .coreSegmentName = "Pose"};
    }

    std::vector<HumanPose>
    asHumanPoses(const wm::ProviderSegment& providerSegment)
    {
        ARMARX_CHECK(not providerSegment.empty()) << "No entities";
        ARMARX_CHECK(providerSegment.size() == 1) << "There should be only one entity!";

        std::vector<HumanPose> humanPoses;
        providerSegment.forEachEntity(
            [&](const wm::Entity& entity)
            {
                const auto& entitySnapshot = entity.getLatestSnapshot();
                ARMARX_CHECK(not entitySnapshot.empty()) << "No entity snapshot instances";

                const auto* entityInstance = &entitySnapshot.getInstance(0);

                ARMARX_CHECK_NOT_NULL(entityInstance);

                const auto aronDto = tryCast<armarx::human::arondto::Body25Pose3D>(*entityInstance);
                ARMARX_CHECK(aronDto) << "Failed casting to Body25Pose3D";

                HumanPose humanPose;
                fromAron(*aronDto, humanPose);

                humanPoses.push_back(humanPose);
            });

        return humanPoses;
    }

    Reader::Result
    Reader::query(const Query& query) const
    {
        const auto qb = buildQuery(query);

        ARMARX_DEBUG << "[MappingDataReader] query ... ";

        const armem::client::QueryResult qResult = memoryReader().query(qb.buildQueryInput());

        ARMARX_DEBUG << "[MappingDataReader] result: " << qResult;

        if (not qResult.success)
        {
            ARMARX_WARNING << "Failed to query data from memory: " << qResult.errorMessage;
            return {.humanPoses = {},
                    .status = Result::Status::Error,
                    .errorMessage = qResult.errorMessage};
        }

        const auto coreSegment = qResult.memory.getCoreSegment(properties().coreSegmentName);

        if (query.providerName.empty())
        {
            std::vector<HumanPose> allHumanPoses;

            coreSegment.forEachProviderSegment(
                [&allHumanPoses](const auto& providerSegment)
                {
                    const std::vector<HumanPose> humanPoses = asHumanPoses(providerSegment);
                    std::copy(
                        humanPoses.begin(), humanPoses.end(), std::back_inserter(allHumanPoses));
                });

            if (allHumanPoses.empty())
            {
                ARMARX_WARNING << "No entities.";
                return {.humanPoses = {},
                        .status = Result::Status::NoData,
                        .errorMessage = "No entities"};
            }

            return Result{.humanPoses = allHumanPoses, .status = Result::Status::Success};
        }

        // -> provider segment name is set
        if (not coreSegment.hasProviderSegment(query.providerName))
        {
            ARMARX_WARNING << "Provider segment `" << query.providerName
                           << "` does not exist (yet).";
            return {.humanPoses = {}, .status = Result::Status::NoData};
        }

        const wm::ProviderSegment& providerSegment =
            coreSegment.getProviderSegment(query.providerName);

        if (providerSegment.empty())
        {
            ARMARX_WARNING << "No entities.";
            return {
                .humanPoses = {}, .status = Result::Status::NoData, .errorMessage = "No entities"};
        }

        try
        {
            const auto humanPoses = asHumanPoses(providerSegment);
            return Result{.humanPoses = humanPoses, .status = Result::Status::Success};
        }
        catch (...)
        {
            return Result{.status = Result::Status::Error,
                          .errorMessage = GetHandledExceptionString()};
        }
    }

} // namespace armarx::armem::human::client
