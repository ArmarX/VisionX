/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::armem_images_server
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "json_conversions.h"

#include <SimoxUtility/color/Color.h>
#include <SimoxUtility/color/json.h>
#include <SimoxUtility/json/util.h>

#include <ArmarXCore/core/time/DateTime.h>
#include <ArmarXCore/core/time/ice_conversions.h>
#include <ArmarXCore/core/time/json_conversions.h>

#include <RobotAPI/libraries/aron/common/aron_conversions/simox.h>
#include <RobotAPI/libraries/aron/common/json_conversions.h>
#include <RobotAPI/libraries/aron/converter/json/NLohmannJSONConverter.h>

#include <VisionX/libraries/armem_human/aron/Profile.aron.generated.h>


namespace armarx::human
{

    void arondto::to_json(simox::json::json& j, const Profile& arondto)
    {
        j["firstName"] = arondto.firstName;
        j["lastName"] = arondto.lastName;
        j["names"] = arondto.names;
        j["gender"] = arondto.gender;
        j["handedness"] = arondto.handedness;
        {
            DateTime birthday;
            birthday = arondto.birthday;
            j["birthday"] = birthday;
        }
        j["heightInMillimeters"] = arondto.heightInMillimeters;
        j["weightInGrams"] = arondto.weightInGrams;
        {
            simox::Color favouriteColor;
            simox::fromAron(arondto.favouriteColor, favouriteColor);
            j["favouriteColor"] = favouriteColor;
        }

        j["roles"] = arondto.roles;

        if (arondto.attributes)
        {
            j["attributes"] = aron::converter::AronNlohmannJSONConverter::ConvertToNlohmannJSON(arondto.attributes);
        }
        else
        {
            j["attributes"] = simox::json::json::object();
        }
    }


    void arondto::from_json(const simox::json::json& j, Profile& arondto)
    {
        using namespace simox::json;

        get_to_if_exists(j, "firstName", arondto.firstName);
        get_to_if_exists(j, "lastName", arondto.lastName);

        get_to_if_exists(j, "names", arondto.names);

        get_to_if_exists(j, "gender", arondto.gender, Gender::DIVERSE);
        get_to_if_exists(j, "handedness", arondto.handedness, Handedness::BOTH);
        {
            DateTime birthday;
            get_to_if_exists(j, "birthday", birthday, DateTime::Invalid());
            arondto.birthday = birthday;
        }
        get_to_if_exists(j, "heightInMillimeters", arondto.heightInMillimeters, -1);
        get_to_if_exists(j, "weightInGrams", arondto.weightInGrams, -1);

        {
            simox::Color favouriteColor;
            get_to_if_exists(j, "favouriteColor", favouriteColor, simox::Color::gray());
            simox::toAron(arondto.favouriteColor, favouriteColor);
        }

        get_to_if_exists(j, "roles", arondto.roles);

        if (j.count("attributes"))
        {
            aron::data::VariantPtr data = nullptr;
            aron::converter::AronNlohmannJSONConverter::ConvertFromNlohmannJSON(data, j.at("attributes"));
            if (auto dict = std::dynamic_pointer_cast<aron::data::Dict>(data))
            {
                arondto.attributes = dict;
            }
            else
            {
                arondto.attributes = nullptr;
            }
        }
        else
        {
            arondto.attributes = nullptr;
        }
    }

    void arondto::to_json(simox::json::json& j, const Gender& arondto)
    {
        j = simox::alg::to_lower(arondto.toString());
    }

    void arondto::from_json(const simox::json::json& j, Gender& arondto)
    {
        arondto.fromString(simox::alg::to_upper(j.get<std::string>()));
    }

    void arondto::to_json(simox::json::json& j, const Handedness& arondto)
    {
        j = simox::alg::to_lower(arondto.toString());
    }

    void arondto::from_json(const simox::json::json& j, Handedness& arondto)
    {
        arondto.fromString(simox::alg::to_upper(j.get<std::string>()));
    }
}
