/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::armem_images_server
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <SimoxUtility/json/json.h>

#include "forward_declarations.h"


namespace armarx::human::arondto
{

    void to_json(simox::json::json& j, const Gender& arondto);
    void from_json(const simox::json::json& j, Gender& arondto);

    void to_json(simox::json::json& j, const Handedness& arondto);
    void from_json(const simox::json::json& j, Handedness& arondto);

    void to_json(simox::json::json& j, const Profile& arondto);
    void from_json(const simox::json::json& j, Profile& arondto);

}
