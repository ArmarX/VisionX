/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::armem_images_server
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "IdentificationSegment.h"

#include <ArmarXCore/core/application/properties/PropertyDefinitionContainer.h>

// #include <VisionX/libraries/armem_human/aron/BODY_25Pose.aron.generated.h>


namespace armarx::armem::server::human
{

    const std::string IdentificationSegment::CORE_SEGMENT_NAME = "Identification";


    IdentificationSegment::IdentificationSegment(armem::server::MemoryToIceAdapter& iceMemory) :
        Base(iceMemory, CORE_SEGMENT_NAME)
    {
    }


    void IdentificationSegment::defineProperties(armarx::PropertyDefinitionsPtr defs, const std::string& prefix)
    {
        Base::defineProperties(defs, prefix);
    }

}
