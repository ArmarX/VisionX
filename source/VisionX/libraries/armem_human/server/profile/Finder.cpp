#include "Finder.h"

#include <SimoxUtility/algorithm/string.h>
#include <SimoxUtility/filesystem/list_directory.h>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/system/ArmarXDataPath.h>
#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>

#include <VisionX/libraries/armem_human/aron/Profile.aron.generated.h>


namespace armarx::armem::server::human::profile
{
    namespace fs = std::filesystem;


    Finder::Finder(const std::string& packageName, const Finder::path& relativeDirectory) :
        packageName(packageName), relativeDirectory(relativeDirectory)
    {
        Logging::setTag("HumanProfileFinder");
    }


    void Finder::setPath(const std::string& path)
    {
        packageName = path;
        absPackageDataDir.clear();
    }


    std::string Finder::getPackageName() const
    {
        return packageName;
    }


    void Finder::init() const
    {
        if (absPackageDataDir.empty())
        {
            CMakePackageFinder packageFinder(packageName);
            absPackageDataDir = packageFinder.getDataDir();
            if (absPackageDataDir.empty())
            {
                ARMARX_WARNING << "Could not find package '" << packageName << "'.";
                // throw LocalException() << "Could not find package '" << packageName << "'.";
            }
            else
            {
                ARMARX_VERBOSE << "Objects root directory: " << _rootDirAbs();

                // Make sure this data path is available.
                armarx::ArmarXDataPath::addDataPaths(std::vector<std::string> {absPackageDataDir});
            }
        }
    }


    bool Finder::isDatasetDirValid(const path& path) const
    {
        return std::filesystem::is_directory(path);
    }


    std::optional<Info>
    Finder::find(const std::string& name) const
    {
        init();
        if (!_ready())
        {
            return std::nullopt;
        }
        path profileDir = absPackageDataDir / relativeDirectory / name;
        if (not fs::is_directory(profileDir))
        {
            return std::nullopt;
        }
        return Info(packageName, absPackageDataDir, relativeDirectory, name);
    }


    std::vector<Info>
    Finder::findAll(bool checkPaths) const
    {
        init();
        if (!_ready())
        {
            return {};
        }

        std::vector<Info> infos;

        ARMARX_INFO << "Scanning " << _rootDirAbs() << " for human profiles ...";

        const bool local = false;
        for (const path& profileDir : simox::fs::list_directory(_rootDirAbs(), local))
        {
            if (fs::is_directory(profileDir))
            {
                infos.emplace_back(packageName, absPackageDataDir, relativeDirectory, profileDir.filename());
            }
        }

        ARMARX_INFO << "Found " << infos.size() << " human profiles.";

        return infos;
    }


    Finder::path Finder::_rootDirAbs() const
    {
        return absPackageDataDir / packageName / relativeDirectory;
    }


    Finder::path Finder::_rootDirRel() const
    {
        return packageName;
    }


    bool Finder::_ready() const
    {
        return !absPackageDataDir.empty();
    }

}

