#pragma once

#include <filesystem>

#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/PackagePath.h>

#include <RobotAPI/libraries/armem_objects/types.h>

#include "Info.h"


namespace armarx::armem::server::human::profile
{
    /**
     * @brief Used to find human profiles in the Prior Knowledge Data repository [1].
     *
     * @see [1] https://gitlab.com/ArmarX/PriorKnowledgeData
     */
    class Finder : Logging
    {
    public:

        using path = std::filesystem::path;

        inline static const std::string DefaultPackageName = "PriorKnowledgeData";
        inline static const std::string DefaultRelativeDirectory = "humans/profiles";


    public:

        Finder(const std::string& packageName = DefaultPackageName,
               const path& relativeDirectory = DefaultRelativeDirectory);

        void setPath(const std::string& path);

        std::string getPackageName() const;

        std::optional<Info> find(const std::string& name) const;
        std::vector<Info> findAll(bool checkPaths = true) const;


    private:

        void init() const;
        bool isDatasetDirValid(const std::filesystem::path& path) const;

        path _rootDirAbs() const;
        path _rootDirRel() const;

        bool _ready() const;


    private:

        /// Name of package containing the object models (ArmarXObjects by default).
        mutable std::string packageName;

        /**
         * @brief Absolute path to data directory (e.g. "/.../repos/ArmarXObjects/data").
         * Empty if package could not be found.
         */
        mutable path absPackageDataDir;

        /// Path to the directory containing objects in the package's data directory.
        path relativeDirectory;

    };
}
