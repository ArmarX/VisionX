#include "Info.h"

#include <SimoxUtility/algorithm/string.h>
#include <SimoxUtility/filesystem/list_directory.h>
#include <SimoxUtility/json.h>
#include <SimoxUtility/shapes/AxisAlignedBoundingBox.h>
#include <SimoxUtility/shapes/OrientedBox.h>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/logging/Logging.h>

#include <RobotAPI/libraries/aron/common/aron_conversions.h>

#include <VisionX/libraries/armem_human/aron/Profile.aron.generated.h>
#include <VisionX/libraries/armem_human/json_conversions.h>


namespace armarx::armem::server::human::profile
{
    namespace fs = std::filesystem;


    Info::Info(const std::string& packageName,
               const path& absPackageDataDir,
               const path& relativePath,
               const std::string& id) :
        _packageName(packageName), _absPackageDataDir(absPackageDataDir),
        _relativePath(relativePath), _id(id)
    {
    }


    void Info::setLogError(bool enabled)
    {
        this->_logError = enabled;
    }


    std::string Info::package() const
    {
        return _packageName;
    }


    std::string Info::id() const
    {
        return _id;
    }


    Info::path Info::_relativeProfileDirectory() const
    {
        return _relativePath / _id;
    }


    PackagePath Info::file(const std::string& _extension, const std::string& suffix) const
    {
        std::string extension = _extension;
        if (extension.at(0) != '.')
        {
            extension = "." + extension;
        }
        std::string filename = _id + suffix + extension;

        PackagePath path(_packageName, _relativeProfileDirectory() / filename);
        return path;
    }


    PackagePath Info::sub_directory(const std::string& suffix) const
    {
        std::string name = _id + suffix;
        return PackagePath(_packageName, _relativeProfileDirectory() / name);
    }


    PackagePath Info::profileJson() const
    {
        return file(".json");
    }


    PackagePath Info::faceImageDir() const
    {
        return sub_directory("_faces-images");
    }


    std::vector<PackagePath> Info::faceImages(const std::vector<std::string>& extensions) const
    {
        const PackagePath directory = faceImageDir();

        std::vector<PackagePath> imageFiles;
        bool local = false;
        for (const fs::path& path : simox::fs::list_directory(directory.toSystemPath(), local))
        {
            for (const std::string& ext : extensions)
            {
                if (fs::is_regular_file(path) and simox::alg::ends_with(path.string(), ext))
                {
                    PackagePath imageFile(
                                _packageName,
                                _relativeProfileDirectory() / (_id + "_faces-images") / path.filename());
                    imageFiles.push_back(imageFile);
                }
            }
        }
        return imageFiles;
    }


    std::optional<Info::Profile>
    Info::loadProfile() const
    {
        nlohmann::json j;
        try
        {
            j = nlohmann::read_json(profileJson().toSystemPath());
        }
        catch (const std::exception& e)
        {
            if (_logError)
            {
                ARMARX_ERROR << e.what();
            }
            return std::nullopt;
        }

        Profile profile = j.get<Profile>();
        for (const PackagePath& imagePath : this->faceImages())
        {
            toAron(profile.faceImagePaths.emplace_back(), imagePath);
        }
        return profile;
    }


    bool Info::checkPaths() const
    {
        namespace fs = std::filesystem;
        bool result = true;

        if (!fs::is_regular_file(profileJson().toSystemPath()))
        {
            if (_logError)
            {
                ARMARX_WARNING << "Expected simox object file for object " << *this << ": " << profileJson().toSystemPath();
            }
            result = false;
        }

        return result;
    }

}

std::ostream&
armarx::armem::server::human::profile::operator<<(std::ostream& os, const Info& rhs)
{
    return os << rhs.id();
}
