#pragma once

#include <filesystem>
#include <optional>
#include <ostream>
#include <string>

#include <ArmarXCore/core/PackagePath.h>

#include <VisionX/libraries/armem_human/forward_declarations.h>


namespace armarx::armem::server::human::profile
{


    /**
     * @brief Accessor for human profile files.
     */
    class Info
    {
    public:
        using path = std::filesystem::path;
        using Profile = armarx::human::arondto::Profile;

    public:

        /**
         * @brief Info
         *
         * @param packageName The ArmarX package.
         * @param absPackageDataDir Absolute path to the package's data directory.
         * @param relativePath The path where human profiles are stored in the data directory.
         * @param id The human profile ID.
         */
        Info(const std::string& packageName,
             const path& absPackageDataDir,
             const path& relativePath,
             const std::string& id);

        virtual ~Info() = default;

        void setLogError(bool enabled);


        std::string package() const;
        std::string id() const;


        PackagePath file(const std::string& extension, const std::string& suffix = "") const;
        PackagePath sub_directory(const std::string& suffix) const;

        PackagePath profileJson() const;

        PackagePath faceImageDir() const;
        std::vector<PackagePath> faceImages(const std::vector<std::string>& extensions = {".png", ".jpg"}) const;


        std::optional<Profile> loadProfile() const;


        /**
         * @brief Checks the existence of expected files.
         * If a file is does not exist, emits a warning returns false.
         * @return True if all existing files are found, false otherwise.
         */
        virtual bool checkPaths() const;


    private:

        path _relativeProfileDirectory() const;


    private:

        std::string _packageName;
        path _absPackageDataDir;
        path _relativePath;

        std::string _id;

        bool _logError = true;

    };

    std::ostream& operator<<(std::ostream& os, const Info& rhs);


    inline bool operator==(const Info& lhs, const Info& rhs)
    {
        return lhs.id() == rhs.id();
    }
    inline bool operator!=(const Info& lhs, const Info& rhs)
    {
        return lhs.id() != rhs.id();
    }
    inline bool operator< (const Info& lhs, const Info& rhs)
    {
        return lhs.id() < rhs.id();
    }
    inline bool operator> (const Info& lhs, const Info& rhs)
    {
        return lhs.id() > rhs.id();
    }
    inline bool operator<=(const Info& lhs, const Info& rhs)
    {
        return lhs.id() <= rhs.id();
    }
    inline bool operator>=(const Info& lhs, const Info& rhs)
    {
        return lhs.id() >= rhs.id();
    }

}
