/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::armem_images_server
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "Segment.h"

#include <ArmarXCore/core/application/properties/PropertyDefinitionContainer.h>

#include <VisionX/libraries/armem_human/aron/Profile.aron.generated.h>

#include "Finder.h"

#include <RobotAPI/libraries/armem/server/MemoryToIceAdapter.h>


namespace fs = std::filesystem;


namespace armarx::armem::server::human::profile
{
    const std::string Segment::CORE_SEGMENT_NAME = "Profile";
    using Profile = armarx::human::arondto::Profile;


    Segment::Segment(armem::server::MemoryToIceAdapter& iceMemory) :
        Base(iceMemory, CORE_SEGMENT_NAME,
             armarx::human::arondto::Profile::ToAronType(), 64)
    {
    }


    Segment::Properties::PriorKnowledge::PriorKnowledge() :
        packageName(Finder::DefaultPackageName)
    {
    }


    void Segment::defineProperties(armarx::PropertyDefinitionsPtr defs, const std::string& prefix)
    {
        Base::defineProperties(defs, prefix);

        defs->optional(properties.priorKnowledge.load, prefix + "pk.load",
                       "Load profiles from prior knowledge on startup.");
        defs->optional(properties.priorKnowledge.packageName, prefix + "pk.packageName",
                       "ArmarX package to load human profiles from.");
    }


    void Segment::init()
    {
        Base::init();

        if (properties.priorKnowledge.load)
        {
            loadPriorKnowledge(properties.priorKnowledge.packageName);
        }
    }


    void Segment::loadPriorKnowledge()
    {
        loadPriorKnowledge(Finder::DefaultPackageName);
    }


    void Segment::loadPriorKnowledge(const std::string& dataPackageName)
    {
        const armem::Time now = armem::Time::Now();

        Finder finder(dataPackageName);
        const std::vector<Info> infos = finder.findAll();

        ARMARX_INFO << "Loading " << infos.size() << " human profiles ...";
        armem::Commit commit;
        for (const Info& info : infos)
        {
            if (std::optional<Profile> profile = info.loadProfile())
            {
                armem::EntityUpdate& update = commit.add();
                update.entityID = getCoreSegment().id()
                        .withProviderSegmentName(dataPackageName)
                        .withEntityName(info.id());
                update.timeCreated = update.timeSent = update.timeArrived = now;
                update.instancesData = { profile->toAron() };
            }
        }

        iceMemory.commit(commit);
    }

}
