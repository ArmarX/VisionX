#pragma once

#include <Eigen/Core>
#include <SimoxUtility/color/Color.h>
#include <map>

namespace armarx::armem::human {

struct Keypoint2D {
  std::string label;

  Eigen::Vector2f position; // which frame?

  float confidence;

  simox::color::Color dominantColor;
};

struct Keypoint3D {
  std::string label;

  Eigen::Vector3f positionRobot; 
  Eigen::Vector3f positionGlobal;

  float confidence;

  simox::color::Color dominantColor;
};

using Keypoint2DIdMap = std::map<std::string, Keypoint2D>;
using Keypoint3DIdMap = std::map<std::string, Keypoint3D>;

struct HumanPose {
  Keypoint2DIdMap keypoint2dMap;
  Keypoint3DIdMap keypoint3dMap;
};

} // namespace armarx::armem::human
