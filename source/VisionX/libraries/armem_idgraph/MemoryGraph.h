/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::MemoryGrapher
 * @author     [Author Name] ( [Author Email] )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <SimoxUtility/color/Color.h>

#include <SemanticObjectRelations/RelationGraph/RelationGraph.h>

#include <RobotAPI/libraries/armem/core/MemoryID.h>


namespace armarx::armem::id_graph
{
    struct MemoryVertex : public semrel::ShapeVertex
    {
        MemoryID memoryID;
        int depth;
        simox::Color fillColor;
    };

    struct MemoryEdge
    {
        std::string referenceKey;
        bool isNowhereLink;
    };

    struct MemoryGraphAttributes
    {
        MemoryID initialMemoryID;
    };

    using MemoryGraph = semrel::RelationGraph<MemoryVertex, MemoryEdge, MemoryGraphAttributes>;

} // namespace armarx::armem::id_graph
