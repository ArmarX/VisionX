/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::components::MemoryGrapher
 * @author     [Author Name] ( [Author Email] )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <map>
#include <optional>

#include <RobotAPI/libraries/armem/client/MemoryNameSystem.h>
#include <RobotAPI/libraries/armem/client/query.h>
#include <RobotAPI/libraries/armem/core/MemoryID.h>
#include <RobotAPI/libraries/aron/core/data/variant/forward_declarations.h>
#include <RobotAPI/libraries/aron/core/type/variant/forward_declarations.h>

#include "MemoryGraph.h"

namespace armarx::armem::id_graph
{
    class MemoryIDResolver
    {
    public:
        MemoryIDResolver(armarx::armem::client::MemoryNameSystem& mns);

        MemoryGraph resolveToGraph(const armarx::armem::MemoryID& memoryID);

    private:
        std::optional<armarx::armem::wm::Memory> resolveID(const armarx::armem::MemoryID& memoryID);

        static std::optional<std::pair<armarx::aron::data::DictPtr, armarx::aron::type::ObjectPtr>>
        extractObjectAndType(const armarx::armem::wm::Memory& memory,
                             const armarx::armem::MemoryID& memoryID);

        static std::map<std::string, armarx::armem::MemoryID>
        getMemoryIDs(const armarx::aron::data::DictPtr& obj,
                     const armarx::aron::type::ObjectPtr& type);

        armarx::armem::client::MemoryNameSystem& mns;
    };
} // namespace armarx::armem::id_graph
