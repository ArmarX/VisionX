/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::components::MemoryGrapher
 * @author     [Author Name] ( [Author Email] )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "json_conversions.h"

#include <SimoxUtility/algorithm/string/string_tools.h>
#include <SimoxUtility/json.h>

#include <RobotAPI/libraries/armem/core/json_conversions.h>


namespace armarx::armem
{

    static const char* nowhereText = "[Nowhere]";


    void
    id_graph::to_json(nlohmann::json& json, const MemoryVertex& vertex)
    {
        json["memoryID"] = vertex.memoryID;
        json["memoryID.str()"] = vertex.memoryID.str();
        json["depth"] = vertex.depth;

        if (not vertex.memoryID.hasMemoryName())
        {
            json["style"]["label"] = nowhereText;
            json["style"]["fill-color"] = simox::Color::gray().to_vector4i();
        }
        else
        {
            std::vector<std::string> items = vertex.memoryID.getItems();
            std::string del = "/";
            if (items.size() > 3)
            {
                del = "/\n";
            }
            json["style"]["label"] = simox::alg::join(items, del);
            json["style"]["fill-color"] = vertex.fillColor.to_vector4i();
        }
    }


    void
    id_graph::from_json(const nlohmann::json& json, MemoryVertex& vertex)
    {
        json.at("memoryID").get_to(vertex.memoryID);
        json.at("depth").get_to(vertex.depth);
    }


    void
    id_graph::to_json(nlohmann::json& json, const MemoryEdge& edge)
    {
        json["key"] = edge.referenceKey;
        json["isNowhereLink"] = edge.isNowhereLink;

        json["style"]["label"] = edge.referenceKey;

        if (edge.isNowhereLink)
        {
            json["style"]["color"] = simox::Color::kit_red().to_vector4i();
        }
    }


    void
    id_graph::from_json(const nlohmann::json& json, MemoryEdge& edge)
    {
        json.at("key").get_to(edge.referenceKey);
        json.at("isNowhereLink").get_to(edge.isNowhereLink);
    }


    void
    id_graph::to_json(nlohmann::json& json, const MemoryGraphAttributes& graph)
    {
        json["initialMemoryID"] = graph.initialMemoryID;
    }


    void
    id_graph::from_json(const nlohmann::json& json, MemoryGraphAttributes& graph)
    {
        json.at("initialMemoryID").get_to(graph.initialMemoryID);
    }
} // namespace armarx::armem::id_graph
