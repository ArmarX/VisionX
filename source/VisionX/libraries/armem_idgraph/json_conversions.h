/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::components::MemoryGrapher
 * @author     [Author Name] ( [Author Email] )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <SimoxUtility/json/json.hpp>

#include "MemoryGraph.h"


namespace armarx::armem::id_graph
{

    void to_json(nlohmann::json& j, const MemoryVertex& vertex);
    void from_json(const nlohmann::json& j, MemoryVertex& vertex);

    void to_json(nlohmann::json& j, const MemoryEdge& edge);
    void from_json(const nlohmann::json& j, MemoryEdge& edge);

    void to_json(nlohmann::json& j, const MemoryGraphAttributes& graph);
    void from_json(const nlohmann::json& j, MemoryGraphAttributes& graph);

} // namespace armarx::armem::id_graph
