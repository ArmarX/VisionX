#pragma once

#include <memory>
#include <vector>

#include <Eigen/Core>

#include <RobotAPI/libraries/armem/core/MemoryID.h>
#include <RobotAPI/libraries/armem/core/wm/memory_definitions.h>
#include <RobotAPI/libraries/armem/client/Reader.h>

#include <VisionX/libraries/armem_images_core/Image.h>
#include <VisionX/libraries/armem_images_core/detail/ImageAdapter.h>


namespace visionx::armem_images
{

    class ArMemToImage : public detail::ImageAdapter
    {
    public:

        ArMemToImage();

        void addImagesRGB(const armarx::armem::MemoryID& entityID, const std::vector<size_t>& imageIndices) override;
        void addImagesDepth(const armarx::armem::MemoryID& entityID, const std::vector<size_t>& imageIndices) override;

        armarx::armem::MemoryID getMemoryID() const override;
        Eigen::Vector2i getImageDimensions() const;

        std::string summarizeStructure() const override;
        std::string printFormats() const;


        void fetchUpdates(const armarx::armem::MemoryID& entityID,
                          const std::vector<armarx::armem::MemoryID>& updatedSnapshotIDs);

        armarx::armem::client::QueryResult fetchLatest();


    private:

        template <class AronImageT>
        void addImages(const armarx::armem::MemoryID& entityID, const std::vector<size_t>& imageIndices);

        void updateAronImages(const armarx::armem::wm::Memory& memory);


    public:

        armarx::armem::client::Reader memoryReader;

        std::vector<std::unique_ptr<ImageBase>> images;
        armarx::armem::Time timestamp;
        bool updated;

    };

}
