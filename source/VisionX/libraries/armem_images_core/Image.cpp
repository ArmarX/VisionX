#include "Image.h"

#include <sstream>

#include <Image/ByteImage.h>

#include <ArmarXCore/core/time/TimeUtil.h>
#include <ArmarXCore/observers/variant/Variant.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/logging/Logging.h>

#include <RobotAPI/libraries/armem/client.h>
#include <RobotAPI/libraries/armem/client/query/Builder.h>
#include <RobotAPI/libraries/armem/client/query/query_fns.h>


namespace visionx::armem_images
{

    template class Image<visionx::armem_images::arondto::ImageRGB>;
    template class Image<visionx::armem_images::arondto::ImageDepth>;


    ImageBase::ImageBase(const armarx::armem::MemoryID& instanceID, size_t imageIndex) :
        instanceID(instanceID), imageIndex(imageIndex)
    {
    }


    ImageBase::~ImageBase()
    {
    }


    bool ImageBase::usePixels(CByteImage** inputImages)
    {
        return usePixels(inputImages[imageIndex]);
    }


    bool ImageBase::usePixels(void** inputPixelBuffers)
    {
        return usePixels(inputPixelBuffers[imageIndex]);
    }


    void ImageBase::addQuery(armarx::armem::client::query::Builder& queryBuilder) const
    {
        namespace qf = armarx::armem::client::query_fns;
        queryBuilder
        .coreSegments(qf::withID(instanceID)).providerSegments(qf::withID(instanceID))
        .entities(qf::withID(instanceID)).snapshots(qf::latest());
    }


    bool ImageBase::areSameSize(const CByteImage& lhs, const CByteImage& rhs)
    {
        return lhs.width == rhs.width && lhs.height == rhs.height;
    }


    bool ImageBase::areSameSize(const cv::Mat& lhs, const cv::Mat& rhs)
    {
        return lhs.cols == rhs.cols && lhs.rows == rhs.rows;
    }


    bool ImageBase::areSameSize(const cv::Mat& lhs, const CByteImage& rhs)
    {
        return lhs.cols == rhs.width && lhs.rows == rhs.height;
    }


    bool ImageBase::areSameSize(const CByteImage& lhs, const cv::Mat& rhs)
    {
        return areSameSize(rhs, lhs);
    }


    void detail::checkNotNull(void* ptr, const char* what)
    {
        ARMARX_CHECK_NOT_NULL(ptr) << what;
    }

}
