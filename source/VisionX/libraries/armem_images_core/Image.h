#pragma once

#include <VisionX/libraries/armem_images_core/aron/ImageRGB.aron.generated.h>
#include <VisionX/libraries/armem_images_core/aron/ImageDepth.aron.generated.h>

#include <VisionX/tools/OpenCVUtil.h>

#include <RobotAPI/libraries/armem/core/MemoryID.h>
#include <RobotAPI/libraries/armem/core/wm/memory_definitions.h>

// IVT
#include <Image/ByteImage.h>


namespace armarx::armem::client::query
{
    class Builder;
}

namespace visionx::armem_images
{
namespace detail
{
    void checkNotNull(void* ptr, const char* what);
}


    /**
     * @brief A class mediating between the ImageProvider/Processor APIs
     * (based on IVT CByteImages or raw pixel buffers) and the ARON image API
     * (based on cv::Mat).
     *
     * This is the non-template base class of `Image`.
     */
    class ImageBase
    {
    public:

        ImageBase(const armarx::armem::MemoryID& instanceID, size_t imageIndex);
        virtual ~ImageBase();


        virtual void resetImage(int rows, int cols) = 0;

        virtual cv::Mat& getImage() = 0;
        virtual const cv::Mat& getImage() const = 0;

        virtual std::string printFormat() const = 0;


        // IMAGE PROCESSOR TO MEMORY

        virtual bool usePixels(CByteImage** inputImages);
        virtual bool usePixels(CByteImage* inputImage) = 0;
        virtual bool usePixels(void** inputPixelBuffers);
        virtual bool usePixels(void* inputPixelBuffers) = 0;

        virtual armarx::aron::data::DictPtr toAron() const = 0;


        // MEMORY TO IMAGE PROVIDER
        void addQuery(armarx::armem::client::query::Builder& queryBuilder) const;

        virtual bool updateAronImage(const armarx::armem::wm::Memory& memory) = 0;
        virtual CByteImage toCByteImage() const = 0;


        static bool areSameSize(const CByteImage& lhs, const CByteImage& rhs);
        static bool areSameSize(const cv::Mat& lhs, const cv::Mat& rhs);
        static bool areSameSize(const cv::Mat& lhs, const CByteImage& rhs);
        static bool areSameSize(const CByteImage& lhs, const cv::Mat& rhs);


    public:

        armarx::armem::MemoryID instanceID;
        size_t imageIndex = 0;

        bool updated = false;

    };



    /**
     * @brief A class template implementing the interface defined by
     * `ImageBase` for a specific aron-generated class.
     *
     * It is assumed that `AronImageT` has an `image` member of the type
     * `cv::Mat`, which is generated when using the `Image` tag in ARON XML.
     */
    template <class AronImageT>
    class Image : public ImageBase
    {
    public:

        Image(const armarx::armem::MemoryID& instanceID, size_t imageIndex) :
            ImageBase(instanceID, imageIndex)
        {
        }


        cv::Mat& getImage() override
        {
            return aronImage.image;
        }
        const cv::Mat& getImage() const override
        {
            return aronImage.image;
        }


        std::string printFormat() const override
        {
            std::stringstream ss;
            ss << aronImage.image.cols << "x" << aronImage.image.rows << " pixels "
               << " x " << aronImage.image.channels() << " channels/pixel";
            return ss.str();
        }


        /// Initialize the image matrix header with the correct size (and type) but
        /// without allocating data.
        void resetImage(int rows, int cols) override
        {
            cv::Mat& mat = aronImage.image;
            switch (mat.type())
            {
            case CV_8UC3:  // Rgb24
                /* The matrix will be reset with an external pixel buffer.
                 * We still need to provide a buffer for the moment, but nullptr
                 * is not allowed. In any case, accessing it would be illegal,
                 * and detecting whether or not the matrix owns memory is not
                 * done by checking the passed buffer pointer (but by the
                 * mat's `u` member).
                 */
                mat = cv::Mat(rows, cols, mat.type(),
                              reinterpret_cast<void*>(1));  // nullptr is not allowed
                break;

            case CV_32FC1:  // Depth32
                // We will convert the data, so the mat needs its own buffer.
                mat.create(rows, cols, mat.type());
                break;
            }
        }


        bool usePixels(CByteImage* inputImage) override
        {
            if (inputImage == nullptr)
            {
                return false;
            }
            detail::checkNotNull(inputImage, "Image::usePixels(CByteImage* inputImage)");
            {
                cv::Mat& mat = aronImage.image;
                switch (mat.type())
                {
                case CV_8UC3:  // Rgb24
                    mat = cv::Mat(inputImage->height, inputImage->width, mat.type(), inputImage->pixels);
                    break;

                case CV_32FC1:  // Depth32
                    visionx::convertWeirdArmarXToDepthInMeters(inputImage->pixels, mat);
                    break;
                }
            }
            return true;
        }


        bool usePixels(void* inputPixelBuffer) override
        {
            if (inputPixelBuffer == nullptr)
            {
                return false;
            }
            detail::checkNotNull(inputPixelBuffer, "Image::usePixels(void* inputPixelBuffer)");
            {
                cv::Mat& mat = aronImage.image;
                switch (mat.type())
                {
                case CV_8UC3:  // Rgb24
                    mat = cv::Mat(mat.rows, mat.cols, mat.type(), inputPixelBuffer);
                    break;

                case CV_32FC1:  // Depth32
                    visionx::convertWeirdArmarXToDepthInMeters(reinterpret_cast<uint8_t*>(inputPixelBuffer), mat);
                    break;
                }
            }
            return true;
        }


        armarx::aron::data::DictPtr toAron() const override
        {
            return aronImage.toAron();
        }


        bool updateAronImage(const armarx::armem::wm::Memory& memory) override
        {
            if (const armarx::armem::wm::EntitySnapshot* snapshot = memory.findLatestSnapshot(instanceID))
            {
                if (snapshot->time() > instanceID.timestamp)
                {
                    if (const armarx::armem::wm::EntityInstance* instance = snapshot->findInstance(instanceID.instanceIndex))
                    {
                        instanceID.timestamp = snapshot->time();
                        aronImage.fromAron(instance->data());
                        updated = true;
                    }
                }
            }
            return updated;
        }


        CByteImage toCByteImage() const override
        {
            bool headerOnly = true;
            const cv::Mat& mat = aronImage.image;
            CByteImage img;
            switch (mat.type())
            {
            case CV_8UC3:  // Rgb24
                img = CByteImage (mat.cols, mat.rows, CByteImage::eRGB24, headerOnly);
                img.pixels = mat.data;
                break;

            case CV_32FC1:  // Depth32
                img = CByteImage(mat.cols, mat.rows, CByteImage::eRGB24);
                visionx::convertDepthInMetersToWeirdArmarX(mat, &img);
                break;
            }
            return img;
        }


    public:

        AronImageT aronImage;

    };


    extern template class Image<visionx::armem_images::arondto::ImageRGB>;
    extern template class Image<visionx::armem_images::arondto::ImageDepth>;

}
