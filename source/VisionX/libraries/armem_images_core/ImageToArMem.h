#pragma once

#include <VisionX/libraries/armem_images_core/Image.h>
#include <VisionX/libraries/armem_images_core/detail/ImageAdapter.h>

#include <RobotAPI/libraries/armem/core/MemoryID.h>
#include <RobotAPI/libraries/armem/core/Time.h>
#include <RobotAPI/libraries/armem/core/forward_declarations.h>
#include <RobotAPI/libraries/armem/client/Writer.h>

#include <memory>
#include <map>
#include <vector>


namespace visionx
{
    class ImageFormatInfo;
}
namespace visionx::armem_images
{

    /**
     * @brief Allows to convert multiple CByteImages to Aron images and
     * memory commits.
     */
    class ImageToArMem : public detail::ImageAdapter
    {
    public:

        ImageToArMem();

        void setWritingMemory(armarx::armem::server::WritingMemoryInterfacePrx memory);
        void setWriter(const armarx::armem::client::Writer& writer);

        void addImagesRGB(const armarx::armem::MemoryID& entityID, const std::vector<size_t>& imageIndices) override;
        void addImagesDepth(const armarx::armem::MemoryID& entityID, const std::vector<size_t>& imageIndices) override;

        armarx::armem::MemoryID getMemoryID() const override;
        std::string summarizeStructure() const override;

        void initImages(int width, int height, CByteImage::ImageType type);
        void initImages(const visionx::ImageFormatInfo& info);
        std::vector<CByteImage> makeCByteImageBuffer();


        /// Build inputs for adding the required provider segments.
        armarx::armem::data::AddSegmentsInput makeAddSegmentsInput(bool clearWhenExists = true);
        /// Add the required provider segments.
        armarx::armem::data::AddSegmentsResult addProviderSegments(bool clearWhenExists = true, int verbose = 1);


        /// Store image data from the given buffer.
        void useImageBuffers(CByteImage** inputImageBuffer, armarx::armem::Time timeProvided);
        void usePixelBuffers(void** inputImageBuffer, armarx::armem::Time timeProvided);

        /// Build the commit.
        armarx::armem::Commit makeCommit() const;
        /// Commit the stored image data.
        void commitImages();


    private:

        template <class AronImageT>
        void addImages(const armarx::armem::MemoryID& entityID, const std::vector<size_t>& imageIndices);

        void updateAronImages(const armarx::armem::wm::Memory& memory);


    public:

        armarx::armem::client::Writer memoryWriter;

        /// Entity ID to memory images (instances).
        std::map<armarx::armem::MemoryID, std::vector<std::unique_ptr<ImageBase>>> imagesByEntity;
        armarx::armem::Time timestamp;

    };

}
