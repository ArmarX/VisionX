#include "ImagesProperties.h"

#include <SimoxUtility/algorithm/string.h>

#include <ArmarXCore/core/application/properties/PropertyDefinitionContainer.h>
#include <ArmarXCore/core/application/properties/PropertyUser.h>


namespace visionx::armem_images
{

    void ImagesProperties::define(armarx::PropertyDefinitionContainer& defs)
    {
        defs.defineOptionalProperty("img.rgb.EntityID", rgbEntityID.str(),
                                    "The RGB image(s) entity ID. Provider segment name can be set via 'img.ProviderName'.");
        defs.defineOptionalProperty("img.rgb.ImageIndices", simox::alg::join(simox::alg::multi_to_string(rgbIndices)),
                                    "Indices of RGB images in provided images");

        defs.defineOptionalProperty("img.depth.EntityID", depthEntityID.str(),
                                    "The depth image(s) entity ID. Provider segment name can be set via 'img.ProviderName'.");
        defs.defineOptionalProperty("img.depth.ImageIndices", simox::alg::join(simox::alg::multi_to_string(depthIndices)),
                                    "Indices of Depth images in provided images.");
    }


    void ImagesProperties::read(armarx::PropertyUser& properties, const std::string& defaultProviderSegmentName)
    {
        // Parse properties.
        rgbEntityID = getEntityID(properties, "img.rgb.EntityID", defaultProviderSegmentName);
        depthEntityID = getEntityID(properties, "img.depth.EntityID", defaultProviderSegmentName);

        rgbIndices = getIndices(properties, "img.rgb.ImageIndices");
        depthIndices = getIndices(properties, "img.depth.ImageIndices");
    }


    armarx::armem::MemoryID
    ImagesProperties::getEntityID(armarx::PropertyUser& properties, const std::string& propertyName, const std::string& defaultProviderSegmentName)
    {
        armarx::armem::MemoryID id(properties.getProperty<std::string>(propertyName));
        if (!id.hasProviderSegmentName())
        {
            id.providerSegmentName = defaultProviderSegmentName;
        }
        return id;
    }


    std::vector<size_t> ImagesProperties::getIndices(armarx::PropertyUser& properties, const std::string& propertyName)
    {
        std::string prop = properties.getProperty<std::string>(propertyName);
        std::vector<size_t> indices;
        if (prop.empty())
        {
            return indices;
        }

        std::vector<std::string> indicesStr = simox::alg::split(prop, ",", true, true);
        std::transform(indicesStr.begin(), indicesStr.end(), std::back_inserter(indices), [](const std::string & s)
        {
            long l = std::stol(s);
            ARMARX_CHECK_NONNEGATIVE(l) << "Image indices must be non-negative.";
            return static_cast<size_t>(l);
        });
        return indices;
    }
}

