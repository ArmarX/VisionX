#pragma once

#include <string>
#include <vector>

#include <ArmarXCore/core/application/properties/forward_declarations.h>

#include <RobotAPI/libraries/armem/core/MemoryID.h>


namespace visionx::armem_images
{

    struct ImagesProperties
    {
    public:

        void define(armarx::PropertyDefinitionContainer& defs);
        void read(armarx::PropertyUser& properties, const std::string& defaultProviderSegmentName);


        static armarx::armem::MemoryID
        getEntityID(armarx::PropertyUser& properties, const std::string& propertyName, const std::string& defaultProviderSegmentName);
        static std::vector<size_t>
        getIndices(armarx::PropertyUser& properties, const std::string& propertyName);


    public:

        armarx::armem::MemoryID rgbEntityID { "Vision/ImageRGB//images" };
        armarx::armem::MemoryID depthEntityID { "Vision/ImageDepth//images" };

        std::vector<size_t> rgbIndices { 0 };
        std::vector<size_t> depthIndices { 1 };

    };


}
