#include "ImageAdapter.h"


namespace visionx::armem_images::detail
{

    ImageAdapter::ImageAdapter(const std::string& debugObserverChannelName) :
        debugObserverChannelName(debugObserverChannelName)
    {
    }


    ImageAdapter::~ImageAdapter()
    {
    }


    void ImageAdapter::setDebugObserver(armarx::DebugObserverInterfacePrx proxy)
    {
        debugObserver = proxy;
    }


    void ImageAdapter::setDebugObserver(armarx::DebugObserverInterfacePrx proxy, const std::string& channelName)
    {
        setDebugObserver(proxy);
        debugObserverChannelName = channelName;
    }

}
