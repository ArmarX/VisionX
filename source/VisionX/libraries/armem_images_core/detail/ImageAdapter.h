#pragma once

#include <string>

#include <ArmarXCore/interface/observers/ObserverInterface.h>

#include <RobotAPI/libraries/armem/core/MemoryID.h>


namespace visionx::armem_images::detail
{

    class ImageAdapter
    {
    public:

        ImageAdapter(const std::string& debugObserverChannelName = "ImageAdapter");
        virtual ~ImageAdapter();


        void setDebugObserver(armarx::DebugObserverInterfacePrx proxy);
        void setDebugObserver(armarx::DebugObserverInterfacePrx proxy, const std::string& channelName);

        virtual void addImagesRGB(const armarx::armem::MemoryID& entityID, const std::vector<size_t>& imageIndices) = 0;
        virtual void addImagesDepth(const armarx::armem::MemoryID& entityID, const std::vector<size_t>& imageIndices) = 0;

        virtual armarx::armem::MemoryID getMemoryID() const = 0;
        virtual std::string summarizeStructure() const = 0;


    protected:

        armarx::DebugObserverInterfacePrx debugObserver;
        std::string debugObserverChannelName;

    };

}
