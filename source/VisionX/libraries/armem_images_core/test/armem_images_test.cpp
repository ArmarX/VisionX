/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::ArMem
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE VisionX::ArmarXLibraries::armem_images

#define ARMARX_BOOST_TEST

#include <VisionX/Test.h>

#include <iostream>

#include <VisionX/libraries/armem_images_core/aron/ImageRGB.aron.generated.h>
#include <VisionX/libraries/armem_images_core/aron/ImageDepth.aron.generated.h>
#include <VisionX/libraries/armem_images_core/Image.h>

#include <opencv2/imgproc/imgproc.hpp>


namespace ImageTest
{
    struct Fixture
    {
        visionx::armem_images::arondto::ImageRGB imageIn;
        armarx::aron::data::DictPtr aron;
        visionx::armem_images::arondto::ImageRGB imageOut;

        Fixture()
        {
            // Should be initialized empty with the correct type.
            BOOST_CHECK_EQUAL(imageIn.image.cols, 0);
            BOOST_CHECK_EQUAL(imageIn.image.rows, 0);
            BOOST_CHECK_EQUAL(imageIn.image.type(), CV_8UC3);
        }
        ~Fixture()
        {
        }


        void fillImage(cv::Mat& image)
        {
            using Pixel = cv::Point3_<uint8_t>;
            image.forEach<Pixel>([](Pixel& pixel, const int index[])
            {
                pixel.x = static_cast<uint8_t>(index[0] % 256);
                pixel.y = static_cast<uint8_t>(index[1] % 256);
                pixel.z = static_cast<uint8_t>(index[0] + index[1] % 256);
            });
        }
        void fillImage(cv::Mat& image, int width, int height)
        {
            image.create(height, width, image.type());
            fillImage(image);
        }


        void checkImage(const CByteImage& image)
        {
            for (int i = 0; i < image.width * image.height * image.bytesPerPixel; ++i)
            {
                BOOST_TEST_CONTEXT("i = " << i)
                {
                    BOOST_CHECK_EQUAL(image.pixels[i], static_cast<unsigned char>(i));
                }
            }
        }

        void checkImage(const cv::Mat& image)
        {
            int i = 0;
            for (int r = 0; r < image.rows; ++r)
            {
                const unsigned char* row = image.ptr<unsigned char>(r);
                for (int c = 0; c < image.cols; ++c)
                {
                    BOOST_TEST_CONTEXT("i = " << i << " | (r, c) = (" << r << ", " << c << ")")
                    {
                        const unsigned char* col = &row[c];
                        for (int l = 0; l < 3; ++l)
                        {
                            BOOST_CHECK_EQUAL(col[l], static_cast<unsigned char>(i));
                            ++i;
                        }
                    }
                }
            }
        }

        void checkImage(CByteImage& image, CByteImage& in)
        {
            BOOST_CHECK_EQUAL(image.width, in.width);
            BOOST_CHECK_EQUAL(image.height, in.height);
            BOOST_CHECK_EQUAL(image.type, in.type);

            checkImage(image);
        }

        void checkImage(cv::Mat& image, cv::Mat& in)
        {
            BOOST_CHECK_EQUAL(image.cols, in.cols);
            BOOST_CHECK_EQUAL(image.rows, in.rows);
            BOOST_CHECK_EQUAL(image.channels(), in.channels());
            BOOST_CHECK_EQUAL(image.depth(), in.depth());
            BOOST_CHECK_EQUAL(image.type(), in.type());

            checkImage(image);
        }
    };

}

BOOST_FIXTURE_TEST_SUITE(ImageTest, ImageTest::Fixture)


BOOST_AUTO_TEST_CASE(test_ImagesRGB_default)
{
    BOOST_CHECK_EQUAL(imageIn.image.rows, 0);
    BOOST_CHECK_EQUAL(imageIn.image.cols, 0);
    BOOST_CHECK_EQUAL(imageIn.image.type(), CV_8UC3);
}


BOOST_AUTO_TEST_CASE(test_ImagesRGB_toAron_fromAron)
{
    fillImage(imageIn.image);
    aron = imageIn.toAron();
    imageOut.fromAron(aron);

    checkImage(imageOut.image, imageIn.image);
}


BOOST_AUTO_TEST_CASE(test_ImagesRGB_toAron_fromAron_modified_image_size)
{
    fillImage(imageIn.image, 200, 100);
    aron = imageIn.toAron();

    // This should not be necessary in the future:
    imageOut.image.create(100, 200, imageOut.image.type());
    imageOut.fromAron(aron);

    checkImage(imageOut.image, imageIn.image);
}


BOOST_AUTO_TEST_CASE(test_ImagesRGB_toAron_fromAron_modified_image_type)
{
    constexpr bool supported = false;
    if constexpr(supported)
    {
        imageIn.image.create(10, 20, CV_32FC1);
        fillImage(imageIn.image);
        aron = imageIn.toAron();
        imageOut.fromAron(aron);

        checkImage(imageOut.image, imageIn.image);
    }
    else
    {
        BOOST_CHECK(!supported);
    }
}

BOOST_AUTO_TEST_SUITE_END()
