/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    VisionX::interface::component
* @author     Markus Grotz <markus dot grotz at kit dot edu>
* @copyright  2015 Humanoids Group, HIS, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/
#pragma once

#include <RobotAPI/interface/armem/server/ReadingMemoryInterface.ice>

#include <VisionX/interface/components/Calibration.ice>
#include <VisionX/interface/core/ImageProviderInterface.ice>


module visionx
{
    module armem_images
    {
        module server
        {

            interface ImageMemoryServer extends
                    visionx::StereoCalibrationCaptureProviderInterface,
                    armarx::armem::server::ReadingMemoryInterface
            {
            };

        };
    };
};
