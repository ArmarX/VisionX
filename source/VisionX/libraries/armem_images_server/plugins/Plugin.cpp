#include "Plugin.h"

#include <VisionX/libraries/armem_images_core/constants.h>
#include <VisionX/libraries/armem_images_core/ImagesProperties.h>
#include <VisionX/libraries/armem_images_core/ImageToArMem.h>
#include <VisionX/libraries/armem_images_core/aron/ImageRGB.aron.generated.h>
#include <VisionX/libraries/armem_images_core/aron/ImageDepth.aron.generated.h>

#include <VisionX/core/ImageProvider.h>

#include <RobotAPI/libraries/armem/server/wm/memory_definitions.h>
#include <RobotAPI/libraries/armem/server/MemoryToIceAdapter.h>


namespace armem = armarx::armem;
namespace wm = armem::server::wm;


namespace visionx::armem_images::server::plugins
{

    Plugin::Plugin(armarx::ManagedIceObject& parent, std::string prefix) :
        armarx::ComponentPlugin(parent, prefix)
        // , workingMemory(std::make_unique<wm::Memory>())
        // , iceAdapter(std::make_unique<armem::server::MemoryToIceAdapter>(workingMemory.get()))
        , properties(std::make_unique<Properties>())
        , imageToArMem(std::make_unique<visionx::armem_images::ImageToArMem>())
    {
        addPlugin(serverPlugin);
        addPluginDependency(serverPlugin);
        ARMARX_CHECK_NOT_NULL(serverPlugin);
    }


    Plugin::~Plugin()
    {
    }

    struct Plugin::Properties
    {
        std::vector<size_t> rgbIndices { 0 };
        std::vector<size_t> depthIndices { 1 };

        void define(armarx::PropertyDefinitionContainer& defs);
        void read(armarx::PropertyUser& properties);
    };


    void
    Plugin::Properties::define(armarx::PropertyDefinitionContainer& defs)
    {
        defs.defineOptionalProperty("img.rgb.ImageIndices", simox::alg::join(simox::alg::multi_to_string(rgbIndices)),
                                    "Indices of RGB images in provided images");
        defs.defineOptionalProperty("img.depth.ImageIndices", simox::alg::join(simox::alg::multi_to_string(depthIndices)),
                                    "Indices of RGB images in provided images");
    }


    void
    Plugin::Properties::read(armarx::PropertyUser& properties)
    {
        rgbIndices = ImagesProperties::getIndices(properties, "img.rgb.ImageIndices");
        depthIndices = ImagesProperties::getIndices(properties, "img.depth.ImageIndices");
    }


    void
    Plugin::postCreatePropertyDefinitions(armarx::PropertyDefinitionsPtr& defs)
    {
        properties->define(*defs);
    }


    void
    Plugin::preOnInitComponent()
    {
        armarx::Component& parent = this->parent<armarx::Component>();
        serverPlugin->workingMemory.name()  = parent.getName();
        properties->read(parent);
    }


    void
    Plugin::postOnInitComponent()
    {
        addRgbImagesEntity(properties->rgbIndices);
        addDepthImagesEntity(properties->depthIndices);
        ARMARX_INFO << "Memory image structure: \n" << imageToArMem->summarizeStructure();
    }


    void
    Plugin::postOnConnectComponent()
    {
        ImageProvider& imageProvider = this->parent<ImageProvider>();
        imageToArMem->initImages(imageProvider.getImageFormat());
    }


    wm::Entity&
    Plugin::addRgbImagesEntity(const std::vector<size_t>& imageIndices)
    {
        wm::CoreSegment& cs = serverPlugin->workingMemory.addCoreSegment(
                    visionx::armem_images::rgbImagesCoreSegmentID.coreSegmentName,
                    visionx::armem_images::arondto::ImageRGB::ToAronType());
        cs.setMaxHistorySize(128);

        wm::ProviderSegment& ps = cs.addProviderSegment(parent().getName());
        wm::Entity& entity = ps.addEntity("image");
        imageToArMem->addImagesRGB(entity.id(), imageIndices);
        return entity;
    }


    wm::Entity&
    Plugin::addDepthImagesEntity(const std::vector<size_t>& imageIndices)
    {
        wm::CoreSegment& cs = serverPlugin->workingMemory.addCoreSegment(
                    visionx::armem_images::depthImagesCoreSegmentID.coreSegmentName,
                    visionx::armem_images::arondto::ImageDepth::ToAronType());
        cs.setMaxHistorySize(128);

        wm::ProviderSegment& ps = cs.addProviderSegment(parent().getName());
        wm::Entity& entity = ps.addEntity("image");
        imageToArMem->addImagesDepth(entity.id(), imageIndices);
        return entity;
    }


    void
    Plugin::useImageBuffers(CByteImage** inputImages, armarx::armem::Time timeProvided)
    {
        imageToArMem->useImageBuffers(inputImages, timeProvided);
    }


    void
    Plugin::usePixelBuffers(void** inputPixelBuffers, armarx::armem::Time timeProvided)
    {
        // inputImageBuffer is not a CByteImage**, but directly points to the pixel data
        imageToArMem->usePixelBuffers(inputPixelBuffers, timeProvided);
    }


    void
    Plugin::commitImages()
    {
        armem::Commit commit = imageToArMem->makeCommit();
        serverPlugin->iceAdapter.commitLocking(commit);
    }


    void Plugin::commitImages(CByteImage** inputImages, armarx::armem::Time timeProvided)
    {
        useImageBuffers(inputImages, timeProvided);
        commitImages();
    }


    void Plugin::commitImages(void** inputPixelBuffers, armarx::armem::Time timeProvided)
    {
        usePixelBuffers(inputPixelBuffers, timeProvided);
        commitImages();
    }

}
