#pragma once

#include <VisionX/libraries/armem_images_core/forward_declarations.h>

#include <RobotAPI/libraries/armem/core/forward_declarations.h>
#include <RobotAPI/libraries/armem/client/forward_declarations.h>
#include <RobotAPI/libraries/armem/server/forward_declarations.h>
#include <RobotAPI/interface/armem/server/ReadingMemoryInterface.h>
#include <RobotAPI/libraries/armem/server/plugins/ReadOnlyPluginUser.h>

#include <RobotAPI/libraries/armem/server/plugins/Plugin.h>

#include <ArmarXCore/core/ComponentPlugin.h>

#include <memory>


class CByteImage;


namespace visionx::armem_images::server::plugins
{

    class Plugin :
        public armarx::ComponentPlugin
    {
    public:

        Plugin(armarx::ManagedIceObject& parent, std::string prefix);
        virtual ~Plugin() override;


        armarx::armem::server::wm::Entity&
        addRgbImagesEntity(const std::vector<size_t>& imageIndices);

        armarx::armem::server::wm::Entity&
        addDepthImagesEntity(const std::vector<size_t>& imageIndices);


        void useImageBuffers(CByteImage** inputImages, armarx::armem::Time timeProvided);
        void usePixelBuffers(void** inputPixelBuffers, armarx::armem::Time timeProvided);

        void commitImages();
        void commitImages(CByteImage** inputImages, armarx::armem::Time timeProvided);
        void commitImages(void** inputPixelBuffers, armarx::armem::Time timeProvided);



    protected:

        virtual void postCreatePropertyDefinitions(armarx::PropertyDefinitionsPtr& properties) override;
        virtual void preOnInitComponent() override;
        virtual void postOnInitComponent() override;
        virtual void postOnConnectComponent() override;


    private:

        struct Properties;
        std::unique_ptr<Properties> properties;

        std::unique_ptr<visionx::armem_images::ImageToArMem> imageToArMem;


        armarx::armem::server::Plugin* serverPlugin = nullptr;

    };

}

namespace visionx::armem_images::server
{
    using plugins::Plugin;
}
namespace visionx::armem_images
{
    using ServerPlugin = server::plugins::Plugin;
}
