#include "PluginUser.h"
#include "Plugin.h"

// #include <ArmarXCore/util/CPPUtility/trace.h>


namespace armem = armarx::armem;


namespace visionx::armem_images::server::plugins
{

    PluginUser::PluginUser()
    {
        addPlugin(plugin);
    }


    PluginUser::~PluginUser()
    {
    }


    Plugin& PluginUser::getImagesServerPlugin()
    {
        return *plugin;
    }


    const Plugin& PluginUser::getImagesServerPlugin() const
    {
        return *plugin;
    }

}
