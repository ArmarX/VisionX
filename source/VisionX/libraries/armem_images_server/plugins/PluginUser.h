#pragma once

#include <RobotAPI/libraries/armem/server/plugins/ReadOnlyPluginUser.h>


namespace visionx::armem_images::server::plugins
{
    class Plugin;


    class PluginUser :
        virtual public armarx::armem::server::ReadOnlyPluginUser
    {
    public:

        PluginUser();
        virtual ~PluginUser() override;


    protected:

        Plugin& getImagesServerPlugin();
        const Plugin& getImagesServerPlugin() const;


    private:

        Plugin* plugin = nullptr;

    };

}

namespace visionx::armem_images::server
{
    using plugins::PluginUser;
}
namespace visionx::armem_images
{
    using ServerPluginUser = server::plugins::PluginUser;
}
