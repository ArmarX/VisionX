/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    visionx::imrec
 * @author     Christian R. G. Dreher <christian.dreher@student.kit.edu>
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


// STD/STL
#include <filesystem>
#include <memory>

// OpenCV 2
#include <opencv2/core/core.hpp>

// IVT
#include <Image/ByteImage.h>


namespace visionx::imrec
{
    /**
     * Interface of a playback strategy
     */
    class AbstractPlaybackStrategy;

    /**
     * Convenience alias for an instance of any playback strategy
     */
    using Playback = std::shared_ptr<visionx::imrec::AbstractPlaybackStrategy>;
}


class visionx::imrec::AbstractPlaybackStrategy
{

public:

    /**
     * @brief Destructor
     */
    virtual ~AbstractPlaybackStrategy();

    /**
     * @brief Indicates whether the instance is configured to be able to play back
     * @return True if it is playing back, false otherwise
     */
    virtual bool isPlayingBack() const = 0;

    /**
     * @brief Gets the amount of frames per second of the recording
     * @return Amount of frames per second of the recording
     */
    virtual unsigned int getFps() const = 0;

    /**
     * @brief Gets the total amout of frames in the recording
     * @return Total amount of frames in the recording
     */
    virtual unsigned int getFrameCount() const = 0;

    /**
     * @brief Gets the height of a frame in pixel
     * @return Height of a frame in pixel
     */
    virtual unsigned int getFrameHeight() const = 0;

    /**
     * @brief Gets the width of a frame in pixel
     * @return Width of a frame in pixel
     */
    virtual unsigned int getFrameWidth() const = 0;

    /**
     * @brief Sets the frame from there the playback should resume afterwards (seek)
     * @param frame Frame from where the playback should be resumed (e.g. "0" to replay from the beginning)
     */
    virtual void setCurrentFrame(unsigned int frame) = 0;

    /**
     * @brief Gets the current frame index of the playback
     * @return The current frame index
     */
    virtual unsigned int getCurrentFrame() const = 0;

    /**
     * @brief Indicates whether the recording has a consecutive frame
     * @return True, if there is a consecutive frame, false otherwise
     */
    virtual bool hasNextFrame() const = 0;

    /**
     * @brief Starts the playback
     * @param filePath Path to the recording to play back
     */
    virtual void startPlayback(const std::filesystem::path& filePath) = 0;

    /**
     * @brief Writes the next frame into an RGB buffer (one byte per channel => 3 byte per pixel)
     * @param buffer Output parameter where the frame data of the next frame should be written to
     * @return True on success, false otherwise (no consecutive frame, error while loading frame)
     */
    virtual bool getNextFrame(void* buffer) = 0;

    /**
     * @brief Writes the next frame into an IVT CByteImage buffer (RGB)
     * @param buffer Output parameter where the frame data of the next frame should be written to
     * @return True on success, false otherwise (no consecutive frame, error while loading frame)
     */
    virtual bool getNextFrame(::CByteImage& buffer) = 0;

    /**
     * @brief Writes the next frame into an OpenCV Mat buffer (BGR)
     * @param buffer Output parameter where the frame data of the next frame should be written to
     * @return True on success, false otherwise (no consecutive frame, error while loading frame)
     */
    virtual bool getNextFrame(cv::Mat& buffer) = 0;

    /**
     * @brief Stops the playback
     */
    virtual void stopPlayback() = 0;

};
