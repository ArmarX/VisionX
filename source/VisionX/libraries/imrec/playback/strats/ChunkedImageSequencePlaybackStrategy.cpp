/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    visionx::imrec
 * @author     Christian R. G. Dreher <christian.dreher@student.kit.edu>
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


// Fallback FPS to be used if the FPS are not to be found in the metadata.csv (Must not be '0'!)
#define FALLBACK_FPS 30


#include <VisionX/libraries/imrec/playback/strats/ChunkedImageSequencePlaybackStrategy.h>


// STD/STL
#include <cstring>
#include <filesystem>
#include <fstream>

// Boost
#include <boost/algorithm/string/predicate.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/tokenizer.hpp>

// OpenCV 2
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

// ArmarX
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/logging/Logging.h>


visionx::imrec::strats::ChunkedImageSequencePlaybackStrategy::ChunkedImageSequencePlaybackStrategy()
{
    this->playingBack = false;
    this->currentFrame = 0;
    this->frameCount = 0;
    this->metadataPath = "/dev/null";
}


visionx::imrec::strats::ChunkedImageSequencePlaybackStrategy::ChunkedImageSequencePlaybackStrategy(const std::filesystem::path& filePath)
{
    this->playingBack = false;
    this->currentFrame = 0;
    this->frameCount = 0;
    this->startPlayback(filePath);
}


visionx::imrec::strats::ChunkedImageSequencePlaybackStrategy::~ChunkedImageSequencePlaybackStrategy()
{
    this->stopPlayback();
}


bool
visionx::imrec::strats::ChunkedImageSequencePlaybackStrategy::isPlayingBack() const
{
    return this->playingBack;
}


unsigned int
visionx::imrec::strats::ChunkedImageSequencePlaybackStrategy::getFps() const
{
    return this->fps;
}


unsigned int
visionx::imrec::strats::ChunkedImageSequencePlaybackStrategy::getFrameCount() const
{
    return this->frameCount;
}


unsigned int
visionx::imrec::strats::ChunkedImageSequencePlaybackStrategy::getFrameHeight() const
{
    return this->frameHeight;
}


unsigned int
visionx::imrec::strats::ChunkedImageSequencePlaybackStrategy::getFrameWidth() const
{
    return this->frameWidth;
}


void
visionx::imrec::strats::ChunkedImageSequencePlaybackStrategy::setCurrentFrame(unsigned int frame)
{
    this->currentFrame = frame;
}


unsigned int
visionx::imrec::strats::ChunkedImageSequencePlaybackStrategy::getCurrentFrame() const
{
    return this->currentFrame;
}


bool
visionx::imrec::strats::ChunkedImageSequencePlaybackStrategy::hasNextFrame() const
{
    return this->currentFrame < this->frameCount;
}

static
std::string
getMetadataVersion(std::map<std::string, std::tuple<std::string, std::string>> metadata)
{
    if (auto it = metadata.find("metadata_version"); it != metadata.end())
    {
        ARMARX_CHECK_EQUAL(std::get<0>(it->second), "string");
        return std::get<1>(it->second);
    }
    else
    {
        return "1";
    }
}


void
visionx::imrec::strats::ChunkedImageSequencePlaybackStrategy::startPlayback(const std::filesystem::path& filePath)
{
    // Find canonical path to metadata.csv given the parameter
    if (std::filesystem::is_directory(filePath))
    {
        this->metadataPath = std::filesystem::canonical(filePath);
    }
    else
    {
        this->metadataPath = std::filesystem::canonical(filePath.parent_path());
    }

    // Init metadata member from metadata.csv
    this->initMetadata();

    // Init chunked image sequence recording related variables required to process the frames
    this->initFps();
    this->initExtension();
    this->initFramesPerChunk();
    this->initFrameCount();
    this->initFrameHeight();
    this->initFrameWidth();

    // Build path for each individual frame
    std::stringstream errors;
    for (unsigned int currentFrameNumber = 0; currentFrameNumber < this->frameCount; ++currentFrameNumber)
    {
        const unsigned int currentChunkNumber = currentFrameNumber / framesPerChunk;
        const std::filesystem::path currentChunk("chunk_" + std::to_string(currentChunkNumber));

        const std::string metadataVersion = getMetadataVersion(metadata);
        std::filesystem::path frameFile = "";
        if (metadataVersion == "1")
        {
            frameFile = std::filesystem::path("frame_" + std::to_string(currentFrameNumber));
        }
        else
        {
            const std::string frameName = "frame_" + std::to_string(currentFrameNumber);
            if (auto it = this->metadata.find(frameName); it != this->metadata.end())
            {
                frameFile = std::filesystem::path(std::get<1>(it->second));
            }
            else
            {
                errors << "\nFilename for frame " << currentFrameNumber << " in chunk " << currentChunkNumber << " is missing!";
            }
        }
        if (not frameFile.empty())
        {
            frameFile = frameFile.replace_extension(this->extension);
            const std::filesystem::path currentFramePath =
                std::filesystem::canonical(this->metadataPath / currentChunk / frameFile);

            this->framePaths.push_back(currentFramePath);
        }
    }
    if (not errors.str().empty())
    {
        ARMARX_ERROR << "The following errors occured when starting the playback: " << errors.str();
    }

    this->playingBack = true;
}


bool
visionx::imrec::strats::ChunkedImageSequencePlaybackStrategy::getNextFrame(void* buffer)
{
    // Return failure on invalid frame
    if (this->currentFrame >= this->frameCount)
    {
        return false;
    }

    // Load frame with OpenCV
    cv::Mat frame;
    this->getNextFrame(frame);

    // Convert to RGB and write data to buffer
    cv::cvtColor(frame, frame, cv::COLOR_BGR2RGB);
    std::memcpy(buffer, frame.data, this->frameHeight * this->frameWidth * static_cast<unsigned int>(frame.channels()));

    // Return success
    return true;
}


bool
visionx::imrec::strats::ChunkedImageSequencePlaybackStrategy::getNextFrame(::CByteImage& buffer)
{
    return this->getNextFrame(buffer.pixels);
}


bool
visionx::imrec::strats::ChunkedImageSequencePlaybackStrategy::getNextFrame(cv::Mat& buffer)
{
    const std::string path = this->framePaths[this->currentFrame++].string();
    buffer = cv::imread(path);

    // Return failure if OpenCV encountered an error
    if (buffer.data == nullptr)
    {
        return false;
    }

    // Check if the dimensions fit the expected image size
    if (static_cast<unsigned int>(buffer.size().height) != this->frameHeight
        or static_cast<unsigned int>(buffer.size().width) != this->frameWidth)
    {
        return false;
    }

    return true;
}


void
visionx::imrec::strats::ChunkedImageSequencePlaybackStrategy::stopPlayback()
{
    this->playingBack = false;
}


void
visionx::imrec::strats::ChunkedImageSequencePlaybackStrategy::initMetadata()
{
    // Read contents
    std::ifstream metadatacsv((this->metadataPath / "metadata.csv").string());
    std::string line;
    while (std::getline(metadatacsv, line))
    {
        boost::tokenizer<boost::escaped_list_separator<char>> tokenizer(line);
        std::vector<std::string> tokens(tokenizer.begin(), tokenizer.end());

        const std::string varName = tokens[0];
        const std::string varType = tokens[1];
        const std::string varValue = tokens[2];

        this->metadata[varName] = std::make_tuple(varType, varValue);
    }
}


void
visionx::imrec::strats::ChunkedImageSequencePlaybackStrategy::initFps()
{
    unsigned int candidate = 0;

    if (auto it = this->metadata.find("fps"); it != this->metadata.end())
    {
        candidate = this->fps = boost::lexical_cast<unsigned int>(std::get<1>(it->second));

        ARMARX_CHECK_GREATER(candidate, 0) << "FPS cannot be '0'";
    }
    else
    {
        // This is not worth a warning.
        ARMARX_INFO << "No 'fps' entry found in the metadata.csv. Assuming '" << FALLBACK_FPS << "' as default. "
                    << "FPS cannot be derived from an image sequence and are expected to be manifested in the metadata.csv file. "
                    << "If the assumed value does not reflect the actual FPS, update the 'fps' variable in the metadata.csv";

        candidate = FALLBACK_FPS; // unchecked, but must not be 0
        this->updateMetadata("fps", "unsigned int", std::to_string(candidate));
    }

    this->fps = candidate;
}


void
visionx::imrec::strats::ChunkedImageSequencePlaybackStrategy::initExtension()
{
    std::string candidate = "";

    if (auto it = this->metadata.find("extension"); it != this->metadata.end())
    {
        candidate = std::get<1>(it->second);

        ARMARX_CHECK_NOT_EQUAL(candidate, "") << "File extension cannot be empty";
    }
    else
    {
        ARMARX_WARNING << "No 'extension' entry found in the metadata.csv. Trying to derive the value now...";

        const std::filesystem::path probeChunk = this->metadataPath / "chunk_0";

        for (const std::filesystem::directory_entry& de : boost::make_iterator_range(std::filesystem::directory_iterator(probeChunk), {}))
        {
            const std::string currentStem = de.path().stem().string();
            const std::string currentExtension = de.path().extension().string();
            const std::string frameTemplate = "frame_";

            if (boost::algorithm::starts_with(currentStem, frameTemplate) and boost::algorithm::starts_with(currentExtension, ".") and currentExtension != ".")
            {
                candidate = currentExtension;
                break;
            }
        }

        ARMARX_CHECK_NOT_EQUAL(candidate, "") << "Could not determine the file extension. Update the metadata.csv file manually for the variable "
                                              "'extension' (type 'string') with the corresponding value";

        ARMARX_INFO << "Determined 'extension' to be '" << candidate << "'";

        this->updateMetadata("extension", "string", candidate);
    }

    this->extension = candidate;
}


void
visionx::imrec::strats::ChunkedImageSequencePlaybackStrategy::initFramesPerChunk()
{
    unsigned int candidate = 0;

    if (auto it = this->metadata.find("frames_per_chunk"); it != this->metadata.end())
    {
        candidate = boost::lexical_cast<unsigned int>(std::get<1>(it->second));

        ARMARX_CHECK_GREATER(candidate, 0) << "Amount of frames per chunk cannot be '0'";
    }
    else
    {
        ARMARX_WARNING << "No 'frames_per_chunk' entry found in the metadata.csv. Trying to derive the value now...";

        candidate = 0;
        std::filesystem::path file = std::filesystem::path("frame_0").replace_extension(this->extension);
        while (std::filesystem::exists(this->metadataPath / "chunk_0" / file))
        {
            file = std::filesystem::path("frame_" + std::to_string(++candidate)).replace_extension(this->extension);
        }

        ARMARX_CHECK_GREATER(candidate, 0) << "Could not determine the amount of frames per chunk. Update the metadata.csv file manually for the variable "
                                           "'frames_per_chunk' (type 'unsigned int') with the corresponding value.";

        ARMARX_INFO << "Determined 'frames_per_chunk' to be '" << candidate << "'";

        this->updateMetadata("frames_per_chunk", "unsigned int", std::to_string(candidate));
    }

    this->framesPerChunk = candidate;
}


void
visionx::imrec::strats::ChunkedImageSequencePlaybackStrategy::initFrameCount()
{
    unsigned int candidate = 0;

    if (auto it = this->metadata.find("frame_count"); it != this->metadata.end())
    {
        candidate = boost::lexical_cast<unsigned int>(std::get<1>(it->second));

        ARMARX_CHECK_GREATER(candidate, 0) << "Amount of frames cannot be '0'";
    }
    else
    {
        ARMARX_WARNING << "No 'frame_count' entry found in the metadata.csv. Trying to derive the value now...";

        candidate = 0;
        std::filesystem::path chunk = "chunk_0";
        unsigned int chunkNumber = 0;
        std::filesystem::path file = std::filesystem::path("frame_0").replace_extension(this->extension);
        while (std::filesystem::exists(this->metadataPath / chunk))
        {
            while (std::filesystem::exists(this->metadataPath / chunk / file))
            {
                file = std::filesystem::path("frame_" + std::to_string(++candidate)).replace_extension(this->extension);
            }

            chunk = "chunk_" + std::to_string(++chunkNumber);
        }

        ARMARX_CHECK_GREATER(candidate, 0) << "Could not determine the amount of frames. Update the metadata.csv file manually for the variable "
                                           "'frame_count' (type 'unsigned int') with the corresponding value";

        ARMARX_INFO << "Determined 'frame_count' to be '" << candidate << "'";

        this->updateMetadata("frame_count", "unsigned int", std::to_string(candidate));
    }

    this->frameCount = candidate;
}


void
visionx::imrec::strats::ChunkedImageSequencePlaybackStrategy::initFrameHeight()
{
    unsigned int candidate = 0;

    if (auto it = this->metadata.find("frame_height"); it != this->metadata.end())
    {
        candidate = boost::lexical_cast<unsigned int>(std::get<1>(it->second));

        ARMARX_CHECK_GREATER(candidate, 0) << "Frame height cannot be '0'";
    }
    else
    {
        ARMARX_WARNING << "No 'frame_height' entry found in the metadata.csv. Trying to derive the value now...";

        const std::filesystem::path chunk = "chunk_0";
        const std::filesystem::path frame = std::filesystem::path("frame_0").replace_extension(this->extension);
        const std::filesystem::path probe = this->metadataPath / chunk / frame;
        cv::Mat frameImage = cv::imread(probe.string());

        ARMARX_CHECK_NOT_NULL(frameImage.data) << "Failed loading first frame. Image file corrupted?";

        candidate = static_cast<unsigned int>(frameImage.size().height);

        ARMARX_CHECK_GREATER(candidate, 0) << "Could not determine the frame height. Update the metadata.csv file manually for the variable "
                                           "'frame_height' (type 'unsigned int') with the corresponding value";

        ARMARX_INFO << "Determined 'frame_height' to be '" << candidate << "'";

        this->updateMetadata("frame_height", "unsigned int", std::to_string(candidate));
    }

    this->frameHeight = candidate;
}


void
visionx::imrec::strats::ChunkedImageSequencePlaybackStrategy::initFrameWidth()
{
    unsigned int candidate = 0;

    if (auto it = this->metadata.find("frame_width"); it != this->metadata.end())
    {
        candidate = boost::lexical_cast<unsigned int>(std::get<1>(it->second));

        ARMARX_CHECK_GREATER(candidate, 0) << "Frame width cannot be '0'";
    }
    else
    {
        ARMARX_WARNING << "No 'frame_width' entry found in the metadata.csv. Trying to derive the value now...";

        const std::filesystem::path chunk = "chunk_0";
        const std::filesystem::path frame = std::filesystem::path("frame_0").replace_extension(this->extension);
        const std::filesystem::path probe = this->metadataPath / chunk / frame;
        cv::Mat frameImage = cv::imread(probe.string());

        ARMARX_CHECK_NOT_NULL(frameImage.data) << "Failed loading first frame. Image file corrupted?";

        candidate = static_cast<unsigned int>(frameImage.size().width);

        ARMARX_CHECK_GREATER(candidate, 0) << "Could not determine the frame width. Update the metadata.csv file manually for the variable "
                                           "'frame_width' (type 'unsigned int') with the corresponding value";

        ARMARX_INFO << "Determined 'frame_width' to be '" << candidate << "'";

        this->updateMetadata("frame_width", "unsigned int", std::to_string(candidate));
    }

    this->frameWidth = candidate;
}


void
visionx::imrec::strats::ChunkedImageSequencePlaybackStrategy::updateMetadata(
    const std::string& varName, const std::string& varType, const std::string& varValue)
{
    if (this->metadata.count(varName) == 0)
    {
        ARMARX_INFO << "Updating metadata.csv file with missing variable '" << varName << "' (type '" << varType << "') with value '" << varValue << "'";

        std::ofstream metadatacsv((this->metadataPath / "metadata.csv").string(), std::ios::out | std::ios::app);
        metadatacsv << varName << "," << varType << "," << varValue << "\n";
    }
}
