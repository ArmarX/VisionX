/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    visionx::imrec
 * @author     Christian R. G. Dreher <christian.dreher@student.kit.edu>
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


// STD/STL
#include <filesystem>
#include <map>
#include <string>
#include <tuple>
#include <vector>

// VisionX
#include <VisionX/libraries/imrec/playback/AbstractPlaybackStrategy.h>


namespace visionx::imrec::strats
{
    /**
     * Strategy to mimic a video playback using a chunked image sequence as recorded by the ImageMonitor
     */
    class ChunkedImageSequencePlaybackStrategy;
}



class visionx::imrec::strats::ChunkedImageSequencePlaybackStrategy :
    public visionx::imrec::AbstractPlaybackStrategy
{

private:

    /**
     * @brief Path to the metadata file (without /metadata.csv)
     */
    std::filesystem::path metadataPath;

    /**
     * @brief Data structure to map variable names to a tuple of their type and value
     */
    std::map<std::string, std::tuple<std::string, std::string>> metadata;

    /**
     * @brief Ordered list of paths to each individual frame
     */
    std::vector<std::filesystem::path> framePaths;

    /**
     * @brief File extension of the frames
     */
    std::filesystem::path extension;

    /**
     * @brief Index of the current frame
     */
    unsigned int currentFrame;

    /**
     * @brief Amount of frames in total
     */
    unsigned int frameCount;

    /**
     * @brief Amount of frames per chunk
     */
    unsigned int framesPerChunk;

    /**
     * @brief Height of a frame
     */
    unsigned int frameHeight;

    /**
     * @brief Width of a frame
     */
    unsigned int frameWidth;

    /**
     * @brief Frames per second of the recording
     */
    unsigned int fps;

    /**
     * @brief Flag to indicate whether the instance is initialised to be able to play back
     */
    unsigned int playingBack;

public:

    /**
     * @brief Default constructor to manually setup later
     */
    ChunkedImageSequencePlaybackStrategy();

    /**
     * @brief Constructor initialising the playback immediately
     * @param filePath Path to the recording
     */
    ChunkedImageSequencePlaybackStrategy(const std::filesystem::path& filePath);

    /**
     * @brief Destructor
     */
    virtual ~ChunkedImageSequencePlaybackStrategy() override;

    /**
     * @brief Indicates whether the instance is configured to be able to play back
     * @return True if it is playing back, false otherwise
     */
    virtual bool isPlayingBack() const override;

    /**
     * @brief Gets the amount of frames per second of the recording
     * @return Amount of frames per second of the recording
     */
    virtual unsigned int getFps() const override;

    /**
     * @brief Gets the total amout of frames in the recording
     * @return Total amount of frames in the recording
     */
    virtual unsigned int getFrameCount() const override;

    /**
     * @brief Gets the height of a frame in pixel
     * @return Height of a frame in pixel
     */
    virtual unsigned int getFrameHeight() const override;

    /**
     * @brief Gets the width of a frame in pixel
     * @return Width of a frame in pixel
     */
    virtual unsigned int getFrameWidth() const override;

    /**
     * @brief Sets the frame from there the playback should resume afterwards (seek)
     * @param frame Frame from where the playback should be resumed (e.g. "0" to replay from the beginning)
     */
    virtual void setCurrentFrame(unsigned int frame) override;

    /**
     * @brief Gets the current frame index of the playback
     * @return The current frame index
     */
    virtual unsigned int getCurrentFrame() const override;

    /**
     * @brief Indicates whether the recording has a consecutive frame
     * @return True, if there is a consecutive frame, false otherwise
     */
    virtual bool hasNextFrame() const override;

    /**
     * @brief Starts the playback
     * @param filePath Path to the recording to play back
     */
    virtual void startPlayback(const std::filesystem::path& filePath) override;

    /**
     * @brief Writes the next frame into a buffer of any form (RGB)
     * @param buffer Output parameter where the frame data of the next frame should be written to
     * @return True on success, false otherwise (no consecutive frame, error while loading frame)
     */
    virtual bool getNextFrame(void* buffer) override;

    /**
     * @brief Writes the next frame into an IVT CByteImage buffer (RGB)
     * @param buffer Output parameter where the frame data of the next frame should be written to
     * @return True on success, false otherwise (no consecutive frame, error while loading frame)
     */
    virtual bool getNextFrame(::CByteImage& buffer) override;

    /**
     * @brief Writes the next frame into an OpenCV Mat buffer (BGR)
     * @param buffer Output parameter where the frame data of the next frame should be written to
     * @return True on success, false otherwise (no consecutive frame, error while loading frame)
     */
    virtual bool getNextFrame(cv::Mat& buffer) override;

    /**
     * @brief Stops the playback
     */
    virtual void stopPlayback() override;

private:

    /**
     * @brief Initialises local datastructure representing the metadata.csv file
     */
    void initMetadata();

    /**
     * @brief Initialises the FPS from metadata file or uses a (guessed) fallback value if not present
     *
     * Metadata will be populated by the guessed value if it was not present
     */
    void initFps();

    /**
     * @brief Initialises the file extension from metadata file or tries to guess it by crawling the folders
     *
     * Metadata will be populated by the guessed value if it was not present
     */
    void initExtension();

    /**
     * @brief Initialises the frames per chunk from metadata file or tries to guess it by crawling the first folder
     *
     * Metadata will be populated by the guessed value if it was not present
     */
    void initFramesPerChunk();

    /**
     * @brief Initialises the frame count from metadata file or tries to guess it by crawling the folders
     *
     * Metadata will be populated by the guessed value if it was not present
     */
    void initFrameCount();

    /**
     * @brief Initialises the frame height from metadata file or guesses it by probing the first frame
     *
     * Metadata will be populated by the guessed value if it was not present
     */
    void initFrameHeight();

    /**
     * @brief Initialises the frame width from metadata file or guesses it by probing the first frame
     *
     * Metadata will be populated by the guessed value if it was not present
     */
    void initFrameWidth();

    /**
     * @brief Helper to populate the metadata file with missing values
     * @param varName Name of the variable
     * @param varType Type of the variable
     * @param varValue Value of the variable
     */
    void updateMetadata(const std::string& varName, const std::string& varType, const std::string& varValue);

};
