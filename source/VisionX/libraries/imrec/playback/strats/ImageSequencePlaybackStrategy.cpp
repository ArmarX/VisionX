/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    visionx::imrec
 * @author     Christian R. G. Dreher <christian.dreher@student.kit.edu>
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


// Default FPS to return
#define DEFAULT_FPS 30


#include <VisionX/libraries/imrec/playback/strats/ImageSequencePlaybackStrategy.h>


// STD/STL
#include <algorithm>
#include <filesystem>

// OpenCV
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

// ArmarX
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/logging/Logging.h>

#include <SimoxUtility/algorithm/string/string_tools.h>


visionx::imrec::strats::ImageSequencePlaybackStrategy::ImageSequencePlaybackStrategy()
{
    this->playingBack = false;
    this->currentFrame = 0;
    this->frameCount = 0;
}


visionx::imrec::strats::ImageSequencePlaybackStrategy::ImageSequencePlaybackStrategy(const std::filesystem::path& filePath)
{
    this->playingBack = false;
    this->currentFrame = 0;
    this->frameCount = 0;
    this->startPlayback(filePath);
}


visionx::imrec::strats::ImageSequencePlaybackStrategy::~ImageSequencePlaybackStrategy()
{
    this->stopPlayback();
}


bool
visionx::imrec::strats::ImageSequencePlaybackStrategy::isPlayingBack() const
{
    return this->playingBack;
}


unsigned int
visionx::imrec::strats::ImageSequencePlaybackStrategy::getFps() const
{
    ARMARX_WARNING << "FPS cannot be reconstructed from a bare image sequence. Using a pre-set default of " << DEFAULT_FPS << " FPS instead";

    return DEFAULT_FPS;
}


unsigned int
visionx::imrec::strats::ImageSequencePlaybackStrategy::getFrameCount() const
{
    return this->frameCount;
}


unsigned int
visionx::imrec::strats::ImageSequencePlaybackStrategy::getFrameHeight() const
{
    return this->frameHeight;
}


unsigned int
visionx::imrec::strats::ImageSequencePlaybackStrategy::getFrameWidth() const
{
    return this->frameWidth;
}


void
visionx::imrec::strats::ImageSequencePlaybackStrategy::setCurrentFrame(unsigned int frame)
{
    this->currentFrame = frame;
}


unsigned int
visionx::imrec::strats::ImageSequencePlaybackStrategy::getCurrentFrame() const
{
    return this->currentFrame;
}


bool
visionx::imrec::strats::ImageSequencePlaybackStrategy::hasNextFrame() const
{
    return this->currentFrame < this->frameCount;
}


void
visionx::imrec::strats::ImageSequencePlaybackStrategy::startPlayback(const std::filesystem::path& filePath)
{
    this->initBasePathPrefixSuffix(filePath);
    this->initFramePaths();
    this->initFrameDimensions();

    this->frameCount = static_cast<unsigned int>(this->framePaths.size());

    this->playingBack = true;
}


bool
visionx::imrec::strats::ImageSequencePlaybackStrategy::getNextFrame(void* buffer)
{
    // Return failure on invalid frame
    if (this->currentFrame >= this->frameCount)
    {
        return false;
    }

    // Load frame with OpenCV
    cv::Mat frame;
    if (!this->getNextFrame(frame))
    {
        return false;
    }

    // Convert to RGB and write data to buffer
    cv::cvtColor(frame, frame, cv::COLOR_BGR2RGB);
    std::memcpy(buffer, frame.data, this->frameHeight * this->frameWidth * static_cast<unsigned int>(frame.channels()));

    // Return success
    return true;
}


bool
visionx::imrec::strats::ImageSequencePlaybackStrategy::getNextFrame(::CByteImage& buffer)
{
    return this->getNextFrame(buffer.pixels);
}


bool
visionx::imrec::strats::ImageSequencePlaybackStrategy::getNextFrame(cv::Mat& buffer)
{
    buffer = cv::imread(this->framePaths[this->currentFrame++].string());

    // Return failure if OpenCV encountered an error
    if (buffer.data == nullptr)
    {
        return false;
    }

    // Check if the dimensions fit the expected image size
    if (static_cast<unsigned int>(buffer.size().height) != this->frameHeight
        or static_cast<unsigned int>(buffer.size().width) != this->frameWidth)
    {
        return false;
    }

    return true;
}


void
visionx::imrec::strats::ImageSequencePlaybackStrategy::stopPlayback()
{
    this->playingBack = false;
}


void
visionx::imrec::strats::ImageSequencePlaybackStrategy::initBasePathPrefixSuffix(const std::filesystem::path& filePath)
{
    // Define helper functions to find a common prefix or suffix between two strings respectively
    auto findCommonPrefix = [](const std::string & s1, const std::string & s2) -> std::string
    {
        std::string commonPrefix = "";

        for (unsigned int i = 0; i < s1.length() and i < s2.length(); ++i)
        {
            if (s1[i] == s2[i])
            {
                commonPrefix += s1[i];
            }
            else
            {
                break;
            }
        }

        return commonPrefix;
    };
    auto findCommonSuffix = [](const std::string & s1, const std::string & s2) -> std::string
    {
        std::string commonSuffix = "";

        for (unsigned int i = 1; i <= s1.length() and i <= s2.length(); ++i)
        {
            if (s1[s1.length() - i] == s2[s2.length() - i])
            {
                commonSuffix = s1[s1.length() - i] + commonSuffix;
            }
            else
            {
                break;
            }
        }

        return commonSuffix;
    };

    // Individual frames for the image sequence will be identified by a requried prefix and suffix, and they have to be in a common folder
    std::filesystem::path basePath;
    std::string requiredPrefix = "";
    std::string requiredSuffix = "";

    // If a directory was given, assume each contained file is a frame
    if (std::filesystem::is_directory(filePath))
    {
        // Set base path
        basePath = std::filesystem::canonical(filePath);

        // Initialise candidates for the common prefix and suffix
        std::string prefixCandidate = "";
        std::string suffixCandidate = "";
        bool candidatesInitialised = false;

        // Iterate over dictionary and narrow down the common prefix and suffix
        for (auto de = std::filesystem::directory_iterator(basePath); de != std::filesystem::directory_iterator{}; ++de)
        {
            const std::string currentFile = de->path().filename().string();

            // If prefixCandidate and suffixCandidate are uninitialised, use currentFile as pivot.
            // This is legit because we assume that each file in the folder is a frame if only a folder path was given as parameter
            if (!candidatesInitialised)
            {
                prefixCandidate = currentFile;
                suffixCandidate = currentFile;
                candidatesInitialised = true;
            }
            else
            {
                prefixCandidate = findCommonPrefix(prefixCandidate, currentFile);
                suffixCandidate = findCommonSuffix(suffixCandidate, currentFile);
            }
        }

        // Use candidates as requirements
        requiredPrefix = prefixCandidate;
        requiredSuffix = suffixCandidate;
    }
    // Otherwise examine given file path
    else
    {
        // Set base path
        basePath = std::filesystem::canonical(filePath.parent_path());

        // Cound wildcards
        std::string filename = filePath.filename().string();
        const unsigned int wildcardCount = static_cast<unsigned int>(std::count(filename.begin(), filename.end(), '*'));

        // Expect wildcard count to be 0 or 1
        ARMARX_CHECK_EXPRESSION(wildcardCount <= 1) << "Cannot parse more than one wildcard";

        // If there's one wildcard, parse the filename for a required prefix and suffix
        if (wildcardCount == 1)
        {
            requiredPrefix = filename.substr(0, filename.find('*'));
            requiredSuffix = filename.substr(filename.find('*') + 1, filename.length());
        }
        // If there's a file without wildcard specified, it is used as a base to find a common prefix and suffix
        else
        {
            // Set candidates to the set filename
            std::string prefixCandidate = filename;
            std::string suffixCandidate = filename;

            // Iterate over dictionary and narrow down the common prefix and suffix
            for (auto de = std::filesystem::directory_iterator(basePath); de != std::filesystem::directory_iterator{}; ++de)
            {
                const std::string currentFile = de->path().filename().string();
                prefixCandidate = findCommonPrefix(prefixCandidate, currentFile);
                suffixCandidate = findCommonSuffix(suffixCandidate, currentFile);
            }

            // Use candidates as requirements
            requiredPrefix = prefixCandidate;
            requiredSuffix = suffixCandidate;
        }
    }

    this->basePath = basePath;
    this->requiredPrefix = requiredPrefix;
    this->requiredSuffix = requiredSuffix;
}


void
visionx::imrec::strats::ImageSequencePlaybackStrategy::initFramePaths()
{
    // Define natural sort function
    unsigned long prefixLength = requiredPrefix.length();
    unsigned long suffixLength = requiredSuffix.length();
    auto naturalSortComparator = [prefixLength, suffixLength](const std::filesystem::path & a, const std::filesystem::path & b) -> bool
    {
        std::string as = a.filename().string();
        std::string bs = b.filename().string();
        unsigned long normalisation = 2; // => (x.length() - 1) - (suffixLength + 1)
        as = as.substr(prefixLength, as.length() - (suffixLength + normalisation));
        bs = bs.substr(prefixLength, bs.length() - (suffixLength + normalisation));

        // If the strings are equal in length, use default string comparison
        if (as.length() == bs.length())
        {
            return as < bs;
        }

        // Compare if lengths differ
        while (as.length() > 0 and bs.length() > 0)
        {
            unsigned int asi = 0;
            unsigned int bsi = 0;
            std::stringstream(as) >> asi;
            std::stringstream(bs) >> bsi;

            // If the first characters are equal and the interpreted integers are equal, remove that and retry comparison
            if (as[0] == bs[0] and asi == bsi)
            {
                unsigned int decimals = 1;

                while (asi /= 10)
                {
                    ++decimals;
                }

                as = as.substr(decimals, as.length());
                bs = bs.substr(decimals, bs.length());
            }
            // If both parts are an integer, compare the integers
            else if ((asi > 0 or (asi == 0 and as[0] == '0')) and (bsi > 0 or (bsi == 0 and bs[0] == '0')))
            {
                return asi < bsi;
            }
            // As a last resort, compare the individual characters
            else
            {
                return as[0] < bs[0];
            }
        }

        return as.length() < bs.length();
    };

    // Using the base path, the prefix, and the suffix, find all matching frames and save their path
    for (auto de = std::filesystem::directory_iterator(basePath); de != std::filesystem::directory_iterator{}; ++de)
    {
        const std::string currentFile = de->path().filename().string();

        // Save frame file path if requirements fit
        if (simox::alg::starts_with(currentFile, this->requiredPrefix) and simox::alg::ends_with(currentFile, this->requiredSuffix))
        {
            this->framePaths.push_back(de->path());
        }
    }

    // Sort the frames alphanumerically (naturally)
    std::sort(this->framePaths.begin(), this->framePaths.end(), naturalSortComparator);
}


void
visionx::imrec::strats::ImageSequencePlaybackStrategy::initFrameDimensions()
{
    const std::filesystem::path probe = this->framePaths.at(0);
    cv::Mat frameImage = cv::imread(probe.string());

    ARMARX_CHECK_EXPRESSION(frameImage.data != nullptr) << "Failed loading first frame. Image file corrupted?";

    this->frameHeight = static_cast<unsigned int>(frameImage.size().height);
    this->frameWidth = static_cast<unsigned int>(frameImage.size().width);
}
