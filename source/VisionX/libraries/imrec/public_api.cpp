/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    visionx::imrec
 * @author     Christian R. G. Dreher <christian.dreher@student.kit.edu>
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include <VisionX/libraries/imrec/public_api.h>


// STD/STL
#include <exception>
#include <thread>

// OpenCV
#include <opencv2/core/core.hpp>

// Simox
#include <SimoxUtility/algorithm.h>

// IVT
#include <Image/ByteImage.h>

// ArmarX
#include <VisionX/libraries/imrec/playback/strats/ChunkedImageSequencePlaybackStrategy.h>
#include <VisionX/libraries/imrec/playback/strats/ImageSequencePlaybackStrategy.h>
#include <VisionX/libraries/imrec/playback/strats/VideoPlaybackStrategy.h>
#include <VisionX/libraries/imrec/record/strats/AVIRecordingStrategy.h>
#include <VisionX/libraries/imrec/record/strats/BMPRecordingStrategy.h>
#include <VisionX/libraries/imrec/record/strats/JPGRecordingStrategy.h>
#include <VisionX/libraries/imrec/record/strats/PNGRecordingStrategy.h>
#include <VisionX/libraries/imrec/record/strats/PNGParallelRecordingStrategy.h>
#include <VisionX/libraries/imrec/record/strats/H264RecordingStrategy.h>


namespace
{
    static const std::map<visionx::imrec::Format, visionx::imrec::RegistryEntry> registry =
    {
        {
            visionx::imrec::Format::bmp_img_seq,
            {
                "bmp_img_seq",
                ".bmp (Bitmap image sequence)",
                visionx::imrec::Framerate::dynamic_fps,
                visionx::imrec::Compression::lossless
            }
        },
        {
            visionx::imrec::Format::png_img_seq,
            {
                "png_img_seq",
                ".png (PNG image sequence, high compression, single-threaded)",
                visionx::imrec::Framerate::dynamic_fps,
                visionx::imrec::Compression::lossless
            }
        },
        {
            visionx::imrec::Format::png_fast_img_seq,
            {
                "png_fast_img_seq",
                ".png (PNG image sequence, fast, single-threaded)",
                visionx::imrec::Framerate::dynamic_fps,
                visionx::imrec::Compression::lossless
            }
        },
        {
            visionx::imrec::Format::png_mt_img_seq,
            {
                "png_img_seq",
                ".png (PNG image sequence, high compression, multi-threaded)",
                visionx::imrec::Framerate::dynamic_fps,
                visionx::imrec::Compression::lossless
            }
        },
        {
            visionx::imrec::Format::png_fast_mt_img_seq,
            {
                "png_fast_img_seq",
                ".png (PNG image sequence, fast, multi-threaded)",
                visionx::imrec::Framerate::dynamic_fps,
                visionx::imrec::Compression::lossless
            }
        },
        {
            visionx::imrec::Format::jpg_img_seq,
            {
                "jpg_img_seq",
                ".jpg (JPG image sequence)",
                visionx::imrec::Framerate::dynamic_fps,
                visionx::imrec::Compression::lossy
            }
        },
        {
            visionx::imrec::Format::avi_default,
            {
                "avi_default",
                ".avi (AVI video)",
                visionx::imrec::Framerate::static_fps,
                visionx::imrec::Compression::lossy
            }
        },
        {
            visionx::imrec::Format::mp4_default,
            {
                "mp4_default",
                ".mp4 (MP4 video)",
                visionx::imrec::Framerate::static_fps,
                visionx::imrec::Compression::lossy
            }
        }
    };

    // Auto generated inverted lookup map below in this namespace.
    static const std::map<std::string, visionx::imrec::Format> registry_inv = []()
    {
        std::map<std::string, visionx::imrec::Format> m;
        for (const auto& [format, reg_entry] : ::registry)
        {
            m[reg_entry.id] = format;
            m[reg_entry.hr_name] = format;
        }
        return m;
    }();
}


const std::string&
visionx::imrec::format2str(const visionx::imrec::Format format)
{
    return ::registry.at(format).id;
}


const std::string&
visionx::imrec::format2hrstr(const visionx::imrec::Format format)
{
    return ::registry.at(format).hr_name;
}


visionx::imrec::Format
visionx::imrec::str2format(const std::string& format_str)
{
    if (::registry_inv.find(format_str) == ::registry_inv.end())
    {
        const std::string err = "Unknown format str '" + format_str + "' for imrec format.";
        throw std::runtime_error{err.c_str()};
    }

    return ::registry_inv.at(format_str);
}


std::vector<visionx::imrec::Format>
visionx::imrec::getFormats()
{
    std::vector<Format> keys;
    std::transform(::registry.begin(), ::registry.end(), std::back_inserter(keys), [](const auto & e)
    {
        return e.first;
    });
    return keys;
}


std::map<visionx::imrec::Format, visionx::imrec::RegistryEntry>
visionx::imrec::getFormatsMap()
{
    return ::registry;
}


bool
visionx::imrec::isFormatLossless(visionx::imrec::Format format)
{
    return ::registry.at(format).compression == Compression::lossless;
}


bool
visionx::imrec::isFormatDynamicFramerate(visionx::imrec::Format format)
{
    return ::registry.at(format).framerate == Framerate::dynamic_fps;
}


visionx::imrec::Playback
visionx::imrec::newPlayback(const std::filesystem::path& path)
{
    using namespace simox::alg;

    // String representation just for string operations, so to lower case
    const std::string pathstr = to_lower(path.string());

    // Checks against metadata.csv to identify a chunked image sequence as used by the ImageMonitor (individual image recordings)
    if (ends_with(pathstr, "metadata.csv") or (std::filesystem::is_directory(path) and std::filesystem::exists(path / "metadata.csv")))
    {
        return std::make_shared<strats::ChunkedImageSequencePlaybackStrategy>(path);
    }

    // If the path is an image file, contains a wildcard, or is a simple directory, try to make sense of the input when interpreted as image sequence
    const bool isImage = ends_with(pathstr, ".png") or ends_with(pathstr, ".jpg")
                         or ends_with(pathstr, ".jpeg") or ends_with(pathstr, ".bmp");
    if (isImage or contains(pathstr, "*") or std::filesystem::is_directory(path))
    {
        return std::make_shared<strats::ImageSequencePlaybackStrategy>(path);
    }

    // Common OpenCV-supported video file extensions
    if (ends_with(pathstr, ".avi") or ends_with(pathstr, ".mp4"))
    {
        return std::make_shared<strats::VideoPlaybackStrategy>(path);
    }

    return nullptr;
}


visionx::imrec::Recording
visionx::imrec::newRecording(const std::filesystem::path& path, const std::string& name, const visionx::imrec::Format format, const double fps)
{
    const unsigned int png_high_quality = 9;
    const unsigned int png_fast_quality = 1;
    const unsigned int jpg_default_quality = 95;
    const unsigned int png_thread_pool_size = std::thread::hardware_concurrency() / 2;

    switch (format)
    {
        case Format::bmp_img_seq:
            return std::make_shared<strats::BMPRecordingStrategy>(path, name);
        case Format::png_img_seq:
            return std::make_shared<strats::PNGRecordingStrategy>(path, name, png_high_quality);
        case Format::png_fast_img_seq:
            return std::make_shared<strats::PNGRecordingStrategy>(path, name, png_fast_quality);
        case Format::png_mt_img_seq:
            return std::make_shared<strats::PNGParallelRecordingStrategy>(path, name, png_high_quality, png_thread_pool_size);
        case Format::png_fast_mt_img_seq:
            return std::make_shared<strats::PNGParallelRecordingStrategy>(path, name, png_fast_quality, png_thread_pool_size);
        case Format::jpg_img_seq:
            return std::make_shared<strats::JPGRecordingStrategy>(path, name, jpg_default_quality);
        case Format::avi_default:
            return std::make_shared<strats::AVIRecordingStrategy>(path, name, fps);
        case Format::mp4_default:
            return std::make_shared<strats::H264RecordingStrategy>(path, name, fps);
    }

    return nullptr;
}


visionx::imrec::Recording
visionx::imrec::newRecording(const std::filesystem::path& path, double fps)
{
    using namespace simox::alg;

    const std::string ext = to_lower(path.extension().string());

    Format format;

    if (ext == ".bmp" or ext == "")  // Default (no extention => assume directory).
    {
        format = Format::bmp_img_seq;
    }
    else if (ext == ".png")
    {
        format = Format::png_img_seq;
    }
    else if (ext == ".jpg" or ext == ".jpeg")
    {
        format = Format::jpg_img_seq;
    }
    else if (ext == ".avi")
    {
        format = Format::avi_default;
    }
    else if (ext == ".mp4")
    {
        format = Format::mp4_default;
    }
    else
    {
        return nullptr;
    }

    return newRecording(path.parent_path(), path.stem().string(), format, fps);
}


void
visionx::imrec::takeSnapshot(const CByteImage& image, const std::filesystem::path& filePath)
{
    strats::PNGRecordingStrategy::recordSnapshot(image, filePath);
}


void
visionx::imrec::takeSnapshot(const cv::Mat& image, const std::filesystem::path& filePath)
{
    strats::PNGRecordingStrategy::recordSnapshot(image, filePath);
}
