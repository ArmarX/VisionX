/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    visionx::imrec
 * @author     Christian R. G. Dreher <christian.dreher@student.kit.edu>
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


// Metadata version to handle potential breaking changes in the future.
#define METADATA_VERSION "2.0"


#include <VisionX/libraries/imrec/record/AbstractRecordingStrategy.h>


// STD/STL
#include <filesystem>

// OpenCV 2
#include <opencv2/opencv.hpp>

// IVT
#include <Image/ByteImage.h>

// ArmarX
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/logging/Logging.h>
#include <VisionX/libraries/imrec/helper.h>


visionx::imrec::AbstractRecordingStrategy::AbstractRecordingStrategy() :
    m_recording{false}
{
    // pass
}


visionx::imrec::AbstractRecordingStrategy::AbstractRecordingStrategy(const std::filesystem::path& file_path) :
    m_file_path{file_path},
    m_recording{false}
{
    // pass
}


visionx::imrec::AbstractRecordingStrategy::~AbstractRecordingStrategy()
{
    if (m_recording)
    {
        stopRecording();
    }
}


bool
visionx::imrec::AbstractRecordingStrategy::isRecording() const
{
    return m_recording;
}


void
visionx::imrec::AbstractRecordingStrategy::startRecording()
{
    ARMARX_CHECK_EXPRESSION(not m_recording) << "Instance has already been initialised for recording";
    ARMARX_CHECK_EXPRESSION(not std::filesystem::exists(m_file_path)) << "File already exists";

    std::filesystem::create_directories(m_file_path.parent_path());

    m_recording = true;
}


void
visionx::imrec::AbstractRecordingStrategy::recordFrame(const CByteImage& frame, const std::chrono::microseconds time)
{
    // Default implementations of recordFrame just convert the parameter and call the other recordFrame method.
    // This way only one method needs to be overridden in the interface-implementing classes

    // Call cv::Mat variant of recordFrame(...)
    cv::Mat cv_frame;
    visionx::imrec::convert(frame, cv_frame);
    recordFrame(cv_frame, time);
}


void
visionx::imrec::AbstractRecordingStrategy::recordFrame(const cv::Mat& frame, const std::chrono::microseconds time)
{
    // Default implementations of recordFrame just convert the parameter and call the other recordFrame method.
    // This way only one method needs to be overridden in the interface-implementing classes

    // Call CByteImage variant of recordFrame(...)
    CByteImage ivt_frame;
    visionx::imrec::convert(frame, ivt_frame);
    recordFrame(ivt_frame, time);
}


void
visionx::imrec::AbstractRecordingStrategy::stopRecording()
{
    writeMetadataLine("frame_count", "unsigned int", std::to_string(m_sequence_number));
    m_recording = false;
    if (m_metadata_file.is_open())
    {
        m_metadata_file.close();
    }
}


std::filesystem::path
visionx::imrec::AbstractRecordingStrategy::getFilePath() const
{
    return m_file_path;
}


std::filesystem::path
visionx::imrec::AbstractRecordingStrategy::getPath() const
{
    return m_file_path.parent_path();
}


std::filesystem::path
visionx::imrec::AbstractRecordingStrategy::getStem() const
{
    return m_file_path.stem();
}


std::filesystem::path
visionx::imrec::AbstractRecordingStrategy::getDotExtension() const
{
    return m_file_path.extension();
}


std::filesystem::path
visionx::imrec::AbstractRecordingStrategy::getMetadataPath() const
{
    std::filesystem::path path = getPath();
    std::filesystem::path filename = getStem().string() + "_metadata.csv";
    return std::filesystem::canonical(path) / filename;
}


std::tuple<unsigned int, std::string>
visionx::imrec::AbstractRecordingStrategy::writeMetadataFrame(const CByteImage& frame, const std::chrono::microseconds timestamp)
{
    writeMetadataLine("frame_height", "unsigned int", std::to_string(static_cast<unsigned int>(frame.height)));
    writeMetadataLine("frame_width", "unsigned int", std::to_string(static_cast<unsigned int>(frame.width)));
    return writeMetadataFrameFileInfo(timestamp);
}


std::tuple<unsigned int, std::string>
visionx::imrec::AbstractRecordingStrategy::writeMetadataFrame(const cv::Mat& frame, const std::chrono::microseconds timestamp)
{
    writeMetadataLine("frame_height", "unsigned int", std::to_string(static_cast<unsigned int>(frame.size().height)));
    writeMetadataLine("frame_width", "unsigned int", std::to_string(static_cast<unsigned int>(frame.size().width)));
    return writeMetadataFrameFileInfo(timestamp);
}


std::tuple<unsigned int, std::string>
visionx::imrec::AbstractRecordingStrategy::writeMetadataFrameFileInfo(const std::chrono::microseconds timestamp)
{
    const unsigned int sequence_number = m_sequence_number++;

    // Write local system reference time of not already done.
    if (m_reference_time.count() == 0)
    {
        m_reference_time = timestamp;
        writeMetadataDatetime("frame_0_system_reference_time", timestamp);
    }

    // Normalize time to time since frame_0.
    const std::chrono::microseconds normalized_time = timestamp - m_reference_time;
    const std::string normalized_time_str = timestamp_to_string(normalized_time);

    // Write frame_<SEQUENCE_NUMBER> = <FRAME_NAME>.
    writeMetadataLine("frame_" + std::to_string(sequence_number), "string", normalized_time_str);

    return {sequence_number, normalized_time_str};
}


void
visionx::imrec::AbstractRecordingStrategy::writeMetadataDatetime(const std::string& var_name, const std::chrono::microseconds timestamp)
{
    writeMetadataLine(var_name + "_us", "long", std::to_string(timestamp.count()));
    writeMetadataLine(var_name + "_hr", "string", datetime_to_string(timestamp));
}


void
visionx::imrec::AbstractRecordingStrategy::writeMetadataLine(const std::string& var_name, const std::string_view var_type, const std::string_view var_value)
{
    if (not m_metadata_file.is_open())
    {
        m_metadata_file.open(getMetadataPath());
        writeMetadataLine("name", "type", "value");
        writeMetadataLine("metadata_version", "string", METADATA_VERSION);
    }

    // If the given variable was already written, do nothing
    if (m_written_metadata_variables.insert(var_name).second == false)
    {
        return;
    }

    m_metadata_file << var_name << "," << var_type << "," << var_value << "\n";
    m_metadata_file.flush();
}
