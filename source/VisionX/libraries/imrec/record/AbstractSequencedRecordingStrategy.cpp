/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    visionx::imrec
 * @author     Christian R. G. Dreher <christian.dreher@student.kit.edu>
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


// Amount of frames per chunk
#define CHUNK_SIZE 100


#include <VisionX/libraries/imrec/record/AbstractSequencedRecordingStrategy.h>


// STD/STL
#include <filesystem>

// OpenCV 2
#include <opencv2/opencv.hpp>

// IVT
#include <Image/ByteImage.h>

// ArmarX
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <VisionX/libraries/imrec/helper.h>


visionx::imrec::AbstractSequencedRecordingStrategy::AbstractSequencedRecordingStrategy() :
    visionx::imrec::AbstractRecordingStrategy()
{
    // pass
}


visionx::imrec::AbstractSequencedRecordingStrategy::AbstractSequencedRecordingStrategy(const std::filesystem::path& filePath, const std::filesystem::path& ext) :
    visionx::imrec::AbstractRecordingStrategy(filePath),
    m_ext{ext}
{
    // pass
}


visionx::imrec::AbstractSequencedRecordingStrategy::~AbstractSequencedRecordingStrategy()
{
    // pass
}


void
visionx::imrec::AbstractSequencedRecordingStrategy::startRecording()
{
    AbstractRecordingStrategy::startRecording();
    std::filesystem::path pathStem = getPath() / getStem();

    ARMARX_CHECK_EXPRESSION(not std::filesystem::exists(pathStem)) << "Folder already exists";

    std::filesystem::create_directory(pathStem);

    writeMetadataLine("frames_per_chunk", "unsigned int", std::to_string(CHUNK_SIZE));
    writeMetadataLine("extension", "string", getDotExtension().string());
}


std::filesystem::path
visionx::imrec::AbstractSequencedRecordingStrategy::getDotExtension() const
{
    return m_ext;
}


std::filesystem::path
visionx::imrec::AbstractSequencedRecordingStrategy::getMetadataPath() const
{
    std::filesystem::path path = getPath() / getStem();
    return std::filesystem::canonical(path) / "metadata.csv";
}


std::filesystem::path
visionx::imrec::AbstractSequencedRecordingStrategy::deriveFramePath(const unsigned int sequence_number, const std::string& frame_name)
{
    const std::filesystem::path frame_filename = frame_name + getDotExtension().string();

    if (sequence_number % CHUNK_SIZE == 0)
    {
        unsigned int chunk_number = sequence_number / CHUNK_SIZE;
        m_frames_chunk = "chunk_" + std::to_string(chunk_number);
        std::filesystem::create_directory(std::filesystem::canonical(getPath() / getStem()) / m_frames_chunk);
    }

    return std::filesystem::canonical(getPath() / getStem() / m_frames_chunk) / frame_filename;
}
