/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    visionx::imrec
 * @author     Christian R. G. Dreher <christian.dreher@student.kit.edu>
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include <VisionX/libraries/imrec/record/strats/BMPRecordingStrategy.h>


// IVT
#include <Image/ByteImage.h>


visionx::imrec::strats::BMPRecordingStrategy::BMPRecordingStrategy()
{
    // pass
}


visionx::imrec::strats::BMPRecordingStrategy::BMPRecordingStrategy(const std::filesystem::path& filePath, const std::string& name) :
    visionx::imrec::AbstractSequencedRecordingStrategy(filePath / name, ".bmp")
{
    // pass
}


visionx::imrec::strats::BMPRecordingStrategy::~BMPRecordingStrategy()
{
    // pass
}


void
visionx::imrec::strats::BMPRecordingStrategy::recordFrame(const CByteImage& frame, const std::chrono::microseconds timestamp)
{
    const auto& [sequence_number, frame_name] = writeMetadataFrame(frame, timestamp);
    const std::filesystem::path path = deriveFramePath(sequence_number, frame_name);
    frame.SaveToFile(path.c_str());
}
