/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    visionx::imrec
 * @author     Christian R. G. Dreher <christian.dreher@student.kit.edu>
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include <VisionX/libraries/imrec/record/strats/H264RecordingStrategy.h>


// OpenCV 2
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp> // cv::VideoWriter

// Simox
#include <SimoxUtility/algorithm.h>


visionx::imrec::strats::H264RecordingStrategy::H264RecordingStrategy()
{
    // pass
}


visionx::imrec::strats::H264RecordingStrategy::H264RecordingStrategy(
    const std::filesystem::path& file_path,
    const std::string& filename,
    const double fps) :
    visionx::imrec::AbstractRecordingStrategy(file_path), m_fps{fps}
{
    std::string filename_ext = filename;
    if (not simox::alg::ends_with(filename_ext, ".mp4"))
    {
        filename_ext += ".mp4";
    }

    m_file_path /= filename_ext;
}


visionx::imrec::strats::H264RecordingStrategy::~H264RecordingStrategy()
{
    // pass
}


void
visionx::imrec::strats::H264RecordingStrategy::startRecording()
{
    AbstractRecordingStrategy::startRecording();
    m_h264_video_writer = std::make_unique<cv::VideoWriter>();
}


void
visionx::imrec::strats::H264RecordingStrategy::recordFrame(
    const cv::Mat& frame,
    const std::chrono::microseconds timestamp)
{
    writeMetadataFrame(frame, timestamp);

    if (not m_h264_video_writer->isOpened())
    {
        const int fourcc = cv::VideoWriter::fourcc('X', '2', '6', '4');
        m_h264_video_writer->open(getFilePath().string(), fourcc, m_fps, frame.size());

        if (not m_h264_video_writer->isOpened())
        {
            ARMARX_ERROR << deactivateSpam() << "Could not open the output video file '"
                         << getFilePath().string() << "' for writing. FRAME DROPPED!";
            return;
        }
    }

    m_h264_video_writer->write(frame);
}


void
visionx::imrec::strats::H264RecordingStrategy::stopRecording()
{
    AbstractRecordingStrategy::stopRecording();
    m_h264_video_writer->release();
}
