/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    visionx::imrec
 * @author     Christian R. G. Dreher <christian.dreher@student.kit.edu>
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include <VisionX/libraries/imrec/record/strats/JPGRecordingStrategy.h>


// STD/STL
#include <vector>

// OpenCV 2
#include <opencv2/opencv.hpp>

// ArmarX
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>


visionx::imrec::strats::JPGRecordingStrategy::JPGRecordingStrategy()
{
    // pass
}


visionx::imrec::strats::JPGRecordingStrategy::JPGRecordingStrategy(const std::filesystem::path& filePath, const std::string& name, unsigned int jpg_quality) :
    visionx::imrec::AbstractSequencedRecordingStrategy(filePath / name, ".jpg"),
    m_jpg_quality{jpg_quality}
{
    // jpg_quality is unsigned, therefore this check is nonsensical!
    // ARMARX_CHECK_GREATER_EQUAL(jpg_quality, 0) << "Quality cannot be lower than 0.";
    ARMARX_CHECK_LESS_EQUAL(jpg_quality, 100) << "Quality cannot be greater than 100.";
}


visionx::imrec::strats::JPGRecordingStrategy::~JPGRecordingStrategy()
{
    // pass
}


void
visionx::imrec::strats::JPGRecordingStrategy::startRecording()
{
    visionx::imrec::AbstractSequencedRecordingStrategy::startRecording();
    writeMetadataLine("jpg_quality", "unsigned int", std::to_string(m_jpg_quality));
}


void
visionx::imrec::strats::JPGRecordingStrategy::recordFrame(const cv::Mat& frame, const std::chrono::microseconds timestamp)
{
    const auto& [sequence_number, frame_name] = writeMetadataFrame(frame, timestamp);
    const std::filesystem::path path = deriveFramePath(sequence_number, frame_name);
    std::vector<int> params {cv::IMWRITE_JPEG_QUALITY, static_cast<int>(m_jpg_quality)};
    cv::imwrite(path.string(), frame, params);
}
