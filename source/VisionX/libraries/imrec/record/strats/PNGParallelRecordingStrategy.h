/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    visionx::imrec
 * @author     Christian R. G. Dreher <christian.dreher@student.kit.edu>
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


// STD/STL
#include <atomic>
#include <condition_variable>
#include <mutex>
#include <string>

// ArmarX
#include <ArmarXCore/core/logging/Logging.h>
#include <VisionX/libraries/imrec/record/AbstractSequencedRecordingStrategy.h>


namespace visionx::imrec::strats
{

    /**
     * Concrete strategy for a PNG image recording
     */
    class PNGParallelRecordingStrategy :
        public AbstractSequencedRecordingStrategy
    {

    protected:

        unsigned int m_png_compression;

        std::mutex m_record_frame_mutex;
        std::mutex m_pool_mutex;
        std::condition_variable m_pool_cv;
        unsigned int m_pool_max_size;
        std::atomic<unsigned int> m_pool_current_size;

    public:

        PNGParallelRecordingStrategy();
        PNGParallelRecordingStrategy(const std::filesystem::path& filePath, const std::string& file_name, unsigned int png_compression = 9, unsigned int thread_pool_size = 1);
        ~PNGParallelRecordingStrategy() override;

        static void recordSnapshot(const cv::Mat& image, const std::filesystem::path& path);
        static void recordSnapshot(const CByteImage& image, const std::filesystem::path& path);

        void startRecording() override;
        void stopRecording() override;
        void recordFrame(const cv::Mat& frame, std::chrono::microseconds timestamp) override;
        void recordFrameAsync(std::filesystem::path path, const cv::Mat& frame);

    };

}
