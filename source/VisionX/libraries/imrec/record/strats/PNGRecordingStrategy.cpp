/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    visionx::imrec
 * @author     Christian R. G. Dreher <christian.dreher@student.kit.edu>
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include <VisionX/libraries/imrec/record/strats/PNGRecordingStrategy.h>


// STD/STL
#include <vector>
#include <filesystem>

// OpenCV 2
#include <opencv2/opencv.hpp>

// ArmarX
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <VisionX/libraries/imrec/helper.h>


visionx::imrec::strats::PNGRecordingStrategy::PNGRecordingStrategy()
{
    // pass
}


visionx::imrec::strats::PNGRecordingStrategy::PNGRecordingStrategy(const std::filesystem::path& filePath, const std::string& file_name, unsigned int png_compression) :
    visionx::imrec::AbstractSequencedRecordingStrategy(filePath / file_name, ".png"),
    m_png_compression{png_compression}
{
    ARMARX_CHECK_GREATER_EQUAL(png_compression, 1) << "Compression cannot be lower than 1.";
    ARMARX_CHECK_LESS_EQUAL(png_compression, 9) << "Compression cannot be greater than 9.";
}


visionx::imrec::strats::PNGRecordingStrategy::~PNGRecordingStrategy()
{
    // pass
}


void
visionx::imrec::strats::PNGRecordingStrategy::recordSnapshot(const cv::Mat& image, const std::filesystem::path& path)
{
    std::filesystem::path snapshotPath(path);

    // Make sure that path does exist, ensure PNG extension
    ARMARX_CHECK_EXPRESSION(std::filesystem::exists(path.parent_path())) << "Cannot take snapshot, path '" + path.parent_path().string() + "' does ot exist";

    if (snapshotPath.extension() != ".png")
    {
        snapshotPath += ".png";
    }

    const int snapshot_compression = 9;
    std::vector<int> params {cv::IMWRITE_PNG_COMPRESSION, snapshot_compression,
                             cv::IMWRITE_PNG_STRATEGY, cv::IMWRITE_PNG_STRATEGY_RLE};
    cv::imwrite(snapshotPath.string(), image, params);
}


void
visionx::imrec::strats::PNGRecordingStrategy::recordSnapshot(const CByteImage& image, const std::filesystem::path& path)
{
    // Covert to OpenCV image and run the CV method
    cv::Mat cv_image;
    visionx::imrec::convert(image, cv_image);
    recordSnapshot(cv_image, path);
}


void
visionx::imrec::strats::PNGRecordingStrategy::startRecording()
{
    visionx::imrec::AbstractSequencedRecordingStrategy::startRecording();
    writeMetadataLine("png_compression", "unsigned int", std::to_string(m_png_compression));
}


void
visionx::imrec::strats::PNGRecordingStrategy::recordFrame(const cv::Mat& frame, const std::chrono::microseconds timestamp)
{
    const auto& [sequence_number, frame_name] = writeMetadataFrame(frame, timestamp);
    const std::filesystem::path path = deriveFramePath(sequence_number, frame_name);
    std::vector<int> params {cv::IMWRITE_PNG_COMPRESSION, static_cast<int>(m_png_compression),
                             cv::IMWRITE_PNG_STRATEGY, cv::IMWRITE_PNG_STRATEGY_RLE};
    cv::imwrite(path.string(), frame, params);
}
