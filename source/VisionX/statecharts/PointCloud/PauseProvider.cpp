/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::PointCloud
 * @author     Markus Grotz ( markus dot grotz at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "PauseProvider.h"

#include "PointCloudStatechartContext.generated.h"

namespace armarx::PointCloud
{
    // DO NOT EDIT NEXT LINE
    PauseProvider::SubClassRegistry PauseProvider::Registry(PauseProvider::GetName(), &PauseProvider::CreateInstance);



    PauseProvider::PauseProvider(const XMLStateConstructorParams& stateData) :
        XMLStateTemplate<PauseProvider>(stateData),  PauseProviderGeneratedBase<PauseProvider>(stateData)
    {
    }

    void PauseProvider::onEnter()
    {

        std::string providerName = in.getProviderName();
        PointCloudStatechartContext* context = getContext<PointCloudStatechartContext>();

        visionx::CapturingPointCloudProviderInterfacePrx providerPrx;

        try
        {
            providerPrx = context->getIceManager()->getProxy<visionx::CapturingPointCloudProviderInterfacePrx>(providerName);
        }
        catch (...)
        {

        }

        if (providerPrx)
        {
            providerPrx->stopCapture();
            emitSuccess();
        }
        else
        {
            emitFailure();
        }
    }



    // DO NOT EDIT NEXT FUNCTION
    XMLStateFactoryBasePtr PauseProvider::CreateInstance(XMLStateConstructorParams stateData)
    {
        return XMLStateFactoryBasePtr(new PauseProvider(stateData));
    }
}
