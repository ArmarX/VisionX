/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::Tools
 * @author     Jan Issac (jan dot issac at gmx dot net)
 * @date       2011
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ImageUtil.h"

#include "TypeMapping.h"

#include <map>
#include <set>
#include <string>

// VisionXCore
#include <VisionX/core/ImageProcessor.h>

// IVT
#include <Image/ImageProcessor.h>
#include <Image/ByteImage.h>
#include <Image/FloatImage.h>

void visionx::tools::convertImage(const visionx::ImageFormatInfo& imageFormat, const visionx::ImageType destinationImageType, void* inputData, void* outputData)
{
    bool succeeded = true;

    visionx::ImageDimension dimension = imageFormat.dimension;
    visionx::ImageType sourceType = imageFormat.type;
    visionx::ImageType destinationType = destinationImageType;

    /* copy images if types are identical */
    if (sourceType == destinationType)
    {
        memcpy(outputData, inputData, imageFormat.bytesPerPixel * imageFormat.dimension.width * imageFormat.dimension.height);
        return;
    }

    /* process conversion */
    CByteImage sourceImage(dimension.width, dimension.height, visionx::tools::convert(sourceType), true);
    sourceImage.pixels = (unsigned char*) inputData;

    CByteImage destinationImage(dimension.width, dimension.height, visionx::tools::convert(destinationType), true);
    destinationImage.pixels = (unsigned char*) outputData;

    switch (sourceType)
    {
        // BayerPattern to {RGB, Gray-Scale}
        case eBayerPattern:
        {
            switch (destinationType)
            {
                case eGrayScale:
                {
                    CByteImage tempRgbImage(dimension.width, dimension.height, CByteImage::eRGB24);
                    ::ImageProcessor::ConvertBayerPattern(&sourceImage, &tempRgbImage, tools::convert(imageFormat.bpType));
                    ::ImageProcessor::ConvertImage(&tempRgbImage, &destinationImage);
                    break;
                }

                case eRgb:
                {
                    ::ImageProcessor::ConvertBayerPattern(&sourceImage, &destinationImage, tools::convert(imageFormat.bpType));
                    break;
                }

                //case eBayerPattern:
                //case eFloat1Channel:
                //case eFloat3Channels:
                default:
                {
                    /* Invalid conversion. This will cause an exception */
                    succeeded = false;
                    break;
                }
            };

            break;
        }

        // RGB to {Gray-Scale}
        case eRgb:
        {
            switch (destinationType)
            {
                case eGrayScale:
                {
                    ::ImageProcessor::ConvertImage(&sourceImage, &destinationImage);
                    return;
                }

                //case eBayerPattern:
                //case eRgb:
                //case eFloat1Channel:
                //case eFloat3Channels:
                default:
                {
                    /* Invalid conversion. This will cause an exception */
                    succeeded = false;
                    break;
                }
            };
        }
        break;

        // Gray-Scale to {RGB}
        case eGrayScale:
        {
            switch (destinationType)
            {
                case eRgb:
                {
                    ::ImageProcessor::ConvertImage(&sourceImage, &destinationImage);
                    return;
                }

                //case eBayerPattern:
                //case eGrayScale:
                // case eFloat1Channel:
                // case eFloat3Channels:
                default:
                {
                    /* Invalid conversion. This will cause an exception */
                    succeeded = false;
                    break;
                }
            };
        }
        break;

        // HDR BayerPattern to {}
        // case eFloat1Channel:
        // HDR RGB to {}
        //case eFloat3Channels:
        default:
        {
            /* Invalid conversion. This will cause an exception */
            succeeded = false;
            break;
        }
    }

    if (!succeeded)
    {
        throw visionx::exceptions::local::UnsupportedImageConversionException(visionx::tools::imageTypeToTypeName(sourceType), visionx::tools::imageTypeToTypeName(destinationType));
    }
}

void visionx::tools::convertImage(const visionx::ImageProviderInfo& imageProviderInfo, void* inputData, void* outputData)
{
    visionx::tools::convertImage(imageProviderInfo.imageFormat, imageProviderInfo.destinationImageType, inputData, outputData);
}

CByteImage* visionx::tools::createByteImage(const visionx::ImageFormatInfo& imageFormat, const visionx::ImageType imageType)
{
    CByteImage::ImageType type = CByteImage::eGrayScale;

    switch (imageType)
    {
        case eBayerPattern:
        case eGrayScale:
            break;

        case eRgb:
            type = CByteImage::eRGB24;
            break;

        //case eFloat1Channel:
        //case eFloat3Channels:
        default:
            throw visionx::exceptions::local::InvalidByteImageTypeException(visionx::tools::imageTypeToTypeName(imageType));
    }

    return new CByteImage(imageFormat.dimension.width, imageFormat.dimension.height, type);
}


CByteImage* visionx::tools::createByteImage(const ImageFormatInfo& imageFormat)
{
    return createByteImage(imageFormat, imageFormat.type);
}


CByteImage* visionx::tools::createByteImage(const visionx::ImageProviderInfo& imageProviderInfo)
{
    return visionx::tools::createByteImage(imageProviderInfo.imageFormat, imageProviderInfo.destinationImageType);
}

CFloatImage* visionx::tools::createFloatImage(const visionx::ImageFormatInfo& imageFormat, const visionx::ImageType imageType)
{
    int numberOfChannels = 1;

    switch (imageType)
    {
        case eFloat1Channel:
            // already set to 1
            break;

        case eFloat3Channels:
            numberOfChannels = 3;
            break;

        //case eBayerPattern:
        //case eGrayScale:
        //case eRgb:
        default:
            throw visionx::exceptions::local::InvalidFloatImageTypeException(visionx::tools::imageTypeToTypeName(imageType));
    }

    return new CFloatImage(imageFormat.dimension.width, imageFormat.dimension.height, numberOfChannels);
}


CFloatImage* visionx::tools::createFloatImage(const visionx::ImageProviderInfo& imageProviderInfo)
{
    return visionx::tools::createFloatImage(imageProviderInfo.imageFormat, imageProviderInfo.destinationImageType);
}


visionx::MonocularCalibration visionx::tools::createDefaultMonocularCalibration()
{
    visionx::MonocularCalibration calibration;
    calibration.cameraParam.width = 640;
    calibration.cameraParam.height = 480;
    calibration.cameraParam.focalLength.resize(2);
    calibration.cameraParam.focalLength[0] = 500;
    calibration.cameraParam.focalLength[1] = 500;
    calibration.cameraParam.principalPoint.resize(2);
    calibration.cameraParam.principalPoint[0] = calibration.cameraParam.width / 2;
    calibration.cameraParam.principalPoint[1] = calibration.cameraParam.height / 2;
    std::vector<std::vector<float> > mat(3);
    std::vector<float> rowVec(3);
    rowVec[0] = 0;
    rowVec[1] = 0;
    rowVec[2] = 0;
    calibration.cameraParam.translation = rowVec;
    mat[0] = rowVec;
    mat[1] = rowVec;
    mat[2] = rowVec;
    mat[0][0] = 1;
    mat[1][1] = 1;
    mat[2][2] = 1;
    calibration.cameraParam.rotation = mat;
    rowVec.push_back(0);
    calibration.cameraParam.distortion = rowVec;

    return calibration;
}
