/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::Tools
 * @author     Jan Issac (jan dot issac at gmx dot net)
 * @date       2011
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <VisionX/interface/core/DataTypes.h>
#include <VisionX/interface/components/Calibration.h>

#include "exceptions/local/UnsupportedImageConversionException.h"
#include "exceptions/local/InvalidByteImageTypeException.h"
#include "exceptions/local/InvalidFloatImageTypeException.h"

class CByteImage;
class CFloatImage;

namespace visionx
{
    struct ImageFormatInfo;
    class ImageProviderInfo;
}
namespace visionx::tools
{
    /**
     * This converts the input image data into a desired destination image data type specified
     * in the given ImageProviderInfo.
     *
     * If source image type equals destination image type, the image is simply copied, otherwise
     * the image is converted if the conversion type is supported.
     *
     * Supported conversions:
     *   - BayerPattern-{bg,gb,gr,rg} to BayerPattern-{bg,gb,gr,rg} (copy only)
     *   - BayerPattern-{bg,gb,gr,rg} to GrayScale
     *   - BayerPattern-{bg,gb,gr,rg} to RGB
     *   - GrayScale to GrayScale (copy only)
     *   - GrayScale to RGB
     *   - RGB to GrayScale
     *   - RGB to RGB (copy only)
     *   - Float single channel to Float single channel  (copy only)
     *   - Float multiple channel to Float multiple channel  (copy only)
     *
     * @param ImageFormatInfo     Image format information which contains the source image type,
     *                              source image size and more details needed for conversions.
     * @param destinationType     destination image type of outputData
     * @param inputData            input data array
     * @param outputData        output data array
     *
     * @throws                     visionx::UnsupportedImageConversion if conversion from a specific source type
     *                             into a destination type is not supported or implemented.
     */
    void convertImage(const ImageFormatInfo& imageFormat, const ImageType destinationImageType, void* inputData, void* outputData);

    /**
     * Simplifies usage of visionx::tools:convertImage in ImageProcessors.
     *
     * @see visionx::tools::convertImage(const ImageFormatInfo&, const ImageType, unsigned char*, unsigned char*)
     *
     * @param ImageProviderInfo Registered image provider information which contains the source image type,
     *                            destination image type, source image size and more details needed for
     *                            conversions.
     * @param inputData            input data array
     * @param outputData        output data array
     *
     * @throws                     visionx::UnsupportedImageConversion if conversion from a specific source type
     *                             into a destination type is not supported or implemented.
     */
    void convertImage(const ImageProviderInfo& imageProviderInfo, void* inputData, void* outputData);

    /**
     * Creates a ByteImage for the destination type specified in the given imageProviderInfo.
     *
     * @param ImageFormatInfo     Image format information which contains the source image size
     * @param imageType         Required VisionX image type which will be mapped onto
     *                             an appropriate CByteImage::ImageType
     *
     * @return                  Appropriate CByteImage
     */
    CByteImage* createByteImage(const ImageFormatInfo& imageFormat, const ImageType imageType);
    CByteImage* createByteImage(const ImageFormatInfo& imageFormat);

    /**
     * Simplifies usage of visionx::tools:convertImage in ImageProcessors.
     *
     * @see visionx::tools::createImage(const ImageFormatInfo&, const ImageType)
     *
     * @param imageProviderInfo        Registered image provider information which contains the
     *                                 ImageFormatInfo and destination image type.
     *
     * @return CByteImage             Appropriate CByteImage
     */
    CByteImage* createByteImage(const ImageProviderInfo& imageProviderInfo);

    /**
     * Creates a FloatImage for the destination type specified in the given imageProviderInfo.
     *
     * @param ImageFormatInfo   Image format information which contains the source image size
     * @param imageType         Required VisionX image type which will be mapped onto
     *                          an appropriate number of channels
     *
     * @return CByteImage       Appropriate CFloatImage
     */
    CFloatImage* createFloatImage(const ImageFormatInfo& imageFormat, const ImageType imageType);

    /**
     * Simplifies usage of visionx::tools:convertImage in ImageProcessors.
     *
     * @see visionx::tools::createFloatImage(const ImageFormatInfo&, const ImageType)
     *
     * @param imageProviderInfo     Registered image provider information which contains the
     *                              ImageFormatInfo and destination image type.
     *
     * @return CByteImage           Appropriate CFloatImage
     */
    CFloatImage* createFloatImage(const ImageProviderInfo& imageProviderInfo);

    /**
     * Creates a MonocularCalibration with all parameters set to a neutral value.
     *
     */
    MonocularCalibration createDefaultMonocularCalibration();

    inline float rgbToDepthValue(unsigned char r, unsigned char g, unsigned char b, bool noiseResistant = false)
    {
        float result = 0;
        if (noiseResistant)
        {
            constexpr char size = 8;
            constexpr int channels = 3;

            int sum = 0;
            auto add = [&](unsigned char v, char channelNumber)
            {
                auto end = size * channels;
                for (int offset = 0; offset < end; offset += channels)
                {
                    auto tmp = (v & 0b1) << (channelNumber + offset);
                    sum += tmp;
                    v = (v >> 1);
                }
            };
            add(r, 0);
            add(g, 1);
            add(b, 2);


            result = sum;
            result /= 256; // stretch back from 24 bit to 16 bit - see depthValueToRGB()
        }
        else
        {
            result = (r + (g << 8) + (b << 16));
        }
        return result;
    }

    inline void depthValueToRGB(unsigned int depthInMM, unsigned char& r, unsigned char& g, unsigned char& b, bool noiseResistant = false)
    {
        if (noiseResistant)
        {
            depthInMM *= 256; // stretch from 16 bit to 24 bit
            constexpr char size = 8;
            constexpr int channels = 3;
            auto toRGB = [&](unsigned int v, char channelNumber)
            {
                int mask = 0b1;
                v = v >> channelNumber;
                unsigned char r = 0;
                for (int offset = 0; offset < size; offset++)
                {
                    int shift = std::max(offset * channels - offset, 0);
                    int masked = v & mask;
                    int shiftedV = masked >> shift;
                    r += shiftedV;
                    mask = mask << channels;
                }
                return r;
            };

            r = toRGB(depthInMM, 0);
            g = toRGB(depthInMM, 1);
            b = toRGB(depthInMM, 2);
        }
        else
        {
            r = depthInMM & 0xFF;
            g = (depthInMM >> 8) & 0xFF;
            b = (depthInMM >> 16) & 0xFF;
        }
    }


}


namespace std
{
    inline ostream& operator<<(ostream& os, const visionx::ImageDimension& dimension)
    {
        os << dimension.width << "x" << dimension.height;
        return os;
    }
}

