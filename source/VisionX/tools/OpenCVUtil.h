#pragma once

#include <opencv2/core/core.hpp>

#include <Calibration/Calibration.h>


class CByteImage;

namespace visionx
{

    /**
     * @brief Copies the contents of an OpenCV matrix to an IVT CByteImage.
     *
     * @param input OpenCV matrix (RGB or grayscale)
     * @param output IVT CByteImage (RGB or grayscale)
     */
    void copyCvMatToIVT(cv::Mat const& input, CByteImage* output);

    /**
     * @brief Copies a floating point depth image (in meters) to an RGB depth representation.
     * Close to the camera: White
     * Farther away: Black
     *
     * @param inputInMeters OpenCV floating point matrix (CV_32FC1) containting depth in meters.
     * @param output IVT CByteImage for visulaizing the depth info in an RGB image.
     */
    void copyCvDepthToGrayscaleIVT(cv::Mat const& inputInMeters, CByteImage* output);


    /**
     * @brief Convert ArmarX encoding of depth information in an RGB image to depth in meters.
     *
     * ArmarX stores the depth in mm in the R and G channel of an RGB image.
     * This is due to the fact that we cannot provide images of different types.
     * So a RGB-D camera needs to use a single format for both color and depth.
     *
     * @param input Weird ArmarX encoding of depth image.
     * @return Floating point matrix containing the depth in meters.
     */
    cv::Mat convertWeirdArmarXToDepthInMeters(CByteImage const* input);

    /**
     * @brief Like `convertWeirdArmarXToDepthInMeters(CByteImage const* input)`,
     * but writes into a pre-initialized cv::Mat with the correct size and type.
     *
     * @param input The raw pixel buffer with weird ArmarX depth encoding.
     * @param output A cv::Mat with the correct size and type.
     */
    void convertWeirdArmarXToDepthInMeters(const uint8_t* input, cv::Mat& output);

    void convertDepthInMetersToWeirdArmarX(const cv::Mat& input, CByteImage* output);


    class CameraParameters;

    /**
     * @brief Fill a propertly initialized camera matrix with the given camera parameters.
     * @param cameraParams The camera parameters.
     * @param output The cv matrix. Must be floating point, i.e. CV_32F or CV_64F.
     */
    void fillCameraMatrix(const visionx::CameraParameters& cameraParams, cv::Mat& output);
    /**
     * @brief Fill a propertly initialized camera matrix with the given camera parameters.
     * @param cameraParams The camera parameters.
     * @param output The cv matrix. Must be floating point, i.e. CV_32F or CV_64F.
     */
    void fillCameraMatrix(const CCalibration::CCameraParameters& ivtCameraParameters, cv::Mat& output);

    /**
     * @brief Builds a camera matrix.
     * @param cameraParams The camera matrix.
     * @return The CV camera matrix.
     */
    template <class CameraParams>
    cv::Mat makeCameraMatrix(const CameraParams& cameraParams,
                             int rows = 3, int cols = 3, int type = CV_64F)
    {
        cv::Mat cameraMatrix = cv::Mat(rows, cols, type, 0.0);
        fillCameraMatrix(cameraParams, cameraMatrix);
        return cameraMatrix;
    }

}
