/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::Tools
 * @author     Jan Issac (jan dot issac at gmx dot net)
 * @date       2011
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "TypeMapping.h"

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

// SLT
#include <vector>
#include <map>

// IVT
#include <Structs/Structs.h>
#include <Image/ByteImage.h>
#include <Math/Math2d.h>
#include <Math/Math3d.h>
#include <Calibration/Calibration.h>
#include <Calibration/StereoCalibration.h>


visionx::ImageType visionx::tools::typeNameToImageType(const std::string& imageTypeName)
{
    static std::map<std::string, visionx::ImageType> nameToTypeMap;

    // initialize image type name to image type integer mapping
    if (nameToTypeMap.empty())
    {
        nameToTypeMap["bayer-pattern"]     = visionx::eBayerPattern;
        nameToTypeMap["gray-scale"]        = visionx::eGrayScale;
        nameToTypeMap["rgb"]               = visionx::eRgb;
        nameToTypeMap["float-1-channel"]   = visionx::eFloat1Channel;
        nameToTypeMap["float-3-channels"]  = visionx::eFloat3Channels;
    }

    std::string lowerCaseTypeName = imageTypeName;
    std::transform(lowerCaseTypeName.begin(), lowerCaseTypeName.end(), lowerCaseTypeName.begin(), ::tolower);

    std::map<std::string, visionx::ImageType>::iterator nameToTypeIter = nameToTypeMap.find(lowerCaseTypeName);

    if (nameToTypeIter == nameToTypeMap.end())
    {
        throw visionx::exceptions::local::InvalidImageTypeNameException(imageTypeName);
    }

    return nameToTypeIter->second;
}


std::string visionx::tools::imageTypeToTypeName(const ImageType imageType)
{
    switch (imageType)
    {
        case visionx::eBayerPattern:
            return "bayer-pattern";

        case visionx::eGrayScale:
            return "gray-scale";

        case visionx::eRgb:
            return "rgb";

        case visionx::eFloat1Channel:
            return "float-1-channel";

        case visionx::eFloat3Channels:
            return "float-3-channel";

        default:
            return "unknown";
    }
}


CByteImage::ImageType visionx::tools::convert(const ImageType visionxImageType)
{
    static std::map<visionx::ImageType, CByteImage::ImageType> visionxTypeToIvtTypeMap;

    // initialize VisionX image type to ByteImage image type mapping
    if (visionxTypeToIvtTypeMap.empty())
    {
        visionxTypeToIvtTypeMap[visionx::eRgb]          = CByteImage::eRGB24;
        visionxTypeToIvtTypeMap[visionx::eGrayScale]    = CByteImage::eGrayScale;
        visionxTypeToIvtTypeMap[visionx::eBayerPattern] = CByteImage::eGrayScale;
    }

    std::map<visionx::ImageType, CByteImage::ImageType>::iterator visionxTypeToIvtTypeIter = visionxTypeToIvtTypeMap.find(visionxImageType);

    if (visionxTypeToIvtTypeIter == visionxTypeToIvtTypeMap.end())
    {
        throw visionx::exceptions::local::UnsupportedImageTypeException(visionx::tools::imageTypeToTypeName(visionxImageType));
    }

    return visionxTypeToIvtTypeIter->second;
}


ImageProcessor::BayerPatternType visionx::tools::convert(const BayerPatternType visionxBayerPatternType)
{
    switch (visionxBayerPatternType)
    {
        case visionx::eBayerPatternBg:
            return ::ImageProcessor::eBayerBG;

        case visionx::eBayerPatternGb:
            return ::ImageProcessor::eBayerGB;

        case visionx::eBayerPatternGr:
            return ::ImageProcessor::eBayerGR;

        case visionx::eBayerPatternRg:
            return ::ImageProcessor::eBayerRG;
    }

    // default value (this is never reached since there are only 4 Bayer Pattern
    // types which are already handled by the switch above)
    return ::ImageProcessor::eBayerRG;
}


CVideoCaptureInterface::VideoMode visionx::tools::convert(const ImageDimension& imageDimension)
{
    // since the width or the height of each available dimension is unique, we use only
    // the width to identify the dimension
    switch (imageDimension.width)
    {
        case 0:
            return CVideoCaptureInterface::eNone;

        case 320:
            return CVideoCaptureInterface::e320x240;

        case 640:
            return CVideoCaptureInterface::e640x480;

        case 800:
            return CVideoCaptureInterface::e800x600;

        case 768:
            return CVideoCaptureInterface::e768x576;

        case 1024:
            return CVideoCaptureInterface::e1024x768;

        case 1280:
            return CVideoCaptureInterface::e1280x960;

        case 1600:
            return CVideoCaptureInterface::e1600x1200;
    }

    return CVideoCaptureInterface::eNone;
}

CVideoCaptureInterface::FrameRate visionx::tools::convert(const float frameRate)
{
    using FrameRateRange = std::pair<float, float>;

    static std::map<FrameRateRange, CVideoCaptureInterface::FrameRate> frameRateMap;

    using FrameRateMapIter = std::map<FrameRateRange, CVideoCaptureInterface::FrameRate>::iterator;

    if (frameRateMap.empty())
    {
        frameRateMap[FrameRateRange(0.f,                1.875f + 1.875f / 2)] = CVideoCaptureInterface::e1_875fps;
        frameRateMap[FrameRateRange(1.875f + 1.875f / 2,  3.75f + 1.875f)]    = CVideoCaptureInterface::e3_75fps;
        frameRateMap[FrameRateRange(3.75f + 1.875f,     7.5f + 3.75f)]      = CVideoCaptureInterface::e7_5fps;
        frameRateMap[FrameRateRange(7.5f + 3.75f,       15.f + 7.5f)]       = CVideoCaptureInterface::e15fps;
        frameRateMap[FrameRateRange(15.f + 7.5f,        30.f + 15.f)]       = CVideoCaptureInterface::e30fps;
        frameRateMap[FrameRateRange(30.f + 15.f,        1024.f)]            = CVideoCaptureInterface::e60fps;
    }

    FrameRateMapIter iter = frameRateMap.begin();

    while (iter != frameRateMap.end())
    {
        if (frameRate >= iter->first.first && frameRate < iter->first.second)
        {
            return iter->second;
        }

        iter++;
    }

    throw visionx::exceptions::local::InvalidFrameRateException(frameRate);
}


Vec2d visionx::tools::convert(const visionx::types::Vec* pVec)
{
    Vec2d vec2d;
    vec2d.x = (*pVec)[0];
    vec2d.y = (*pVec)[1];

    return vec2d;
}


Vec3d visionx::tools::convert(const visionx::types::Vec& vec)
{
    Vec3d vec3d;
    vec3d.x = vec[0];
    vec3d.y = vec[1];
    vec3d.z = vec[2];

    return vec3d;
}


Mat3d visionx::tools::convert(const visionx::types::Mat& mat)
{
    Mat3d ivtMat3d;
    ARMARX_CHECK_EQUAL(mat.size(), 3);
    ARMARX_CHECK_EQUAL(mat[0].size(), 3);
    ARMARX_CHECK_EQUAL(mat[1].size(), 3);
    ARMARX_CHECK_EQUAL(mat[2].size(), 3);


    ivtMat3d.r1 = mat[0][0];
    ivtMat3d.r2 = mat[0][1];
    ivtMat3d.r3 = mat[0][2];
    ivtMat3d.r4 = mat[1][0];
    ivtMat3d.r5 = mat[1][1];
    ivtMat3d.r6 = mat[1][2];
    ivtMat3d.r7 = mat[2][0];
    ivtMat3d.r8 = mat[2][1];
    ivtMat3d.r9 = mat[2][2];

    return ivtMat3d;
}


visionx::types::Vec visionx::tools::convert(const Vec2d& ivtVec2d)
{
    std::vector<float> vec(2);
    vec[0] = ivtVec2d.x;
    vec[1] = ivtVec2d.y;

    return vec;
}


visionx::types::Vec visionx::tools::convert(const Vec3d& ivtVec3d)
{
    std::vector<float> vec(3);
    vec[0] = ivtVec3d.x;
    vec[1] = ivtVec3d.y;
    vec[2] = ivtVec3d.z;

    return vec;
}


visionx::types::Mat visionx::tools::convert(const Mat3d& ivtMat3d)
{
    std::vector<std::vector<float> > mat(3);
    std::vector<float> rowVec(3);

    rowVec[0] = ivtMat3d.r1;
    rowVec[1] = ivtMat3d.r2;
    rowVec[2] = ivtMat3d.r3;
    mat[0] = rowVec;

    rowVec = std::vector<float>(3);
    rowVec[0] = ivtMat3d.r4;
    rowVec[1] = ivtMat3d.r5;
    rowVec[2] = ivtMat3d.r6;
    mat[1] = rowVec;

    rowVec = std::vector<float>(3);
    rowVec[0] = ivtMat3d.r7;
    rowVec[1] = ivtMat3d.r8;
    rowVec[2] = ivtMat3d.r9;
    mat[2] = rowVec;

    return mat;
}


CStereoCalibration* visionx::tools::convert(const visionx::StereoCalibration& stereoCalibration)
{
    CStereoCalibration* ivtStereoCalibration = new CStereoCalibration();

    CCalibration ivtCalibrationLeft;
    CCalibration ivtCalibrationRight;

    const visionx::CameraParameters& cameraParamLeft = stereoCalibration.calibrationLeft.cameraParam;
    const visionx::CameraParameters& cameraParamRight = stereoCalibration.calibrationRight.cameraParam;

    Mat3d R = convert(cameraParamLeft.rotation);
    Vec3d t = convert(cameraParamLeft.translation);
    ARMARX_CHECK_EQUAL(cameraParamLeft.focalLength.size(), 2);
    ARMARX_CHECK_EQUAL(cameraParamLeft.principalPoint.size(), 2);
    ARMARX_CHECK_EQUAL(cameraParamLeft.distortion.size(), 4);
    ivtCalibrationLeft.SetCameraParameters
    (
        cameraParamLeft.focalLength[0],
        cameraParamLeft.focalLength[1],
        cameraParamLeft.principalPoint[0],
        cameraParamLeft.principalPoint[1],
        cameraParamLeft.distortion[0],
        cameraParamLeft.distortion[1],
        cameraParamLeft.distortion[2],
        cameraParamLeft.distortion[3],
        R,
        t,
        cameraParamLeft.width,
        cameraParamLeft.height
    );

    R = convert(cameraParamRight.rotation);
    t = convert(cameraParamRight.translation);
    ARMARX_CHECK_EQUAL(cameraParamRight.focalLength.size(), 2);
    ARMARX_CHECK_EQUAL(cameraParamRight.principalPoint.size(), 2);
    ARMARX_CHECK_EQUAL(cameraParamRight.distortion.size(), 4);

    ivtCalibrationRight.SetCameraParameters
    (
        cameraParamRight.focalLength[0],
        cameraParamRight.focalLength[1],
        cameraParamRight.principalPoint[0],
        cameraParamRight.principalPoint[1],
        cameraParamRight.distortion[0],
        cameraParamRight.distortion[1],
        cameraParamRight.distortion[2],
        cameraParamRight.distortion[3],
        R,
        t,
        cameraParamRight.width,
        cameraParamRight.height
    );

    ivtStereoCalibration->SetSingleCalibrations(ivtCalibrationLeft, ivtCalibrationRight);

    ivtStereoCalibration->rectificationHomographyLeft  = convert(stereoCalibration.rectificationHomographyLeft);
    ivtStereoCalibration->rectificationHomographyRight = convert(stereoCalibration.rectificationHomographyRight);

    return ivtStereoCalibration;
}



CCalibration* visionx::tools::convert(const visionx::MonocularCalibration& calibration)
{
    CCalibration* ivtCalibration = new CCalibration();

    const visionx::CameraParameters& cameraParam = calibration.cameraParam;

    Mat3d R = convert(cameraParam.rotation);
    Vec3d t = convert(cameraParam.translation);

    ivtCalibration->SetCameraParameters
    (
        cameraParam.focalLength[0],
        cameraParam.focalLength[1],
        cameraParam.principalPoint[0],
        cameraParam.principalPoint[1],
        cameraParam.distortion[0],
        cameraParam.distortion[1],
        cameraParam.distortion[2],
        cameraParam.distortion[3],
        R,
        t,
        cameraParam.width,
        cameraParam.height
    );

    return ivtCalibration;
}

visionx::StereoCalibration visionx::tools::convert(const CStereoCalibration& ivtStereoCalibration)
{
    visionx::StereoCalibration stereoCalibration;

    const CCalibration::CCameraParameters& ivtCalibParamLeft = ivtStereoCalibration.GetLeftCalibration()->GetCameraParameters();
    const CCalibration::CCameraParameters& ivtCalibParamRight = ivtStereoCalibration.GetRightCalibration()->GetCameraParameters();

    visionx::CameraParameters& cameraParamLeft  = stereoCalibration.calibrationLeft.cameraParam;
    visionx::CameraParameters& cameraParamRight = stereoCalibration.calibrationRight.cameraParam;

    // Left camera parameters
    cameraParamLeft.width  = ivtCalibParamLeft.width;
    cameraParamLeft.height = ivtCalibParamLeft.height;

    cameraParamLeft.principalPoint  = convert(ivtCalibParamLeft.principalPoint);
    cameraParamLeft.focalLength     = convert(ivtCalibParamLeft.focalLength);
    cameraParamLeft.rotation        = convert(ivtCalibParamLeft.rotation);
    cameraParamLeft.translation     = convert(ivtCalibParamLeft.translation);

    cameraParamLeft.distortion.push_back(ivtCalibParamLeft.distortion[0]);
    cameraParamLeft.distortion.push_back(ivtCalibParamLeft.distortion[1]);
    cameraParamLeft.distortion.push_back(ivtCalibParamLeft.distortion[2]);
    cameraParamLeft.distortion.push_back(ivtCalibParamLeft.distortion[3]);

    // right camera parameters
    cameraParamRight.width  = ivtCalibParamRight.width;
    cameraParamRight.height = ivtCalibParamRight.height;

    cameraParamRight.principalPoint = convert(ivtCalibParamRight.principalPoint);
    cameraParamRight.focalLength    = convert(ivtCalibParamRight.focalLength);
    cameraParamRight.rotation       = convert(ivtCalibParamRight.rotation);
    cameraParamRight.translation    = convert(ivtCalibParamRight.translation);

    cameraParamRight.distortion.push_back(ivtCalibParamRight.distortion[0]);
    cameraParamRight.distortion.push_back(ivtCalibParamRight.distortion[1]);
    cameraParamRight.distortion.push_back(ivtCalibParamRight.distortion[2]);
    cameraParamRight.distortion.push_back(ivtCalibParamRight.distortion[3]);

    stereoCalibration.rectificationHomographyLeft  = convert(ivtStereoCalibration.rectificationHomographyLeft);
    stereoCalibration.rectificationHomographyRight = convert(ivtStereoCalibration.rectificationHomographyRight);

    return stereoCalibration;
}


visionx::MonocularCalibration visionx::tools::convert(const CCalibration& ivtCalibration)
{
    visionx::MonocularCalibration calibration;
    const CCalibration::CCameraParameters& ivtCalibParam = ivtCalibration.GetCameraParameters();

    calibration.cameraParam.width  = ivtCalibParam.width;
    calibration.cameraParam.height = ivtCalibParam.height;

    calibration.cameraParam.principalPoint = convert(ivtCalibParam.principalPoint);
    calibration.cameraParam.focalLength    = convert(ivtCalibParam.focalLength);
    calibration.cameraParam.rotation       = convert(ivtCalibParam.rotation);
    calibration.cameraParam.translation    = convert(ivtCalibParam.translation);

    calibration.cameraParam.distortion.push_back(ivtCalibParam.distortion[0]);
    calibration.cameraParam.distortion.push_back(ivtCalibParam.distortion[1]);
    calibration.cameraParam.distortion.push_back(ivtCalibParam.distortion[2]);
    calibration.cameraParam.distortion.push_back(ivtCalibParam.distortion[3]);

    return calibration;
}


visionx::types::Region visionx::tools::convert(const MyRegion& ivtRegion)
{
    visionx::types::Region region;

    visionx::tools::arrayToVector(ivtRegion.pPixels, ivtRegion.nPixels, region.pixels);
    region.nPixels = ivtRegion.nPixels;

    region.minX = ivtRegion.min_x;
    region.maxX = ivtRegion.max_x;
    region.minY = ivtRegion.min_y;
    region.maxY = ivtRegion.max_y;

    region.ratio = ivtRegion.ratio;

    region.centroid = visionx::tools::convert(ivtRegion.centroid);

    return region;
}


MyRegion visionx::tools::convert(const visionx::types::Region& region)
{
    MyRegion ivtRegion;

    ivtRegion.pPixels = visionx::tools::vectorToArray(region.pixels);
    ivtRegion.nPixels  = region.nPixels;

    ivtRegion.min_x = region.minX;
    ivtRegion.max_x = region.maxX;
    ivtRegion.min_y = region.minY;
    ivtRegion.max_y = region.maxY;

    ivtRegion.ratio = region.ratio;

    ivtRegion.centroid = visionx::tools::convert(&region.centroid);

    return ivtRegion;
}


visionx::types::ObjectColor visionx::tools::convert(const ObjectColor& ivtObjectColor)
{
    return (visionx::types::ObjectColor) ivtObjectColor;
}


ObjectColor visionx::tools::convert(const visionx::types::ObjectColor& objectColor)
{
    return (ObjectColor) objectColor;
}


visionx::types::ObjectType visionx::tools::convert(const ObjectType& ivtObjectType)
{
    return (visionx::types::ObjectType) ivtObjectType;
}


ObjectType visionx::tools::convert(const visionx::types::ObjectType& objectType)
{
    return (ObjectType) objectType;
}


visionx::types::Transformation3d visionx::tools::convert(const Transformation3d& ivtTransformation3d)
{
    visionx::types::Transformation3d transformation3d;

    transformation3d.translation = visionx::tools::convert(ivtTransformation3d.translation);
    transformation3d.rotation = visionx::tools::convert(ivtTransformation3d.rotation);

    return transformation3d;
}


Transformation3d visionx::tools::convert(const visionx::types::Transformation3d& transformation3d)
{
    Transformation3d ivtTransformation3d;

    ivtTransformation3d.translation = visionx::tools::convert(transformation3d.translation);
    ivtTransformation3d.rotation = visionx::tools::convert(transformation3d.rotation);

    return ivtTransformation3d;
}


Object3DEntry visionx::tools::convert(const visionx::types::Object3DEntry& object3DEntry)
{
    Object3DEntry ivtObject3DEntry;

    ivtObject3DEntry.region_left        = convert(object3DEntry.regionLeft);
    ivtObject3DEntry.region_right       = convert(object3DEntry.regionRight);
    ivtObject3DEntry.region_id_left     = object3DEntry.regionIdLeft;
    ivtObject3DEntry.region_id_right    = object3DEntry.regionIdRight;
    ivtObject3DEntry.type               = convert(object3DEntry.type);
    ivtObject3DEntry.color              = convert(object3DEntry.color);
    ivtObject3DEntry.pose               = convert(object3DEntry.pose);
    ivtObject3DEntry.world_point        = convert(object3DEntry.worldPoint);
    ivtObject3DEntry.orientation        = convert(object3DEntry.orientation);
    ivtObject3DEntry.sName              = "";
    ivtObject3DEntry.sName             += object3DEntry.name;
    ivtObject3DEntry.sOivFilePath       = "";
    // ivtObject3DEntry.sOivFilePath      += objectEntry.oivFilePath;
    ivtObject3DEntry.data               = nullptr;
    ivtObject3DEntry.class_id           = object3DEntry.classId;
    ivtObject3DEntry.quality            = object3DEntry.quality;
    ivtObject3DEntry.quality2           = object3DEntry   .quality2;

    return ivtObject3DEntry;
}


visionx::types::Object3DEntry visionx::tools::convert(const Object3DEntry& ivtObject3DEntry)
{
    visionx::types::Object3DEntry object3DEntry;

    object3DEntry.regionLeft    = convert(ivtObject3DEntry.region_left);
    object3DEntry.regionRight   = convert(ivtObject3DEntry.region_right);
    object3DEntry.regionIdLeft  = ivtObject3DEntry.region_id_left;
    object3DEntry.regionIdRight = ivtObject3DEntry.region_id_right;
    object3DEntry.type          = convert(ivtObject3DEntry.type);
    object3DEntry.color         = convert(ivtObject3DEntry.color);
    object3DEntry.pose          = convert(ivtObject3DEntry.pose);
    object3DEntry.worldPoint    = convert(ivtObject3DEntry.world_point);
    object3DEntry.orientation   = convert(ivtObject3DEntry.orientation);
    object3DEntry.name          = "";
    object3DEntry.name         += ivtObject3DEntry.sName;
    //objectEntry.sOivFilePath = "";
    // ivtObject3DEntry.sOivFilePath += objectEntry.oivFilePath;
    object3DEntry.classId       = ivtObject3DEntry.class_id;
    object3DEntry.quality       = ivtObject3DEntry.quality;
    object3DEntry.quality2      = ivtObject3DEntry.quality2;

    return object3DEntry;
}

Object3DList visionx::tools::convert(const visionx::types::Object3DList& object3DList)
{
    Object3DList ivtObject3DList(object3DList.size());

    visionx::types::Object3DList::const_iterator iter = object3DList.begin();

    int i = 0;

    while (iter != object3DList.end())
    {
        ivtObject3DList[i] = convert(*iter);

        iter++;
    }

    return ivtObject3DList;
}


visionx::types::Object3DList visionx::tools::convert(const Object3DList& ivtObject3DList)
{
    visionx::types::Object3DList object3DList(ivtObject3DList.size());

    Object3DList::const_iterator iter = ivtObject3DList.begin();

    int i = 0;

    while (iter != ivtObject3DList.end())
    {
        object3DList[i] = convert(*iter);

        iter++;
    }

    return object3DList;
}


Eigen::Matrix3f visionx::tools::convertIVTtoEigen(const Mat3d m)
{
    Eigen::Matrix3f result;
    result(0, 0) = m.r1;
    result(0, 1) = m.r2;
    result(0, 2) = m.r3;
    result(1, 0) = m.r4;
    result(1, 1) = m.r5;
    result(1, 2) = m.r6;
    result(2, 0) = m.r7;
    result(2, 1) = m.r8;
    result(2, 2) = m.r9;
    return result;
}


Eigen::Vector3f visionx::tools::convertIVTtoEigen(const Vec3d v)
{
    Eigen::Vector3f result(v.x, v.y, v.z);
    return result;
}


visionx::types::Vec visionx::tools::convertEigenVecToVisionX(Eigen::VectorXf v)
{
    visionx::types::Vec res;
    res.resize(v.rows());
    for (int i = 0; i < v.rows(); i++)
    {
        res[i] = v[i];
    }
    return res;
}


visionx::types::Mat visionx::tools::convertEigenMatToVisionX(Eigen::MatrixXf m)
{
    visionx::types::Mat res;
    res.resize(m.cols());
    for (int i = 0; i < m.cols(); i++)
    {
        visionx::types::Vec v;
        Eigen::VectorXf vec = m.block(0, i, m.rows(), 1);
        v = convertEigenVecToVisionX(vec);
        res[i] = v;
    }
    return res;
}
