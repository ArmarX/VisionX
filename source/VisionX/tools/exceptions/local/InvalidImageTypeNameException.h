/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    VisionX::Tools
* @author     Jan Issac <jan dot issac at gmx dot de>
* @copyright  2011 Humanoids Group, HIS, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/core/exceptions/Exception.h>

#include <string>

namespace visionx::exceptions::local
{
    class InvalidImageTypeNameException: public armarx::LocalException
    {
    public:
        std::string imageTypeName;

        InvalidImageTypeNameException(std::string imageTypeName)
            : armarx::LocalException(" Invalid image type name: " + imageTypeName),
              imageTypeName(imageTypeName) { }

        ~InvalidImageTypeNameException() noexcept override { }

        virtual std::string ice_name() const
        {
            return "visionx::exceptions::local::InvalidImageTypeNameException";
        }
    };
}
