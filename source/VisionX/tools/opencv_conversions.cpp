#include "opencv_conversions.h"

#include <cstring>

#include <opencv2/imgproc.hpp>

#include <Image/ByteImage.h>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include <VisionX/interface/components/Calibration.h>


void visionx::convertIvtRgbToCvGray(const CByteImage& ivt_rgb, cv::Mat& cv_gray)
{
    cv::Mat cv_rgb(cv::Size(ivt_rgb.width, ivt_rgb.height), CV_8UC3);
    std::memcpy(cv_rgb.data, ivt_rgb.pixels,
                static_cast<unsigned int>(ivt_rgb.width * ivt_rgb.height * ivt_rgb.bytesPerPixel));

    cv_gray.create(cv::Size(ivt_rgb.width, ivt_rgb.height), CV_8UC1);
    cv::cvtColor(cv_rgb, cv_gray, cv::COLOR_RGB2GRAY);
}


cv::Mat visionx::makeCameraMatrix(const visionx::CameraParameters& cameraParam)
{
    ARMARX_CHECK_EQUAL(cameraParam.focalLength.size(), 2);
    ARMARX_CHECK_EQUAL(cameraParam.principalPoint.size(), 2);
    const float nodalPointX = cameraParam.principalPoint.at(0);
    const float nodalPointY = cameraParam.principalPoint.at(1);
    const float focalLengthX = cameraParam.focalLength.at(0);
    const float focalLengthY = cameraParam.focalLength.at(1);

    cv::Mat camera_matrix = cv::Mat(3, 4, CV_64F, 0.0);
    camera_matrix.at<double>(0, 2) = double(nodalPointX);
    camera_matrix.at<double>(1, 2) = double(nodalPointY);
    camera_matrix.at<double>(0, 0) = double(focalLengthX);
    camera_matrix.at<double>(1, 1) = double(focalLengthY);
    camera_matrix.at<double>(2, 2) = 1.0;

    return camera_matrix;
}
