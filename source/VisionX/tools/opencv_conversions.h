#pragma once

#include <opencv2/core/core.hpp>

class CByteImage;

namespace visionx
{

    void convertIvtRgbToCvGray(const CByteImage& ivt_rgb, cv::Mat& cv_gray);


    class CameraParameters;
    cv::Mat makeCameraMatrix(const visionx::CameraParameters& cameraParams);

}

