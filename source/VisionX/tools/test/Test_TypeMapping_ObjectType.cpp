/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::Tools::Tests
 * @author     Jan Issac (jan dot issac at gmx dot net)
 * @date       2011
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#define BOOST_TEST_MODULE VisionX::tools::TypeMapping::ObjectType
#define ARMARX_BOOST_TEST
#include <VisionX/Test.h>

// IVT
#include <Structs/Structs.h>
#include <Structs/ObjectDefinitions.h>

// VisionXTools
#include <VisionX/tools/TypeMapping.h>

// VisionXInterface
#include <VisionX/interface/units/ObjectRecognitionUnit.h>

BOOST_AUTO_TEST_CASE(ObjectColorIvtToVisionXMappingTest)
{
    BOOST_CHECK(visionx::tools::convert(eYellow)  == visionx::types::eYellow);
    BOOST_CHECK(visionx::tools::convert(eYellow2) == visionx::types::eYellow2);
    BOOST_CHECK(visionx::tools::convert(eYellow3) == visionx::types::eYellow3);

    BOOST_CHECK(visionx::tools::convert(eOrange)  == visionx::types::eOrange);
    BOOST_CHECK(visionx::tools::convert(eOrange2) == visionx::types::eOrange2);
    BOOST_CHECK(visionx::tools::convert(eOrange3) == visionx::types::eOrange3);

    BOOST_CHECK(visionx::tools::convert(eRed)  == visionx::types::eRed);
    BOOST_CHECK(visionx::tools::convert(eRed2) == visionx::types::eRed2);
    BOOST_CHECK(visionx::tools::convert(eRed3) == visionx::types::eRed3);

    BOOST_CHECK(visionx::tools::convert(eBlue)  == visionx::types::eBlue);
    BOOST_CHECK(visionx::tools::convert(eBlue2) == visionx::types::eBlue2);
    BOOST_CHECK(visionx::tools::convert(eBlue3) == visionx::types::eBlue3);

    BOOST_CHECK(visionx::tools::convert(eGreen)  == visionx::types::eGreen);
    BOOST_CHECK(visionx::tools::convert(eGreen2) == visionx::types::eGreen2);
    BOOST_CHECK(visionx::tools::convert(eGreen3) == visionx::types::eGreen3);

    BOOST_CHECK(visionx::tools::convert(eWhite)    == visionx::types::eWhite);
    BOOST_CHECK(visionx::tools::convert(eSkin)    == visionx::types::eSkin);
    BOOST_CHECK(visionx::tools::convert(eColored) == visionx::types::eColored);
    BOOST_CHECK(visionx::tools::convert(eNone)    == visionx::types::eNone);
}


BOOST_AUTO_TEST_CASE(ObjectColorVisionXToIvtMappingTest)
{
    BOOST_CHECK(visionx::tools::convert(visionx::types::eYellow)  == eYellow);
    BOOST_CHECK(visionx::tools::convert(visionx::types::eYellow2) == eYellow2);
    BOOST_CHECK(visionx::tools::convert(visionx::types::eYellow3) == eYellow3);

    BOOST_CHECK(visionx::tools::convert(visionx::types::eOrange)  == eOrange);
    BOOST_CHECK(visionx::tools::convert(visionx::types::eOrange2) == eOrange2);
    BOOST_CHECK(visionx::tools::convert(visionx::types::eOrange3) == eOrange3);

    BOOST_CHECK(visionx::tools::convert(visionx::types::eRed)  == eRed);
    BOOST_CHECK(visionx::tools::convert(visionx::types::eRed2) == eRed2);
    BOOST_CHECK(visionx::tools::convert(visionx::types::eRed3) == eRed3);

    BOOST_CHECK(visionx::tools::convert(visionx::types::eBlue)  == eBlue);
    BOOST_CHECK(visionx::tools::convert(visionx::types::eBlue2) == eBlue2);
    BOOST_CHECK(visionx::tools::convert(visionx::types::eBlue3) == eBlue3);

    BOOST_CHECK(visionx::tools::convert(visionx::types::eGreen)  == eGreen);
    BOOST_CHECK(visionx::tools::convert(visionx::types::eGreen2) == eGreen2);
    BOOST_CHECK(visionx::tools::convert(visionx::types::eGreen3) == eGreen3);

    BOOST_CHECK(visionx::tools::convert(visionx::types::eWhite)    == eWhite);
    BOOST_CHECK(visionx::tools::convert(visionx::types::eSkin)    == eSkin);
    BOOST_CHECK(visionx::tools::convert(visionx::types::eColored) == eColored);
    BOOST_CHECK(visionx::tools::convert(visionx::types::eNone)    == eNone);
}
