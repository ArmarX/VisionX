/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once


// hand localisation
#include "../HandLocalisationConstants.h"
#include "../HandModel/HandModeliCub.h"
#include "../ParticleFilter/Polygon.h"

// IVT
#include "Image/ByteImage.h"
#include "Calibration/StereoCalibration.h"
#include "Calibration/Calibration.h"

// movemaster
#include "MoveMasterModel.h"


namespace visionx
{
    class CHandModelVisualizer
    {
    public:
        // constructor
        CHandModelVisualizer(CStereoCalibration* pCalibration, bool bUseLeftCamera = true);

        // destructor
        ~CHandModelVisualizer();


        // update the hand configuration
        void UpdateHandModel(double* pConfig, bool bUpdateOpenInventorModel = true, bool bDrawCylinderInHand = false);


        // draw fingertips into an image
        void DrawFingertips(CByteImage* pImage);

        // draw simple hand model for PF
        void DrawHandModelV2(CByteImage* pImage, bool bLeftCameraImage = true);

        // draw hand into an image
        void DrawHand(CByteImage* pImage);

        // draw segmented image with hand/object/background regions
        void DrawSegmentedImage(CByteImage* pImage, bool bUseOpenInventorModel = true);
        void DrawSegmentedImageWithoutOpenInventor(CByteImage* pSegmentedImage);

        // draw a +
        static void DrawCross(CByteImage* pGreyImage, int x, int y, int nBrightness);
        static void DrawCross(CByteImage* pColorImage, int x, int y, int r, int g, int b);


        static void DrawLineIntoImage(CByteImage* pImage, int x1, int y1, int x2, int y2, int red, int green, int blue);


        //void OutputProjectionImage(CByteImage* pImage, bool write_to_file=false);
        //void OutputProjectionImagePolygon(CByteImage* pImage, bool write_to_file=false);
        //void PaintPolygon(CByteImage* pImage, Polygon* pol);
        //void PaintLineSequence(Vec3d linePoints[], int nPoints, CByteImage* pImage);


    private:
    public: //TODO: make private again

        CHandModelV2* m_pHandModelV2;
        CMoveMasterModel* m_pMoveMaster;

        CStereoCalibration* m_pStereoCalibration;
        float m_fFocalLengthX;
        float m_fFocalLengthY;
        float m_fPrincipalPointX;
        float m_fPrincipalPointY;


        void DrawPolygon(CByteImage* pImage, ConvexPolygonCalculations::Polygon& pPolygon);
        void DrawPolygonGrayscale(CByteImage* pImage, ConvexPolygonCalculations::Polygon& pPolygon);

        void DrawQuadrangleAlongA3DLine(CByteImage* pImage, Vec3d vStart, Vec3d vEnd, float fWidthAtStart, float fWidthAtEnd);

        void ConfigureMoveMasterModel(double* pConfig, bool bDrawCylinderInHand);

        void ExtractAnglesFromRotationMatrix(const Mat3d& mat, Vec3d& angles);
    };
}

