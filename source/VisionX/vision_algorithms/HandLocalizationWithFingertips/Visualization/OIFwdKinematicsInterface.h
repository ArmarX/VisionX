/*
 *  OIFwdKinematicsInterface.h
 *
 *
 *  Created by A. Bierbaumm on 28.03.08.
 *  Copyright 2008 IAIM Universitaet Karlsruhe.
 *  All rights reserved.
 *
 */

#pragma once

/** \file OIFwdKinematicsInterface.h
* The header file for OIFwdKinematicsInterface.
*/

#ifdef WIN32
/*
#include <Inventor/Win/SoWin.h>
#include <Inventor/Win/viewers/SoWinExaminerViewer.h>
#define SO_WINDOW HWND
#define SO_PLATFORM SoWin
#define SO_EXAMINER_VIEWER SoWinExaminerViewer
*/
#include <Inventor/Qt/SoQt.h>
#include <Inventor/Qt/viewers/SoQtExaminerViewer.h>
#define SO_WINDOW QWidget*
#define SO_PLATFORM SoQt
#define SO_EXAMINER_VIEWER SoQtExaminerViewer
#else
#include <Inventor/Qt/SoQt.h>
#include <Inventor/Qt/viewers/SoQtExaminerViewer.h>
#define SO_WINDOW QWidget*
#define SO_PLATFORM SoQt
#define SO_EXAMINER_VIEWER SoQtExaminerViewer
#endif

#include <Inventor/SbLinear.h>
#include <Inventor/nodes/SoSeparator.h>
#include <qapplication.h>
#include <string>

#ifdef HAVE_COLLISION_DET
#include <Inventor/SoInteraction.h>
#include <Inventor/collision/SoIntersectionDetectionAction.h>
#endif

// meins
#include <Inventor/nodes/SoPerspectiveCamera.h>
#include <Inventor/nodes/SoDirectionalLight.h>
#include <Inventor/SoOffscreenRenderer.h>
//#include <Inventor/SoPerspectiveCamera.h>




namespace visionx
{
    class OIFwdKinematicsInterface
    {
    public:
        OIFwdKinematicsInterface(std::string filename = "");
        virtual ~OIFwdKinematicsInterface();
        void expandFileNodes();
        void update();
        void run();
        void initviewer();
        bool isInitialized();

        bool getPath(std::string name, SoPath*& p, SoNode* r = NULL);
        bool getTranslation(SbVec3f& translation, SoPath* p);
        bool setJointAngle(SoPath* p, float angle);

        // meins
        SbViewportRegion* m_pViewportRegion;
        SoPerspectiveCamera* m_pCamera;
        SoDirectionalLight* m_pLight;
        SoSeparator* m_pDavidsRoot;
        SoOffscreenRenderer* m_pOffscreenRenderer;
        float m_fFocalLengthY;



    protected:
        virtual bool buildScenery();
        bool readSceneryFromFile();
        SoSeparator* root;

#ifdef HAVE_COLLISION_DET
        static SoIntersectionDetectionAction::Resp OIFwdKinematicsInterface::intersectionCB(void* closure,
                const SoIntersectingPrimitive* pr1,
                const SoIntersectingPrimitive* pr2);
#endif

    private:

        int argc;
        char** args;
        QApplication* iQApplication;
        std::string sceneryFilename;

        SO_WINDOW window;
        SO_EXAMINER_VIEWER* viewer;

#ifdef HAVE_COLLISION_DET
        SoIntersectionDetectionAction* ida;
#endif

        bool iReady;
    };
}
